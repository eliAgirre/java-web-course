package modelo;

import interfaces.Edificio;
import interfaces.InstalacionDeportiva;

public class Polideportivo implements Edificio,InstalacionDeportiva{
	
	private String nombrePoli; 
	private int tipoInstalacion; 
	private float ancho, largo; 
	
	public Polideportivo(String nombrePoli, int tipoInstalacion, float ancho, float largo){ 
		
		this.nombrePoli = nombrePoli; 
		this.tipoInstalacion = tipoInstalacion; 
		this.ancho = ancho; 
		this.largo = largo; 
	}
	
	public void setNombre(String dato){nombrePoli = dato;} 
	
	public void setTipoInstalacion(int num){tipoInstalacion = num;}
	
	public void setSuperficieEdificio(float dato1, float dato2){ancho = dato1; largo = dato2;}
	
	public float getSuperficieEdificio(){return (ancho * largo);}
	
	public float getTipoInstalacion(){return tipoInstalacion;}
	
	public String toString(){ 
		
		return "Nombre del Establecimiento: "+nombrePoli+"\nTipo De Instalacion: "+tipoInstalacion+"\nSuperficie: "+Math.round(getSuperficieEdificio())+"m^2\n"; 
	} 
}
