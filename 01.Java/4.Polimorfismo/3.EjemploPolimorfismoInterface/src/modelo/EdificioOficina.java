package modelo;

import interfaces.Edificio;

public class EdificioOficina implements Edificio { 
	
	private int numeroOficinas; 
	private float ancho, largo; 
	
	public EdificioOficina(int numeroOficinas, float ancho, float largo){
		this.numeroOficinas = numeroOficinas; 
		this.ancho = ancho; 
		this.largo = largo;
	}
	
	public void setNumeroOficina(int dato){numeroOficinas = dato;}
	
	public void setSuperficieEdificio(float dato1, float dato2){ancho = dato1; largo = dato2;}
	
	public int getNumeroOficina(){return numeroOficinas;}
	
	public float getSuperficieEdificio(){return (ancho * largo);}
	
	public String toString(){ 
		return "Numero de oficinas: "+numeroOficinas+"\nSuperficie: "+Math.round(getSuperficieEdificio())+"m^2\n"; 
	} 
}
