package interfaces;

public interface Edificio { 
	
	float getSuperficieEdificio();
	
	String toString(); 
}
