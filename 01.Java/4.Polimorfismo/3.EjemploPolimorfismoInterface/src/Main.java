
import interfaces.Edificio;

import java.util.ArrayList; 
import java.util.List; 
import java.util.Iterator; 

import modelo.EdificioOficina;
import modelo.Polideportivo;

public class Main { 
	public static void main(String[]args){ 
		
		Edificio polideportivo1 = new Polideportivo("Balboa",2,30.50f,20.56f); 
		Edificio polideportivo2 = new Polideportivo("Espinar",1,14.150f,25.75f); 
		Edificio polideportivo3 = new Polideportivo("Margarita",3,50.345f,20.39f); 
		
		Edificio EdificioOficina1 = new EdificioOficina(35,70.34f,80.54f); 
		Edificio EdificioOficina2 = new EdificioOficina(25,50.35f,34.45f); 
		
		List <Edificio> lista = new ArrayList <Edificio>(); 
		lista.add(polideportivo1); 
		lista.add(polideportivo2); 
		lista.add(polideportivo3); 
		lista.add(EdificioOficina1); 
		lista.add(EdificioOficina2); 
		
		Iterator<Edificio> recorre = lista.iterator(); 
		
		while(recorre.hasNext()){ 
			
			Edificio aux = recorre.next(); 
			System.out.println(aux.toString()); 
		} 
	} 
}
