package polimorfismo;
class ClsRectangulo extends Poligono
{
	
	
	public ClsRectangulo(float base,float altura)
	{
		super(base,altura);
	}
	
	public float area()
	{
		return (super.getBase()*super.getAltura());
	}	
}
