package polimorfismo;
import java.io.IOException;

public class ClsAreaPoligonos 
{
	public char seleccionarTipoPoligono()throws IOException
	{
		System.out.println("Seleccione el tipo de poligono (t/r)");
		char c=(char)System.in.read();
		return c;	
	}
	
	public Poligono seleccionarPoligono(char c)
	{
		Poligono miPoligono;//Clase abstracta
		switch (c)
		{
			case 't':
				miPoligono=new ClsTriangulo(4,3);
				break;
				
			case 'r':
				miPoligono=new ClsRectangulo(4,5);
				break;
				
			default:
				miPoligono=null;	
		}
		return miPoligono;
	}

	
}
