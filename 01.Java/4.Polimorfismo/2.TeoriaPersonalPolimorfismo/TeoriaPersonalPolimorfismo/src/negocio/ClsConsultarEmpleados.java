package negocio;
import java.io.IOException;

import modelo.*;

public class ClsConsultarEmpleados 
{
	public char elegirEmpleado() throws IOException
	{
		System.out.println("*****Elige empleado*****");
		System.out.println("     Administrativo (a)");
		System.out.println("     Programador (p)");
		System.out.println("     Jefe Proyecto (j)");
		
		char c=(char)System.in.read();
		return c;
	}
	
	public ClsEmpleado consultarEmpleado(char c)
	{
		ClsEmpleado miEmpleado; //clase Abstracta
		
		if (c=='a')		
			miEmpleado=new ClsAdministrativo("Pedro","12345",300.5f,123.f);
		
		else if(c=='p')
		
			miEmpleado=new ClsProgramador("Juan","2234",234.9f,234.5f);
		
		else if(c=='j')
		
			miEmpleado=new ClsJefeProyecto("Ana","2678",678f,34.4f,78.9f);
		
		else
		
			miEmpleado=null;
		
		return miEmpleado;
	}
	
}
