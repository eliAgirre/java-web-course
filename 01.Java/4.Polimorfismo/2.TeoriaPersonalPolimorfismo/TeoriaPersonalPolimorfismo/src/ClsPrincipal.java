import java.io.IOException;

import negocio.ClsConsultarEmpleados;
import modelo.ClsEmpleado;


public class ClsPrincipal 
{

	public static void main(String[] args)  throws IOException
	{
		ClsEmpleado miEmpleado; //Clase Abstracta
		ClsConsultarEmpleados miConsultarEmpleados=new ClsConsultarEmpleados();
		
		
		char valor=miConsultarEmpleados.elegirEmpleado();
		
		miEmpleado=miConsultarEmpleados.consultarEmpleado(valor);
		
		if(miEmpleado!=null)
		{
			System.out.println("Dni=" + miEmpleado.getDni());
			System.out.println("Nombre=" + miEmpleado.getNombre());
			System.out.println("Total=" + miEmpleado.total());
		}
		else
		{
			System.out.println("No existe ese tipo de empleado");
		}

			
	}	
}
