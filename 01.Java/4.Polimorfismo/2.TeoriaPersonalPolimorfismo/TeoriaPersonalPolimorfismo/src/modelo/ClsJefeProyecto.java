package modelo;
public class ClsJefeProyecto extends ClsInformatico
{

	private float incentivo=0F;
	private float horas_extras=0f;
	
	public ClsJefeProyecto(String nombre,String dni,
		float sueldo,float incentivo,float horas_extras)
	{
		super(nombre,dni,sueldo);
		
		this.incentivo=incentivo;
		this.horas_extras=horas_extras;
	}
	
	public float total()
	{
		return (super.total()+incentivo+horas_extras);
	}
	
}
