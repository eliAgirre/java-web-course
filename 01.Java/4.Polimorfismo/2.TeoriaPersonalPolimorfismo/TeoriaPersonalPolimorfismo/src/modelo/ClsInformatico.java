package modelo;
public class ClsInformatico extends ClsEmpleado
{
	private float sueldo=0.0f;
	
	public ClsInformatico(String nombre,String dni,float sueldo)
	{
		super(nombre,dni);
		this.sueldo=sueldo;
	}
	public float total()
	{
		return (sueldo);
	}
		
}
