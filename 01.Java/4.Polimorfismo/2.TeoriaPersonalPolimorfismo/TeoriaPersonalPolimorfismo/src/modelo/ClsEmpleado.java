package modelo;
public abstract class ClsEmpleado
{
	private String nombre="";
	private String dni="";
	
	public ClsEmpleado(String nombre,String dni)
	{
		this.nombre=nombre;
		this.dni=dni;
	}
	
	public void setDni(String dni)
	{
		this.dni=dni;
	}
	
	public void setNombre(String nombre)
	{
		this.nombre=nombre;
	}
	
	public String getDni()
	{
		return dni;
	}
	
	public String getNombre()
	{
		return nombre;
	}
	public abstract float total();
	
}
