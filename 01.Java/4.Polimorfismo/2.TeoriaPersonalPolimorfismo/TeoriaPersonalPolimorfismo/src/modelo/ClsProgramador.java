package modelo;
public class ClsProgramador extends ClsInformatico
{
	private float plus;
	public ClsProgramador(String nombre,String dni,float sueldo,float plus)
	{
		super(nombre,dni,sueldo);
		this.plus=plus;
	}
	public float total()
	{
		return (super.total()+plus);
	}
}
