package modelo;
public class ClsAdministrativo extends ClsEmpleado
{
	
	private float sueldo=0.0f;
	private float dieta=0.0f;
	
	public ClsAdministrativo(String nombre,String dni,float sueldo,float dieta)
	{
		super(nombre,dni);
		this.sueldo=sueldo;
		this.dieta=dieta;
	}
	public float total()
	{
		return (sueldo+dieta);
	}
		
}
