import negocio.Clase1;
import negocio.Clase2;

import utilidades.Teclado;

public class Principal {

	public static void main(String[] args) {		
		
		int op=0;
		
		while(op!=3){
			
			System.out.println("1 Clase 1");
			System.out.println("2 Clase 2");
			System.out.println("3 Salir");
			op=Integer.parseInt(Teclado.teclado("Elige opcion"));
			
			switch(op){
				
				case 1:
					new Clase1().ver();
					break;
				case 2:
					new Clase2().ver();
					break;
				case 3:
					break;
				default:
					System.out.println("Error");
			}
		} // while
		
		System.out.println("Total clase1: "+Clase1.getTotalVisitas());
		System.out.println("Total clase2: "+(new Clase2().getTotalVisitas()-1));

	}

}
