
public class Personal {
	
	private String dni;
	private int sueldo;
	private String apellido;
	private int plus;
	
	public String getDni() {
		return dni;
	}
	public int getSueldo() {
		return sueldo;
	}
	public String getApellido() {
		return apellido;
	}
	public int getPlus() {
		return plus;
	}
	
	public void setDni(String dni) {
		this.dni = dni;
	}
	public void setSueldo(int sueldo) {
		this.sueldo = sueldo;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public void setPlus(int plus) {
		this.plus = plus;
	}
	
	public int getTotal(){
		
		return this.plus+this.sueldo;
	}
	
	@Override
	public String toString() {
		return "Personal dni: "+dni+", sueldo: " + sueldo + ", apellido: "
				+ apellido + ", plus: " + plus;
	}
	
}
