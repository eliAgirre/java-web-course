
public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Principal p=new Principal();
		
		ClsVer claseVer=new ClsVer();
		
		Personal persona1=new Personal();
		Personal persona2=new Personal();
		
		persona1.setDni("1");
		persona1.setApellido("AA");
		persona1.setPlus(11);
		persona1.setSueldo(111);
		
		persona2.setDni("2");
		persona2.setApellido("BB");
		persona2.setPlus(22);
		persona2.setSueldo(222);
		
		System.out.println("************* Persona1 ******************");
		/*System.out.println("DNI: "+persona1.getDni());
		System.out.println("Apellido: "+persona1.getApellido());
		System.out.println("Total: "+persona1.getTotal());*/
		
		//System.out.println(persona1.toString());
		
		visualizar(persona1);
		p.ver(persona1);		
		claseVer.ver(persona1);
		
		System.out.println("************* Persona2 ******************");
		/*System.out.println("DNI: "+persona2.getDni());
		System.out.println("Apellido: "+persona2.getApellido());
		System.out.println("Total: "+persona2.getTotal());*/		
		
		//System.out.println(persona2.toString());
		
		visualizar(persona2);
		p.ver(persona2);
		claseVer.ver(persona2);
			
		
		
		String var="99";
		
		//Integer a=Integer.parseInt(var);
		//Integer a2=new Integer(var);
		
		int a=Integer.parseInt(var);
		int a2=new Integer(var).intValue();
		
		System.out.println("A: "+a);
		System.out.println("A2: "+a2);		
				
	}
	
	private void ver(Personal p){
		
		System.out.println("DNI: "+p.getDni());
		System.out.println("Apellido: "+p.getApellido());
		System.out.println("Total: "+p.getTotal());
		
	}
	
	public static void visualizar(Personal p){
		
		System.out.println("DNI: "+p.getDni());
		System.out.println("Apellido: "+p.getApellido());
		System.out.println("Total: "+p.getTotal());
		
	}

}
