package modelo;

public class Almacen_TipoAlmacen {
	
    private TipoAlmacen tipoalmacen;
    private Almacen almacen;
    private boolean estado;

    public TipoAlmacen getTipoalmacen() {
        return tipoalmacen;
    }
    public void setTipoalmacen(TipoAlmacen tipoalmacen) {
        this.tipoalmacen = tipoalmacen;
    }
    public Almacen getAlmacen() {
        return almacen;
    }
    public void setAlmacen(Almacen almacen) {
        this.almacen = almacen;
    }
    public boolean getEstado() {
        return estado;
    }
    public void setEstado(boolean estado) {
        this.estado = estado;
    }    
}