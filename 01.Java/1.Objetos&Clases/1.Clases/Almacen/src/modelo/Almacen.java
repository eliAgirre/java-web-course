package modelo;
public class Almacen {
	
    private Integer idAlmacen;
    private String aCodigo;
    private String aDescripcion;
    private String aLugar;
    private String aObservaciones;
    private boolean aEstado;

    public Integer getIdAlmacen() {
        return idAlmacen;
    }
    public void setIdAlmacen(Integer idAlmacen) {
        this.idAlmacen = idAlmacen;
    }
    public String getaCodigo() {
        return aCodigo;
    }
    public void setaCodigo(String aCodigo) {
        this.aCodigo = aCodigo;
    }
    public String getaDescripcion() {
        return aDescripcion;
    }
    public void setaDescripcion(String aDescripcion) {
        this.aDescripcion = aDescripcion;
    }    
    public String getaLugar() {
        return aLugar;
    }
    public void setaLugar(String aLugar) {
        this.aLugar = aLugar;
    }
    public String getaObservaciones() {
        return aObservaciones;
    }
    public void setaObservaciones(String aObservaciones) {
        this.aObservaciones = aObservaciones;
    }
    public boolean getaEstado() {
        return aEstado;
    }
    public void setaEstado(boolean aEstado) {
        this.aEstado = aEstado;
    }   
}
