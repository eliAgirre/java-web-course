package modelo;


public class TipoAlmacen {
	
    private Integer idtipoalmacen;
    private String nombre;

    public Integer getIdtipoalmacen() {
        return idtipoalmacen;
    }
    public void setIdtipoalmacen(Integer idtipoalmacen) {
        this.idtipoalmacen = idtipoalmacen;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }  
}
