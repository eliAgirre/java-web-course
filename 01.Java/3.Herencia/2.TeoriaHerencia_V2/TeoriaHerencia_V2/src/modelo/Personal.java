package modelo;

public abstract class Personal 
{
	private String dni;
	private String nombre;
	private String apellido;	
	private Nomina nomina;
	
	public Personal(){}
	
	public Personal(String dni, String nombre, String apellido) {
		this.dni = dni;
		this.nombre = nombre;
		this.apellido = apellido;		
	}
	
	public Personal(String dni, String nombre, 
			String apellido, Nomina nomina)
					
	{
		this.dni = dni;
		this.nombre = nombre;
		this.apellido = apellido;
		setNomina( nomina );
	}
	
	public abstract int getTotal();
	
	public final String getDni() {
		return dni;
	}
	public final void setDni(String dni) {
		this.dni = dni;
	}
	public final String getNombre() {
		return nombre;
	}
	public final void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public final String getApellido() {
		return apellido;
	}
	public final void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public final Nomina getNomina() {
		return nomina;
	}
	public final void setNomina(Nomina nomina) 
	{
		this.nomina = nomina;
	}	
}
