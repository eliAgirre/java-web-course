package modelo;

public final class Administrativo extends Personal
{
	private int productividad;	
	
	public Administrativo() {
		super();
	}

	public Administrativo(String dni, String nombre, String apellido,
			Nomina nomina,int productividad) 
	{
		super(dni, nombre, apellido, nomina);
		setProductividad(productividad);
	}

	public Administrativo(String dni, String nombre, 
			String apellido,int productividad) 
	{
		super(dni, nombre, apellido);
		setProductividad(productividad);
	}

	public  int getProductividad() {
		return productividad;
	}

	public  void setProductividad(int productividad) {
		this.productividad = productividad;
	}
	
	public  int getTotal()
	{
		return productividad + this.getNomina().getTotal();		
	}

	@Override
	public String toString() {
		return "Administrativo [productividad=" + productividad
				+ ", getProductividad()=" + getProductividad()
				+ ", getTotal()=" + getTotal() + ", getDni()=" + getDni()
				+ ", getNombre()=" + getNombre() + ", getApellido()="
				+ getApellido() + ", getNomina()=" + getNomina() + "]";
	}


	
	
	
}
