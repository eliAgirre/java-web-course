import tratamientos.ExceptionPlus;
import tratamientos.ExceptionSueldo;
import modelo.Administrativo;
import modelo.Informatico;
import modelo.Junior;
import modelo.Nomina;
import modelo.Personal;
import modelo.Senior;


public class Principal {

	public static void main(String[] args) {
		
		try
		{
			Principal principal=new Principal();
			Administrativo administrativo=
				new Administrativo
				("1","A","AA",new Nomina(1001,1002),1003);
			
			
			
			System.out.println("*******Administrativo*********");
			ver(administrativo);
			/*
			Informatico informatico=
					new Informatico
					("2","B","BB",new Nomina(2001,2002),2003);
				
				
			System.out.println("*******Informato*********");
			System.out.println(informatico.toString());	
			*/
			Senior senior=
					new Senior
					("3","C","CC",new Nomina(3001,3002),3003,3004);
				
			
			System.out.println("*******Senior*********");
			ver(senior);
			
			Junior junior=
					new Junior
					("4","D","DD",new Nomina(4002,4002),4003,4004);
				
			
			System.out.println("*******Junior*********");
			ver(junior);
			
			
		}
		
		catch(ExceptionSueldo e){System.out.println(e.getMessage());}
		catch(ExceptionPlus e2){System.out.println(e2.getMessage());}			
	}
	private static void ver(Personal p)
	{
		System.out.println("dni="+ p.getDni());
		System.out.println("dni="+ p.getTotal());
	}

}
