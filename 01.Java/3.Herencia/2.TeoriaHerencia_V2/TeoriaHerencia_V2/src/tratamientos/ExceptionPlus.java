package tratamientos;

public class ExceptionPlus extends RuntimeException
{

	private static final long serialVersionUID = 1L;
	
	public ExceptionPlus(String msg)
	{
		super(msg);
	}
	
}
