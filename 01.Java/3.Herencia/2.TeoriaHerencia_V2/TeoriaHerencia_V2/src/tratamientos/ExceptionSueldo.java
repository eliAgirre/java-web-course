package tratamientos;

public class ExceptionSueldo extends RuntimeException
{

	private static final long serialVersionUID = 1L;
	
	public ExceptionSueldo(String msg)
	{
		super(msg);
	}
	
}
