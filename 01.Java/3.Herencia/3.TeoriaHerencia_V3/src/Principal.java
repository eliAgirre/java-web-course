
import tratamientos.ExceptionPlus;
import tratamientos.ExceptionSueldo;
import modelo.Administrativo;
import modelo.Junior;
import modelo.Personal;
import modelo.Senior;


public class Principal {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		
		try{
			Principal principal=new Principal();
			
			Administrativo administrativo=new Administrativo("1","A","AA",1001,1002,1003);			
			System.out.println("*******Administrativo*********");
			ver(administrativo);
			System.out.println("");

			Senior senior=new Senior("3","C","CC",3001,3002,3003,3004);
			System.out.println("*******Senior*********");
			ver(senior);
			System.out.println("");
			
			Junior junior=new Junior("4","D","DD",4002,4002,4003,4004);			
			System.out.println("*******Junior*********");
			ver(junior);
			System.out.println("");
			
		}		
		catch(ExceptionSueldo e){
			System.out.println(e.getMessage());
		}
		catch(ExceptionPlus e2){
			System.out.println(e2.getMessage());
		}	
		
	} // main
	
	private static void ver(Personal p){
		
		System.out.println("dni: "+ p.getDni());
		System.out.println("dni: "+ p.getTotal());
		
	} // ver

}
