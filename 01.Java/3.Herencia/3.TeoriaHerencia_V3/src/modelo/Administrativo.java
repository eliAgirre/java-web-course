package modelo;

public final class Administrativo extends Personal
{
	private int productividad;	
	
	public Administrativo() {
		super();
	}

	public Administrativo(String dni, String nombre, String apellido, int sueldo, 
			int plus,int productividad) {
		
		super(dni, nombre, apellido, sueldo, plus);
		setProductividad(productividad);
	}

	public  int getProductividad() {
		return productividad;
	}

	public  void setProductividad(int productividad) {
		this.productividad = productividad;
	}
	
	@SuppressWarnings("static-access")
	public float getTotal(){

		return (productividad+((super.getSueldo()+super.getPlus())*this.IRPF));		
	}

	@Override
	public String toString() {
		return "Administrativo [productividad=" + productividad
				+ ", getProductividad()=" + getProductividad()
				+ ", getTotal()=" + getTotal() + ", getDni()=" + getDni()
				+ ", getNombre()=" + getNombre() + ", getApellido()="
				+ getApellido() + ", getSueldo()=" +getSueldo()+ ", getPlus()=" +getPlus()+"]";
	}
	
}
