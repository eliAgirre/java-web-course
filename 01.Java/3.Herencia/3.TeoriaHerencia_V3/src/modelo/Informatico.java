package modelo;

public abstract class Informatico extends Personal{
	
	private int incentivos;
	
	public Informatico() {
		super();
	}

	public Informatico(String dni, String nombre, String apellido, 
			int sueldo, int plus,int incentivos) {
		super(dni, nombre, apellido, sueldo, plus);
		setIncentivos(incentivos);
	}

	public abstract float getTotal();
	
	public final int getIncentivos() {
		return incentivos;
	}

	public final void setIncentivos(int incentivos) {
		this.incentivos = incentivos;
	}


	@Override
	public String toString() {
		return "Informatico [incentivos=" + incentivos + ", getIncentivos()="
				+ getIncentivos() + ", getDni()=" + getDni() + ", getNombre()="
				+ getNombre() + ", getApellido()=" + getApellido()
				+ ", getSueldo()=" + getSueldo() + ", getPlus()=" + getPlus()
				+ "]";
	}
		
}
