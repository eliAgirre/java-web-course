package modelo;

import interfaces.Personal_Interno;
import tratamientos.ExceptionPlus;
import tratamientos.ExceptionSueldo;

public abstract class Personal implements Personal_Interno {
	
	private String dni;
	private String nombre;
	private String apellido;	
	private int sueldo;
	private int plus;
	
	public Personal(){} // constructor vacio
	
	public Personal(String dni, String nombre, String apellido, int sueldo, int plus) {
		
		this.dni = dni;
		this.nombre = nombre;
		this.apellido = apellido;
		setSueldo(sueldo);
		setPlus(plus);
		
	} // constructor
	
	// getters
	public final String getDni() {
		return dni;
	}
	
	public final String getNombre() {
		return nombre;
	}
	
	public final String getApellido() {
		return apellido;
	}
	
	public int getSueldo() {
		return sueldo;
	}
	
	public int getPlus() {
		return plus;
	}
	
	// setters
	public final void setDni(String dni) {
		this.dni = dni;
	}
	
	public final void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public final void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public void setSueldo(int sueldo) {
		
		try {
			if(sueldo<1001 || sueldo>5000)
				throw new ExceptionSueldo("Sueldo erroneo");		
			else
				this.sueldo = sueldo;
		}
		catch(ExceptionSueldo e){
			throw e;
		}
		catch(Exception e2){
			System.err.println(e2.getMessage());
		}
	
	}
	
	public void setPlus(int plus) {
		
		if(plus<1001 || plus>5000)
			throw new ExceptionPlus("Plus erroneo");		
		else
			this.plus = plus;	
	}

	// metodo abstracto
	public abstract float getTotal();
	
}
