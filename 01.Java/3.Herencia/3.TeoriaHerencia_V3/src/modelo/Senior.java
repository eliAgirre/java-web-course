package modelo;

public final class Senior extends Informatico{
	
	private int comida;	
	
	public Senior() {
		super();
	}

	public Senior(String dni, String nombre, String apellido, 
			int sueldo, int plus,int incentivos,int comida) {
		super(dni, nombre, apellido, sueldo, plus, incentivos);
		setComida(comida);
	}

	public int getComida() {
		return comida;
	}

	public void setComida(int pepe) {
		this.comida = pepe;
	}
	
	@SuppressWarnings("static-access")
	public float getTotal(){

		return (comida+((+super.getIncentivos()+super.getSueldo()+super.getPlus())*this.IRPF));
	}

	@Override
	public String toString() {
		return "Senior [comida=" + comida + ", getComida()=" + getComida()
				+ ", getTotal()=" + getTotal() + ", getIncentivos()="
				+ getIncentivos() + ", toString()=" + super.toString()
				+ ", getDni()=" + getDni() + ", getNombre()=" + getNombre()
				+ ", getApellido()=" + getApellido() + ", getSueldo()="
				+ getSueldo()+ ", getPlus()="
						+ getPlus() + "]";
	}

	
	
	
		
}
