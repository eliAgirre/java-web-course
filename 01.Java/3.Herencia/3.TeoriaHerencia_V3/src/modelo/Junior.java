package modelo;

public final class Junior extends Informatico{
	
	private int transporte;	
	
	public Junior() {
		super();
	}

	public Junior(String dni, String nombre, String apellido, 
			int sueldo, int plus,int incentivos,int transporte){
		super(dni, nombre, apellido, sueldo, plus,incentivos);
		setTransporte(transporte);
	}

	public int getTransporte() {
		return transporte;
	}

	public void setTransporte(int juan) {
		this.transporte = juan;
	}
	
	@SuppressWarnings("static-access")
	public float getTotal(){

		return (transporte+((+super.getIncentivos()+super.getSueldo()+super.getPlus())*this.IRPF));
		
	}

	@Override
	public String toString() {
		return "Junior [comida=" + transporte + ", getComida()=" + getTransporte()
				+ ", getTotal()=" + getTotal() + ", getIncentivos()="
				+ getIncentivos() + ", toString()=" + super.toString()
				+ ", getDni()=" + getDni() + ", getNombre()=" + getNombre()
				+ ", getApellido()=" + getApellido() + ", getPlus()="
				+ getSueldo() + ", getSueldo()="
						+ getPlus() +"]";
	}

	
	
	
		
}
