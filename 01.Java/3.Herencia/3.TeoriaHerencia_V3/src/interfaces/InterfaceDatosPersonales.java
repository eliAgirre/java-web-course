package interfaces;

public interface InterfaceDatosPersonales {
	
	// getters
	public  String getDni();	
	public String getNombre();
	public String getApellido();	
	
	// setteres
	public void setDni(String dni);
	public void setNombre(String nombre);
	public void setApellido(String apellido);
	
	// metodo abstract
	public abstract float getTotal();

}
