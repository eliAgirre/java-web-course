package interfaces;

public interface InterfaceDatosNomina {
	
	// getters
	public int getSueldo();
	public int getPlus();
	
	// setters
	public void setSueldo(int sueldo);	
	public void setPlus(int plus);

}
