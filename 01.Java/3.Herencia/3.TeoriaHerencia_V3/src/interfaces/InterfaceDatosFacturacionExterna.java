package interfaces;

public interface InterfaceDatosFacturacionExterna {
	
	// getters
	public int getPrecioHora();
	public int getNumHora();
	
	// setters
	public void setPrecioHora(int precioHora);
	public void setNumHora(int numHora);

}
