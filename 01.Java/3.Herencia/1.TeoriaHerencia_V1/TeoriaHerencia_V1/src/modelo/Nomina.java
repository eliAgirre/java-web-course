package modelo;

import tratamientos.ExceptionPlus;
import tratamientos.ExceptionSueldo;

public class Nomina {
	private int sueldo;
	private int plus;
	
	public Nomina(){}
	
	public Nomina(int sueldo, int plus) 
	{
		setSueldo(sueldo);
		setPlus(plus);
	}
	public int getSueldo() {
		return sueldo;
	}
	public void setSueldo(int sueldo) {
		try
		{
			if(sueldo<1001 || sueldo>5000)
				throw new ExceptionSueldo("Sueldo erroneo");		
			else
				this.sueldo = sueldo;
		}
		catch(ExceptionSueldo e){
			throw e;
		}
		catch(Exception e2)
		{
			System.err.println(e2.getMessage());
		}
	
	}
	
	protected int getPlus() {
		return plus;
	}
	protected void setPlus(int plus) {
		if(plus<1001 || plus>5000)
			throw new ExceptionPlus("Plus erroneo");		
		else
			this.plus = plus;	
	}

	@Override
	public String toString() {
		return "Nomina [sueldo=" + sueldo + ", plus=" + plus + "]";
	}
	public int getTotal()
	{
		return sueldo+plus;
	}
	
	
}
