package modelo;

public class Senior extends Informatico{
	
	private int comida;	
	
	public Senior() {
		super();
	}

	public Senior(String dni, String nombre, String apellido, 
			Nomina nomina,int incentivos,int comida) 
	{
		super(dni, nombre, apellido, nomina,incentivos);
		setComida(comida);
	}

	public Senior(String dni, String nombre, 
		String apellido,int incentivos,int comida) 
	{
		super(dni, nombre, apellido,incentivos);
		setComida(comida);
	}

	public int getComida() {
		return comida;
	}

	public void setComida(int pepe) {
		this.comida = pepe;
	}
	
	public int getTotal()
	{
		return comida+super.getTotal();
		
	}

	@Override
	public String toString() {
		return "Senior [comida=" + comida + ", getComida()=" + getComida()
				+ ", getTotal()=" + getTotal() + ", getIncentivos()="
				+ getIncentivos() + ", toString()=" + super.toString()
				+ ", getDni()=" + getDni() + ", getNombre()=" + getNombre()
				+ ", getApellido()=" + getApellido() + ", getNomina()="
				+ getNomina() + "]";
	}

	
	
	
		
}
