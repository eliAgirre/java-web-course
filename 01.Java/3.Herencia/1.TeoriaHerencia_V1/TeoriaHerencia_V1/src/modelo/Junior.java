package modelo;

public class Junior extends Informatico{
	
	private int transporte;	
	
	public Junior() {
		super();
	}

	public Junior(String dni, String nombre, String apellido, 
			Nomina nomina,int incentivos,int transporte) 
	{
		super(dni, nombre, apellido, nomina,incentivos);
		setTransporte(transporte);
	}

	public Junior(String dni, String nombre, 
		String apellido,int incentivos,int transporte) 
	{
		super(dni, nombre, apellido,incentivos);
		setTransporte(transporte);
	}

	public int getTransporte() {
		return transporte;
	}

	public void setTransporte(int juan) {
		this.transporte = juan;
	}
	
	public int getTotal()
	{
		return transporte+super.getTotal();
		
	}

	@Override
	public String toString() {
		return "Junior [comida=" + transporte + ", getComida()=" + getTransporte()
				+ ", getTotal()=" + getTotal() + ", getIncentivos()="
				+ getIncentivos() + ", toString()=" + super.toString()
				+ ", getDni()=" + getDni() + ", getNombre()=" + getNombre()
				+ ", getApellido()=" + getApellido() + ", getNomina()="
				+ getNomina() + "]";
	}

	
	
	
		
}
