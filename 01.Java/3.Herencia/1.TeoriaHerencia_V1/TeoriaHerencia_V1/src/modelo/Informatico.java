package modelo;

public class Informatico extends Personal{
	private int incentivos;

	
	
	public Informatico() {
		super();
	}

	public Informatico(String dni, String nombre, String apellido, 
			Nomina nomina,int incentivos) 
	{
		super(dni, nombre, apellido, nomina);
		setIncentivos(incentivos);
	}

	public Informatico(String dni, String nombre, 
		String apellido,int incentivos) 
	{
		super(dni, nombre, apellido);
		setIncentivos(incentivos);
	}

	public int getIncentivos() {
		return incentivos;
	}

	public void setIncentivos(int incentivos) {
		this.incentivos = incentivos;
	}
	
	public int getTotal()
	{
		return incentivos+super.getNomina().getTotal();
		
	}

	@Override
	public String toString() {
		return "Informatico [incentivos=" + incentivos + ", getIncentivos()="
				+ getIncentivos() + ", getDni()=" + getDni() + ", getNombre()="
				+ getNombre() + ", getApellido()=" + getApellido()
				+ ", getNomina()=" + getNomina() + ", getClass()=" + getClass()
				+ "]";
	}
	
	
	
		
}
