import java.io.IOException;

import modelo.Hijo4;

public class Principal {
	
	public static void main(String[] args) throws IOException {
		
		try{		
			Hijo4 hijo4=new Hijo4(4,0);
			System.out.println("suma: "+hijo4.getSuma());
			System.out.println("producto: "+hijo4.getProducto());
			System.out.println("resta: "+hijo4.getResta());
			System.out.println("division: "+hijo4.getDivision());
		}
		catch(ArithmeticException ae){
			System.out.println("Division por cero");
		}
		
	}

}
