package modelo;

public class Hijo3 extends Hijo2{
	
	public Hijo3(){ super(); }

	public Hijo3(int numero1, int numero2) {
		
		super(numero1, numero2);
		
	}
	
	public int getResta(){
		
		return super.getNumero1()-super.getNumero2();
	}

}
