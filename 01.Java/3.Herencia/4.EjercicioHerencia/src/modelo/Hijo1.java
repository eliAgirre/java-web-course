package modelo;

public class Hijo1 extends Padre{
	
	public Hijo1(){ super(); }

	public Hijo1(int numero1, int numero2) {
		
		super(numero1, numero2);
		
	}
	
	public int getSuma(){
		
		return super.getNumero1()+super.getNumero2();
	}

}
