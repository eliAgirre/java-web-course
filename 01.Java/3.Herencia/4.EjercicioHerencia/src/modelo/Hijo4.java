package modelo;

public class Hijo4 extends Hijo3{

	public Hijo4(){ super(); }

	public Hijo4(int numero1, int numero2) {
		
		super(numero1, numero2);

	}
	
	public int getDivision() throws ArithmeticException{
		
		return super.getNumero1()/super.getNumero2();
	}
	
}
