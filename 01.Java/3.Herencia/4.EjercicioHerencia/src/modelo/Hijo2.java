package modelo;

public class Hijo2 extends Hijo1 {

	public Hijo2(){ super(); }

	public Hijo2(int numero1, int numero2) {
		
		super(numero1, numero2);

	}
	
	public int getProducto(){
		
		return super.getNumero1()*super.getNumero2();
	}
	
}
