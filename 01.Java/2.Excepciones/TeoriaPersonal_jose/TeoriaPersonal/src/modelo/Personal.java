package modelo;

public class Personal 
{
	private String dni;
	private String nombre;
	private String apellido;	
	private Nomina nomina;
	
	public Personal(){}
	
	public Personal(String dni, String nombre, String apellido) {
		this.dni = dni;
		this.nombre = nombre;
		this.apellido = apellido;		
	}
	
	public Personal(String dni, String nombre, 
			String apellido, Nomina nomina)
					
	{
		this.dni = dni;
		this.nombre = nombre;
		this.apellido = apellido;
		setNomina( nomina );
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public Nomina getNomina() {
		return nomina;
	}
	public void setNomina(Nomina nomina) 
	{
		this.nomina = nomina;
	}

	@Override
	public String toString() {
		return "Personal [dni=" + dni + ", nombre=" + nombre + ", apellido="
				+ apellido + ", nomina=" + nomina + "]";
	}
	
}
