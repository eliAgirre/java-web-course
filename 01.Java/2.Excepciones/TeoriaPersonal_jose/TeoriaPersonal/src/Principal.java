import tratamientos.ExceptionPlus;
import tratamientos.ExceptionSueldo;
import modelo.Nomina;
import modelo.Personal;


public class Principal {

	public static void main(String[] args) {
		
		try
		{
			Principal principal=new Principal();
			Personal persona1=new Personal("1","A","AA",
					new Nomina(1001,1002));
			
			System.out.println("*******Persona 1*********");
			principal.ver(persona1);
			
			Personal persona2=new Personal();
			Nomina nomina=new Nomina();
				
			persona2.setDni("2");
			persona2.setNombre("B");		
			persona2.setApellido("BB");
			persona2.setNomina(nomina);		
			nomina.setSueldo(2001);
			nomina.setPlus(2001);
			persona2.setNomina(nomina);
			System.out.println("****** Persona 2 ********");
			principal.ver(persona2);
			
			/*
			persona2.setDni("2");
			persona2.setNombre("B");		
			persona2.setApellido("BB");
			persona2.setNomina(nomina);		
			persona2.getNomina().setSueldo(222);
			persona2.getNomina().setPlus(22);
			*/
		}
		catch(ExceptionSueldo e){System.out.println(e.getMessage());}
		catch(ExceptionPlus e2){System.out.println(e2.getMessage());}			
	}
	private  void ver(Personal p)
	{
		System.out.println(p.toString());
	}
}
