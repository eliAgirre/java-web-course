import tratamientos.ExceptionPlus;
import tratamientos.ExceptionSueldo;
import modelo.Nomina;
import modelo.Personal;

public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try{
			
			Principal principal=new Principal();
			
			Personal persona1=new Personal("1","A","AA", new Nomina(1001,1002));
			Personal persona2=new Personal();
			
			// Preferible no crear objeto Nomina en el constructor Personal, por la dependencia de objetos
			Nomina nomina=new Nomina();
					
			persona2.setDni("2");
			persona2.setNombre("B");
			persona2.setApellido("BB");
			persona2.setNomina(nomina);
			persona2.getNomina().setSueldo(2001);
			persona2.getNomina().setPlus(2002);
			persona2.setNomina(nomina);
			
			
			System.out.println("************* Persona1 ******************");
			principal.ver(persona1);
					
			System.out.println("************* Persona2 ******************");
			principal.ver(persona2);
		}
		catch(ExceptionSueldo es){ System.out.println(es.getMessage()); }
		catch(ExceptionPlus ep){ System.out.println(ep.getMessage()); }
				
	}
	
	private void ver(Personal p){
		System.out.println(p.toString());
	}
}
