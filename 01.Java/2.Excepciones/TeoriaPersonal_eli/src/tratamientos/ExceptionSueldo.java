package tratamientos;

public class ExceptionSueldo extends /*Exception*/ RuntimeException{
	
	// Seņalizado, no es obligatorio
	private static final long serialVersionUID=1L;
	
	public ExceptionSueldo(String msg){
		
		super (msg); // pasar el msg personalizdo al padre (Exception)
		
	} // constructor

}
