import java.io.*; 
public class Teclado
{
	public String teclado(String args) 
	{
		String sCadena=null;
		try
		{
			System.out.print(args + " ");
			sCadena=(new BufferedReader(new InputStreamReader
					(System.in)).readLine());
		}
		catch(IOException e)
		{
			System.out.println(e.getMessage());
		}
		return sCadena;
	}
}
