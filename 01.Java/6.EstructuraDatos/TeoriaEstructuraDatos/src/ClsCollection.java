import java.util.Iterator;
import java.util.ArrayList;
import java.util.Collection;

public class ClsCollection 
{
	private Collection<String> Lista = new ArrayList<String>();
	
	public  void metodo1(String a,String b)
	{
		Lista.add(a);
		Lista.add(b);
		
		//Lista.remove(a); (funciona)
		//Lista.remove(0); (funciona)
		
		if(Lista.contains("hola")) //Para saber si existe un elemento
		{
			System.out.println("Existe en Collection" );
		}
		else
		{
				System.out.println("No Existe en Collection" );	
		}
		
		for(int i=0;i<Lista.size();i++)
		{
			System.out.println("Metodo Get del Collection="
					+ ((ArrayList<String>) Lista).get(i));
		}
		
		
		for(String ListaAux:Lista)
		{
			System.out.println("For Collection=" +ListaAux );
		}
		
		Iterator <String>MiIterator=Lista.iterator();
		
		while (MiIterator.hasNext())
		{
			System.out.println("Iterator con Collection=" + MiIterator.next().toString());
			
		}

	}
}
