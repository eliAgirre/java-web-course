import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

public class ClsList
{
	private List<String> Lista = new ArrayList<String>();
	
	public  void metodo1(String a,String b)
	{
		Lista.add(a);
		Lista.add(b);
		
		//Lista.remove(a);  (funciona)
		//Lista.remove(0);  (funciona)
		
		for(int i=0;i<Lista.size();i++)
		{
			System.out.println("Metodo Get del List=" + Lista.get(i));
		}
		
		for(String ListaAux:Lista)
		{
			System.out.println("For List=" +ListaAux );
		}
		
		if(Lista.contains("hola")) //Para saber si existe un elemento
		{
			System.out.println("Existe en List" );
		}
		else
		{
			System.out.println("No Existe en List" );	
		}
				
		Iterator <String>MiIterator=Lista.iterator();
		
		while (MiIterator.hasNext())
		{
			System.out.println("Iterator con List=" +
				MiIterator.next());			
		}

	}
	
}
