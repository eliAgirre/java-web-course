import java.util.Iterator;
import java.util.TreeMap; //Para que salga ordenado
import java.util.Map;
import java.util.HashMap;

public class ClsMap 
{
	private TreeMap<String,String> Lista = 
			new TreeMap<String,String>();
	//private Map<String,String> Lista = new HashMap<String,String>();
	
	public  void metodo1(String a,String b)
	{
		Lista.put("1",a);
		Lista.put("2",b);
	
		//Lista.remove(a); (Funciona)
		//Lista.remove(0); (Funciona)
		
		if(Lista.containsKey("1")) //Para saber si existe la clave
		{
			System.out.println("Existe la clave");
		}
		else
		{
			System.out.println("Existe la clave");
		}
		
		boolean existe =Lista.containsValue("hola"); //Para saber si existe el valor
		if(existe)
		{
			System.out.println("Existe el valor en el Map");	
		}
		else 
		{
			System.out.println("No Existe el valor en el Map");
		}
	
		Iterator<?> MiIterator = Lista.entrySet().iterator();

		while (MiIterator.hasNext()) 
		{
			Map.Entry elemento = (Map.Entry)MiIterator.next();
			System.out.println("Iterator con Map=" + 
				elemento.getKey() + " " + elemento.getValue());
		}
		
		System.out.println("Valor en el Map=" + Lista.get("2")); 
		//Recogemos un valor 
	
	
	}
}
