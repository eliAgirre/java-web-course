import java.util.*; //obligatorio
public class ClsHashTable
{

	private Hashtable<String,String> myHashTable=
			new Hashtable<String,String>();
	
	public void metodo1(String a,String b)
	{
		//Persona myPersona=new Persona();
		
		myHashTable.put("1",a);
		myHashTable.put("2",b);
		myHashTable.put("3","Nuevo");
		
		//myHashTable.remove("1"); (Funciona)
				
		myHashTable.remove("3");

		System.out.println("*******containsKey******");
		
		if(myHashTable.containsKey("2"))
				System.out.println("Existe");
		else 
			System.out.println("No existe");
		System.out.println("****************");
		
		System.out.println("******size******");
		System.out.println("Numero de registros=" + 
				myHashTable.size());		
		System.out.println("*****Fin size******");
		
		//myHashTable.keys()---> para recorrerse el primer miembro
		Enumeration<String> myenum=myHashTable.keys();
		while(myenum.hasMoreElements())
		{
			String direccion=myenum.nextElement();
			//String direccion=(String)myenum.nextElement();
			//String direccion=myenum.nextElement().toString();
			//String direccion=String.valueOf(myenum.nextElement());
			System.out.println("La clave es " + direccion);
		}
		
		/*
		for(Enumeration myenum=myHashTable.keys();
			myenum.hasMoreElements();)
		{
			String direccion=myenum.nextElement();
			System.out.println("La direccion es " + direccion);
		}
		*/
		
		//myHashTable.elements()--->para recorrerse el segundo miembro(personas)
		
		myenum=myHashTable.elements();
		
		while(myenum.hasMoreElements())
		{
			String persona=myenum.nextElement();
			System.out.println("El valor " + persona);
		}
		/*
		for(Enumeration myenum=myHashTable.elements();myenum.hasMoreElements();)
		{
			String persona=myenum.nextElement();
			System.out.println("El nombre es " + persona);
		}
		*/
		//get--> para recoger el valor del segundo miembro
		System.out.println("*************");
		
		String str=myHashTable.get("2"); 
		if(str==null)System.out.println("No existe");
		else System.out.println("El valor " + str);
	}
}
