import java.util.Iterator;
import java.util.ArrayList;

public class Arraylist 
{
	private ArrayList<String> Lista = new ArrayList<String>();
	
	public  void metodo1(String a,String b)
	{
		Lista.add(a);
		Lista.add(b);
		
		//Lista.remove(a); (funciona)
		//Lista.remove(0); (funciona)
		
		if(Lista.contains("hola")) //Para saber si existe un elemento
		{
			System.out.println("Existe en ArrayList" );
		}
		else
		{
			System.out.println("No Existe en ArrayList" );	
		}
		
		
		for(int i=0;i<Lista.size();i++)
		{
			System.out.println("Metodo Get del Arraylist="
					+ Lista.get(i));
		}
		
		for(String ListaAux:Lista)
		{
			System.out.println("For ArrayList=" +ListaAux );
		}
				
		Iterator <String>MiIterator=Lista.iterator();
		
		while (MiIterator.hasNext())
		{
			System.out.println("Iterator con ArrayList="
					+MiIterator.next());
			
		}

	}

}
