package tratamientos;

public class ExceptionPlus extends Exception /*RuntimeException*/{
	
	// Seņalizado, no es obligatorio
	private static final long serialVersionUID=1L;
	
	public ExceptionPlus(String msg){
		
		super (msg); // pasar el msg personalizdo al padre (Exception)
		
	} // constructor

}
