package modelo;

import tratamientos.ExceptionPlus;
import tratamientos.ExceptionSueldo;

public class Nomina {
	
	private int sueldo;
	private int plus;
	
	public Nomina(){} // constructor vacio
	
	public Nomina(int sueldo, int plus) throws ExceptionSueldo, ExceptionPlus {
		
		// Si sueldo o plus es mayor que la condicion de set se guardar en la bd con error
		//this.sueldo = sueldo;
		//this.plus = plus;
		
		setSueldo(sueldo);
		setPlus(plus);
		
	} // constructor
	
	public int getSueldo() {
		return sueldo;
	}
	public int getPlus() {
		return plus;
	}
	
	public void setSueldo(int sueldo) throws ExceptionSueldo {
		
		if(sueldo<1001 || sueldo>5000){
			throw new ExceptionSueldo("Sueldo erroneo");
		}
		else{
			this.sueldo = sueldo;
		}
	}
	public void setPlus(int plus) throws ExceptionPlus {
		
		if(plus<1001 || plus>5000){
			throw new ExceptionPlus("Plus erroneo");
		}
		else{
			this.plus = plus;
		}
	}

	@Override
	public String toString() {
		return "Nomina [sueldo=" + sueldo + ", plus=" + plus + "]";
	}

}
