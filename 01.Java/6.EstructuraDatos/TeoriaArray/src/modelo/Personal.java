package modelo;

import tratamientos.ExceptionPlus;
import tratamientos.ExceptionSueldo;

public class Personal {
	
	private String dni;
	private String nombre;
	private String apellido;	
	private Nomina nomina; // objeto Nomina
	
	public Personal(){}
	
	public Personal(String dni, String nombre, String apellido) {
		
		this.dni = dni;
		this.nombre = nombre;
		this.apellido = apellido;
	}

	public Personal(String dni, String nombre, String apellido, Nomina nomina) throws ExceptionSueldo, ExceptionPlus {
		
		this.dni = dni;
		this.nombre = nombre;
		this.apellido = apellido;
		//this.nomina = nomina;
		setNomina(nomina);
	}	
	
	public String getDni() {
		return dni;
	}
	public String getNombre() {
		return nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public Nomina getNomina() {
		return nomina;
	}
	
	public void setDni(String dni) {
		this.dni = dni;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public void setNomina(Nomina nomina) throws ExceptionSueldo, ExceptionPlus{
		this.nomina = nomina;
	}

	@Override
	public String toString() {
		return "Personal [dni=" + dni + ", nombre=" + nombre + ", apellido="
				+ apellido + ", nomina=" + nomina + "]";
	}
			
}
