package negocio;

import tratamientos.ExceptionPlus;
import tratamientos.ExceptionSueldo;
import modelo.Nomina;
import modelo.Personal;

public class TeoriaArray {

	private int[] array=new int[5];
	private int[] array2={2,4,6,8};
	
	private Personal[] personas=new Personal[3];
	
	private int a=0;
	private int total;
	
	public TeoriaArray() throws ExceptionSueldo, ExceptionPlus{
		
		array[0]=1;
		array[1]=2;
		array[2]=3;
		array[3]=4;
		array[4]=5;
		
		// Cargar los datos de un array de objetos
		personas[a++]=new Personal("1","A","AA", new Nomina(1111,1112));
		personas[a++]=new Personal("2","B","BB", new Nomina(2111,2112));
		personas[a++]=new Personal("3","C","CC", new Nomina(3111,3112));
		
	} // constructor
	
	public void ver(){
		
		try{
		
			/*for(int a=0;a<array2.length+1;a++){
				
				System.out.println(array2[a]);
						
			}*/
			
			for(int a=0;a<personas.length;a++){
				
				System.out.println("dni: "+personas[a].getDni());
				total=personas[a].getNomina().getSueldo()+personas[a].getNomina().getPlus();
				System.out.println("Total: "+total);
				
			}
		}
		catch(ArrayIndexOutOfBoundsException aie){
			
			System.out.println("Te has salido");
		}
	}
}
