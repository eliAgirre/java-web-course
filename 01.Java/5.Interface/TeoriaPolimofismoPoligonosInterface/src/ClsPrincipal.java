import interfaces.InterfacePoligono;

import java.io.IOException;

import polimorfismo.ClsAreaPoligonos;

public class ClsPrincipal {	

	public static void main(String[] args) throws IOException {
		
		InterfacePoligono miPoligono;
		
		// crea objetos triangulo o rectangulo, o sino no existe
		ClsAreaPoligonos miAreasPoligonos=new ClsAreaPoligonos();
		char valor=miAreasPoligonos.seleccionarTipoPoligono();
		
		miPoligono=miAreasPoligonos.seleccionarPoligono(valor);
		
		if(miPoligono!=null){
			
			System.out.println("El area es:" + miPoligono.area());
		}
		else{
			
			System.out.println("No existe este tipo de poligono");
		}
	} // main	
}
