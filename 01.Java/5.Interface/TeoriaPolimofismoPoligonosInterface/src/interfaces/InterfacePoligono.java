package interfaces;

public interface InterfacePoligono {
	
	
	public float getBase();
	public float getAltura() ;
	
	public void setBase(float base);	
	public void setAltura(float altura);
	
	public abstract float area();
	
}
