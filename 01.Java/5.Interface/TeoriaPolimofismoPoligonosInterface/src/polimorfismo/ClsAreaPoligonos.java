package polimorfismo;
import interfaces.InterfacePoligono;

import java.io.IOException;

public class ClsAreaPoligonos  {
	
	public char seleccionarTipoPoligono()throws IOException {
		
		System.out.println("Seleccione el tipo de poligono (t/r)");
		char c=(char)System.in.read();
		return c;
		
	} // seleccionarTipoPoligono 
	
	public InterfacePoligono seleccionarPoligono(char c) {
		
		InterfacePoligono miPoligono; 
		
		switch (c){
		
			case 't':
				miPoligono=new ClsTriangulo(4,3);
				break;
				
			case 'r':
				miPoligono=new ClsRectangulo(4,5);
				break;
				
			default:
				miPoligono=null;	
		}
		
		return miPoligono;
		
	} // seleccionarPoligono

	
}
