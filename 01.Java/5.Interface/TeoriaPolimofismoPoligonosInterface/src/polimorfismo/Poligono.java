package polimorfismo;

import interfaces.InterfacePoligono;

public abstract class Poligono implements InterfacePoligono 
{
	private float base;
	private float altura;
	
	public Poligono(float base,float altura){
		
		this.base=base;
		this.altura=altura;
		
	} // constructor
	
	// getters
	public float getBase() {
		return base;
	}
	public float getAltura() {
		return altura;
	}
	
	// setters
	public void setBase(float base) {
		this.base = base;
	}
	public void setAltura(float altura) {
		this.altura = altura;
	}
	
	// metodo abstract
	public abstract float area();
}
