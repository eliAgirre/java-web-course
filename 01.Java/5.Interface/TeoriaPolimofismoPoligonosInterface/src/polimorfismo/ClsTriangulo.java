package polimorfismo;
public class ClsTriangulo extends Poligono
{
	public ClsTriangulo(float base,float altura)
	{
		super(base,altura);
	}
	
	public float area()
	{
		return (super.getBase()*super.getAltura()/2);
	}
}
