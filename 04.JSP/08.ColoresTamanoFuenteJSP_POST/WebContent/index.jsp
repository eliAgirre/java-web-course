<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Colores y tamanos JSP</title>
<script type="text/javascript">

	function fcambio(argumento){
	
		document.forms[0].txtOculto.value=argumento;
		document.forms[0].submit();
	}

	
</script>
</head>
<body>

	<form action="index.jsp" method="post">
	
		<%
			
			String cambio=request.getParameter("txtOculto");
			String colorletra="";
			String tamano="";
			
			HttpSession sesion=request.getSession(true); //Para grabar en la sesion
			
			if(cambio==null){
				colorletra="black";
				sesion.setAttribute("color",colorletra);
			}
			else if(cambio.equals("0")){
				colorletra="red";
				sesion.setAttribute("color",colorletra);
			}
			else if(cambio.equals("1")){
				colorletra="green";
				sesion.setAttribute("color",colorletra);
			}
			else if (cambio.equals("2")){
				colorletra="blue";
				sesion.setAttribute("color",colorletra);
			}
			
			if(cambio==null){
				tamano="1";
				sesion.setAttribute("tamano",tamano);
			}
			else if(cambio.equals("3")){
				tamano="3";
				sesion.setAttribute("tamano",tamano);
			}
			else if(cambio.equals("5")){
				tamano="5";
				sesion.setAttribute("tamano",tamano);
			}
			else if (cambio.equals("7")){
				tamano="7";
				sesion.setAttribute("tamano",tamano);
			}
			
			out.println("<font size='"+sesion.getAttribute("tamano").toString()+"' color='"+sesion.getAttribute("color").toString()+"'>Hola</font><br>");
			
		%>
		
		<input type="button" value="Rojo" name="color" onclick="fcambio(0);">
		<input type="button" value="Verde" name="color" onclick="fcambio(1);">
		<input type="button" value="Azul" name="color" onclick="fcambio(2);"><br>
		
		<input type="button" value="3" name="size" onclick="fcambio(3);" />
		<input type="button" value="5" name="size" onclick="fcambio(5);" />
		<input type="button" value="7" name="size" onclick="fcambio(7);" /><br>

		<input type="hidden" name="txtOculto">
	
	</form>

</body>
</html>