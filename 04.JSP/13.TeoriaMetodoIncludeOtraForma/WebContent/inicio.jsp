<%@page contentType="text/html; charset=iso-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
"http://www.w3.org/TR/REC-html40/loose.dtd">
<html>
<head>
<title>metodo include</title>
</head>
<script language="javaScript">
	function includepag(pagina) {
		document.location.href = "inicio.jsp?pag=" + pagina;
	}
</script>

<body>
	<form name="botones">
		<p
			style="text-align: center; font-weigth: text-align:center; color: olive; font-size: 25px">
			M&eacute;todo include de jsp</p>
		<%
			String sPag = "";
			try {

				String sMostrar = request.getParameter("pag");
				if (sMostrar.equals("1"))
					sPag = "include1.jsp";
				else if (sMostrar.equals("2"))
					sPag = "include2.html";
				else if (sMostrar.equals("3"))
					sPag = "include3.html";
			} catch (NullPointerException e) {

				//Primera vez
				sPag = "include0.jsp";
			}
		%>
		<p>
		
		<jsp:include page="<%=sPag%>" flush="true" />
		
		<p style="color: green; font-size: 25px">y continuamos con nuestra hoja...</p>

		<input type="button" name="btn1" value="Pag.1" onclick="includepag('1');"><br>
		
		<input type="button" name="btn2" value="Pag.2" onclick="includepag('2');"><br>

		<input type="button" name="btn3" value="Pag.3" onclick="includepag('3');"><br>
		
	</form>
</body>
</html>
