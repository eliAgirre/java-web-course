<%@page contentType="text/html; charset=iso-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
"http://www.w3.org/TR/REC-html40/loose.dtd">
<html>
<head>
<title>Uso de Bean</title>
</head>
<body>
<p style="color:red;font-size:20px">
 Uso de los JavaBean 
</p>
<!-- jsp:useBean id="nombre" class="paquete.Clase" /> (el paquete es obligatorio) 
 la clase esta situada en 
 "F:\JRun\jsm-default\classes\Bean.Uso_de_los_JavaBean_con_Jsp\ClsMensaje.class"
 -->
<jsp:useBean id="mibean" class="modelo.Mensaje"/>

<!--hay dos formas de utilizaci�n de Bean desde jsp.
	(Con expresiones o con metodos propios de jsp). Son sinonimos
-->
<ol>	
	<li>
		<!--Expresi�n para leer-->
		Valor incial(getProperty);
		<label style="font-style:italic">
			<jsp:getProperty name="mibean"
				property="mensaje"/> 
		</label>
	</li>	
	<li>
		<!-- M�todo jsp para leer-->
		 Valor incial (expresi&oacute;n JSP); 
		<label style="font-style:italic">
			 <%=mibean.getMensaje()%> 
		</label>		
	</li>	
	<li>
		<!--Expresi�n para escribir-->
		<jsp:setProperty name="mibean" 
				 property="mensaje"
				 value="Nuevo mensaje con expresion"/>
						 
		Valor despues de establecer la propiedad con setProperty:
		<!-- Expresi�n para leer-->
		<label style="font-style:italic">
			<jsp:getProperty name="mibean" 
					property="mensaje"/>
		</label> 
	</li>		
	<li>
		<!--M�todo jsp para escribir-->
		<%mibean.setMensaje("Nuevo mensaje con metodo jsp"); %>
		
	    Valor despues de establecer la propiedad con un scriptlet:
	    <!-- M�todo jsp para leer-->
	    <label style="font-style:italic">
	    	<%=mibean.getMensaje()%> 
	    </label> 
	  </li>
 </ol>
</body>
</html>
