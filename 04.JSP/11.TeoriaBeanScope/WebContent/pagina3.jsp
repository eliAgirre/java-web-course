<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<HTML>
<HEAD>
<TITLE>Compartir Beans</TITLE>
</HEAD>

<BODY>

<TABLE BORDER=5 ALIGN="CENTER">
  <TR><TH CLASS="TITLE">
     Accesos Pagina 3</TABLE>
<P>

<jsp:useBean id="contador" 
             class="modelo.ClsTeoria"
             scope="session"/>
			 
  <jsp:setProperty name="contador" 
                   property="nombre"
                   value="Tercera pagina con beans" />


<h1><b>Pagina 3</b></h1> <p>
<A HREF="pagina1.jsp">Pagina1.jsp</A><br> 
<A HREF="pagina2.jsp">Pagina2.jsp</A><p> 

Valor de la propiedad en el bean=
<b><jsp:getProperty name="contador" property="nombre" /></b>

<P>
En conjunto las tres paginas has sido accedidas
<jsp:getProperty name="contador" property="recuentoAccesos" />
veces.
            
</BODY>
</HTML>