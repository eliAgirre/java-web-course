<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE>Compartir Beans</TITLE>
</HEAD>

<BODY>

<TABLE BORDER=5 ALIGN="CENTER">
  <TR><TH CLASS="TITLE">
      Accesos Pagina 1</TABLE>
<P>


<jsp:useBean id="contador" class="modelo.ClsTeoria"
             scope="session"/>
		 
  <jsp:setProperty name="contador" 
                   property="nombre"
                   value="Primera pagina con beans" />


<h1><b>Pagina 1</b></h1> <p>
<A HREF="pagina2.jsp">Pagina 2.jsp</A> <br>
<A HREF="pagina3.jsp">Pagina 3.jsp</A><p> 
<!--
 scope="page"/> la propiedad "recuentoAccesos" del
 			    bean siempre estaria a 1 ya que se iniciazila
				cada vez que entra en la pagina

 scope="page"/> la propiedad "recuentoAccesos" se mantiene 
 				en cada session
 
 scope="application"/> la propiedad "recuentoAccesos" se mantiene 
 				en todas las session				
-->

Valor de la propiedad en el bean=
<b><jsp:getProperty name="contador" property="nombre" /></b>

<P>
En conjunto las tres paginas has sido accedidas
<jsp:getProperty name="contador" property="recuentoAccesos" />
veces.
             
</BODY>
</HTML>