package modelo;

public abstract class Trabajador {
	
	private String dni;
	private String nombre;
	private int ciudad;
	private double sueldo;
	private double complementos;

	public abstract double getTotal();

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public int getCiudad() {
		return ciudad;
	}

	public void setCiudad(int ciudad) {
		this.ciudad = ciudad;
	}
	public double getSueldo() {
		return sueldo;
	}
	public void setSueldo(double sueldo) {
		this.sueldo = sueldo;
	}
	public double getComplementos() {
		return complementos;
	}
	public void setComplementos(double complementos) {
		this.complementos = complementos;
	}
}
