package modelo;

public class TrabajadorMadrid extends Trabajador {
	
	private double cp;
	private double dietas;
	
	public double getCp() {
		return cp;
	}

	public void setCp(double cp) {
		this.cp = cp;
	}
	
	public double getDietas() {
		return dietas;
	}

	public void setDietas(double dietas) {
		this.dietas = dietas;
	}

	@Override
	public double getTotal() {
		
		return this.getSueldo()+this.getComplementos()+this.dietas;
	}

}
