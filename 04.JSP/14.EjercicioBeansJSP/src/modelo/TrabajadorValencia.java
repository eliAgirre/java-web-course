package modelo;

public class TrabajadorValencia extends Trabajador {
	
	private double plus;
	
	public double getPlus() {
		return plus;
	}

	public void setPlus(double plus) {
		this.plus = plus;
	}

	@Override
	public double getTotal() {
		
		return this.getSueldo()+this.getComplementos()+this.plus;
	}

}
