<%@page contentType="text/html; charset=iso-8859-1"%>
<%@page isErrorPage="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Error</title>
<script language='javascript'>
	function retroceder() {
		history.back(); //retrocede a la pagina anterior (directiva_import.html)
	}
</script>
</head>
<body>
	<form>
		<center>
			<font size='5'><b> Se ha producido un error al intentar procesar la procesar la pagina </b></font> <br> 
			<font size='4' color='red'> Tipo de error&nbsp;&nbsp; <%=(String) session.getAttribute("error")%>
				<%
					session.removeAttribute("error");
				%>
			</font>
			<p>
		</center>
		<input type='button' value='Volver' onclick='retroceder();'>
	</form>
</body>
</html>