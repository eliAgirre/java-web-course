<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page errorPage="error.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Resultados</title>
</head>
<body>

	<%	
		String aux=request.getParameter("sueldo");
		double dSueldo=new Double(aux).doubleValue();
		aux=request.getParameter("complementos");
		double dComplementos=new Double(aux).doubleValue();
		aux=request.getParameter("city");
		int iCiudad=new Integer(aux).intValue();
		
		//por si falla algun campo numerico
		session.setAttribute("error", "Campo numerico erroneo y/o obligatorio");
		
		if(iCiudad==0){ // Madrid 
			
			aux=request.getParameter("cp");
			double dCP=new Double(aux).doubleValue();
			aux=request.getParameter("dietas");
			double dDietas=new Double(aux).doubleValue();%>

			<jsp:useBean id="trabajadorM" class="modelo.TrabajadorMadrid" scope="session"/> 
			<jsp:setProperty name="trabajadorM" property="cp" value="<%=dCP %>"/>
			<jsp:setProperty name="trabajadorM" property="sueldo" value="<%=dSueldo %>"/>
			<jsp:setProperty name="trabajadorM" property="complementos" value="<%=dComplementos %>"/> 
			<jsp:setProperty name="trabajadorM" property="dietas" value="<%=dDietas %>"/>
			
			<label><b>Dni: </b></label><jsp:getProperty name="trabajadorM" property="dni"/><br/>
			<label><b>Nombre: </b></label><jsp:getProperty name="trabajadorM" property="nombre"/><br/>
			<label><b>CP: </b></label><jsp:getProperty name="trabajadorM" property="cp"/><br/>
			<label><b>Sueldo: </b></label><jsp:getProperty name="trabajadorM" property="sueldo"/>&euro;<br/>
			<label><b>Complementos: </b></label><jsp:getProperty name="trabajadorM" property="complementos"/>&euro;<br/>
			<label><b>Dietas: </b></label><jsp:getProperty name="trabajadorM" property="dietas"/>&euro;<br/>
			<label><b>Total: </b></label><jsp:getProperty name="trabajadorM" property="total"/>&euro;<br/><br/> <%
			
		}
		else{ // Valencia 
			
			aux=request.getParameter("plus");
			double dPlus=new Double(aux).doubleValue();%>
	
			<jsp:useBean id="trabajadorV" class="modelo.TrabajadorValencia" scope="session"/> 
			<jsp:setProperty name="trabajadorV" property="sueldo" value="<%=dSueldo %>"/>
			<jsp:setProperty name="trabajadorV" property="complementos" value="<%=dComplementos %>"/> 
			<jsp:setProperty name="trabajadorV" property="plus" value="<%=dPlus %>"/> 
			
			<label><b>Dni: </b></label><jsp:getProperty name="trabajadorV" property="dni"/><br/>
			<label><b>Nombre: </b></label><jsp:getProperty name="trabajadorV" property="nombre"/><br/>
			<label><b>Sueldo: </b></label><jsp:getProperty name="trabajadorV" property="sueldo"/>&euro;<br/>
			<label><b>Complementos: </b></label><jsp:getProperty name="trabajadorV" property="complementos"/>&euro;<br/>
			<label><b>Plus: </b></label><jsp:getProperty name="trabajadorV" property="plus"/>&euro;<br/>
			<label><b>Total: </b></label><jsp:getProperty name="trabajadorV" property="total"/>&euro;<br/><br/> <%
			
		}
	%>
	
	<a href="index.jsp">Formulario inicial</a>

</body>
</html>