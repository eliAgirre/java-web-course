<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page errorPage="error.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Pagina2</title>
</head>
<body>

	<%	
		String sDni=request.getParameter("dni");
		String sNombre=request.getParameter("nombre");
		String aux=request.getParameter("ciudad");
		int iCiudad=new Integer(aux).intValue();
		String sPag="";
		
		if (sDni.isEmpty()){
			session.setAttribute("error","El DNI es obligatorio");
			sDni = null;
		}
		if (sNombre.isEmpty()){
			session.setAttribute("error","El nombre es obligatorio");
			sNombre = null;
		}
		
		sDni.length();
		sNombre.length();
		
		if(iCiudad==0){ // Madrid %>
	
			
			<jsp:useBean id="trabajadorM" class="modelo.TrabajadorMadrid" scope="session"/> 
			<jsp:setProperty name="trabajadorM" property="dni" value="<%=sDni %>"/>
			<jsp:setProperty name="trabajadorM" property="nombre" value="<%=sNombre %>"/>
			<jsp:setProperty name="trabajadorM" property="ciudad" value="<%=iCiudad %>"/> <%
			sPag = "includes/Madrid.html";
		}
		else{ // Valencia %>
	
			<jsp:useBean id="trabajadorV" class="modelo.TrabajadorValencia" scope="session"/> 
			<jsp:setProperty name="trabajadorV" property="dni" value="<%=sDni %>"/>
			<jsp:setProperty name="trabajadorV" property="nombre" value="<%=sNombre %>"/>
			<jsp:setProperty name="trabajadorV" property="ciudad" value="<%=iCiudad %>"/> <%
			sPag = "includes/Valencia.html";
			
		}
	%>
		<jsp:include page="<%=sPag%>" flush="true" />

</body>
</html>