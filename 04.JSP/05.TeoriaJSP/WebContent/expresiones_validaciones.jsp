<%@page contentType="text/html; charset=iso-8859-1"%> <!-- directiva contextType para que traduzca a HTML -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
"http://www.w3.org/TR/REC-html40/loose.dtd">
<html>
<head>
<title>Expresiones y declaraciones</title>
<%
    //EXPRESIONES 
    // es una expresión no adminte "private"
    int iExpresion1=0; 
    int iExpresion2=0;
%>   	

	<!-- DECLARACIONES(static) -->
<%! // obligatorio la "!"
	private int iDeclaracion1=0;
	private int iDeclaracion2=0;
%>
 
</head>
<body>

	<form>
	
		EXPRESIONES<br>
		
		ha accedido a la variable iExpresion1 <%=++iExpresion1%><br>
		
		ha accedido a la variable iExpresion2 <%=++iExpresion2%>
		<p>
			DECLARACIONES<br>
			
			ha accedido a la variable iDeclaracion1: <%=++iDeclaracion1%><br>
			
			ha accedido a la variable iDeclaracion2: <%=++iDeclaracion2%>
		<p>
		
		<!-- llama a tomcat, y tomcat lanza una nueva -->
		<a href="expresiones_validaciones.jsp">Pulsar</a>
	</form>
	s
</body>
</html>
