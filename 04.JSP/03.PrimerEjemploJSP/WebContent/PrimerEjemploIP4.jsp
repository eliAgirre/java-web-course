<%@page contentType="text/html; charset=iso-8859-1"%> <!-- directivas, por encima de HTML pero no es obligatorio -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
"http://www.w3.org/TR/REC-html40/loose.dtd">
<%
	//Variable de JSP
	String sMensaje= "Hola Mundo";
%>

<%@page import="java.net.InetAddress;"%> <!-- directiva import -->
<html>
<head>
<title>PrimerEjmeplo</title>

<script language='javascript'>

function ver(){
	
	//alert("El mensaje es " + document.forms[0].txt1.value); //funciona
	alert("El mensaje es " + "<%=sMensaje%>"); 
	// mensaeje a mostrar con =
}

</script>
</head>
<body>
	<form>
		<%-- Comentario de jsp --%>
		<p style="font-size:30px;text-align:center">Ejemplo de jsp</p>
		
		<!-- utilizo un estilo para la caja de texto txt1 -->
		<input type='text' style="color:red;font-style:italic" name='txt1'  value="<%=sMensaje%>">
		<br>
		
		<!-- objetos predefinidos 
			request(HttpServletRequest)
			response(HttpServletResponse)
			session(HttpSession)
			out(PrinterWriter)
			application(ServletContext)
			config(ServletConfig)
			pageContext(nueva clase)
			page(this)
		-->
		<p style='color:blue'>
		
			Fecha y hora:<%=new java.util.Date()%> <br>
			
			<!-- mayusculas -->
			Nombre de Host:<%=InetAddress.getLocalHost().getHostAddress().toUpperCase()%> <br>
			
			Su identificador de sesion es:<%=session.getId()%>
		</p>
		
		<input type="button" value="Pulsar" onclick="ver();"><br>
		
	</form>
</body>
</html>
