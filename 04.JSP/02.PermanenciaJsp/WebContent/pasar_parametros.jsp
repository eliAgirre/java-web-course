<%@page contentType="text/html; charset=iso-8859-1"%>
<%--<%@page contentType="text/html; charset=iso-8859-1"%>
	Para que el Netscape interprete el HTML sin problemas
	(El Explorer ya viene por defecto con (text/html)

 <%@page contentType="text/plain; charset=iso-8859-1"%> 
	El Nescape mostrar�a el html como si fuese texto
--%>	


<%-- PREDETERMINADA (NO HACE FALTA PONERLA)
  <%@page session="true" %> (Predeterminado )
  Asume que la variable predefinida session deber� vincularse a la sesion en curso,
  en caso de existir alguna, de otro modo se generar� una nueva sesion y se vincular� con
  session
 <%@page session="false" %> 
 significa que no se deber�n utilizar sesiones autom�ticas y cualquier intento
 de accedera la variable predefinida session dar� un error en el momento en que 
 la jsp sea traducida a servlet
  --%>

  <%@page session="true"%>
 <%--<%@page import="javax.servlet.http.HttpSession"--%>

 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
"http://www.w3.org/TR/REC-html40/loose.dtd">
<HTML>
<HEAD>
<TITLE>Pasar parametros(Ejemplos de scriptlets</TITLE>
<script language=javascript>

function continuar(){

	alert("Y funciona");
}

</script>
<%
		
	//HttpSession hs =request.getSession(true);

	//if (hs.isNew())
	//	System.out.println("nueva");
	//else
	//{	
	//	System.out.println("vieja");		
	//	hs.invalidate();		
	//	hs =request.getSession(true);
	//	System.out.println("nueva2=" + hs.isNew());
	//}
	
	if (session.isNew()){
		System.out.println("nueva");
	}
	else{
		
		System.out.println("vieja");		
		session.invalidate(); // invalida		
		session=request.getSession(true); // recupera la session
		System.out.println("nueva2="+session.isNew());
	}

	String destino=""; //Para almacenar la ruta de pagina error en el jsp_forward
	//Recuperamos los datos de la sesion (pagina pasar_parametros.html)
 	String sUsuario=request.getParameter("txtusuario");
	String sPassword=request.getParameter("txtpassword");
%>
	
</HEAD>
<BODY bgcolor=olive>
<form>
<font color=blue size=4>
<%
	if(sUsuario.equalsIgnoreCase("angel") && sPassword.equalsIgnoreCase("miguel")){
		
		out.println("Datos correctos "+"<br>"+ "Usuario:" +sUsuario+"<br>"+"Password:"+sPassword);
	}
	else {
		
		//Grabamos el dato en la sesion
		//hs.putValue("txtusuario",sUsuario); 
		//Grabamos el dato en la sesion
		//hs.putValue("txtpassword",sPassword); 
		
		// request.setAttribute("txtusuario",sUsuario);
		// session.setAttribute("txtusuario",sUsuario);
		//session.putValue("txtusuario",sUsuario); 
		//session.putValue("txtpassword",sPassword); 
		request.setAttribute("txtusuario",sUsuario); 
		request.setAttribute("txtpassword",sPassword); 
		destino="error_pasar_parametros.jsp";
		// si no son correctos mandamos la pagina de error(error_pasar_parametros.jsp)
		//	funciona tanto con el sendRedirect como con el 
		//  <jsp:forward page="<%=destino--%>"/> 
		//es mejor el forward ya que la pagina se carga en el servidor
		%> <!-- Obligatorio cerrar-->
		<!-- etiquetas personalizadas, nunca dentro van dentro de Java pero si admite Java -->
			<jsp:forward page="<%=destino%>"/> 
		<%
	}
%>	
</font>
<p><center>
<input type=button value=Continuar onclick=continuar();>
</center>
</form>
</BODY>
</HTML>
