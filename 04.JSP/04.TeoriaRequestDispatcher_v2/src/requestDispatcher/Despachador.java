package requestDispatcher;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import utilidades.Navegar;

public class Despachador extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	public void doPost (HttpServletRequest req,HttpServletResponse resp) throws ServletException, IOException{
		
		doGet(req,resp);
	}
	
	public void doGet (HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		
		//Recuperamos los parametros
		String Susuario=request.getParameter("usuario"); 
		String Spassword=request.getParameter("password"); 
		
		//para grabar en la sesion esta tiene que estar a true
		HttpSession hs=request.getSession(true); 
			
		if (!hs.isNew()){ // Por si no esta a true, la invalidamos y la
			//volvermos a poner a true
		
			hs.invalidate(); // anula
			hs=request.getSession(true); // pide otra nueva
		}
			
		request.setAttribute("usuario",Susuario);
		request.setAttribute("password",Spassword);
	
		if(Susuario.equalsIgnoreCase("angel") && Spassword.equals("miguel")){			
			
			request.setAttribute("clave","true");			
		}
		else{ 
			request.setAttribute("clave","false"); // se modifica el valor
		}
		
		Navegar.navegar("resultado.jsp",request,response);
	}
	
}
