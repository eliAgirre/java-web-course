<%@page contentType="text/html; charset=iso-8859-1"%>
<%--<%@page contentType="text/html; charset=iso-8859-1"%>
	Para que el Netscape interprete el HTML sin problemas
	(El Explorer ya viene por defecto con (text/html)


   <%@page contentType="text/plain; charset=iso-8859-1"%> 
	El Nescape mostraría el html como si fuese texto
--%>
	
<%@page import="paqueteA.*;"%>  
<%-- 
	<%@page import="java.util.*"%> 
	esta directiva sirve para importar paquetes 
	(ej <%@page import="java.util.*"%>)
	en el caso de que el paquete no sea predefinido hay que colocarlo en
	"F:\JRun\jsm-default\classes\nombre_paquete
	(ej La Clase ClsSumar.class situado en el paqueteA esta situada en
	 "F:\JRun\jsm-default\classes\paqueteA" 
--%>

<%--obtiene el error y llama a otro jsp a mostrar--%>
<%@page errorPage="pagina_error.jsp"%>
<%-- En caso de producirse cualquier error en esta pagiana
     (falta un argumento, mala transformación de datos etc..
      no se mostraría
      esta pagina y saltaría de forma automática a 
      la pagina "pagina_error.jsp"
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
"http://www.w3.org/TR/REC-html40/loose.dtd">
<HTML>
<HEAD>
<TITLE>directivas import, errorPage e isErrorPage</TITLE>
<%

	//Grabo los errores en la sesion
	int iNumero1=0;
	int iNumero2=0;
	
	session.setAttribute("error","Los argumento son obligatorios y numericos");	
	
	iNumero1=Integer.parseInt(request.getParameter("numero1"));	
	
	iNumero2=Integer.parseInt(request.getParameter("numero2"));
	

	ClsSumar Suma=new ClsSumar();
	int valor=Suma.sumar(iNumero1,iNumero2);
	
%>

</HEAD>
<BODY>
	<form>
		<center>
			<font size=5><b>
				directivas import, errorPage e isErrorPage
			</b></font>
		</center>
		
		<%-- en jsp no es necesario transformar los int a String para mostrar los
		datos en html--%>
		<font color='blue'>
			El resultado de <%=iNumero1%> + <%=iNumero2%> 
			es <%=valor%>
		</font>
		
	</form>
</BODY>
</HTML>