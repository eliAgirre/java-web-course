<%@page contentType="text/html; charset=iso-8859-1"%>
<%@page isErrorPage="true"%> <!-- convierte esta pagina en la pagina de error, para tratar el error -->
<!--para convertir esta pagina en pagina de error automatica....
  Gracias a esta directiva, en caso de producirse un error en la pagina
  "directiva_import.jsp" se muesta automaticamente esta pagina
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
"http://www.w3.org/TR/REC-html40/loose.dtd">
<HTML>
<HEAD>
<TITLE>directivas import, errorPage e isErrorPage</TITLE>

<script language='javascript'>

function retroceder(){
	
	history.back(); //retrocede a la pagina anterior (directiva_import.html)
}

</script>
</HEAD>
<BODY> 

	<form>
		<center>
			<font size='5'><b>
				Se ha producido un error al intentar procesar la procesar la pagina
			</b></font>
			<br>
			<font size='4' color='red'>
				
				<!-- con request no hace falta hacer el remove -->
				<!-- (String)request.metodo("error")  -->
				
				<%=(String)session.getAttribute("error")%>
				<!-- borra la session -->
				<%session.removeAttribute("error");%>
			</font>
			<p>
		</center>
		
		<input type='button' value='Volver' onclick='retroceder();'>
		 
	</form>
	
</BODY>
</HTML>
