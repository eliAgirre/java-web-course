package Bean.Servlet_bean_jsp;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ClsServletGrabar extends HttpServlet
{
   	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest req,HttpServletResponse resp) throws ServletException, IOException{
		
			String sNombre=req.getParameter("txtnombre");
			String sDni=req.getParameter("txtdni");
		          
         	// para grabar el bean en el objeto "application"
			//ServletContext sc = getServletContext();
			
			//Para grabar el bean en el objeto session
			HttpSession hs=req.getSession(true);
 
			ClsBeanPersonal MyBean = new ClsBeanPersonal();
			
			//Grabamos en el bean
			MyBean.setNombre(sNombre);
			MyBean.setDni(sDni);
			
			//Para poner el bean en la sesion
		   	hs.setAttribute("prueba_servlet",MyBean);
			resp.sendRedirect("servlet_bean_jsp1.jsp");
	}
}