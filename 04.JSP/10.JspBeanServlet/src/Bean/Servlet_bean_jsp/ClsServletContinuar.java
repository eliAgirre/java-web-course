package Bean.Servlet_bean_jsp;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ClsServletContinuar extends HttpServlet
{
   	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest req,HttpServletResponse resp) throws ServletException, IOException{
	 		//Para leer el bean en el objeto aplicación
			//ServletContext sc = getServletContext();

			//Para leer el bean en el objeto session
			HttpSession hs=req.getSession(true);
			
			//Para leer el bean es obligatorio el cash de tipo "Object" o Clase
			ClsBeanPersonal MyBean=(ClsBeanPersonal)hs.getAttribute("prueba_servlet");

			//Grabamos las propiedades en el bean
			int misueldo=0;
			try{
				misueldo=Integer.parseInt(req.getParameter("txtsueldo"));
			}
			catch(NumberFormatException e){
				misueldo=0;
			}
			finally{
				MyBean.setSueldo(misueldo);
			}
			MyBean.setDireccion(req.getParameter("txtdireccion"));
			
			resp.sendRedirect("servlet_bean_jsp2.jsp");
		}
}
