package Bean.Servlet_bean_jsp;
public class ClsBeanPersonal
{

	private String nombre;
	private String dni;
	private String direccion;
	private int sueldo;

	public String getNombre()
	{
		return (nombre);
	}
	
	public String getDni()
	{
		return (dni);
	}
		
	public String getDireccion()
	{
		return (direccion);
	}
	
	public int getSueldo()
	{
		return (sueldo);
	}
	
	public void setNombre(String nombre)
	{
		this.nombre=nombre;
	}
	
	public void setDni(String dni)
	{
		this.dni=dni;
	}
	
	public void setDireccion(String direccion)
	{
		this.direccion=direccion;
	}
	
	public void setSueldo(int sueldo)
	{
		this.sueldo=sueldo;
	}
}
