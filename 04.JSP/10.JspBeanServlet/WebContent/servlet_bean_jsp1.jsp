<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE>Servlet-Bean-Jsp</TITLE>
</HEAD>
<BODY>
<TABLE BORDER=5 ALIGN="CENTER">
  <TR><TH CLASS="TITLE">
      Pagina 1.jsp</TABLE>
<P>
<!-- class: ruta -->
<jsp:useBean id="prueba_servlet" class="Bean.Servlet_bean_jsp.ClsBeanPersonal"
             scope="session"/>
<br>
Los datos introducidos son<br>
Nombre=
<b><jsp:getProperty name="prueba_servlet" property="nombre"/></b>
<br>
Dni=
<b><jsp:getProperty name="prueba_servlet" property="dni"/></b>
<p>
<form action=ClsServletContinuar method=post>
Introduzca nuevos datos<br>
Direccion&nbsp;<Input type=text name=txtdireccion><br>
Sueldo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Input type=text name=txtsueldo><p>

<%--
 funciona (para comprobar que se puede cambiar las
		  propiedades del bean desde cualquer jsp)
<jsp:setProperty 
	name="prueba_servlet"
	property="nombre" 
	value="cambiado" />
--%>	

<input type=submit value=Continuar>
</form>       
</BODY>
</HTML>