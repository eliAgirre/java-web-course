<%@page contentType="text/html; charset=iso-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
"http://www.w3.org/TR/REC-html40/loose.dtd">
<html>
<head>
<title>metodo include</title>
</head>
<body>
	<form>
		<p
			style="text-align: center; font-size: 25px; color: olive; font-weight: bold">
			<!-- acento(&vocalacute; ej �=&acutea)-->
			Prueba de del m&eacute;todo include de jsp
		</p>
		<%-- <jsp_include page="URL relativo2 flush="true"/> 
      Este metodo sirve para incluir documentos html o jsp(en algunos sevidores
      falla) en nuestra p�gina jsp
--%>
		<ul>
			<!-- litas ordenadas -->
			<!-- flush:true es obligatorio -->
			<li><jsp:include page="include1.html" flush="true" /></li>
			<li><jsp:include page="include2.html" flush="true" /></li>
			<li><jsp:include page="include3.html" flush="true" /></li>
			<li><jsp:include page="include4.jsp" flush="true" /></li>
			<li><jsp:include page="/carpeta/archivo.txt" flush="true" /></li>
		</ul>
		<p style="font-size: 25px; color: red">y continuamos con nuestra
			hoja</p>
	</form>
</body>
</html>