package holaMundo;
import java.io.*;
import javax.servlet.*;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

//@WebServlet("/pepe")
public class ServletHolaMundo extends HttpServlet {
  
  	private static final long serialVersionUID = 1L;
  	
  	public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
  		
  		// llama a doGet y le pasa request y response
  		doGet(request,response);
  		
  	} // doPost

	public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		
		String msg="Hola Mundo";
		
		//Conveniente, no obligatorio 
		response.setContentType("text/html"); // se traduce texto al html
		//response.setContentType("plain"); 
		  
		/* Saco la salida */
		ServletOutputStream out=response.getOutputStream(); // jsp out
		// parecido a printwriter --> para ficheros
		
		/* Mando una p�gina html f�cil */
		out.println("<html>");
		out.println("<head><title>"+msg+"</title></head>");
		out.println("<body>");
		out.println("<h1>"+msg+"</h1>");
		out.println("</body>");
		out.println("</html>");
		
		/* Cierro la salida para mandarla */            
		out.close(); // puede dar problemas en los servidores si se comenta
		
	} // doGet
  
}