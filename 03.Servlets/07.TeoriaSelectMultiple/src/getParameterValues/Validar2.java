package getParameterValues;

import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class Validar2 extends HttpServlet {
	
	public void doPost(HttpServletRequest req,HttpServletResponse resp) throws ServletException, IOException{
		
			resp.setContentType("text/html"); //Conveniente, no obligatorio
			String Salida="";
			
			// se obtiene los parametros desde el formulario
			String Susuario=req.getParameter("USUARIO");
			String Spassword=req.getParameter("PASSWORD");
			
			if(Susuario.equals("angel") && Spassword.equals("miguel")){ //Salida="Bien";
				
				//Para recoger los argumentos de una <select> de html
				String[] arr=req.getParameterValues("HOBBIE");
				
				if (arr!=null){
					
					for(int a=0;a< arr.length;a++){
						
						//Salida=Salida + " " + arr[a];
						Salida += " " + arr[a];
					}
				}
				else
					Salida="Ninguno";	
			}
			else
			Salida="Mal";
			ServletOutputStream out = resp.getOutputStream();
			 out.println("<HTML>\n" +
		     "<HEAD><TITLE>Post WWW</TITLE></HEAD>\n" +
		       "<BODY>\n" + "<H1>Ha elegido:" + Salida + "</H1>\n" +
		           "</BODY></HTML>");
		                
		    /* Cierro la salida para mandarla */            
		    out.close(); 
	}
}