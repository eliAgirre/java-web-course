package parametrosAmuchosServlet;

import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class ClsParametrosGenerales extends HttpServlet{
	
	String nombre=null;
	String apellido1=null;
	String apellido2=null;
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException,IOException { 
		
		//recogida de los valores del formulario
		nombre=req.getParameter("nombre");
		apellido1=req.getParameter("apellido1");
		apellido2=req.getParameter("apellido2");
		
		// se obtiene desde web.xml
		String parametro = getServletContext().getInitParameter("parametro1");
		
		//salida por pantalla de la respuesta del servidor
		resp.setContentType("text/html");
		
		PrintWriter out = resp.getWriter();
		out.println("");
		out.println("");
		out.println("");
		out.println("");
		out.println("");
		out.println("");
		out.println("Los datos introducidos son:");
		out.println("NOMBRE:" + nombre);
		out.println("APELLIDO1:" + apellido1);
		out.println("APELLIDO2:" + apellido2);
		out.println("");
		out.println("");
		out.println("");
		out.println("parametero: " + parametro);
		out.flush(); // solo obligatorio en jsp para los includes
		out.close(); // lanza la pagina
		
	}
}
