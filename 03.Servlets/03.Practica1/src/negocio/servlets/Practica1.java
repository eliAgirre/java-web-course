package negocio.servlets;
import negocio.classes.Operar;
import vista.EscribeHtml;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Practica1
 */
public class Practica1 extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private String titulo="practica1";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Practica1() {} // constructor vacio

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//System.out.println("Get");
		//doPost(request,response);
		mostrar(request,response);
		
	} // doGet

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//System.out.println("Post");
		mostrar(request,response);
		
		
	}  // doPost
	
	private void mostrar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html"); // conveniente, no obligatorio
		
		// obtener los parametros desde form
		int num1=new Integer(request.getParameter("txt1")).intValue();
		int num2=new Integer(request.getParameter("txt2")).intValue();
		
		// Se crea el objeto Operar y se le llama al metodo los parametros a sumar
		int suma=new Operar().suma(num1, num2);
		
		ServletOutputStream out=response.getOutputStream();
		
		EscribeHtml escribe=new EscribeHtml(out); 
		
		escribe.escribeCabecera(titulo);
	    out.print("<p>La suma es: "+suma+"</p>");
	    out.print("<p>");
	    out.print("<a href='index.html'>Volver</a>");
	    escribe.escribePie();
		
	} // mostrar

}
