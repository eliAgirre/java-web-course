package negocio.servlets;
import modelo.Resultado;
import negocio.classes.Operar;
import vista.EscribeHtml;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Practica1
 */
public class Practica1 extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private String titulo="practica1";
	private Operar operar=new Operar();
	private Resultado resultados=new Resultado();
	private ArrayList<Resultado> arrayList=new ArrayList<Resultado>();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Practica1() {} // constructor vacio

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//System.out.println("Get");
		//doPost(request,response);
		mostrar(request,response);
		
	} // doGet

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//System.out.println("Post");
		mostrar(request,response);
		
		
	}  // doPost
	
	private void mostrar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html"); // conveniente, no obligatorio
		
		// obtener los parametros desde form
		int num1=new Integer(request.getParameter("txt1")).intValue();
		int num2=new Integer(request.getParameter("txt2")).intValue();
		
		// obtener las radios desde el form
		String[] array=request.getParameterValues("operacion");
		
		int resultado=0;
		String msg="";
		
		if (array!=null){
			
			for(int i=0;i<array.length;i++){
				
				if(array[i].equals("0")){
					
					msg="suma";
					resultado=operar.suma(num1, num2);
					resultados=new Resultado(msg,resultado);
					arrayList.add(resultados);
					
				}
				if(array[i].equals("1")){
					
					msg="resta";
					resultado=operar.resta(num1, num2);
					resultados=new Resultado(msg,resultado);
					arrayList.add(resultados);
				}
				if(array[i].equals("2")){
					
					msg="producto";
					resultado=operar.producto(num1, num2);
					resultados=new Resultado(msg,resultado);
					arrayList.add(resultados);
				}
			}
		}
		else{
			msg="Mal";
			resultado=0;
		}
		
		
		ServletOutputStream out=response.getOutputStream();
		
		EscribeHtml escribe=new EscribeHtml(out); 
		
		escribe.escribeCabecera(titulo);
	    for(int i=0;i<arrayList.size();i++){
	    	
	    	out.print("<p>"+arrayList.get(i).getTipo()+" es: "+arrayList.get(i).getResultado()+"</p>");
	    }
	    out.print("<p>");
	    out.print("<a href='index.html'>Volver</a>");
	    escribe.escribePie();
		
	} // mostrar

}
