package modelo;

public class Resultado {
	
	private String tipo;
	private int resultado;
	
	public Resultado() {

	}
	
	public Resultado(String tipo, int resultado) {
		
		this.tipo = tipo;
		this.resultado = resultado;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public int getResultado() {
		return resultado;
	}
	public void setResultado(int resultado) {
		this.resultado = resultado;
	}
	
	

}
