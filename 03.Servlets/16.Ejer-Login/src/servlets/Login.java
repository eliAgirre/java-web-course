package servlets;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Login extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private Hashtable<String,String> hashTable=new Hashtable<String,String>();
       
    public Login() {} // constructor vacio

	public void init(ServletConfig config) throws ServletException {
		
		//No es obligatorio pero si conveniente
    	super.init(config);
    	
    	// se obtienen los nombres desde el xml
    	Enumeration<String> e=config.getInitParameterNames();
    	
    	while(e.hasMoreElements()){ // mientras que contenga datos
			
    		String key=e.nextElement(); // se guarda el usuario siguiente
			String valor=config.getInitParameter(key); // obtiene el valor	
			hashTable.put(key,valor);
		}
    	
    } // init


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// obtener los parametros desde form
		String user=request.getParameter("user");
		String pass=request.getParameter("pass");
		
		String keys="";
		String valor="";
		
		if(hashTable.containsKey(user)){ // si el hashtable contiene la clave usuario
			
			Enumeration<String> users=hashTable.keys();
			
			while(users.hasMoreElements()) { // mientras que contenga datos el hashtable
				
				keys=(String)users.nextElement(); // se guarda el siguiente key
				
				if(keys.equals(user)){ // si el usuario y la clave son iguales
					
					valor=hashTable.get(user); // se guarda el valor del ese mismo user
					
					if(valor.equals(pass)){ // si el password coincide con el valor del hashtable
						
						response.sendRedirect("confirmar.html");
						
					}
					else{
						response.sendError(402,"No autorizado");
					}
					
				}
				
			} // while
			
		}
		else{  
			
			// El usuario no existe en hashtable, llama a una pagina HTML sin datos
			response.sendRedirect("error.html");
		}
		
	} // doPost

}
