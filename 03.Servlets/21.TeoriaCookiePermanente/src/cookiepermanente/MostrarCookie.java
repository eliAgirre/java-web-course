package cookiepermanente;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

//Genera una tabla con las cookies asociasas

public class MostrarCookie extends HttpServlet{
	
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException {
		
	    response.setContentType("text/html");
	    
	    PrintWriter out = response.getWriter();
	    
	    String titulo = "Las cookies activas";
	    
	    out.println("<html><head><title>" + titulo + "</head></title> \n" +
	                "<body bgcolor=\"#FDF5E6\">\n" +
	                "<h1 align=\"center\">" + titulo + "</h1>\n" +
	                "<table border='1' align=\"center\">\n" +
	                "<tr bgcolor=\"#FFAD00\">\n" +
	                "  <th>Cookie Nombre</th>\n" +
	                "  <th>Cookie Valor</th>" +
	                "</tr>");
	    
	    //recupera el array de cookies
	    Cookie[] cookies=request.getCookies();
	    
	    Cookie cookie;
	    
		String sValor="";
		
	    for(int i=0;i<cookies.length;i++){
	    	
	      cookie=cookies[i];
	      
	      //devuelve el valor de la cookie
		  sValor=cookie.getValue();
		  
	      out.println("<tr>\n" +
	      
	    		  	  //devuelve el nombre de la cookie
	    		  	  "  <td>" + cookie.getName() + "</td>\n" + 
	                  "  <td>" + sValor + "</td>" +
	                  "</tr>"); 
	    }
	    
	    out.println("</table></body></html>");
	    out.close();
	    
	  }
}
