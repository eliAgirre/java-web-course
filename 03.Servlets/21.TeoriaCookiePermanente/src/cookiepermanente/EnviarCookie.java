package cookiepermanente;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/*
  Establece 6 cookies: tres que s�lo duran la sesion y otras
  tres que duran una hora(sin importar si el navegador se reinicia)

Nota las cookies est�n compuestas de nombre,valor
Tanto el valor como el nombre no pueden llevar ni blancos ni caracteres especiales
*/
public class EnviarCookie extends HttpServlet{
	
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException {
	    
		for(int i=0; i<3; i++){
			
		    // Por defecto "maxAge es -1", indicando que la cookie s�lo dura 
			// la sesion del navegador en curso (mientras no se cierre el navegador)
		    Cookie cookie = new Cookie("Cookie_Sesion_" + i,"Valor_Cookie_Sesion_" + i);
		    
		    response.addCookie(cookie); //a�ade la cookie
		    
		    
		    cookie = new Cookie("Cookie_Persistente_" + i,"Valor_Cookie_Persistente_" + i);
		    // La cookie es v�lida por una hora, sin importar si el usuario
		    // sale del navegador, reinicia la computadora, o lo que sea
		    cookie.setMaxAge(3600); //Persistencia de la cookie en segundos (1 hora)
		    //cookie.setMaxAge(60*60*24*365); //Persistencia por un a�o
		    response.addCookie(cookie);
		    
	    } // for
		
		// llama a la pagina sin datos
		response.sendRedirect("VerCookies.html");
	  } 
}
