package holaMundo;
import java.io.*;
import javax.servlet.*;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

//@WebServlet("/ServletHolaMundo")
public class ServletHolaMundo extends HttpServlet {
  
  	private static final long serialVersionUID = 1L;

	public void service(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html"); //Conveniente, no obligatorio 
		  
		/* Saco la salida */
		ServletOutputStream out=response.getOutputStream();
		
		/* Mando una p�gina html f�cil */
		out.println("<html>\n"+"<head><title>Hola Mundo</title></head>\n"+
		            "<body>\n"+"<h1>Hola Mundo</h1>\n"+"</body></html>");
		            
		/* Cierro la salida para mandarla */            
		out.close();               
	}
  
}