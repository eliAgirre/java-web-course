package senderror;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
public class Error_p extends HttpServlet 
{
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest req,
        HttpServletResponse resp) 
        		throws ServletException, IOException
	{
		//Lanza un codigo de error
		
		//resp.sendError(401);// Unauthorized, ingles
		
		// En castellano,  code, mensaje
		resp.sendError(401,"No autorizado");
		
		//resp.sendError (503); //Service Unavailable
		//resp.sendError (503,"Servicio no disponible"); //En castellano
	}
}