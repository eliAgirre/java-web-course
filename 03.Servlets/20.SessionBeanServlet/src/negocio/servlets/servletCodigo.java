package negocio.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.Libro;


public class servletCodigo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public servletCodigo() {
    	
        super();
    } // constructor


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		mostrar(request,response);
		
	} // doGet


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		mostrar(request,response);
		
	} // doPost
	
	private void mostrar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession sesion=request.getSession();
		
		// se crea el objeto libro y se establece el codigo
		Libro libro=new Libro();
		libro.setCodigo(new Integer(request.getParameter("txtCodigo")));
		
		// se agrega el libro a la sesion, y no hace falta mas
		sesion.setAttribute("libro", libro);
		
		response.sendRedirect("pagina2.html");
		
	} // mostrar

}
