package negocio.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.Libro;


public class servletAutor extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public servletAutor() {
    	
        super();
    } // constructor


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		mostrar(request,response);
		
	} // doGet


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		mostrar(request,response);
		
	} // doPost
	
	private void mostrar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession sesion=request.getSession();
		
		String autor=request.getParameter("txtAutor");
		
		// recupera la sesion del libro
		Libro libro=(Libro) sesion.getAttribute("libro");
		libro.setAutor(autor);
		
		// llama a la html sin datos
		response.sendRedirect("pagina3.html");
		
	} // mostrar

}
