package negocio.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.Libro;


public class servletTitulo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public servletTitulo() {
    	
        super();
    } // constructor


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		mostrar(request,response);
		
	} // doGet


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		mostrar(request,response);
		
	} // doPost
	
	private void mostrar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession sesion=request.getSession(false);
		
		String titulo=request.getParameter("txtTitulo");
		
		// recupera la sesion del libro
		Libro libro=(Libro) sesion.getAttribute("libro");
		
		String autor=libro.getAutor();
		String codigo=Integer.toString(libro.getCodigo());
		
		ServletOutputStream out=response.getOutputStream();
		
		out.print("<html>");
		out.print("<head><title>Datos session</title></head>");
		out.print("<body>");
		out.print("<h3>Datos de la session</h3>");
		out.print("<p>Codigo: "+codigo+"</p>");
		out.print("<p>Autor: "+autor+"</p>");
		out.print("<p>Titulo: "+titulo+"</p>");
		out.println("<form>");
		out.println("<input type='button' value='Inicio'");
		out.println("onClick=document.location.href='index.html'>");
		out.println("</form>");
		out.print("</body>");
		out.print("</html>");
		
	} // mostrar

}
