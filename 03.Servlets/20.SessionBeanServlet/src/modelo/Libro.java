package modelo;

public class Libro {
	
	private int codigo;
	private String autor;
	private String titulo;
	
	public Libro(){}	
	
	public Libro(int codigo, String autor, String titulo) {
		
		this.codigo = codigo;
		this.autor = autor;
		this.titulo = titulo;
	}

	// getters
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getAutor() {
		return autor;
	}
	
	// setters
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

}
