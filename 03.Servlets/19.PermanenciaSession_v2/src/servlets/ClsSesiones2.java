package servlets;

import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class  ClsSesiones2 extends HttpServlet {
	
	protected void doPost(HttpServletRequest req,HttpServletResponse resp) throws ServletException, IOException {
		
		doGet(req,resp);
		
	} // doPost
	
	@SuppressWarnings("null")
	protected void doGet(HttpServletRequest req,HttpServletResponse resp) throws ServletException, IOException {
		
		String sTxt3=req.getParameter("txt3");
		String sTxt4=req.getParameter("txt4");
	
		HttpSession hs=req.getSession(true); //Para grabar en la sesion
		
		if (hs!=null){ //Para comprobar que se ha recuperado la sesi�n
			System.err.println("obtenida sesion");
		}
		else{
			
			hs.invalidate();
			hs =req.getSession(true);
		}  
		
		/***Deprecation (Funciona)******/
		// hs.putValue(Object,valor);
			//Object -->"nombre" 
			//String-->"valor"
		//hs.putValue("txt3",sTxt3); //Grabar el dato en la sesi�n
		//hs.putValue("txt4",sTxt4);
		/************/
		
		//setAttribute(Object,valor);
			//Object -->"nombre" 
			//String-->"valor"
		
		//Grabar el dato en la sesi�n
		hs.setAttribute("txt3",sTxt3);
		hs.setAttribute("txt4",sTxt4);
		
		// llama a la pagina html sin datos
		resp.sendRedirect("sesiones3.html");
		
	} // doGet
}