package servlets;

import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class  ClsSesiones3 extends HttpServlet {
	
	protected void doPost(HttpServletRequest req,HttpServletResponse resp) throws ServletException, IOException{
		
		doGet(req,resp);
		
	} // doPost
	
	protected void doGet(HttpServletRequest req,HttpServletResponse resp) throws ServletException, IOException{
		
		HttpSession hs=req.getSession(false); 	//Para terminar la sesi�n
		
		if(hs!=null){
			
			//****Deprecation (funciona)*******
			//Recuperar el dato de la sesi�n (si se usa putValue
			//String sTxt1=(String)hs.getValue("txt1"); 
			
			//Recuperamos los datos de la sesi�n
			String sTxt1=hs.getAttribute("txt1").toString();
			// Sinonimo
			String sTxt2=String.valueOf(hs.getAttribute("txt2"));
			//Sinonimo
			String sTxt3=(String)hs.getAttribute("txt3");
			String sTxt4=(String)hs.getAttribute("txt4");
			
			String sTxt5=req.getParameter("txt5");
			String sTxt6=req.getParameter("txt6");
			
			//Para mandar la Pagina din�micamente
			resp.setContentType("text/html");
			PrintWriter out=resp.getWriter();
		
			out.println("<HTML><HEAD><TITLE>Sesiones</TITLE></HEAD><BODY> \n" +
				"<form><center> El resultado de la sesion</center><h1> \n" +
				"Txt1=" + sTxt1 + "<br> \n" +
				"Txt2=" + sTxt2 + "<br> \n" +
				"Txt3=" + sTxt3 + "<br> \n" +	
				"Txt4=" + sTxt4 + "<br> \n" +	
				"Txt5=" + sTxt5 + "<br> \n" +	
				"Txt6=" + sTxt6 + "<br> \n" +	
				"</h1></form></BODY></HTML>");
			out.close();
		}
		else{
			resp.sendError(302,"PAGINA NO ENCONTRADA"); // Pagina no encontrada por perder la conexion
		}
		
	} // doGet
}
