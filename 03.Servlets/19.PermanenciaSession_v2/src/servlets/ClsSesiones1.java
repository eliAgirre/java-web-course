package servlets;

import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class  ClsSesiones1 extends HttpServlet {
	
	protected void doPost(HttpServletRequest req,HttpServletResponse resp) throws ServletException, IOException{
		
		doGet(req,resp);
		
	} // doPost
	
	protected void doGet(HttpServletRequest req,HttpServletResponse resp) throws ServletException, IOException{
		
		String sTxt1=req.getParameter("txt1");
		String sTxt2=req.getParameter("txt2");
	
		HttpSession hs=req.getSession(true); //Para grabar en la sesion
		
		if (hs.isNew()){
			
			System.err.println("obtenida sesion"); // grabar en los log
		}
		else{
			
			hs.invalidate(); // limpia la session
			hs=req.getSession(true);
		} 

		/***Deprecation (Funciona)*******/
		// hs.putValue(Object,valor);
			//Object -->"nombre" 
			//String-->"valor"
		//hs.putValue("txt1",sTxt1); //Grabar el dato en la sesi�n
		//hs.putValue("txt2",sTxt2);
		/************************/
		
		//setAttribute(Object,valor);
			//Object -->"nombre" 
			//String-->"valor"
		
		//Grabar el dato en la sesi�n
		hs.setAttribute("txt1",sTxt1); 
		hs.setAttribute("txt2",sTxt2);

		// llama a la pagina html sin datos
		resp.sendRedirect("sesiones2.html");
		
	} // doGet
}