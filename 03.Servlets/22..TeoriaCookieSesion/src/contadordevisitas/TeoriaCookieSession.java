package contadordevisitas;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class TeoriaCookieSession extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private String mensaje="";
	private String salida="";
	private Cookie miGalleta=null;
	
	private int contador=0;
	
	public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request,response);
	}
	
	public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		
		/******Teoria***
			Las cookies por defecto son de sesi�n
			Para hacerlas permanentes 
			hay que asignarlas caducidad
		*/
		
		//devuelve un array con todas las cookies
		Cookie misGalletas[]=request.getCookies();
		
		//No existen galletas de mi servidor
		if(misGalletas==null){
			
			crearCookiePrimeraVez(response);
		}
		else{
			
			for(int i=0;i<misGalletas.length;i++){
				
				//cookie.getName()-->devuelve el nombre de la cookie
				if (misGalletas[i].getName().equals("visitas")){
					
					miGalleta=misGalletas[i];
					break;
				}
			}
			//No existe la galleta buscada galleta en mi servidor
			if(miGalleta==null){
				
				crearCookiePrimeraVez(response);
			}
			//Existe la galleta buscada en mi servidor
			else {
				
				//cookie.getValue()--> leemos el valor de la cookie
				mensaje=miGalleta.getValue();
				
				contador=Integer.parseInt(mensaje);
				
				salida="Esta es la " +String.valueOf(++contador)+" vez que vista la pagina";

				//setValue-->para asignar un nuevo valor a la cookie
				miGalleta.setValue(String.valueOf(contador));
				// se agrega la micookie a los cookies
				response.addCookie(miGalleta);
			}
		}
		
		response.setContentType("text/html");
		ServletOutputStream out=response.getOutputStream();
        /* Mando una p�gina html */
         out.println ("<HTML>\n" +
                "<HEAD><TITLE>Cookie</TITLE></HEAD>\n" +
                "<BODY>\n" +
                "<H1>" + salida + "</H1>\n" +
                "</BODY></HTML>");
		 out.close();
	}
	
	private void crearCookiePrimeraVez(HttpServletResponse response ){
		
		//bloque para crear la cookie
		Cookie miGalleta=new Cookie("visitas","1");
		
		 //Para que salga la primera vez por pantalla
		salida="Es la primera vez que "+ "visita esta pagina";
		
		// agrega o sobreescribe la galleta a los cookies
		response.addCookie(miGalleta);
	}

}
