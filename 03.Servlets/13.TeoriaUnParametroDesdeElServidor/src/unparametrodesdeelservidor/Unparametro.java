package unparametrodesdeelservidor;
import java.io.*;
import java.util.Enumeration;

import javax.servlet.*;
import javax.servlet.http.*;

public class Unparametro extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private String var_texto;
	
    public void init(ServletConfig sc)throws ServletException {
    	
    	//llamo al constructor(no es obligatorio 
    	//  pero si muy conveniente)
    	super.init(sc);
    	
    	//Para leer la etiqueta "<init-param>"
    	// del Web.xml conociendo su "<param-name>"
        var_texto=sc.getInitParameter("parametro1");
        
        String varNombre="";
        String varValue="";
        
    	//Para leer la etiqueta "<init-param>"
    	// del Web.xml SIN conocer su "<param-name>" (nombre)
        Enumeration<String> e=sc.getInitParameterNames();
        
        while(e.hasMoreElements()){
        	
        	varNombre= e.nextElement();
        	System.out.println("Nombre=" + varNombre);
        	 			       	 
        	varValue=sc.getInitParameter(varNombre); // accede al valor
        	System.out.println("Value=" + varValue);
        	
        }       
       
		if(var_texto==null){
			
			var_texto="fallo al inicializar";
		}
		
	} // init

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
    	doPost(request, response); //llamo al doPost
    } // doGet

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	//Conveniente, no obligatorio
    	response.setContentType("text/html"); 
		ServletOutputStream out = response.getOutputStream();
        out.println("<html>\n" + 
					"<head><title>Servlet con un parametro</title></head>\n" +
					"<body>\n<h1>" + var_texto + "</h1>\n" + 
					"</body></html>");
        out.close();//cierro la salida para mandarla
        
    } // doPost
}

