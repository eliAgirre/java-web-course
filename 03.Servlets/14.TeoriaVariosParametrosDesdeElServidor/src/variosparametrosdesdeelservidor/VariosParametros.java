package variosparametrosdesdeelservidor;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
public class VariosParametros extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private String var_texto="";
	
	public void init(ServletConfig sc) throws ServletException{
		
		//No es obligatorio pero si conveniente
		super.init(sc); // 
		
		Enumeration<String> e=sc.getInitParameterNames();
		
		while(e.hasMoreElements()){
			
			String nombre=e.nextElement(); // se guarda el nombre
			String valor=sc.getInitParameter(nombre); // obtiene el valor
			
			var_texto +=nombre + ": " + valor + "<br>";
		}
	} // init
	
	public void doPost(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException {
		 
		doGet(request,response);
	} // doPost
	
	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException {
		
		 response.setContentType("text/html"); 
		 /* Imprimo la salida */
		 ServletOutputStream out = response.getOutputStream();
    
		 /* Mando una p�gina html */
		 out.println("<html>\n" +
                "<head><title>Parametros</title></head>\n" +
                "<body>\n" +
                "<h1>" + var_texto + "</h1>\n" +
                "</body></html>");
                
		 /* Cierro la salida para mandarla */            
		 out.close();  
		 
	}  // doGet
}