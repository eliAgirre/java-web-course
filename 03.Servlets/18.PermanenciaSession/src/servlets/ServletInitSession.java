package servlets;

import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class  ServletInitSession extends HttpServlet {

	protected void doGet(HttpServletRequest req,HttpServletResponse resp) throws ServletException, IOException {
		
		// Se obtiene los datos desde form
		String sNombre=req.getParameter("nombre");
		String sApellido=req.getParameter("apellido");
		
		//req.getSession(boolean) para recuperar una sesion (Devuelve un HttpSession)
		//boolean=true (Para continuar con la sesion)
		//boolean=fale (Para terminar con la sesion)

		//HttpSession.isNew() 
		// Para saber si una sesion es nueva

		// con true se recupera, y el false es para el ultimo servlet a terminar
		HttpSession hs=req.getSession(true); 

		if (hs.isNew()){ // devuelve verdadero si es la 1era vez, sino false
			
			System.out.println("obtenida sesion");
		}
		else{
			
			hs.invalidate(); //Para borrar una sesion
			hs=req.getSession(true);
		}  
		
		// obtiene la ID de la session
		System.out.println("ID servlet1: "+hs.getId());
		
		// guarda en la session
		hs.setAttribute("sesion", hs.getId());
		
		/***Deprecation (Funciona)***
				Para grabar en una sesion
				 hs.putValue("String",Object);
					Object-->"nombre"
					String -->"valor" 
				hs.putValue("nombre",sNombre);
				hs.putValue("apellido",sApellido);
				hs.getValue("nombre");
		 */

		//Para grabar en una sesion (Sinonimo)
		hs.setAttribute("nombre",sNombre);
		hs.setAttribute("apellido",sApellido);

		// llama a la pagina de html sin datos
		resp.sendRedirect("pag1.html");
		
	} // doGet
}