package servlets;

import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class ServletSaludadorSession extends HttpServlet {

	public void doGet (HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		
		//se obtiene la sesi�n del usuario 
		HttpSession miSesion = req.getSession(false);
		
		System.out.println("ID servlet2: "+miSesion.getId());
		
		// se comprueba si la sesion ID son iguales
		if(miSesion.getAttribute("sesion").toString().equals(miSesion.getId())){
			System.out.println("iguales");
		}
		else{
			System.out.println("diferentes");
		}

		res.setContentType("text/html");

		PrintWriter out = res.getWriter();
		if (miSesion==null){
			
			//se ha perdido la conexion de la sesi�n user
			mostrarPaginaError(res.getWriter());
		}
		else{
			
			/**Deprecation (funciona)***
			getValue(Object) Para leer datos de una sesion

				mostrarPaginaOK(res.getWriter(), 
					(String)miSesion.getValue("nombre"),
						(String)miSesion.getValue("apellido"));
			 */
			mostrarPaginaOK(res.getWriter(),(String)miSesion.getAttribute("nombre"),(String)miSesion.getAttribute("apellido"));
		}
		out.close();		
		
	} // doGet

	public void doPost (HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		doGet(req, res);
		
	} // doPost

	public String getServletInfo() {
		
		// se ejecuta el primero siempre
		
		return "Un servlet ejemplo de permanecia de datos de un usuario";
		
	} // getServletInfo

	public void mostrarPaginaError(PrintWriter out){
		
		out.println("<html>");
		out.println("	<head><title>Servlet Saludador</title></head>");
		out.println("	<body bgcolor='#808080'>");	
		out.println("		<center>");
		out.println("			<font face='Arial' color='#FFFF00'> ");	
		out.println("				Su usuario ha caducado, por favor vuelva a autenticarse!!<br><br>");						
		out.println("			</font> ");	
		out.println("			<form>");
		out.println("				<input type='button' value='Inicio'");
		out.println("					onClick=document.location='pag0.html'>");
		out.println("			</form>");		
		out.println("		<center>");
		out.println("	</body>");
		out.println("</html>");
		
	} // mostrarPaginaError

	public void mostrarPaginaOK(PrintWriter out, String sNombre,String sApellido){
		
		out.println("<html>");
		out.println("	<head><title>Servlet Saludador</title></head>");
		out.println("	<body bgcolor='#808080'>");	
		out.println("		<center>");
		out.println("			<font face='Arial' color='#FFFF00'> ");	
		out.println("				Usuario: <b>" + sNombre + "</b>");
		out.println("				Apellido: <b>" + sApellido + "</b><br><br>");
		out.println("				Que tenga un buen d�a<BR><BR>");
		out.println("			</font> ");			
		out.println("			<form>");
		out.println("				<input type='button' value='Inicio'");
		out.println("	    			 onClick=document.location.href='pag0.html'>");
		out.println("			</form>");	
		out.println("		</center>");	
		out.println("	</body>");
		out.println("</html>");		
		
	} // mostrarPaginaOK
}