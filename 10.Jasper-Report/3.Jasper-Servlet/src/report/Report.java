package report;

import modelo.Personal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.j2ee.servlets.ImageServlet;

public class Report extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private JRBeanCollectionDataSource dataSource;
	private JasperReport jasperReport;
    private JasperPrint jasperPrint;    
    private Map<String, Object> argumentos = new HashMap<String, Object>();
    private String ruta;
    private ServletOutputStream out;

   
    public Report(){}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try{
			
		   out=response.getOutputStream();
		   ruta=getServletContext().getRealPath("/WEB-INF/informes");
		   System.out.println(ruta);
		   
		   argumentos.put("P_SUBTITULO", "Curso de JasperReports");		  
		   argumentos.put("P_TITULO", "Listado con Map");		  
		   argumentos.put("LOGO_URL", ruta+"/"+"logo.jpg");	
		   
		   //1-Llenar el datasource con la informacion de la base de datos o estructura,     	
		   Collection<Personal> lista=populateData();    	
		   dataSource=new JRBeanCollectionDataSource(lista);	    	
    	
		   //2-Compilamos el archivo XML y lo cargamos en memoria
		   jasperReport=JasperCompileManager.compileReport(ruta+"/plantilla.jrxml");
        		
		   //3-Llenamos el reporte con la información (de la dataSource) y 
		   //parámetros necesarios para la consulta
		   jasperPrint = JasperFillManager.fillReport(jasperReport, argumentos, dataSource);
		   		   
		   if(request.getParameter("arg").equals("1"))procesoPdf(request,response);
		   else if(request.getParameter("arg").equals("2"))procesoHtml(request,response);
		   else if(request.getParameter("arg").equals("3"))descargaPdf(request,response);
		}
		
		catch (Exception e){
		      e.printStackTrace();
		}   	
	}
	
	protected void procesoPdf(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
	   
		try{	       
		  
		   response.setContentType("application/pdf");
		   
		   //Enviamos el Pdf
		   JRExporter exporter = new JRPdfExporter();
		   exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		   exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);	
		   
		   exporter.exportReport();
	   }
	   catch (Exception e){
		   
	      e.printStackTrace();
	   }
	}
	protected void procesoHtml(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
        
		/*
		 Problema:El reporte no carga algunas imagenes que 
		          usa Jasper para alinear el contenido.
		          Esto ocurre por que lo que hace Jasper es generar tags img 
		          src= "Ruta de la imagen", y las imagenes no se encuentran.
		          
		 Solucion: Registrar El Servlet ImageServlet (API de JasperReports).
		 		   en el web.xml

		*/
		
	   try{
		   
		   response.setContentType("text/html");
		   //Enviamos el Html			   
		 
		   JRHtmlExporter exporterHtml = new JRHtmlExporter();
		 
		   exporterHtml.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		 
		   exporterHtml.setParameter(JRExporterParameter.OUTPUT_STREAM, out);	
		   
		   exporterHtml.setParameter(JRHtmlExporterParameter.IMAGES_URI,request.getContextPath() + "/informes?image=");
		   
		   //Esto nos soluciona el problema de las imagenes transparentes 
		   //que crea jasper para alinear el contenido
		   exporterHtml.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN,Boolean.FALSE);
		   
		   request.getSession().setAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jasperPrint);
		  			   
		   exporterHtml.exportReport();
	   }
	   catch (Exception e){
		   
	      e.printStackTrace();
	   }  
  	        
	}
	
	protected void descargaPdf(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
	   
		try{
			
		   out = response.getOutputStream();
	       response.setHeader("Cache-Control", "no-cache");
	       response.setHeader("Pragma", "no-cache");
	       response.setDateHeader("Expires", 0);
		   response.setContentType("application/pdf");
		   
		   // Enviamos el Pdf
		   byte[] bites = JasperExportManager.exportReportToPdf(jasperPrint); // crea ficheros
		   response.setHeader("Content-disposition", "attachment; filename=plantilla.pdf");
		   response.setContentLength(bites.length);
		   out.write(bites);
		   out.close();
	   }
	   catch (Exception e){
	      e.printStackTrace();
	   }
	}	
		
	private Collection<Personal> populateData(){
		
	  Collection<Personal> list = new ArrayList<Personal>();

	  for(int a=0;a<100;a++){
		  
		  list.add(new Personal("Nombre "+a,"Apellido "+a,"Direccion "+a,   new Integer(a+10)));
	  }
	  return list;	 	
  }
}
