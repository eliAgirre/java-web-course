package report;

import java.util.HashMap;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;

/*
 Nota: Cambiar en el iReport: (Quitar la propiedad uuid en java)
  Menu Herramientas/Opciones/Boton iReport
  Pestana:General/Compatibiliad
  Elegir:JasperReports4.5.0
  
 Nota: Para los parametros que no sean String hay que escribir es atributo class
 Ej: 
 	<textFieldExpression class="java.util.Date">
 		<![CDATA[new java.util.Date()]]>
 	</textFieldExpression> 	
 	
 	<textFieldExpression class="java.lang.Integer">
 		<![CDATA[$F{edad}]]>
 	</textFieldExpression>
 	
  Un reporte basado en JasperReports consta de los siguiente elementos:
	�Title:
		Titulo del reporte. La informaci�n contenida en esta banda ser� mostrada una sola vez
		en la primera p�gina del reporte.
		
	�Page Header: 
		Encabezado de p�gina. La informaci�n contenido en esta banda ser� mostrada
		en cada p�gina del reporte.
		
	�Column Header: 
		Encabezado de columna. El uso m�s frecuente de esta banda es para
		desplegar los encabezados de las tablas de datos en los reportes tipo cuadr�culas.
		
	�Detail: 
		Detalle del reporte. En esta banda incluyen los campos que constituyen el detalle del
		reporte. En un reporte tipo cuadr�cula se colocan los campos de la tabla.
		
	�Column Footer: 
		Pie de columna. Se utiliza generalmente para totalizar el detalle de cada campo (columna).
		Tanto el encabezado de columna, como el pi� de columna solo ser�n
		mostrados cuando inicia el set de datos y cuando termina.
		Si se requiere que el encabezado de las columnas aparezca en todas las p�ginas, se deber�n
		colocar los encabezados en el elemento Page Header.
		
	�Page Footer: 
		Ser� mostrado en todas las p�ginas del reporte.
		
	�Sumary: 
		Resumen. Se muestra �nicamente en la �ltima p�gina del reporte.Generalmente es
		usado para consolidar datos
		mostrados en las otras partes del reporte
 
 */

public class Principal{
	
	public static void main(String[] args){
		
		String reportName="plantilla";
		
		JasperReport jasperReport;
		JasperPrint jasperPrint;
		
		try{
			
			//1-Compilamos el archivo XML y lo cargamos en memoria
			jasperReport=JasperCompileManager.compileReport(reportName+".jrxml");

			//2-Llenamos el reporte con la informaci�n y par�metros necesarios 
			//(En este caso nada)
			jasperPrint=JasperFillManager.fillReport(jasperReport, new HashMap(), new JREmptyDataSource());

			
			//*********Primera forma*************
			//3-Exportamos el reporte a pdf y html y lo guardamos en disco
			/*
			JasperExportManager.exportReportToPdfFile(
					jasperPrint, reportName+".pdf");
			
	        JasperExportManager.exportReportToHtmlFile(
	                jasperPrint, reportName+".html");
	        
	        JRExporter exporter = new JRPdfExporter();
	        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
	        exporter.setParameter(JRExporterParameter.OUTPUT_FILE, 
	        		new java.io.File(reportName + ".pdf"));
	        exporter.exportReport();
	        
	        //Fin Primera forma
	        */
	        
			//******* Segunda Forma************
	        JRExporter exporter=new JRPdfExporter();
	        
	        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
	        exporter.setParameter(JRExporterParameter.OUTPUT_FILE, new java.io.File(reportName+".pdf"));
	        exporter.exportReport();
			
			JRHtmlExporter exporterHtml=new JRHtmlExporter();
			
			exporterHtml.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);			 
			exporterHtml.setParameter(JRExporterParameter.OUTPUT_FILE, new java.io.File(reportName+".html"));
			exporterHtml.exportReport();
			
			//Fin Segunda Forma
			System.out.println("Report realizado!");
		}
		catch (JRException e){
			
			e.printStackTrace();
		}

	}
}
