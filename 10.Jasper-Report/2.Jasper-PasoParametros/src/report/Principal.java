package report;

import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;


public class Principal{
	
  public static void main(String[] args){

    try{
    	
    	//1. Se compila el reporte
    	JasperCompileManager.compileReportToFile("plantilla.jrxml");

    	//2. Se llena el reporte con la informacion necesaria
    	Map<String, Object> argumentos = new HashMap<String, Object>();
    	
    	argumentos.put("ARGUMENTO1", "Titulo de documento");
    	argumentos.put("ARGUMENTO2", "Cabecera 1");
    	argumentos.put("ARGUMENTO3", "Cabecera 2");
    	argumentos.put("ARGUMENTO_DETALLE", "Esto es el detalle");
    	argumentos.put("ARGUMENTO_DETALLE2", "Esto es una prueba");
    	

    	JasperPrint jasperPrint = JasperFillManager.fillReport("plantilla.jasper", argumentos, new JREmptyDataSource());

    	//3. Se exporta a PDF y Html
    	JasperExportManager.exportReportToPdfFile(jasperPrint, "plantilla.pdf");
    	
        JasperExportManager.exportReportToHtmlFile(jasperPrint, "plantilla.html");

    	System.out.println("Fin!");
    }
    catch (JRException e){
    	
      e.printStackTrace();
    }
  }
}
