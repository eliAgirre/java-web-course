<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><s:text name="pantalla.titulo"/></title>
</head>
<body>
	
	<!--formulario struts-->
	<s:form action="accionResultado">
		
		<s:combobox label="Seleccione una opcion" name="pantalla" list="#{'0':'19', '1':'21', '2':'23'}" />
		
		<s:submit key="login.botonsubmit" cssClass="clase1"/>
	
	</s:form>

</body>
</html>