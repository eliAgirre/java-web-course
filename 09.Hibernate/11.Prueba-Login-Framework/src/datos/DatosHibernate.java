package datos;


import utilidades.HibernateUtil;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import modelo.Usuario;

public class DatosHibernate {
	
	private SessionFactory sessionFactory=null;
	
	private void abrirSessionFactory(){

		 try { 
			  sessionFactory=HibernateUtil.getSessionFactory();
		 }
		 catch (HibernateException e){
			 
		     System.err.println("Ocurrió un error en la inicialización de la SessionFactory: " + e); 		     
		 } 
	} // abrirSessionFactory
	
	private void cerrarSessionFactory(){
		sessionFactory.close();
	} // cerrarSessionFactory

	public boolean grabarUno(Usuario usuario){
		
		abrirSessionFactory();
		
		boolean resultado = false;
		
		Session session=sessionFactory.openSession();
		Transaction ts=session.beginTransaction();
		
		try{
			
			session.save(usuario); 
			ts.commit();
			resultado=true;
						
		}
		catch(HibernateException he) {
			ts.rollback();
			resultado=false;
		}
		finally{
			session.close();			
		}
		
		return resultado;
		
	} // grabarUno
	
	public Usuario buscaUnoCodigo(int codigo) {
		
		abrirSessionFactory();

		Session session=sessionFactory.openSession();
		
		// Si no lo encuentra devuelve null
		Usuario persona=(Usuario) session.get(Usuario.class, codigo); 
 
		cerrarSessionFactory();
		
		return persona;       
	}	 
}
