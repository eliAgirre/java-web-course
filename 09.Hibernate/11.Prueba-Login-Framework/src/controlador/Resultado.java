package controlador;

import java.util.Map;

import negocio.OperarImp;

import org.springframework.web.context.WebApplicationContext;






//import negocio.OperarImp;
import utilidades.SpringUtil;
import modelo.Usuario;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class Resultado extends ActionSupport{
	
	private static final long serialVersionUID = 1L;
	
	private String username;
	private String password;
	private String dni;
	private String nombre;
	private String apellido;
	private int micro;
	private int iPantalla;
	private String pantalla;
	private String sMicro;
	@SuppressWarnings("unused")
	private String msg="";
	
	@SuppressWarnings({ "unchecked" })
	public String execute() {
		
		WebApplicationContext context=SpringUtil.getWebApplicationContext();
		
		Usuario usuario=(Usuario) context.getBean("usuarioID");
		
		Map<String,Usuario> session=(Map<String,Usuario>)ActionContext.getContext().getSession();
		
		OperarImp operar=(OperarImp)context.getBean("operarImpID");
		
	    // obtiene el tipo de micro desde la sesion
		Usuario usuarioSession=(Usuario)session.get("usuario");
		
		int iPantalla=Integer.parseInt(getPantalla());
		
		usuario.setiPantalla(iPantalla);
		usuarioSession.setiPantalla(iPantalla);
		
		int iTotal=operar.getTotal(usuarioSession);
		
		Map<String,Integer> sessionTotal=(Map<String,Integer>)ActionContext.getContext().getSession();
		
		sessionTotal.put("total", iTotal); // se guarda en la session el total
		
		int iMicro=usuarioSession.getMicro();
		
		sMicro=String.valueOf(iMicro);
		
		usuario.setUsername(usuarioSession.getUsername());
		usuario.setPassword(usuarioSession.getPassword());
		usuario.setDni(usuarioSession.getDni());
		usuario.setNombre(usuarioSession.getNombre());
		usuario.setApellido(usuarioSession.getApellido());
		
		if(operar.grabarUno(usuario)){
	
			msg="Registro grabado";
		}
		else{
			msg="Registro no grabado";
		}
		
		return "SUCCESS";
	}
	
	// getters
	public String getUsername() {
		return username;
	}
	public String getPassword() {
		return password;
	}
	public String getDni() {
		return dni;
	}
	public String getNombre() {
		return nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public int getMicro() {

		return micro;
	}
	public int getiPantalla() {
		return iPantalla;
	}

	public String getPantalla() {
		return pantalla;
	}

	// setters
	public void setPantalla(String pantalla) {
		this.pantalla = pantalla;
	}
	
	// otros metodos
	public String getNombreMicro() {
		
		if(sMicro.equals("0"))return "Intel";
		else return "Amd";
	}
	
	public String getSizePantalla(){
		
		String size="";
		
		if(pantalla.equals("0"))size="19'";
		else if(pantalla.equals("1"))size="21'";
		else if(pantalla.equals("2"))size="23'";
		
		return size;
	}
	
}