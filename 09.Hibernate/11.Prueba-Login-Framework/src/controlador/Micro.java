package controlador;

import java.util.List;
import java.util.Map;

import modelo.Usuario;

//import org.springframework.web.context.WebApplicationContext;

//import utilidades.SpringUtil;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class Micro extends ActionSupport{
	
	private static final long serialVersionUID = 1L;
	
	private String dni;
	private String nombre;
	private String apellido;
	private List<String> micro;
	
	@SuppressWarnings("unchecked")
	public String execute(){
		
		//WebApplicationContext context=SpringUtil.getWebApplicationContext();
		
		//Usuario usuario=(Usuario)context.getBean("usuarioID");
		
		Map<String,Usuario> session=(Map<String,Usuario>)ActionContext.getContext().getSession();
		
		// obtiene el tipo de micro desde la sesion
		Usuario usuario=(Usuario)session.get("usuario");
		
		String sMicro=getMicro().get(0); // se obtiene si es 0 o 1
		int iMicro=Integer.parseInt(sMicro);
		
		usuario.setDni(getDni());
		usuario.setNombre(getNombre());
		usuario.setApellido(getApellido());
		usuario.setMicro(iMicro);
		
		/*usuario.setDni(getDni());
		usuario.setNombre(getNombre());
		usuario.setApellido(getApellido());
		usuario.setMicro(iMicro);*/
		
		session.put("usuario", usuario); // se guarda en la sesion
		
		return "SUCCESS";
	}

	public String getDni() {
		return dni;
	}
	public String getNombre() {
		return nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public List<String> getMicro() {
		return micro;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public void setMicro(List<String> micro) {
		this.micro = micro;
	}
}
