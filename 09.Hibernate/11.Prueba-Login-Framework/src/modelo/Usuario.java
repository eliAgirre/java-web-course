package modelo;

import java.io.Serializable;

public class Usuario implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String username;
	private String password;
	private String dni;
	private String nombre;
	private String apellido;
	private int micro;
	private int iPantalla;
	
	public String getUsername() {
		return username;
	}
	public String getPassword() {
		return password;
	}
	public String getDni() {
		return dni;
	}
	public String getNombre() {
		return nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public int getMicro() {

		return this.micro;
	}
	
	
	public int getiPantalla() {
		return iPantalla;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public void setMicro(int micro) {
		
		this.micro=micro;
	}
	public void setiPantalla(int iPantalla) {
		this.iPantalla = iPantalla;
	}
	
	@Override
	public String toString() {
		return "Usuario => username: "+username+", password: "+password+", dni:"+dni+", nombre: "+nombre+", micro:"+micro+", pantalla:"+iPantalla;
	}
}
