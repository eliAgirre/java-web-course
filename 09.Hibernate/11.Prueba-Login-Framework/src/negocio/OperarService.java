package negocio;

import modelo.Usuario;

public interface OperarService {

	public boolean grabarUno(Usuario u);
	public Usuario buscarUno(int codigo);
	public int getTotal(Usuario u);
	
}