package negocio;

import datos.DatosHibernate;
import modelo.Usuario;

public class OperarImp implements OperarService {
	
	private DatosHibernate datosHibernate;
	private static final int PRECIOPANTALLA=75;
	
	public OperarImp(){}

	@Override
	public boolean grabarUno(Usuario u) {
		return datosHibernate.grabarUno(u);
	}

	@Override
	public Usuario buscarUno(int codigo) {
		
		Usuario usuario=datosHibernate.buscaUnoCodigo(codigo);
		
		return usuario;
	}
	
	@Override
	public int getTotal(Usuario u) {

		int iMicro=0;
		
		if(u.getMicro()==0) iMicro=100;
		else iMicro=50;
		
		return iMicro+getPreciopantalla();
	}
	
	public DatosHibernate getDatosHibernate() {
		return datosHibernate;
	}

	public void setDatosHibernate(DatosHibernate datosHibernate) {
		this.datosHibernate = datosHibernate;
	}

	public static int getPreciopantalla() {
		return PRECIOPANTALLA;
	}

}
