<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registro</title>
</head>
<body>

		<s:form action="accionRegistro">
		
		<s:textfield name="username" key="Introduce nombre" />
		<s:password name="password" key="Introduce password" />
		<s:textfield name="comentario" key="Introduce comentario" />
		
		<s:submit key="Aceptar" />
		<s:reset key="Borrar" cssStyle="background-color:white;"/>
	
	</s:form>
	
	<a href="<s:url action='inicio_fin_accion'/>">Volver</a>
	
	<%--Salida de errores--%>
	<s:fielderror>
		<s:param value="%{'error_user'}" />	
	</s:fielderror>
	
	<s:fielderror cssStyle="color:blue">
		<s:param>errorpassword</s:param>
	</s:fielderror>
	
	<s:fielderror cssStyle="color:purple">
		<s:param>errorcomentario</s:param>
	</s:fielderror>

</body>
</html>