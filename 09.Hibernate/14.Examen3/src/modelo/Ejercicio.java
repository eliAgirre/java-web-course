package modelo;

import java.io.Serializable;

public class Ejercicio implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String nombre;
	private String pass;
	private String comentario;
	
	public Ejercicio(){} // vacio
	
	public Ejercicio(String nombre, String pass, String comentario) {
		
		super();
		this.nombre = nombre;
		this.pass = pass;
		this.comentario = comentario;
	}

	// getters
	public String getNombre() {
		return nombre;
	}
	public String getPass() {
		return pass;
	}
	public String getComentario() {
		return comentario;
	}
	
	// setters
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	@Override
	public String toString() {
		return "Ejercicio => nombre: "+nombre+", pass: "+pass+", comentario: "+comentario;
	}
}
