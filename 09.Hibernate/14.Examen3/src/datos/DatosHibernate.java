package datos;

import utilidades.HibernateUtil;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import modelo.Ejercicio;

public class DatosHibernate {
	
	private SessionFactory sessionFactory=null;
	
	private void abrirSessionFactory(){

		 try { 
			  sessionFactory=HibernateUtil.getSessionFactory();
		 }
		 catch (HibernateException e){
			 
		     System.err.println("Ocurri� un error en la inicializaci�n de la SessionFactory: " + e); 		     
		 } 
	} // abrirSessionFactory
	
	private void cerrarSessionFactory(){
		sessionFactory.close();
	} // cerrarSessionFactory
	
	public boolean grabarUno(Ejercicio usuario){
		
		abrirSessionFactory();
		
		boolean resultado=false;
		
		Session session=sessionFactory.openSession();
		Transaction ts=session.beginTransaction();
		
		try{
			session.save(usuario);
			ts.commit(); 
			resultado=true;
		}
		catch(HibernateException he) {
			ts.rollback();
			resultado=false;
		}
		finally{
			session.close();			
		}
		
		return resultado;
		
	}
	
	public Ejercicio buscaUnoClave(String clave) {
	
		abrirSessionFactory();
	
		Session session=sessionFactory.openSession();
		
		Ejercicio usuario=null;
		
		Query query=session.createQuery("from Ejercicio e where e.nombre = :nombre");
		query.setParameter("nombre", clave);
		
		// si la lista contiene m�s de 1
		if(query.list().size()>0){
			
			// devuelve el primer usuario que encuentre
			usuario=(Ejercicio) query.uniqueResult();
		}
	
		cerrarSessionFactory();
		
		return usuario;       

	} // buscarUnoClave
	
	public Ejercicio buscaUnoPassword(String password) {
		
		abrirSessionFactory();
	
		Session session=sessionFactory.openSession();
		
		Ejercicio usuario=null;
		
		Query query=session.createQuery("from Ejercicio e where e.pass = :password");
		query.setParameter("password", password);
		
		// si la lista contiene m�s de 1
		if(query.list().size()>0){
			
			// devuelve el primer usuario que encuentre
			usuario=(Ejercicio) query.uniqueResult();
		}
	
		cerrarSessionFactory();
		
		return usuario;       

	} // buscaUnoPassword
		
	public Ejercicio buscaPorCampos(String clave,String password) {
		
		abrirSessionFactory();
	
		Session session=sessionFactory.openSession();
		
		Ejercicio usuario=null;
		
		Query query=session.createQuery("from Ejercicio e where e.nombre = :clave and e.pass = :password");
		query.setParameter("clave", clave);
		query.setParameter("password", password);
		
		// si la lista contiene m�s de 1
		if(query.list().size()>0){
			
			// devuelve el primer usuario que encuentre
			usuario=(Ejercicio) query.uniqueResult();
		}
	
		cerrarSessionFactory();
		
		return usuario;       

	} // buscaUnoPassword
}
