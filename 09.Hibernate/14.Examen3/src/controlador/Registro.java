package controlador;

import modelo.Ejercicio;
import negocio.OperarImp;

import org.springframework.web.context.WebApplicationContext;

import utilidades.SpringUtil;

//import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class Registro extends ActionSupport{
	
	private static final long serialVersionUID = 1L;
	
	private String username;
	private String password;
	private String comentario;
	
	private String resultado;
	
	private Ejercicio usuario;
	private Ejercicio usuario2;
	private Ejercicio usuario3;
	
	// business logic
	public String execute() {
		
		WebApplicationContext context=SpringUtil.getWebApplicationContext();
		
		OperarImp operar=(OperarImp) context.getBean("operarImpID");
		usuario = (Ejercicio) context.getBean("ejerciciofinalID");
		usuario2 = (Ejercicio) context.getBean("ejerciciofinalID");
		usuario3 = (Ejercicio) context.getBean("ejerciciofinalID");
		//usuario=operar.buscarUno(getUsername());
		usuario=operar.buscaPorCampos(getUsername(), getPassword());
		
		if(usuario==null){ // si no existe el nombre
			
			// se busca por password
			usuario2=operar.buscaUnoPassword(getPassword());
			
			if(usuario2==null){ // si no existe el password
								
				usuario3.setNombre(getUsername());
				usuario3.setPass(getPassword());
				usuario3.setComentario(getComentario());
				
				// se graba el usuario
				if(operar.grabarUno(usuario3)){
					
					resultado="Usuario registrado";
					return "SUCCESS_BIEN";
				}
				else{
					resultado="Usuario no registrado";
					return "SUCCESS_MAL";
				}
			}
			else{
				
				if(getUsername().equalsIgnoreCase(usuario3.getNombre()) && getPassword().equalsIgnoreCase(usuario3.getPass())){
					
					resultado="El usuario y el password ya existe";
					return "SUCCESS_MAL";
				}
			}
			
		}
		usuario3.setNombre(getUsername());
		usuario3.setPass(getPassword());
		usuario3.setComentario(getComentario());
		
		// se graba el usuario
		if(operar.grabarUno(usuario3)){
			
			resultado="Usuario registrado";
			return "SUCCESS_BIEN";
		}
		else{
			resultado="Usuario no registrado";
			return "SUCCESS_MAL";
		}
	}
	
	// simple validation
	public void validate(){
		
		if(getUsername().length()==0){
			addFieldError("error_user", getText("login.user.requerido"));
		}
		if(getPassword().length()==0){
			addFieldError("errorpassword", getText("login.password.requerido"));
		}
		if(getComentario().length()==0){
			addFieldError("errorcomentario", getText("signin.comentario.requerido"));
		}
		
	}
	
	
	// getters
	public String getUsername() {
		return username;
	}
	public String getPassword() {
		return password;
	}
	public String getComentario() {
		return comentario;
	}
	public String getResultado() {
		return resultado;
	}

	// setters
	public void setUsername(String username) {
		this.username = username;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	
}