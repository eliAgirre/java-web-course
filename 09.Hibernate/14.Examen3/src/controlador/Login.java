package controlador;

import java.util.Map;

import negocio.OperarImp;

import org.springframework.web.context.WebApplicationContext;

import utilidades.SpringUtil;
import modelo.Ejercicio;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class Login extends ActionSupport{
	
	private static final long serialVersionUID = 1L;
	
	private String username;
	private String password;
	
	private String resultado;	
	
	// business logic
	@SuppressWarnings("unchecked")
	public String execute() {
		
		WebApplicationContext context=SpringUtil.getWebApplicationContext();
		
		Map<String,String> session=(Map<String,String>)ActionContext.getContext().getSession();
		
		OperarImp operar=(OperarImp) context.getBean("operarImpID");
		Ejercicio usuario=(Ejercicio) context.getBean("ejerciciofinalID");
		//usuario=operar.buscarUno(getUsername());
		usuario=operar.buscaPorCampos(getUsername(), getPassword());
		
		if(usuario==null){
			
			resultado="No existe ese usuario y contraseņa.";
			return "SUCCESS_MAL";	
		}
		else{
			
			if(getUsername().equals(usuario.getNombre()) && getPassword().equals(usuario.getPass())){
				
				session.put("comentario", usuario.getComentario());
				resultado="Comentario: "+session.get("comentario");
				return "SUCCESS_BIEN";		
			}
			else{
				resultado="Usuario no autorizado";
				return "SUCCESS_MAL";
			}
			
		}
	}

	// simple validation
	public void validate(){
		
		if(getUsername().length()==0){
			addFieldError("error_user", getText("login.user.requerido"));
		}
		if(getPassword().length()==0){
			addFieldError("errorpassword", getText("login.password.requerido"));
		}
		
	}

	// getters
	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getResultado() {
		return resultado;
	}

	// setters
	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}