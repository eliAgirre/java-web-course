package negocio;

import modelo.Ejercicio;

public interface OperarService {

	public boolean grabarUno(Ejercicio u);
	public Ejercicio buscarUno(String clave);
	public Ejercicio buscaUnoPassword(String password);
	public Ejercicio buscaPorCampos(String clave,String password);
	
}