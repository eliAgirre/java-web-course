package negocio;

import datos.DatosHibernate;
import modelo.Ejercicio;

public class OperarImp implements OperarService {
	
	private DatosHibernate datosHibernate;
	
	public DatosHibernate getDatosHibernate() {
		return datosHibernate;
	}

	public void setDatosHibernate(DatosHibernate datosHibernate) {
		this.datosHibernate = datosHibernate;
	}
	
	public OperarImp(){}

	@Override
	public boolean grabarUno(Ejercicio u) {
		return datosHibernate.grabarUno(u);
	}

	@Override
	public Ejercicio buscarUno(String clave) {
		
		Ejercicio usuario=datosHibernate.buscaUnoClave(clave);
		
		return usuario;
	}

	@Override
	public Ejercicio buscaUnoPassword(String password) {
		
		Ejercicio usuario=datosHibernate.buscaUnoPassword(password);
		
		return usuario;
	}

	@Override
	public Ejercicio buscaPorCampos(String clave, String password) {
		
		Ejercicio usuario=datosHibernate.buscaPorCampos(clave, password);
		
		return usuario;
	}
}