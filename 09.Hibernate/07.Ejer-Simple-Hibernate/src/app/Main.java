package app;

import java.util.Iterator;
import java.util.List;

import modelo.Cliente;
import datos.Operar;

public class Main {
	
	public static void main(String[] args) {
		
		Operar operar=new Operar();
		
		operar.abrirSessionFactory();

		Cliente cliente1=new Cliente();
		Cliente cliente2=new Cliente();
		Cliente cliente3=new Cliente();
		
		cliente1.setCodigo(1);
		cliente1.setNombre("nombre1");
		cliente1.setApellido("apellido1");
		cliente1.setDni("1");
		
		if(operar.grabarUno(cliente1))System.out.println("Registro grabado con codigo "+cliente1.getCodigo());
		else System.out.println("No se puede grabar registro");
		
		cliente2.setCodigo(2);
		cliente2.setNombre("nombre2");
		cliente2.setApellido("apellido2");
		cliente2.setDni("2");
	
		if(operar.grabarUno(cliente2))System.out.println("Registro grabado con codigo "+cliente2.getCodigo());
		else System.out.println("No se puede grabar registro");
		
		cliente3.setCodigo(3);
		cliente3.setNombre("nombre3");
		cliente3.setApellido("apellido3");
		cliente3.setDni("3");
	
		if(operar.grabarUno(cliente3))System.out.println("Registro grabado con codigo "+cliente3.getCodigo());
		else System.out.println("No se puede grabar registro");
		
		System.out.println(" ");
		System.out.println("Lista de clientes: ");
		
		List<Cliente> listaClientes=operar.listaClientes(); // hibernate devuelve un list
		
		Iterator<Cliente> it=listaClientes.iterator();
		
		while (it.hasNext()){
			
			Cliente c=it.next();
			System.out.println(c);
		}
		
		cliente2=operar.buscarUno(cliente2.getCodigo());
		int iCodigo=cliente2.getCodigo();
		cliente2.setNombre("eli");
		if(operar.actualizaUno(cliente2)){
			
			System.out.println(" ");
			System.out.println("Cliente modificado: ");
			cliente2=operar.buscarUno(cliente2.getCodigo());
			
			System.out.println("Codigo: "+iCodigo);
			System.out.println("Nombre: "+cliente2.getNombre());
			System.out.println("Apellido: "+cliente2.getApellido());
			System.out.println("Dni: "+cliente2.getDni());
			
		}
		else{
			System.out.println("El cliente con el codigo "+iCodigo+" no ha sido borrado");
		}
		
		cliente3=operar.buscarUno(cliente3.getCodigo());
		iCodigo=cliente3.getCodigo();
		if(operar.eliminaUno(cliente3)){
			System.out.println(" ");
			System.out.println("Cliente borrado con el codigo: "+iCodigo);
		}
		else{
			System.out.println("El cliente con el codigo "+iCodigo+" no ha sido borrado");
		}
		
		operar.cerrarSessionFactory();
	}

}