package modelo;

public class Cliente {
	
	private int codigo;
	private String nombre;
	private String apellido;
	private String dni;
	
	public Cliente(){}
	
	public Cliente(int codigo, String nombre, String apellido, String dni) {
		
		this.codigo = codigo;
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
	}

	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	
	
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}

	@Override
	public String toString() {
		return "Cliente => codigo:"+codigo+", nombre: "+nombre+", apellido: "+apellido+", dni:"+dni;
	}

}
