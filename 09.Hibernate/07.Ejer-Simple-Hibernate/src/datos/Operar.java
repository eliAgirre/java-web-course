package datos;

import java.util.List;

import modelo.Cliente;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Operar {
	
	private SessionFactory sessionFactory=null; // SessionFactory -> pull de conexiones
	
	public void abrirSessionFactory(){
		
		//La clase org.hibernate.SessionFactory es el objeto que permitirá
		//crear sesiones de Hibernate hacia la base de datos. 
		 try {
			 
			  sessionFactory=new Configuration().configure().buildSessionFactory();
		 }
		 catch (HibernateException e) {
		     System.err.println("Ocurrió un error en la inicialización de la SessionFactory: "+e); 		     
		 } 
	}
	
	public void cerrarSessionFactory(){
		
		sessionFactory.close();
	}
	
	public boolean grabarUno(Cliente cliente){
		
		boolean resultado=false;
		
		Session session=sessionFactory.openSession();
		Transaction cambio=session.beginTransaction();
		
		try{
			
			session.save(cliente); // Se guarda el registro
			cambio.commit(); // acepta la transaccion
			resultado=true;
						
		}
		catch(HibernateException he) { // Existe el registro
			
			cambio.rollback();
			resultado=false;
		}
		finally{
			
			session.close();	
		}
		return resultado;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Cliente> listaClientes(){
		
		Session session=sessionFactory.openSession();
			
		// HQL
		List<Cliente> clientes=session.createQuery("from Cliente c").list();
		
		session.close();
		return clientes;				
	}
	
	public boolean actualizaUno(Cliente cliente) {
	   	
		Session session=sessionFactory.openSession();
		Transaction cambio=session.beginTransaction();
		
		boolean sw=false;
		
		try {
			
			session.update(cliente);  // actualiza los datos del bean
	        cambio.commit(); 
	        sw=true;
	        
	    }
	    catch (HibernateException he) {
	    	
	    	cambio.rollback();
	    }
	    finally {
	    	
	    	session.close(); 
	    } 
		return sw;
	}
	
	public Cliente buscarUno(int codigo) {
		
		Session session=sessionFactory.openSession(); 		

		 // Si no lo encuentra el metodo get devuelve persona=null, SOLO SIRVE PARA PK
		Cliente cliente=(Cliente) session.get(Cliente.class, codigo); 

		session.close(); 
		
		return cliente;       
	}
	
	public boolean eliminaUno(Cliente cliente)  {
		
		Session session=sessionFactory.openSession();
		Transaction cambio=session.beginTransaction();
		
		boolean sw=false;
		
		try {
			
            session.delete(cliente); // borra el registro
            cambio.commit(); 
            sw=true;
        } 
		catch (HibernateException he) {
			
			cambio.rollback();
        } 
		finally {
			
            session.close();
        }
		return sw;
    }
	
}
