package app;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import datos.Operar;

import modelo.Libro;
import modelo.Persona;

public class App 
{

	public static void main(String[] args) 
	{
		Operar operar=new Operar();		
		
		operar.abrirSessionFactory();	
		/*		 
		En la tabla libro se crea la columna UNION_ID_PERSONA (FK)
		con el mismo valor de la columna ID_PERSONA de la tabla persona (PK)		 
		*/
		Libro libro1 = new Libro(); 		
		libro1.setTitulo("Titulo 1");
		libro1.setId(11);
		
		Libro libro2=new Libro (22,"Titulo2");
		
		Persona persona1 = new Persona();
		persona1.setNombre("Juan");
		persona1.setDni("1111");
		persona1.setProvincia("sevilla");
		persona1.setId(111);
		
		persona1.addLibro(libro1);
		persona1.addLibro(libro2);

	    //grabarUno
		if(operar.grabarUno(persona1))
			System.out.println("Registro grabado");
		else System.out.println("No se puede grabar registro");			


		//grabarVarios
		Vector<Persona> personas=new Vector<Persona>();		
		
		Libro libro3=new Libro (33,"Titulo3");
		Libro libro4=new Libro (44,"Titulo4");
		Libro libro5=new Libro (55,"Titulo5"); 
		Libro libro6=new Libro (66,"Titulo6");
		Libro libro7=new Libro (77,"Titulo7");
		Libro libro8=new Libro (88,"Titulo8");
		Libro libro9=new Libro (99,"Titulo9");
		
		List<Libro> librosPersona2=new  ArrayList<Libro>();
		librosPersona2.add(libro3);
		librosPersona2.add(libro4);
		librosPersona2.add(libro5);
		
		List<Libro> librosPersona3=new  ArrayList<Libro>();
		librosPersona3.add(libro6);
		librosPersona3.add(libro7);
		
		List<Libro> librosPersona4=new  ArrayList<Libro>();
		librosPersona4.add(libro8);
		librosPersona4.add(libro9);
				
	
		personas.add(new Persona(222,"Pedro","2222", "sevilla",  librosPersona2));	
		personas.add(new Persona(333,"Luis", "3333" ,"madrid",   librosPersona3));
		personas.add(new Persona(444,"Ana",  "4444" ,"madrid",   librosPersona4));
		
		System.out.println("Numero de registros grabados=" + 
				operar.grabarVarios(personas));   	   
		
		//actualizaUno 
		//Modificamos persona1,libro1 y los actualizamos 
		persona1.setNombre("Enrique");
		persona1.setDni("100");
		libro1.setTitulo("Titulo 100");
							
		if(operar.actualizaUno(persona1))System.out.println("Registros modificados"
				+ persona1.toString());		
		else System.out.println("No se puede modificar el registro");
		
		//Borramos a persona1
		if(operar.eliminaUno(persona1))System.out.println("Registro borrado");				
		else System.out.println("No se puede borrar el registro");
		
		//eliminaMuchos	
		int numeroRegistrosBorrados=operar.eliminaMuchos("madrid");
		System.out.println("Numero de registros borrados=" + numeroRegistrosBorrados );
		
		
		//buscaTodos
		List<Persona> personas1=null;
		personas1=operar.buscaTodos();
		Iterator<Persona> it = personas1.iterator();		
		
		while (it.hasNext())
		{
			Persona p = it.next();		
			
			System.out.println("******Persona***************");
			System.out.println("Codigo Persona=" + p.getId());
			System.out.println("Dni=" + p.getDni());
			System.out.println("Nombre=" + p.getNombre());
			System.out.println("Provincia=" + p.getProvincia());		
			
			List<Libro> libros =new ArrayList<Libro>(); 		
					
			libros=p.getLibros();
			System.out.println("   Libros   "); 
			for(int a=0;a<libros.size();a++)
			{
				System.out.println("     Codigo libro=" +libros.get(a).getId()); 
				System.out.println("     Titulo=" +libros.get(a).getTitulo());				
			}		
		}
		
		//buscaUnoCodigo
		long codigoBuscar=222;
		Persona persona = operar.buscaUnoCodigo(codigoBuscar);
		if(persona==null)System.out.println("No existe el codigo "+ codigoBuscar);
		else System.out.println("Encontrado***" + persona.toString());
		
		//buscaVariosCodigos
		long codInicio=100;
		long codFinal=300;
		List<Persona> per=operar.buscaVariosCodigos(codInicio,codFinal);
		
		if(per.size()==0)System.out.println("No existen registros");
		else
		{
			Iterator<Persona> it2 = per.iterator();
			while (it2.hasNext())
			{
				Persona p = it2.next();
				System.out.println(p);
			}
		}
		
		//buscaUnoNombre	
		Libro libro10=new Libro (100,"Titulo100");
		Libro libro11=new Libro (101,"Titulo101");
		Libro libro12=new Libro (102,"Titulo102");
		
		List<Libro> librosPersona100=new  ArrayList<Libro>();
		librosPersona100.add(libro10);
		librosPersona100.add(libro11);
		librosPersona100.add(libro12);
		
		Persona persona100=new Persona(1000,"Tomas","100" ,"madrid",librosPersona100);
		
		operar.grabarUno(persona100);
		Persona p2=null;
		p2= operar.buscaUnoNombre("Tomas");
		
		if(p2!=null)System.out.println("Encontrado***" + p2.toString());
		else System.out.println("No existe nombre"); 		

	    operar.cerrarSessionFactory();
	  
	}
}
