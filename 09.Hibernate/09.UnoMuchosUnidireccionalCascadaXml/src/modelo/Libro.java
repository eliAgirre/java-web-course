package modelo;

import java.io.Serializable;

public class Libro implements Serializable  
{ 
	private static final long serialVersionUID = 1L;
	
	private long id; 
    private String titulo;

    
    public Libro(long id, String titulo) {
		super();
		this.id = id;
		this.titulo = titulo;
	}

	public Libro() 
    { 
    }  

    public long getId() 
    { 
        return id; 
    }  

    public void setId(long id) 
    { 
        this.id = id; 
    }  
    
    public String getTitulo() 
    { 
        return titulo; 
    }  

    public void setTitulo(String titulo) 
    { 
        this.titulo = titulo; 
    }

	@Override
	public String toString() {
		return "Libro [id=" + id + ", titulo=" + titulo.trim() + "]";
	} 
    
}


