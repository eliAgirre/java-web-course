package modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Persona implements Serializable
{ 
	private static final long serialVersionUID = 1L;
	
	private long id; 
    private String nombre; 
    private List<Libro> libros = new ArrayList<Libro>();  
    
    private String dni;
	private String provincia;

    public Persona() 
    { 
    }  

    public Persona(long id, String nombre,String dni,String provincia,List<Libro> libros) 
    {
		super();
		this.id = id;
		this.nombre = nombre;
		this.dni=dni;
		this.provincia=provincia;
		this.libros = libros;
	}
    
    //Agregar un libro a la lista
    public void addLibro(Libro libro) 
    { 
        this.libros.add(libro); 
    }
   
    public void borraLibro(Libro libro)
    {
    	this.libros.remove(libro);
    }

	public long getId() 
    { 
        return id; 
    }  

    public void setId(long id) 
    { 
        this.id = id; 
    }  

    public String getNombre() 
    { 
        return nombre; 
    }  

    public void setNombre(String nombre) 
    { 
        this.nombre = nombre; 
    }  

    public List<Libro> getLibros() 
    { 
        return libros; 
    }  

    public void setLibros(List<Libro> libros) 
    { 
        this.libros = libros; 
    }  

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	@Override
	public String toString() {
		return "Persona [id=" + id + ", nombre=" + nombre.trim() + ", libros="
				+ libros + "]";
	} 
    
}

