package app;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import datos.Operar;

import modelo.Persona;;

public class App 
{

	public static void main(String[] args) 
	{
		Operar operar=new Operar();
		Persona persona=null;
		long codigoBuscar=2;
		
		operar.abrirSessionFactory();
		
		Persona p1=new Persona(66,"Juan","66","madrid");
				
		//grabarUno
		if(operar.grabarUno(p1))
			System.out.println("Registro grabado");
		else System.out.println("No se puede grabar registro");
		
		//grabarVarios
		Vector<Persona> personas=new Vector<Persona>();
		
		personas.add((new Persona(11,"Pedro",   "11",  "sevilla")));
		personas.add((new Persona(22,"Ana",     "22",  "madrid")));
		personas.add((new Persona(33,"Maria" ,  "33",  "sevilla")));
		personas.add((new Persona(44,"Antonio", "44",  "sevilla")));
		personas.add((new Persona(55,"Javier" , "55",  "madrid")));
				
		System.out.println("Numero de registros grabados=" + operar.grabarVarios(personas));
		
		//actualizaUno
		//Modificamos p1 y lo actualizamos 
		p1.setDni("100");
		p1.setNombre("Enrique");
		if(operar.actualizaUno(p1))System.out.println("Registro modificado"
				+ p1.toString());		
		else System.out.println("No se puede modificar el registro");
		
		//eliminaUno
		//Borramos a p1
		if(operar.eliminaUno(p1))System.out.println("Registro borrado");				
		else System.out.println("No se puede borrar el registro");
		
		//eliminaMuchos	
		int numeroRegistrosBorrados=operar.eliminaMuchos("sevilla");
		System.out.println("Numero de registros borrados=" + numeroRegistrosBorrados );
		
		
		//buscaTodos
		List<Persona> personas1=operar.buscaTodos();
		Iterator<Persona> it = personas1.iterator();
		
		while (it.hasNext())
		{
			Persona p = it.next();
			System.out.println(p);
		}
		
		//buscaUnoCodigo
		persona = operar.buscaUnoCodigo(codigoBuscar);
		if(persona==null)System.out.println("No existe el codigo "+ codigoBuscar);
		else System.out.println("Encontrado***" + persona.toString());		
		
		//buscaVariosCodigos
		long codInicio=2;
		long codFinal=10;
		List<Persona> per=operar.buscaVariosCodigos(codInicio,codFinal);
		Iterator<Persona> it2 = per.iterator();		
		
		while (it2.hasNext())
		{
			Persona p = it2.next();
			System.out.println(p);
		}
		
		//buscaUnoNombre		
		operar.grabarUno(new Persona(200,"Tomas","200","Jaen"));
		Persona p2=null;
		p2= operar.buscaUnoNombre("Tomas");
		
		if(p2!=null)System.out.println("Encontrado***" + p2.toString());
		else System.out.println("No existe nombre");
		
		operar.cerrarSessionFactory();		
		
	}
	
}

