package modelo;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;

@Entity
@Table(name="Personas")
public class Persona implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	//@GeneratedValue(strategy=GenerationType.AUTO) Autoincremento	
	private long codigo;
	
	//@Column(name="otronombre") //Si queremos renombrar la fila
	@Column(name="nombre",length=30)
	private String nombre;
	
	@Column(length=30,unique=true)
	private String dni;
	
	@Column(length=30)
	private String provincia;
	

	/* Es obligatorio 
	 * (puede ser privado si es que no 
	 * quieren permitir que alguien m�s lo utilice*/
	@SuppressWarnings("unused")
	private Persona() {}

	@Override
	public String toString() {
		return "Persona [codigo=" + codigo + ", nombre=" + nombre.trim() + ", dni="
				+ dni.trim() + ", provincia=" + provincia.trim() + "]";
	}

	public Persona(long codigo,String nombre, String dni,String provincia) {
		super();
		this.codigo=codigo;
		this.nombre = nombre;
		this.dni = dni;
		this.provincia=provincia;
	}

	public long getCodigo() {
		return codigo;
	}

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

}
