package modelo;

import java.io.Serializable; //no es obligatorio pero es una buena pr�ctica

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import javax.persistence.Id;
import javax.persistence.CascadeType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;


@Entity
@Table(name="persona")
public class Persona implements Serializable 
{	
	private static final long serialVersionUID = 1L;

	
	@Id //Obligatorio un identificador
	
	private long codigo;
	
	@Column(name="nombre",length=30)
	private String nombre;
	
	@Column(length=30,unique=true)
	private String dni;
	
	@Column(length=30)
	private String provincia;
	
	//@OneToOne //(Sin cascada)
	
	//cascade="persist,save-update,delete" fetch="select"/>  
	@OneToOne(cascade={CascadeType.ALL})
		
	//Relacion por clave primaria y externa al mismo tiempo.
	@PrimaryKeyJoinColumn //Opcional

	/*
	 fetch="select" (Por defecto en XML, obligatorio en anotaciones) 
	 Implica que por cada objeto de la tabla direccion que se 
	 quiera recuperar se lanzar� una nueva consulta a la base de datos.
     */
	
	@Fetch(FetchMode.SELECT)
	private Direccion direccion;
	

	/* Es obligatorio 
	 * (puede ser privado si es que no 
	 * quieren permitir que alguien m�s lo utilice*/
	public Persona() {}

	
    @Override
	public String toString() {
		return "Persona [codigo=" + codigo + ", nombre=" + nombre.trim() + ", dni="
				+ dni.trim() + ", provincia=" + provincia.trim() +
				", direccion=" + direccion + "]";
	}

	public Direccion getDireccion()
    {
        return direccion;
    }

    public void setDireccion(Direccion direccion)
    {
        this.direccion = direccion;
    }
	
	public Persona(long codigo,String nombre, String dni, String provincia,
			Direccion direccion) 
	{
		super();	
		this.codigo=codigo;
		this.nombre = nombre;
		this.dni = dni;
		this.provincia = provincia;
		this.direccion = direccion;
	}

	public long getCodigo() {
		return codigo;
	}

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
}
