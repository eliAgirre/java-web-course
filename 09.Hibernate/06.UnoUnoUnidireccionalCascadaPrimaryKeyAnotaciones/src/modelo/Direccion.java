package modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="direccion")
public class Direccion implements Serializable
{

	private static final long serialVersionUID = 1L;
	
	@Id //Obligatorio un identificador
	//@GeneratedValue(strategy=GenerationType.AUTO) Autoincremento
	private long id;
	
	//@Column(name="otronombre") //Si queremos renombrar la fila
	@Column(name="calle",length=30)
    private String calle;
	
	@Column(length=5)
    private String codigoPostal;

	public Direccion()
    {
    }
        
    public Direccion(long id,String calle, String codigoPostal) {
		super();
		this.id=id;
		this.calle = calle;
		this.codigoPostal = codigoPostal;
	}
    
    @Override
	public String toString() {
		return "Direccion [id=" + id + ", calle=" + calle.trim() + ", codigoPostal="
				+ codigoPostal.trim() + "]";
	}

	public long getId()
    {
        return id;
    }
	
    public void setId(long id)
    {
        this.id = id;
    }
    
    public String getCalle()
    {
        return calle;
    }

    public void setCalle(String calle)
    {
        this.calle = calle;
    }

    public String getCodigoPostal()
    {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal)
    {
        this.codigoPostal = codigoPostal;
    }
}
