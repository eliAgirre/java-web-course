package app;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import datos.Operar;

import modelo.Direccion;
import modelo.Persona;

public class App 
{

	public static void main(String[] args) 
	{
		Operar operar=new Operar();		
		
		operar.abrirSessionFactory();	

		
		Persona persona1 = new Persona(); 
		
		persona1.setCodigo(11);		
		persona1.setDni("11");
	    persona1.setNombre("Ana");  	   
	    persona1.setProvincia("Madrid");
	    
	    Direccion direccion1 = new Direccion(); 
	    direccion1.setId(persona1.getCodigo());
	    direccion1.setCalle("Calle 11"); 
	    direccion1.setCodigoPostal("11"); 
	    
	    persona1.setDireccion(direccion1);
	    
	    //grabarUno
		if(operar.grabarUno(persona1))
			System.out.println("Registro grabado");
		else System.out.println("No se puede grabar registro");		

		//grabarVarios
		Vector<Persona> personas=new Vector<Persona>();
		
		personas.add(new Persona(22,"Pedro",   "22",  "sevilla" ,new Direccion(22,"Calle 22","22")));	
		personas.add(new Persona(33,"Luis",    "33",  "madrid"  ,new Direccion(33,"Calle 33","33")));
		personas.add(new Persona(44,"Maria" ,  "44",  "sevilla", new Direccion(44,"Calle 44","44")));
		personas.add(new Persona(55,"Antonio", "55",  "sevilla" ,new Direccion(55,"Calle 55","55")));
		personas.add(new Persona(66,"Javier"  ,"66",  "madrid"  ,new Direccion(66,"Calle 66","66")));

		System.out.println("Numero de registros grabados=" + 
				operar.grabarVarios(personas));   	   
	  
		//actualizaUno (Actualizacion en cascada)
		//Modificamos persona1 y lo actualizamos 
		persona1.setDni("100");
		persona1.setNombre("Enrique");
		direccion1.setCalle("Calle 100");
				
		if(operar.actualizaUno(persona1))System.out.println("Registro modificado"
				+ persona1.toString());		
		else System.out.println("No se puede modificar el registro");
		
		//eliminaUno (Eliminacion en cascada)
		//Borramos a persona1
		if(operar.eliminaUno(persona1))System.out.println("Registro borrado");				
		else System.out.println("No se puede borrar el registro");

		//eliminaMuchos	
		int numeroRegistrosBorrados=operar.eliminaMuchos("sevilla");
		System.out.println("Numero de registros borrados=" + numeroRegistrosBorrados );
		
		
		//buscaTodos
		List<Persona> personas1=operar.buscaTodos();
		Iterator<Persona> it = personas1.iterator();
		
		while (it.hasNext())
		{
			Persona p = it.next();
			System.out.println("*********************");
			System.out.println("Codigo=" + p.getCodigo());
			System.out.println("Dni=" + p.getDni());
			System.out.println("Nombre=" + p.getNombre());
			System.out.println("Provincia=" + p.getProvincia());
			System.out.println("Calle=" + p.getDireccion().getCalle());
			System.out.println("Codigo Postal=" + p.getDireccion().getCodigoPostal());			
		}
		
		//buscaUnoCodigo
		long codigoBuscar=33;
		Persona persona = operar.buscaUnoCodigo(codigoBuscar);
		if(persona==null)System.out.println("No existe el codigo "+ codigoBuscar);
		else System.out.println("Encontrado***" + persona.toString());		
		
		
		//buscaVariosCodigos
		long codInicio=22;
		long codFinal=100;
		List<Persona> per=operar.buscaVariosCodigos(codInicio,codFinal);
		if(per.size()==0)System.out.println("No existen registros");
		else
		{
			Iterator<Persona> it2 = per.iterator();		
			
			while (it2.hasNext())
			{
				Persona p = it2.next();
				System.out.println(p);
			}
		}
		
		//buscaUnoNombre	
		operar.grabarUno(new Persona(100,"Tomas","200","Jaen", 
				new Direccion(100,"Calle 200","200")));
		Persona p2=null;
		p2= operar.buscaUnoNombre("Tomas");
		
		if(p2!=null)System.out.println("Encontrado***" + p2.toString());
		else System.out.println("No existe nombre"); 
		

	    operar.cerrarSessionFactory();
	}

}
