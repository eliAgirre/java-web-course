package datos;

import java.util.List;
import java.util.Vector;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import modelo.Pais;
import modelo.Presidente;

public class Operar {
	
private SessionFactory sessionFactory=null;
	
	public void abrirSessionFactory(){
		
		//La clase org.hibernate.SessionFactory es el objeto que permitirá
		//crear sesiones de Hibernate hacia la base de datos. 
		 try { 
			  sessionFactory= new Configuration().configure().buildSessionFactory();
		 }
		 catch (HibernateException e) {
			 
		     System.err.println("Ocurrió un error en la inicialización de la SessionFactory: " + e); 		     
		 } 
	}
	public void cerrarSessionFactory()
	{
		sessionFactory.close();
	}
	@SuppressWarnings("finally")
	public boolean grabarUnPais(Pais pais)
	{
		System.out.println("*****Grabar un Pais******");
		boolean resultado = false;
		Session session_1 = sessionFactory.openSession();
		Transaction tx_1 = session_1.beginTransaction();
		
		try
		{
			session_1.save(pais); 
			tx_1.commit();
			resultado=true;
						
		}
		catch(HibernateException he) 
		{
			tx_1.rollback();
			resultado=false;
		}
		finally
		{
			session_1.close();
			return resultado;	
		}
	}	
	
	@SuppressWarnings("finally")
	public long grabarVarios(Vector<Pais> paises)
	{
		// Guardar 5 paises
		System.out.println("*****Grabar varios*****");
		long numeroRegistrosGrabados=0;
		
		Session session_2 = sessionFactory.openSession();
		Transaction tx_2 = session_2.beginTransaction();

		try
		{
			int i=0;
			for (i = 0; i<paises.size(); i++) 
			{
				session_2.save(paises.get(i)); 
				//session_2.persist(paises.get(i)); Funciona			
			}
			tx_2.commit();	
			numeroRegistrosGrabados=i;
		}
		catch(HibernateException he) 
		{
			numeroRegistrosGrabados=0;
			tx_2.rollback();
		}
		finally
		{
			session_2.close();
			return numeroRegistrosGrabados;
		}			
	}
	
	@SuppressWarnings("finally")
	public boolean actualizaUno(Pais pais) 
	{ 
	   	System.out.println("*****Actualiza un pais*****");
		Session session_3 = sessionFactory.openSession();
		Transaction tx_3 = session_3.beginTransaction();
		boolean sw=false;
		try 
	    { 	 
	    	session_3.update (pais);
	        tx_3.commit(); 
	        sw=true;
	        
	    }
	    catch (HibernateException he) 
	    { 
	    	tx_3.rollback();
	    }
	    finally 
	    { 
	        session_3.close(); 
	        return sw;
	    } 
	}
	
	@SuppressWarnings("finally")
	public boolean eliminaUno(Pais pais) {
		
		System.out.println("*****Elimina uno*****");
		Session session_4 = sessionFactory.openSession();
		Transaction tx_4 = session_4.beginTransaction();
		boolean sw=false;
		
		try {             
            session_4.delete(pais); 
            tx_4.commit(); 
            sw=true;
        } 
		catch (HibernateException he){ 
			tx_4.rollback();
        } 
		finally { 
            session_4.close(); 
            return sw;
        } 
    } 
	
	@SuppressWarnings({ "unchecked" })
	public int eliminaMuchos(String continenteArg) {
		
		System.out.println("*****Elimina muchos*****");
		Session session_5 = sessionFactory.openSession();
		Transaction tx_5 = session_5.beginTransaction();
		int resultado=0;
		List<Pais> paises=null;
		
		Query query =session_5.createQuery		
			("from Pais p where p.continente = :continenteBorrar");
		
		query.setParameter("continenteBorrar", continenteArg);
		
		try
		{
			if(query.list().size()>0)
			{
				paises=query.list();
				for(int a=0;a<paises.size();a++)
				{
					 session_5.delete(paises.get(a)); 
					 resultado+=1;
				}
				tx_5.commit(); 				
			}	
		}

		catch (HibernateException he) 
		{ 
			tx_5.rollback();
			resultado=0;	        
		}
		session_5.close(); 
        return resultado;
    }
	
	 @SuppressWarnings("unchecked")
	public List<Pais> buscaTodos(){
		 
		System.out.println("*****Buscar todos*****");
		Session session_6 = sessionFactory.openSession();
		List<Pais> paises=null;
			
		paises = session_6.createQuery
			("from Pais p order by p.id asc").list();
		
		session_6.close();
		
		return paises;				
	}
	
	public Pais buscaUnoCodigoPais(long codigoBuscar) {
		
		System.out.println("*****Buscar un pais por clave*****");
		Session session_7 = sessionFactory.openSession();
		Pais pais = null;  
		
			//Si no lo encuentra el metodo get devuelve pais=null
		pais = (Pais) session_7.get(Pais.class, codigoBuscar); 
 
		return pais;       
	}
	
	public Presidente buscaUnoCodigoPresidente(long codigoBuscar) {
		
		System.out.println("*****Buscar un presidente por clave*****");
		Session session_8 = sessionFactory.openSession();
		Presidente presidente = null;  
		
		//Si no lo encuentra el metodo get devuelve presidente=null
		presidente=(Presidente) session_8.get(Presidente.class, codigoBuscar); 
 
		return presidente;       
	}
	
	@SuppressWarnings("unchecked")
	public List<Pais> buscaVariosCodigosPais(long codigoInicio,long codigoFinal) {
		
		System.out.println("*****Busca varios por el codigo pais*****" );
		Session session_9 = sessionFactory.openSession();

		Query query = session_9.createQuery("from Pais p where p.id between :cInicio and :cFinal");
		query.setParameter("cInicio", codigoInicio);	
		query.setParameter("cFinal", codigoFinal);			
		
		List<Pais> resultado= query.list(); 
        session_9.close(); 
        return  resultado;   
    } 
	
	@SuppressWarnings("unchecked")
	public List<Presidente> buscaVariosCodigosPresidente(long codigoInicio,long codigoFinal) {
		
		System.out.println("*****Busca varios por el codigo presidente*****" );
		Session session_10 = sessionFactory.openSession();

		Query query = session_10.createQuery
			("from Presidente p where p.id between :cInicio and :cFinal");
		query.setParameter("cInicio", codigoInicio);	
		query.setParameter("cFinal", codigoFinal);			
		
		List<Presidente> resultado= query.list(); 
        session_10.close(); 
        return  resultado;   
     } 
	
	public Pais buscaUnoNombrePais(String nombrearg) {
		
		System.out.println("*****Busca un Pais por el nombre*****" );
		Session session_11 = sessionFactory.openSession();
		
		Pais pais = null;	

		//No se escribe la comilla doble en los string 
		Query query = session_11.createQuery
			("from Pais p where p.nombre = :nombre");
		query.setParameter("nombre", nombrearg );	
		
		//Si lo encuentra
		if(query.list().size()>0)
			//persona=(Persona) query.list().get(0); //Funciona
			pais=(Pais) query.uniqueResult();
 
        session_11.close(); 
        return  pais;   
     } 
	
	public Presidente buscaUnoNombrePresidente(String nombrearg) { 
		System.out.println("*****Busca un Presidente por el nombre*****" );
		Session session_12 = sessionFactory.openSession();
		
		Presidente presidente = null;	

		//No se escribe la comilla doble en los string 
		Query query = session_12.createQuery("from Presidente p where p.nombre = :nombre");
		query.setParameter("nombre", nombrearg );	
		
		//Si lo encuentra
		if(query.list().size()>0)
			//persona=(Persona) query.list().get(0); //Funciona
			presidente=(Presidente) query.uniqueResult();
 
        session_12.close(); 
        return  presidente;   
     }    
}
