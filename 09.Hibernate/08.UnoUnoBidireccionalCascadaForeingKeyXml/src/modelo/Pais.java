package modelo;

import java.io.Serializable;

public class Pais implements Serializable  {
	
	private static final long serialVersionUID = 1L;
	
	private long id;  
	private String nombre;
	private String continente;
	
	private Presidente presidente;  
	
	public Pais(){}
	
    public Pais(long id, String nombre, String continente,Presidente presidente) {
    	
		super();
		this.id = id;
		this.nombre = nombre;
		this.continente=continente;
		this.presidente = presidente;
	}
    
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Presidente getPresidente() {
		return presidente;
	}
	public void setPresidente(Presidente presidente) {
		this.presidente = presidente;
	}

	public String getContinente() {
		return continente;
	}

	public void setContinente(String continente) {
		this.continente = continente;
	}

	@Override
	public String toString() {
		return "Pais [id=" + id + ", nombre=" + nombre.trim() + ", presidente="
				+ presidente + "]";
	}	
	
}
