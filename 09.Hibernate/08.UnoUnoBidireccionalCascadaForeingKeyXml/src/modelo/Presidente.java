package modelo;

import java.io.Serializable;

public class Presidente implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private long id; 
	private String nombre;     

	private Pais pais; 
	 
	public Presidente(){}
	 
	public Presidente(long id,String nombre){
		
		this.id=id;
		this.nombre=nombre;
	}
	
	public Presidente(String nombre){
		this.nombre=nombre;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

	@Override
	public String toString() {
		return "Presidente [id=" + id + ", nombre=" + nombre.trim() + "]";
	}
	
}
