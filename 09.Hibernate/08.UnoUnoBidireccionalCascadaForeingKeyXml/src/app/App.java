package app;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import datos.Operar;

import modelo.Pais;
import modelo.Presidente;

public class App {

	public static void main(String[] args) {
		
		Operar operar=new Operar();		
		
		operar.abrirSessionFactory();	
		
	    Presidente presidente1 = new Presidente();
	    presidente1.setNombre("Presidente_11");
	    presidente1.setId(111);
	    
		Pais pais1 = new Pais();
		pais1.setId(11);
		pais1.setNombre("China");
		pais1.setContinente("Asia");
		
		
		pais1.setPresidente(presidente1);	    
	   	presidente1.setPais(pais1);
	   	
	    //La columna "id_fk de la tabla pais se graba de forma 
	    //automatica con el valor de la propiedad id de la clase presidente
	   	
	    //los objetos Presidente se almacenaran en cascada
	    
	    //grabarUnPais
		if(operar.grabarUnPais(pais1))
			System.out.println("Pais grabado");
		else System.out.println("No se puede grabar el Pais");			
		
	  	//grabarVarios	    
	    Presidente presidente2=new Presidente(222,"Presidente_22");
	    Presidente presidente3=new Presidente(333,"Presidente_33");
	    Presidente presidente4=new Presidente(444,"Presidente_44");
	    Presidente presidente5=new Presidente(555,"Presidente_55");
	    Presidente presidente6=new Presidente(666,"Presidente_66");
	    
	    
	    Pais pais2=new Pais(22,"Alemania","Europa", presidente2);
	    Pais pais3=new Pais(33,"Francia", "Europa", presidente3);
	    Pais pais4=new Pais(44,"Vietnam", "Asia",   presidente4);
	    Pais pais5=new Pais(55,"Japon",   "Asia",   presidente5);
	    Pais pais6=new Pais(66,"Italia",  "Europa", presidente6);
	    
	    presidente2.setPais(pais2);
	    presidente3.setPais(pais3);
	    presidente4.setPais(pais4);
	    presidente5.setPais(pais5);
	    presidente6.setPais(pais6);	    
	    
		Vector<Pais> paises=new Vector<Pais>();
		
		paises.add(pais2);
		paises.add(pais3);
		paises.add(pais4);
		paises.add(pais5);
		paises.add(pais6);

		System.out.println("Numero de registros grabados=" + 
				operar.grabarVarios(paises));   	
		   	  
		//actualizaUno (Actualizacion en cascada)
		//Modificamos pais y lo actualizamos 
		pais1.setNombre("Austria_100");
		presidente1.setNombre("Presidente_100");
				
		if(operar.actualizaUno(pais1))System.out.println("Registro modificado"
				+ pais1.toString());		
		else System.out.println("No se puede modificar el registro");
		
		
		//eliminaUno (Eliminacion en cascada)
		//Borramos a pais
		if(operar.eliminaUno(pais1))System.out.println("Registro borrado");				
		else System.out.println("No se puede borrar el registro");
		
		//eliminaMuchos	
		int numeroRegistrosBorrados=operar.eliminaMuchos("Asia");
		System.out.println("Numero de registros borrados=" + numeroRegistrosBorrados );		
		
		//buscaTodos
		List<Pais> paises1=operar.buscaTodos();
		Iterator<Pais> it = paises1.iterator();
		
		while (it.hasNext())
		{
			Pais p = it.next();
			System.out.println("************* Todos ********");
			System.out.println("Codigo pais=" + p.getId());
			System.out.println("Nombre pais=" + p.getNombre());
			System.out.println("Continente pais=" + p.getContinente());
			System.out.println("Nombre presidente=" + p.getPresidente().getNombre());
			
		}
		
		//buscaUnoCodigoPais
		long codigoBuscar=33;
		Pais pais = operar.buscaUnoCodigoPais(codigoBuscar);
		if(pais==null)System.out.println("No existe el codigo "+ codigoBuscar);
		else System.out.println("Encontrado***" + pais.toString());		
		
		codigoBuscar=333;
		//buscaUnoCodigoPresidente
		//Al ser bidireccional puede buscar en cualquier tabla
		Presidente presidente = operar.buscaUnoCodigoPresidente(codigoBuscar);
		if(presidente==null)System.out.println("No existe el codigo "+ codigoBuscar);
		else
		{
			System.out.println("********Datos Presidente *************");
			System.out.println("Codigo pais=" + presidente.getId());
			System.out.println("Nombre presidente=" + presidente.getNombre());
			System.out.println("Nombre pais=" + presidente.getPais().getNombre());
			System.out.println("Continente pais=" + presidente.getPais().getContinente());	
		}
			
		//buscaVariosCodigosPais
		long codInicio=22;
		long codFinal=33;
		List<Pais> paises2=operar.buscaVariosCodigosPais(codInicio,codFinal);
		
		if(paises2==null) System.out.println("No existe pais");
		else
		{
			Iterator<Pais> it2 = paises2.iterator();		
			
			while (it2.hasNext())
			{
				Pais p = it2.next();
				System.out.println(p);
			}
			
		}
		
		//buscaVariosCodigosPresidente
		List<Presidente> presidentes=operar.buscaVariosCodigosPresidente(codInicio,codFinal);
		Iterator<Presidente> itPresidentes = presidentes.iterator();		
		
		while (itPresidentes.hasNext())
		{
			Presidente presidenteResultado = itPresidentes.next();
			System.out.println("********Datos Presidente *************");
			System.out.println("Codigo pais=" + presidenteResultado.getId());
			System.out.println("Nombre presidente=" + presidenteResultado.getNombre());
			System.out.println("Nombre pais=" + presidenteResultado.getPais().getNombre());
			System.out.println("Continente pais=" + presidenteResultado.getPais().getContinente());	;
		}
		
		//buscaUnoNombrePais	
	    Presidente presidente200 = new Presidente();
	    presidente200.setNombre("Presidente_200");
	    presidente200.setId(2000);
	    
		Pais pais200 = new Pais();
		pais200.setId(200);
		pais200.setNombre("Bulgaria");
		pais200.setContinente("Europa");
		pais200.setPresidente(presidente200);
	    
	   	presidente200.setPais(pais200);
	   	
		operar.grabarUnPais(pais200);
		Pais pais7=null;
		pais7= operar.buscaUnoNombrePais("Bulgaria");
		
		if(pais7!=null)System.out.println("Encontrado***" + pais7.toString());
		else System.out.println("No existe nombre"); 
		
		//buscaUnoNombrePresidente
		Presidente presidente7=null;
		presidente7=operar.buscaUnoNombrePresidente("Presidente_200");
		
		if(presidente7!=null)
		{
			System.out.println("********Datos Presidente *************");
			System.out.println("Codigo pais=" + presidente7.getId());
			System.out.println("Nombre presidente=" + presidente7.getNombre());
			System.out.println("Nombre pais=" + presidente7.getPais().getNombre());
			System.out.println("Continente pais=" + presidente7.getPais().getContinente());	
		}
		else System.out.println("No existe nombre del presidente");
	 
	}
}
