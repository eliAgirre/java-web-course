package modelo;

import java.io.Serializable; //no es obligatorio pero es una buena pr�ctica, para no perder los datos

public class Persona implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private long codigo;
	private String nombre;
	private String dni;
	private String provincia;
	

	/* Es obligatorio (puede ser privado si es que no quieren permitir que alguien m�s lo utilice*/
	@SuppressWarnings("unused")
	private Persona() {}	
	
	public Persona(long codigo,String nombre, String dni,String provincia) {
		
		super();
		this.codigo=codigo;
		this.nombre = nombre;
		this.dni = dni;
		this.provincia=provincia;
	}

	public long getCodigo() {
		return codigo;
	}

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	
	@Override
	public String toString() {
		return "Persona => codigo: "+codigo+", nombre: "+nombre.trim()+", dni: "+dni.trim()+ ", provincia: "+provincia.trim();
	}

}
