package datos;

import java.util.List;
import java.util.Vector;

import modelo.Persona;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.Query;


public class Operar 
{
	private SessionFactory sessionFactory=null;
	
	public void abrirSessionFactory()
	{
		//La clase org.hibernate.SessionFactory es el objeto que permitir�
		//crear sesiones de Hibernate hacia la base de datos. 
		 try 
		 { 
			  sessionFactory= new Configuration().configure().buildSessionFactory();
		 }
		 catch (HibernateException e) 
		 { 
		     System.err.println
		     	("Ocurri� un error en la inicializaci�n de la SessionFactory: " + e); 		     
		 } 
	}
	public void cerrarSessionFactory()
	{
		sessionFactory.close();
	}
	
	@SuppressWarnings("finally")
	public boolean grabarUno(Persona persona)
	{
		System.out.println("*****Grabar un registro*****");
		boolean resultado = false;
		Session session_1 = sessionFactory.openSession();
		Transaction tx_1 = session_1.beginTransaction();
		
		/*uso de un bloque try/finally para asegurar el cierre de la 
		 * sesi�n de Hibernate que se ha abierto (org.hibernate.Session ), 
		 * y tambi�n se hace uso de un bloque try/finally 
		 * para asegurar el cierre de la transacci�n (org.hibernate.Transaction )
		 * */
		
		try
		{
			//Si existe el registro no entra en el catch (no falla ni avisa)
			//La HibernateException la provoca el commit
			session_1.save(persona);
			tx_1.commit();
			resultado=true;
						
		}
		catch(HibernateException he) 
		{
			tx_1.rollback();
			resultado=false;
		}
		finally
		{
			session_1.close();
			return resultado;			
		}
	}
	
	@SuppressWarnings("finally")
	public long grabarVarios(Vector<Persona> personas)
	{
		// Guardar 5 personas
		System.out.println("*****Grabar varios*****");
		long numeroRegistrosGrabados=0;
		
		Session session_2 = sessionFactory.openSession();
		Transaction tx_2 = session_2.beginTransaction();

		try
		{
			int i=0;
			for (i = 0; i<personas.size(); i++) 
			{
				session_2.save(personas.get(i)); 
				//session_2.persist(personas.get(i)); //Funciona			
			}
			
			tx_2.commit();	
			numeroRegistrosGrabados=i;			
		}
		catch(HibernateException he) 
		{
			numeroRegistrosGrabados=0;
			tx_2.rollback();
		}
		finally
		{
			session_2.close();
			return numeroRegistrosGrabados;
		}	
		
	}
	@SuppressWarnings("finally")
	public boolean actualizaUno(Persona persona) 
	{ 
	   	System.out.println("*****Actualiza uno*****");
		Session session_3 = sessionFactory.openSession();
		Transaction tx_3 = session_3.beginTransaction();
		boolean sw=false;
		try 
	    { 	 
	    	session_3.update(persona); 
	        tx_3.commit(); 
	        sw=true;
	        
	    }
	    catch (HibernateException he) 
	    { 
	    	tx_3.rollback();
	    }
	    finally 
	    { 
	        session_3.close(); 
	        return sw;
	    } 
	}
	
	@SuppressWarnings("finally")
	public boolean eliminaUno(Persona persona)  
    { 
		System.out.println("*****Elimina uno*****");
		Session session_4 = sessionFactory.openSession();
		Transaction tx_4 = session_4.beginTransaction();
		boolean sw=false;
		try 
        {             
            session_4.delete(persona); 
            tx_4.commit(); 
            sw=true;
        } 
		catch (HibernateException he) 
        { 
			tx_4.rollback();
        } 
		finally 
        { 
            session_4.close(); 
            return sw;
        } 
    } 
	
	@SuppressWarnings("finally")
	public int eliminaMuchos(String provinciaArg)  
    { 
		System.out.println("*****Elimina muchos*****");
		Session session_5 = sessionFactory.openSession();
		Transaction tx_5 = session_5.beginTransaction();
		int resultado=0;
		
		Query query = session_5.createQuery
		("delete from Persona p where p.provincia = :provinciaBorrar");
		
		query.setParameter("provinciaBorrar", provinciaArg);	
		
		try
		{
			resultado=query.executeUpdate();
			tx_5.commit(); 				
		}
		
		catch (HibernateException he) 
        { 
			tx_5.rollback();
			
        } 
		finally 
        { 
            session_5.close(); 
            return resultado;
        } 
     } 	
	
	@SuppressWarnings("unchecked")
	public List<Persona> buscaTodos()
	{
		System.out.println("*****Buscar todos*****");
		Session session_6 = sessionFactory.openSession();
		List<Persona> personas=null;
			
		personas = session_6.createQuery
			("from Persona p order by p.dni asc").list();
		
		session_6.close();
		
		return personas;				
	}

	public Persona buscaUnoCodigo(String codigoInicio,String codigoFinal) 
	{ 
		System.out.println("*****Buscar por clave*****");
		Session session_7 = sessionFactory.openSession();
		Persona persona = null; 

		Query query = session_7.createQuery
			("from Persona where idCodigo1 " +
					"= :codigo1 and idCodigo2 = :codigo2");		
		
		query.setParameter("codigo1", codigoInicio);	
		query.setParameter("codigo2", codigoFinal);	;
			
		//Si lo encuentra
		if(query.list().size()>0)
				//persona=(Persona) query.list().get(0); //Funciona
				persona=(Persona) query.uniqueResult();

		session_7.close(); 
		return persona;       
	}
	
		
	public Persona buscaUnoNombre(String nombrearg)  
    { 
		System.out.println("*****Busca uno por el nombre*****" );
		Session session_9 = sessionFactory.openSession();
		
		Persona persona = null;	

		//No se escribe la comilla doble en los string 
		Query query = session_9.createQuery
			("from Persona where nombre = :nombre");
		query.setParameter("nombre", nombrearg );
				
		//Si lo encuentra
		if(query.list().size()>0)
			//persona=(Persona) query.list().get(0); //Funciona
			persona=(Persona) query.uniqueResult();
 
        session_9.close(); 
        return  persona;   
     }   
}
