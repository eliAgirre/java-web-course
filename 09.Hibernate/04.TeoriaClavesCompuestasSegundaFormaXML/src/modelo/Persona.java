package modelo;

import java.io.Serializable; //no es obligatorio pero es una buena pr�ctica


public class Persona implements Serializable 
{	
	private static final long serialVersionUID = 1L;
	
	private String idCodigo1;
	private String idCodigo2;
	private String nombre;
	private String dni;
	private String provincia;
	

	/* Es obligatorio 
	 * (puede ser privado si es que no 
	 * quieren permitir que alguien m�s lo utilice*/
	@SuppressWarnings("unused")
	private Persona() {}


	public Persona(String idCodigo1, String idCodigo2, String nombre,
			String dni, String provincia) {
		super();
		this.idCodigo1 = idCodigo1;
		this.idCodigo2 = idCodigo2;
		this.nombre = nombre;
		this.dni = dni;
		this.provincia = provincia;
	}


	public String getIdCodigo1() {
		return idCodigo1;
	}


	public void setIdCodigo1(String idCodigo1) {
		this.idCodigo1 = idCodigo1;
	}


	public String getIdCodigo2() {
		return idCodigo2;
	}


	public void setIdCodigo2(String idCodigo2) {
		this.idCodigo2 = idCodigo2;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getDni() {
		return dni;
	}


	public void setDni(String dni) {
		this.dni = dni;
	}


	public String getProvincia() {
		return provincia;
	}


	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

}
