package app;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/* Claves Compuestas
Creamos un bean que represente a esta clave con las columnas 
que queremos que formen la clave compuesta (idCodigo1 + idCodigo2)
*/

import datos.Operar;

import modelo.Persona;
import modelo.UnionID; //Clave compuesta

public class App {

	public static void main(String[] args) {
		
		Operar operar=new Operar();
		Persona persona=null;		
		
		operar.abrirSessionFactory();
		
		UnionID union1=new UnionID("11","111");
		
		Persona p1=new Persona(union1,"Juan","1273","madrid");
				
		//GrabarUno
		if(operar.grabarUno(p1))
			System.out.println("Registro grabado");
		else System.out.println("No se puede grabar registro");		
				
		//GrabarVarios
		Vector<Persona> personas=new Vector<Persona>();
		
		UnionID union2=new UnionID("22","222");
		UnionID union3=new UnionID("33","333");
		UnionID union4=new UnionID("44","444");
		UnionID union5=new UnionID("55","555");
		UnionID union6=new UnionID("66","666");
		
		personas.add((new Persona(union2,"Pedro",   "1234",  "sevilla")));		
		personas.add((new Persona(union3,"Ana",     "8123",  "madrid")));		
		personas.add((new Persona(union4,"Maria" ,  "6553",  "sevilla")));
		personas.add((new Persona(union5,"Antonio", "4545",  "sevilla")));
		personas.add((new Persona(union6,"Javier" , "3345",    "madrid")));
				
		System.out.println("Numero de registros grabados="+operar.grabarVarios(personas));
		
		
		//ActualizaUno
		//Modificamos p1 y lo actualizamos 
		p1.setDni("100");
		p1.setNombre("Enrique");
		if(operar.actualizaUno(p1))System.out.println("Registro modificado"+p1.toString());		
		else System.out.println("No se puede modificar el registro");
		
		//EliminaUno
		//Borramos a p1
		if(operar.eliminaUno(p1))System.out.println("Registro borrado");				
		else System.out.println("No se puede borrar el registro");
		
		//EliminaMuchos	
		int numeroRegistrosBorrados=operar.eliminaMuchos("sevilla");
		System.out.println("Numero de registros borrados=" + numeroRegistrosBorrados );
		
		
		//BuscaTodos
		List<Persona> personas1=operar.buscaTodos();
		Iterator<Persona> it = personas1.iterator();
		while (it.hasNext()){
			
			Persona p = it.next();
			ver(p);	
		}
		
		//BuscaUnoCodigo
		Persona p2=new Persona(new UnionID("99","999"),"Roberto","9875","madrid");
				
		//Grabamos un registro y lo buscamos
		if(operar.grabarUno(p2))System.out.println("Registro grabado");
		else System.out.println("No se puede grabar registro");	
		
		UnionID codigoBuscar=p2.getUnionid();
		persona = operar.buscaUnoCodigo(codigoBuscar);
		if(persona==null)System.out.println("No existe el codigo "+ codigoBuscar);
		else ver(p2);	
				
		//BuscaUnoNombre		
		operar.grabarUno(new Persona(new UnionID("100","1000"),"Tomas","100","Jaen"));
		Persona p3=null;
		p3= operar.buscaUnoNombre("Tomas");
		
		if(p3!=null)ver(p3);
		else System.out.println("No existe nombre");
				
		operar.cerrarSessionFactory();		
		
	}
	
	private static void ver(Persona p){
		
		System.out.println(" ******* ");
		System.out.println("Codigo1="+ p.getUnionid().getIdCodigo1());
		System.out.println("Codigo2="+ p.getUnionid().getIdCodigo2());
		System.out.println("Nombre=" + p.getNombre());
		System.out.println("Dni=" + p.getDni());
		System.out.println("Provincia="+ p.getProvincia());		
		
	}
	
}