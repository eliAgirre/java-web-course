/*
 Representa la clave compuesta
 */

package modelo;

import java.io.Serializable;

public class UnionID implements Serializable {

	private static final long serialVersionUID = 1L;
	
	//Clave compuesta (idCodigo1 + idCodigo2)
	private String idCodigo1;
	private String idCodigo2;
	
	public UnionID(){}	
	
	public UnionID(String idCodigo1, String idCodigo2) {
		super();
		this.idCodigo1 = idCodigo1;
		this.idCodigo2 = idCodigo2;
	}
	
	public String getIdCodigo1() {
		return idCodigo1;
	}
	
	public void setIdCodigo1(String idCodigo1) {
		this.idCodigo1 = idCodigo1;
	}
	
	public String getIdCodigo2() {
		return idCodigo2;
	}
	
	public void setIdCodigo2(String idCodigo2) {
		this.idCodigo2 = idCodigo2;
	}

}
