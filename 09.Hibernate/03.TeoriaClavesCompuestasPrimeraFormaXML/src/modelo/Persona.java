package modelo;

import java.io.Serializable; //no es obligatorio pero es una buena pr�ctica


public class Persona implements Serializable {	
	
	private static final long serialVersionUID = 1L;
	
	private UnionID unionid; //Clave compuesta
	private String nombre;
	private String dni;
	private String provincia;
	

	/* Es obligatorio 
	 * (puede ser privado si es que no 
	 * quieren permitir que alguien m�s lo utilice*/
	@SuppressWarnings("unused")
	private Persona() {}


	public UnionID getUnionid() {
		return unionid;
	}


	public Persona(UnionID unionid, String nombre, String dni, String provincia) {
		
		super();
		this.unionid = unionid;
		this.nombre = nombre;
		this.dni = dni;
		this.provincia = provincia;
	}


	public void setUnionid(UnionID unionid) {
		this.unionid = unionid;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getDni() {
		return dni;
	}


	public void setDni(String dni) {
		this.dni = dni;
	}


	public String getProvincia() {
		return provincia;
	}


	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

}
