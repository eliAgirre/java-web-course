<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Mostrar</title>
   </head>
<body>
   <div style="background-color:#FFFFcc;font-size:15pt">Mostrar</div>
   Dni: <s:property value="dni"/>
   <br>
   Nombre: <s:property value="nombre"/>
   <br>
   Provincia: <s:property value="provincia"/>
   <br>
   Calle: <s:property value="calle"/>
   <br>
   Codigo Postal: <s:property value="codigoPostal"/>
   <br>
   <a href="<s:url action='Carga_Pagina_menu'/>">Volver</a>
</body>
</html>