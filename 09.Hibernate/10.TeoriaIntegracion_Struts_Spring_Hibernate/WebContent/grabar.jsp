<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <title>Grabar</title>
</head>
<body>
    <!--Llamanos a la accion Grabar del struts.xml-->
  <s:form action="Grabar"> 
	<div style="background-color:#FFFFcc;font-size:15pt">Grabar</div>
	<table>
		<tr>
			<td>				
				<s:textfield name="codigo" label="Introduce codigo"/>				
			</td>		
		</tr>
		<tr>
			<td>
				<!--Grabamos el nombre en la propiedad setNombre()-->
				<s:textfield name="nombre" label="Introduce nombre"/>
			</td>		
		</tr>
		<tr>
			<td>
				<s:textfield name="dni" label="Introduce Dni"/>
			</td>
		</tr>
		<tr>		
			<td>
				<s:textfield name="provincia" label="Introduce Provincia"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:textfield name="calle" label="Introduce Calle"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:textfield name="codigoPostal" label="Introduce CP"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:submit value="Aceptar"/>
			</td>
		</tr>
	</table>   
   </s:form>	   
</body>	
</html>
