package modelo;

import java.io.Serializable;

public class Direccion implements Serializable
{

	private static final long serialVersionUID = 1L;
	
	private long id;
    private String calle;
    private String codigoPostal;

	public Direccion(){ }
        
    public Direccion(long id,String calle, String codigoPostal) {
		super();
		this.id=id;
		this.calle = calle;
		this.codigoPostal = codigoPostal;
	}

	public long getId()
    {
        return id;
    }
	
    public void setId(long id)
    {
        this.id = id;
    }
    
    public String getCalle()
    {
        return calle;
    }

    public void setCalle(String calle)
    {
        this.calle = calle;
    }

    public String getCodigoPostal()
    {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal)
    {
        this.codigoPostal = codigoPostal;
    }
}
