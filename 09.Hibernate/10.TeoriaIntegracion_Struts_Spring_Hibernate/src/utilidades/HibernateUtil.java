package utilidades;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	    
    private static final SessionFactory sessionFactory;
    
    /*
    bloque est�tico o inicializador est�tico, 
    se ejecuta una sola vez, cuando se carga la clase
    nos sirve para inicialzar la propiedad statica "sessionFactory"
    dentro de un try un catch
    
    En ocasiones es necesario escribir c�digo para inicializar los datos est�ticos, 
 	creando alg�n otro objeto de alguna clase o realizando alg�n tipo de control
    */
    static {
    	
        try {
        	/*
        	  sessionFactory = 
        	  	new Configuration().configure("/package1/package2/hibernate/hibernate.cfg.xml").
        	  		buildSessionFactory();

        	 */

            sessionFactory = new Configuration().configure("/utilidades/hibernate.cfg.xml").buildSessionFactory();
           
        } 
        //sessionFactory necesita para terminar de inicilizarse lanzar la
       //"ExceptionInInitializerError"
        catch (Throwable ex){ //Super clase de cualquier excepcion lanzable 
        
            System.err.println("Fallo de inicializacion" + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

  }

