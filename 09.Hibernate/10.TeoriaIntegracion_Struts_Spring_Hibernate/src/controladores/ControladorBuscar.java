package controladores;

import modelo.Persona;
import negocio.OperarImp;

import org.springframework.web.context.WebApplicationContext;

import utilidades.SpringUtil;

import com.opensymphony.xwork2.ActionSupport;

public class ControladorBuscar extends ActionSupport{

	private static final long serialVersionUID = 1L;
	
	private long codigo;
	private String nombre;
	private String dni;
	private String provincia;
	
	private String calle;
    private String codigoPostal;
    private String resultado;
	
	public String execute() {
		
    	WebApplicationContext context=SpringUtil.getWebApplicationContext();
		
		OperarImp operarImp=(OperarImp) context.getBean("idOperarImp");
		
		Persona persona=(Persona) context.getBean("idDatosPersonales");
		
		persona=operarImp.buscarUno(codigo);
		
		if(persona==null){ 
			
			resultado="No existe la persona";
			return "NOSUCCESS";
		}
		else{
			
			resultado="Registro encontrado";
			
			codigo=persona.getCodigo();
			nombre=persona.getNombre();
			dni=persona.getDni();
			provincia=persona.getProvincia();
			calle=persona.getDireccion().getCalle();
			codigoPostal=persona.getDireccion().getCodigoPostal();
			
			return "SUCCESS";
		}
					
    }

	public long getCodigo() {
		return codigo;
	}

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}	
}
