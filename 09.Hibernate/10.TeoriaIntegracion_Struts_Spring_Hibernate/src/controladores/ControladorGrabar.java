package controladores;

import modelo.Persona;
import negocio.OperarImp;

import org.springframework.web.context.WebApplicationContext;

import utilidades.SpringUtil;

import com.opensymphony.xwork2.ActionSupport;

public class ControladorGrabar extends ActionSupport{
	
	private static final long serialVersionUID = 1L;
	
	private String resultado;
    
	private long codigo;
	private String nombre;
	private String dni;
	private String provincia;
	
	private String calle;
    private String codigoPostal;
    
    public String execute() {
    	
     	WebApplicationContext context=SpringUtil.getWebApplicationContext();
		
		OperarImp operarImp=(OperarImp) context.getBean("idOperarImp");
		
		Persona persona=(Persona) context.getBean("idDatosPersonales");
				
		persona.setCodigo((codigo));
		persona.setNombre(nombre);
		persona.setDni(dni);
		persona.setProvincia(provincia);
		
		persona.getDireccion().setId(codigo);
		persona.getDireccion().setCalle(calle);
		persona.getDireccion().setCodigoPostal(codigoPostal);
				
		if(operarImp.grabarUno(persona))
			resultado="Registro grabado";					
		else 		
			resultado="No se puede grabar registro";
		    	
    	return "SUCCESS";
    }     
 
    public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public String getNombre() 
    {
        return nombre;
    }

    public void setNombre(String nombre) 
    {
        this.nombre = nombre;
    }    

	public long getCodigo() {
		return codigo;
	}

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
 }
