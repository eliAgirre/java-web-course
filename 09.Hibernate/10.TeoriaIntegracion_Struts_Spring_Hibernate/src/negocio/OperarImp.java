package negocio;

import modelo.Persona;
import datos.DatosHibernate;

public class OperarImp {
		
	private DatosHibernate datosHibernate;
	
	public OperarImp(){}

	public DatosHibernate getDatosHibernate() {
		return datosHibernate;
	}

	public void setDatosHibernate(DatosHibernate datosHibernate) {
		this.datosHibernate = datosHibernate;
	}
	
	public boolean grabarUno(Persona p){
		return datosHibernate.grabarUno(p);
	}
	
	public Persona buscarUno(long codigo){
		
		Persona persona=datosHibernate.buscaUnoCodigo(codigo);
		
		return persona;
		
	}
	
}
