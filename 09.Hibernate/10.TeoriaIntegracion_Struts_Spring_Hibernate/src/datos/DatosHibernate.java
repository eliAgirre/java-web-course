package datos;


import utilidades.HibernateUtil;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import modelo.Persona;

public class DatosHibernate {
	
	private SessionFactory sessionFactory=null;
	
	private void abrirSessionFactory()
	{
		//La clase org.hibernate.SessionFactory es el objeto que permitirá
		//crear sesiones de Hibernate hacia la base de datos. 
		 try 
		 { 
			 //Si queremos recoger la session
			 //sessionFactory= (SessionFactory) HibernateUtil.getSessionFactory().
			 //		getCurrentSession();
			  
			  sessionFactory=  HibernateUtil.getSessionFactory();
			
		 }
		 catch (HibernateException e) 
		 { 
		     System.err.println
		     	("Ocurrió un error en la inicialización de la SessionFactory: " + e); 		     
		 } 
	}
	
	private void cerrarSessionFactory()
	{
		sessionFactory.close();
	}
	@SuppressWarnings("finally")
	public boolean grabarUno(Persona persona){
		abrirSessionFactory();
		System.out.println("*****Grabar un registro******");
		boolean resultado = false;
		Session session_1 = sessionFactory.openSession();
		Transaction tx_1 = session_1.beginTransaction();
		
		try
		{
			
			// Si en el fichero "Persona.hbm.xml" no existiera la opcion
			// ",save-update" en la etiqueta "<one-to-one" solo podriamos 
			// grabar con la opcion "persist" ya que solo grabariamos en
			// la tabla personas. 
			
			session_1.save(persona); 
			tx_1.commit();
			resultado=true;
						
		}
		catch(HibernateException he) 
		{
			tx_1.rollback();
			resultado=false;
		}
		finally
		{
			session_1.close();
			return resultado;			
		}
	}
	
	public Persona buscaUnoCodigo(long codigoBuscar) 
	{ 
		abrirSessionFactory();
		System.out.println("*****Buscar por clave*****");
		Session session_7 = sessionFactory.openSession();
	
		Persona persona = null;  
		
		//Si no lo encuentra el metodo get devuelve persona=null
		persona = (Persona) session_7.get(Persona.class, codigoBuscar); 
 
		cerrarSessionFactory();
		return persona;       
	}	 
}
