package instrumentos;

import java.util.Iterator;
import java.util.List;

public class Orquesta {

	private List<Instrumento> instrumentos;
	
	public List<Instrumento> getInstrumentos() {
		
		return instrumentos;
		
	} // get

	public void setInstrumentos(List<Instrumento> instrumentos) {
		
		this.instrumentos = instrumentos;
		
	} // set

	public void play(){
	
		System.out.println("###Inicio Reproduciendo orquesta###");
		
		Iterator<Instrumento> it=instrumentos.iterator();
		
		while (it.hasNext()) {
			
			it.next().play();
		}
		
		System.out.println("###Fin Reproduciendo orquesta###");
	}
}
