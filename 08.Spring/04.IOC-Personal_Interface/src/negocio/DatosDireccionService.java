package negocio;

public interface DatosDireccionService 
{
	public String getCalle();
	public int getCodigoPostal();
}
