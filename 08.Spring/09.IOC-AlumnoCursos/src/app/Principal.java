package app;
import modelo.Alumno;
import negocio.Matricula;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class Principal {


	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		Alumno alumno=new Alumno("A", "AA","AAA");
		
		ApplicationContext appFactoria=new FileSystemXmlApplicationContext(".\\conf\\spring.xml");
				
		//Matricula miMatricula=(Matricula) appFactoria.getBean("idMatricula");
		Matricula miMatricula=(Matricula) appFactoria.getBean("idMatricula_alias"); // alias spring
		
		miMatricula.matricularse(alumno);

		System.out.println("Comentario: "+miMatricula.getComentarioCurso());
		System.out.println("Curso: "+miMatricula.getCurso());
		
		System.out.println(miMatricula);
	}

}
