package modelo;

public class Alumno {
	
	private String codigo;
	private String nombre;
	private String dni;
	
	
	public Alumno(String codigo, String nombre, String dni) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.dni = dni;
	}
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	
	@Override
	public String toString() {
		return "Alumno [codigo=" + codigo + ", nombre=" + nombre + ", dni="+ dni + ", getCodigo()=" + getCodigo() + ", getNombre()="
				+ getNombre() + ", getDni()=" + getDni() + "]";
	}
	
	
}
