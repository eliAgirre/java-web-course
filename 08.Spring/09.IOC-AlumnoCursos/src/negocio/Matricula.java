package negocio;

import modelo.Alumno;

public class Matricula {
	
	private Curso curso;
	private String comentarioCurso;	
	
	public Matricula() {}
	
	public Curso getCurso() {
		
		return curso;
	}
	
	public void setCurso(Curso curso) {
		
		this.curso = curso;
	}
	
	public String getComentarioCurso() {
		
		return comentarioCurso;
	}
	public void setComentarioCurso(String comentarioCurso) {
		
		this.comentarioCurso = comentarioCurso;
	}
	
	@Override
	public String toString() {
		
		return "Matricula [curso=" +curso+ ", comentarioCurso="+comentarioCurso+"]";
	}
	
	public void matricularse(Alumno alumno) {
		
		System.out.println("###Inicio Matricula###");
		
		curso.rellenaMatricula(alumno);
		
		System.out.println("###Fin Matricula###");
	}
	
	
}
