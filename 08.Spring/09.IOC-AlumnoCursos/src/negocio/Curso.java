package negocio;

import modelo.Alumno;

public interface Curso {

	public abstract void rellenaMatricula(Alumno alumno);
}
