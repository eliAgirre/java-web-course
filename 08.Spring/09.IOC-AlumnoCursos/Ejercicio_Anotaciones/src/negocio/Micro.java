package negocio;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

//Spring
@Component("Micro")

public class Micro {
	
	@Value("Intel")
	private String tipo;
	
	@Value("I7")
	private String caracteristica;	
	
	
	public Micro() {
		
	}
		
	public Micro(String tipo, String caracteristica) {
		this.tipo = tipo;
		this.caracteristica = caracteristica;
	}
	
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getCaracteristica() {
		return caracteristica;
	}
	public void setCaracteristica(String caracteristica) {
		this.caracteristica = caracteristica;
	}
}
