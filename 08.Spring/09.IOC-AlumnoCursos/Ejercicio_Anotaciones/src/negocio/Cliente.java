package negocio;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

//Spring
@Component("cliente")

public class Cliente  {
	
	@Value("1235678")
	private String dni;
	
	@Value("Juan")
	private String nombre;
	
	//@Autowired 
	private Pc pc;
	

	public Cliente() {
	
	}	
	
	@Autowired
	public Cliente( Pc pc) {
		
		this.pc = pc;
	}


	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Pc getPc() {
		return pc;
	}

	public void setPc(Pc pc) {
		this.pc = pc;
	}
	
	public void ver(){
		
		System.out.println(getDni());
		System.out.println(getNombre());
		System.out.println(getPc().getMarca());
		System.out.println(getPc().getMicro().getTipo());
		System.out.println(getPc().getMicro().getCaracteristica());
	}
	
	

}
