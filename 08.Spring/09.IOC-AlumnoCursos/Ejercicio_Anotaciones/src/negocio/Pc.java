package negocio;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

//Spring
@Component("Pc")

//Java
//@Configuration

public class Pc {
	
	@Value("Dell")
	private String marca;
	
	private Micro micro;	
	
	public Pc() {
		
	}


	@Autowired
	public Pc( Micro micro) {
		
		this.micro = micro;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public Micro getMicro() {
		return micro;
	}
	
	//@Autowired
	public void setMicro(Micro micro) {
		this.micro = micro;
	}
}
