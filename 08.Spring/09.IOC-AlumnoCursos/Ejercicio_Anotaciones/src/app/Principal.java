package app;






import negocio.Cliente;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

public class Principal 
{


	public static void main(String[] args) 
	{
		@SuppressWarnings("resource")
		ApplicationContext appFactoria = new FileSystemXmlApplicationContext(".\\conf\\spring.xml");
		
		Cliente beanCliente = (Cliente)appFactoria.getBean("cliente");
		
		beanCliente.ver();

 
	}

}
