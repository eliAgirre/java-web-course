package app;


import negocio.ClienteService;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

public class Principal 
{


	public static void main(String[] args) 
	{
		ApplicationContext appFactoria = new ClassPathXmlApplicationContext("spring.xml");
		
		//ClassPathResource recurso = new ClassPathResource("spring.xml");		
		//BeanFactory appFactoria = new XmlBeanFactory(recurso);

		ClienteService beanCliente = (ClienteService)appFactoria.getBean("idCliente");
		
		beanCliente.ver();


	}

}
