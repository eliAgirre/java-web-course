package negocio;

public class Micro {
	
	private String tipo;
	private String caracteristica;	
	
	public Micro(String tipo, String caracteristica) {
		this.tipo = tipo;
		this.caracteristica = caracteristica;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getCaracteristica() {
		return caracteristica;
	}
	public void setCaracteristica(String caracteristica) {
		this.caracteristica = caracteristica;
	}
	
	

}
