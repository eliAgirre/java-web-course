package negocio;

public class Cliente implements ClienteService {
	
	private String dni;
	private String nombre;
	
	private Pc pc;
	

	public Cliente() {
	
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Pc getPc() {
		return pc;
	}

	public void setPc(Pc pc) {
		this.pc = pc;
	}
	
	public void ver(){
		
		System.out.println(getDni());
		System.out.println(getNombre());
		System.out.println(getPc().getMarca());
		System.out.println(getPc().getMicro().getTipo());
		System.out.println(getPc().getMicro().getCaracteristica());
	}
	
	

}
