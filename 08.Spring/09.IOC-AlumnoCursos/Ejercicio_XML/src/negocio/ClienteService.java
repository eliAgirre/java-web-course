package negocio;

public interface ClienteService {
	
	public String getDni();
	public String getNombre();
	public Pc getPc();
	public void ver();

}
