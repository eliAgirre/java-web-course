package negocio;

public class Pc {
	
	private String marca;
	
	private Micro micro;
	
	public Pc(String marca, Micro micro) {
		this.marca = marca;
		this.micro = micro;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public Micro getMicro() {
		return micro;
	}

	public void setMicro(Micro micro) {
		this.micro = micro;
	}
}
