package modelo;

public class Coche {

	private String codigo;
	private String marca;
	
	public Coche (String c, String m){
		
		codigo = c;
		marca = m;		
		
	}
	
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	
}
