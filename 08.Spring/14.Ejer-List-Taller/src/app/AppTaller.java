package app;

import modelo.Coche;
import negocio.SeguroCoche;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppTaller {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ApplicationContext appFactoria = new ClassPathXmlApplicationContext("spring.xml");
		
		//SeguroCoche sc = (SeguroCoche)appFactoria.getBean("Seguro");
		SeguroCoche sc = (SeguroCoche)appFactoria.getBean("Seguro", SeguroCoche.class);
		Coche c = new Coche ("0xxxxx", "Renault");
		
		sc.repararCoche(c);
				
		
	}

}
