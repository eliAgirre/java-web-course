package negocio;

import modelo.Coche;

public class TallerCarroceria implements Taller{


	public void repararCoche(Coche c) {
		
		System.out.println("### INICIO TALLER CARROCERIA ###");
		System.out.println("Matricula: " + c.getCodigo());
		System.out.println("Marca: " + c.getMarca());
		System.out.println("### FIN TALLER CARROCERIA ###");	
		
	}

}
