package negocio;

import modelo.Coche;

public class TallerMecanica implements Taller{


	public void repararCoche(Coche c) {
		
		System.out.println("### INICIO TALLER MECANICA ###");
		System.out.println("Matricula: " + c.getCodigo());
		System.out.println("Marca: " + c.getMarca());
		System.out.println("### FIN TALLER MECANICA ###");	
		
	}

}
