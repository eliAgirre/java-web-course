package negocio;

import java.util.Iterator;
import java.util.List;

import modelo.Coche;

public class SeguroCoche {
	
	private List<Taller> talleres;
	
	public List<Taller> getTalleres() {
		return talleres;
	}

	public void setTalleres(List<Taller> talleres) {
		this.talleres = talleres;
	}

	public void repararCoche(Coche c){
	
		System.out.println("### Inicio Seguro ###");
		
		Iterator<Taller> it = talleres.iterator();
		while (it.hasNext()) {
			it.next().repararCoche(c);
		}
		System.out.println("### Fin Seguro ###");
	}
	
	
	
	
	
	

}
