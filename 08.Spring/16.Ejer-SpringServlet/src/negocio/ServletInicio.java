package negocio;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.Personal;

import org.springframework.context.ApplicationContext;

import utilidades.Navegar;
import utilidades.SpringUtil;

public class ServletInicio extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

    public ServletInicio() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		inicio(request,response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		inicio(request,response);
	}
	
	protected void inicio(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ApplicationContext context=SpringUtil.getApplicationContext(getServletContext());
		
		HttpSession sesion=request.getSession();
		
		Personal personal=(Personal) context.getBean("personalID");
		
		personal.setDni(request.getParameter("dni"));
		personal.setNombre(request.getParameter("nombre"));
		
		sesion.setAttribute("personal", personal);
		
		Navegar.navegar("datosEconomicos.html", request, response);
		
	}

}
