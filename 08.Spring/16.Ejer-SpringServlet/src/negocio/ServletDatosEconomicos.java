package negocio;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.Personal;

import org.springframework.context.ApplicationContext;

import utilidades.Navegar;
import utilidades.SpringUtil;

public class ServletDatosEconomicos extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public ServletDatosEconomicos() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		datosEconomicos(request,response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		datosEconomicos(request,response);
	}
	
	protected void datosEconomicos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ApplicationContext context=SpringUtil.getApplicationContext(getServletContext());
		
		HttpSession sesion=request.getSession();
		
		Personal personal=(Personal) sesion.getAttribute("personal");
		
		personal.getNomina().setSueldo(Integer.valueOf(request.getParameter("sueldo")));
		personal.getNomina().setPlus(Integer.valueOf(request.getParameter("plus")));
		
		OperarImp operar=(OperarImp) context.getBean("operarID");
		
		sesion.setAttribute("total", operar.getTotal(personal));
		
		Navegar.navegar("resultados.jsp", request, response);
	}

}
