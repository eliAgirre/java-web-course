package negocio;

public class HolaMundoImp implements HolaMundoService{

	/* Un bean tiene que tener constructor por defecto
	 * si va a ser instanciado byName o byType.
	 */
	public HolaMundoImp() {
		System.out.println("Constructor Hola Mundo");
	}
	
	//	public HolaMundo(int i) {}
	public void saludo() {
		System.out.println("Hola Mundo");
	}
}
