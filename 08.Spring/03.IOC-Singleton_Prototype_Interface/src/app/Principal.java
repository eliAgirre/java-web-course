package app;
import negocio.ServicioBeanService;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Principal {


	@SuppressWarnings("resource")
	public static void main(String[] args) {
	
		ApplicationContext appFactoria = new ClassPathXmlApplicationContext("spring.xml");
		ServicioBeanService sBean_1 = (ServicioBeanService) appFactoria.getBean("idservicioBean_1");
		ServicioBeanService sBean_2 = (ServicioBeanService) appFactoria.getBean("idservicioBean_2");
		
		System.out.println("****Bean 1****");
		sBean_1.leer();
		System.out.println("****Bean 2****");
		sBean_2.leer();
		System.out.println("****Bean 1****");
		sBean_1.cambiar();
		sBean_1.leer();
		System.out.println("****Bean 2****");
		sBean_2.leer();
	}

}
