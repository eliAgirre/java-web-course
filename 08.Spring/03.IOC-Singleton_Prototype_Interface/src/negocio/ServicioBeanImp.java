package negocio;
import modelo.Bean;

public class ServicioBeanImp implements ServicioBeanService{
	
	private Bean bean;
	
	public Bean getBean() {
		return bean;
	}
	public void setBean(Bean bean) {
		this.bean = bean;
	}
	
	public void cambiar(){
		
		bean.setDni("1");
		bean.setNombre("nuevo");

	}
	public void leer(){
		
		System.out.println("Dni1=" +bean.getDni());
		System.out.println("Nombre=" +bean.getNombre());
	}
	
}
