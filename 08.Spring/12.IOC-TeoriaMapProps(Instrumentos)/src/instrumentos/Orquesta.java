package instrumentos;

import java.util.Iterator;
import java.util.Map; // Para el Map
import java.util.Properties; // Para el Prop

public class Orquesta {

	// Para el Map
	private Map<String,Instrumento> instrumentosMap;
	
	//No los utilizo
	//public Map<String, Instrumento> getInstrumentosMap() {
	//	return instrumentosMap;
	//}


	public void setInstrumentosMap(Map<String,Instrumento> instrumentosMap) {
		
		this.instrumentosMap = instrumentosMap;
	}

	// Para el Prop
	private Properties instrumentosProp;

	//No los utilizo
	//public Properties getInstrumentosProp() {
	//	return instrumentosProp;
	//}


	public void setInstrumentosProp(Properties instrumentosProp) {
		
		this.instrumentosProp = instrumentosProp;
	}


	public void play(){
	
		System.out.println("###Inicio Reproduciendo orquesta###");

		// Prop
		// devuelve las claves
		Iterator<Object> itr=instrumentosProp.keySet().iterator();		
		
		String str;
		System.out.println("###Prop###");
		
		while(itr.hasNext()){
			
			// saca el literal
			str=(String) itr.next();
			System.out.println(	str + ":"+ instrumentosProp.getProperty(str) );
		} 
		
		// Map
		System.out.println("###Map###");
		
		// Para el Map
		for(String key:instrumentosMap.keySet()){
			
			System.out.print(key+":");
			
			// se obtiene el objeto Instrumento
			Instrumento inst=instrumentosMap.get(key);
			
			inst.play();
		}
		
		System.out.println("###Fin Reproduciendo orquesta###");
	}
}
