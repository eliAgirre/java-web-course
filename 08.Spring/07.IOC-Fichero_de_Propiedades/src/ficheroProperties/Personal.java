package ficheroProperties;

public class Personal 
{
	private String dni;
	private String nombre;
	private String apellido;
	
	private Nominas nomina;

		
	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getApellido() 
	{
		return apellido;
	}

	public void setApellido(String apellido) 
	{
		this.apellido = apellido;
	}

	public String getNombre() 
	{
		return nombre;
	}

	public void setNombre(String nombre) 
	{
		this.nombre = nombre;
	}

	public Nominas getNomina() {
		return nomina;
	}

	public void setNomina(Nominas nomina) {
		this.nomina = nomina;
	}	
}
