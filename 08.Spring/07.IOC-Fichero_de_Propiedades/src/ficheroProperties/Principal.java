package ficheroProperties;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class Principal {

	@SuppressWarnings("resource")
	public static void main(String args[]) {
		
		ApplicationContext factoria = new FileSystemXmlApplicationContext("./conf/spring.xml");

		Personal miPersonal = (Personal)factoria.getBean("personal");
		
		System.out.println("Dni=" + miPersonal.getDni());
		System.out.println("Nombre=" + miPersonal.getNombre());
		System.out.println("Apellido=" + miPersonal.getApellido());
		System.out.println("Sueldo=" + miPersonal.getNomina().getSueldo());
	}
	
}
