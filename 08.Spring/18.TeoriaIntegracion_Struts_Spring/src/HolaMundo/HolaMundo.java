package HolaMundo;

//import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
//import org.springframework.web.context.support.WebApplicationContextUtils;

import vista.Operar;
import utilidades.SpringUtil;

import com.opensymphony.xwork2.ActionSupport;

public class HolaMundo extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final String SALUDO = "Hola ";
    private String customSaludo;
    private String nombre="";      
   
    
    public String execute() {
    	
    	//  Funciona 
    	/*
		WebApplicationContext context =
				WebApplicationContextUtils.
					getRequiredWebApplicationContext
						(ServletActionContext.getServletContext());
		
    	*/
    	//Mejor instancia estatica
    	
    	WebApplicationContext context=SpringUtil.getWebApplicationContext();
		
		Operar beanOperar=(Operar) context.getBean("idOperar");
		
		System.out.println("Dni: "+beanOperar.getDatosPersonales().getDni());
		
		System.out.println("Nombre: "+beanOperar.getDatosPersonales().getNombre());
	
		System.out.println("Calle: "+beanOperar.getDatosDireccion().getCalle());
		
		System.out.println("Codigo Postal: "+beanOperar.getDatosDireccion().getCcodigoPostal());
		
    	setCustomSaludo(SALUDO+getNombre());   
    	
    	return "VALOR_SALUDO";
    }   
   

 
    public String getNombre() {
    	
        return nombre;
    }

    public void setNombre(String nombre) {
    	
        this.nombre = nombre;
    }   

    
    public String getCustomSaludo(){
    	
    	return  customSaludo;
    }
    
    public void setCustomSaludo( String customSaludo ){
    	
    	this.customSaludo = customSaludo;
    }
}