package modelo;

public class DatosPersonales 
{
	private String dni;
	private String nombre;
	
	
	
	public DatosPersonales() {
		super();
	}
	public String getDni() 
	{
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	
}
