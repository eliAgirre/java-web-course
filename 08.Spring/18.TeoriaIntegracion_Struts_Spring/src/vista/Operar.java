package vista;

import modelo.DatosDireccion;
import modelo.DatosPersonales;

public class Operar {
	
	DatosPersonales datosPersonales;
	DatosDireccion datosDireccion;
	
	public Operar(DatosPersonales datosPersonales,DatosDireccion datosDireccion){
		
		this.datosPersonales=datosPersonales;
		this.datosDireccion=datosDireccion;
	}
	
	public DatosPersonales getDatosPersonales() {
		
		return datosPersonales;
	}
	public void setDatosPersonales(DatosPersonales datosPersonales) {
		
		this.datosPersonales = datosPersonales;
	}
	public DatosDireccion getDatosDireccion() {
		
		return datosDireccion;
	}
	public void setDatosDireccion(DatosDireccion datosDireccion) {
		
		this.datosDireccion = datosDireccion;
	}
	
}
