<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
   <title>Nombre</title>
</head>
<body>

    <hr>
	<h4>Introduce nombre</h4> 	

	<!--Llamanos a la accion HolaMundo del struts.xml-->
	<s:form action="HolaMundo">
	
		<!--Grabamos el nombre en la propiedad setNombre() del servlet-->
		<s:textfield name="nombre" label="Tu nombre"/>
    	<s:submit/>
	</s:form>
    <hr>
    	
</body>
</html>