package negocio;

public class Operar {
	
	private DatosPersonalesService datosPersonales ;
	private DatosDireccionService datosDireccion ;	
	
	
	public DatosPersonalesService getDatosPersonales() {
		return datosPersonales;
	}

	public void setDatosPersonales(DatosPersonalesService datosPersonales) {
		this.datosPersonales = datosPersonales;
	}

	public DatosDireccionService getDatosDireccion() {
		return datosDireccion;
	}

	public void setDatosDireccion(DatosDireccionService datosDireccion) {
		this.datosDireccion = datosDireccion;
	}

	public Operar(){
		
	}
	
	public Operar(DatosPersonalesService datosPersonales,DatosDireccionService datosDireccion) {
		
		this.datosPersonales = datosPersonales;
		this.datosDireccion = datosDireccion;
	}

	public void mostrarDatos()
	{
		System.out.println("Dni=" + datosPersonales.getDni());
		System.out.println("Nombre=" + datosPersonales.getNombre());
		
		System.out.println("Calle=" + datosDireccion.getCalle());
		System.out.println("Codigo Postal=" + datosDireccion.getCodigoPostal());
	}
	
}
