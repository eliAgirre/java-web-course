package negocio;

public interface DatosPersonalesService 
{
	public String getDni(); 	
	public void setDni(String dni);	
	public String getNombre();	
	public void setNombre(String nombre);
}
