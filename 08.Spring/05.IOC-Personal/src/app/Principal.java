package app;

//import org.springframework.beans.factory.BeanFactory;
//import org.springframework.beans.factory.xml.XmlBeanFactory;
import negocio.Operar;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
//import org.springframework.core.io.ClassPathResource;

public class Principal 
{


	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		ApplicationContext appFactoria = new ClassPathXmlApplicationContext("spring.xml");
		
		//ClassPathResource recurso = new ClassPathResource("spring.xml");		
		//BeanFactory appFactoria = new XmlBeanFactory(recurso);

		Operar beanOperar = (Operar)appFactoria.getBean("idOperar");
		
		beanOperar.mostrarDatos();


	}

}
