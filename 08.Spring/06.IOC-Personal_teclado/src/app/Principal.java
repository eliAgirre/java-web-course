package app;

import modelo.Nomina;
import modelo.Personal;
import negocio.Operar;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import utilidades.Teclado;

public class Principal {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		ApplicationContext appFactoria = new ClassPathXmlApplicationContext("spring.xml");
		
		String sDni=Teclado.teclado("Introduce DNI: ");
		String sNombre=Teclado.teclado("Introduce nombre: ");
		int iSueldo=Integer.parseInt(Teclado.teclado("Introduce sueldo: "));
		int iPlus=Integer.parseInt(Teclado.teclado("Introduce plus: "));
		
		Nomina nomina=(Nomina)appFactoria.getBean("Nomina");
		nomina.setSueldo(iSueldo);
		nomina.setPlus(iPlus);
		
		Personal personal=(Personal)appFactoria.getBean("Personal");
		personal.setDni(sDni);
		personal.setNombre(sNombre);

		Operar beanOperar=(Operar)appFactoria.getBean("Operar");
		
		beanOperar.ver(personal);

	}

}
