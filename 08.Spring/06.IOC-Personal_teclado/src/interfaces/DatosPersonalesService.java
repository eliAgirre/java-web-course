package interfaces;

import modelo.Nomina;

public interface DatosPersonalesService {
	
	public String getDni(); 	
	public String getNombre();
	public Nomina getNomina();
	
	public void setDni(String dni);	
	public void setNombre(String nombre);
	public void setNomina(Nomina nomina);
}
