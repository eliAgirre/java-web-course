package utilidades;
import java.io.*; 

public class Teclado {
	
	public static String teclado(String msg) {
		
		String sCadena=null;
		
		try{
			
			System.out.print(msg + " ");
			sCadena=(new BufferedReader(new InputStreamReader(System.in)).readLine());
		}
		catch(IOException e){
			System.out.println(e.getMessage());
		}
		return sCadena;
	}
}
