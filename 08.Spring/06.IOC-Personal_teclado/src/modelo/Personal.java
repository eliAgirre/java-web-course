package modelo;

import interfaces.DatosPersonalesService;

public class Personal implements DatosPersonalesService{
	
	private String dni;
	private String nombre;
	private Nomina nomina;
	
	// getters
	public String getDni() {
		return dni;
	}
	public String getNombre() {
		return nombre;
	}
	public Nomina getNomina() {
		return nomina;
	}
	
	// setters
	public void setDni(String dni) {
		this.dni = dni;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setNomina(Nomina nomina) {
		this.nomina = nomina;
	}
	
}