package modelo;

public class Nomina {
	
	private int sueldo;
	private int plus;
	
	public Nomina(){}
	
	public Nomina(int sueldo, int plus) {
		
		setSueldo(sueldo);
		setPlus(plus);
	}
	public int getSueldo() {
		
		return sueldo;
	}
	public int getPlus() {
		
		return plus;
	}
	
	public void setSueldo(int sueldo) {
		
		this.sueldo = sueldo;
	}
	
	public void setPlus(int plus) {

		this.plus = plus;	
	}
	
	public int getTotal(){
		
		return this.sueldo+this.plus;
	}

	@Override
	public String toString() {
		return "Nomina [sueldo=" + sueldo + ", plus=" + plus + "]";
	}	
	
}
