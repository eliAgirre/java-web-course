package app;

import java.util.Iterator;

import modelo.Alumno;
import negocio.GestionAlumnos;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class App {


	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		ApplicationContext appFactoria=new FileSystemXmlApplicationContext("./conf/spring.xml");
		
		GestionAlumnos miGestionAlumnos=(GestionAlumnos)appFactoria.getBean("servAlumnos");
		
		Iterator<Alumno> miIterator=miGestionAlumnos.getAlumno().iterator();
		
		while (miIterator.hasNext()){
			
			Alumno alumno = miIterator.next();
			System.out.println(alumno.toString());
		}
	}

}