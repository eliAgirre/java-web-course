package datos;


//Imitamos la logica de Datos
//Grabamos 10 alumnos

import java.util.ArrayList;
import java.util.List;

import modelo.Alumno;

public class AlumnoDAO {

	public List<Alumno> getAlumnos() {
		
		List<Alumno> miAlumnos=new ArrayList<Alumno>();
		
		for (int i=0;i<10;i++) {
			
			miAlumnos.add(new Alumno(i,"Pedro "+i));
		}
		
		return miAlumnos;
	}
}
