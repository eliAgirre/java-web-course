package negocio;
//Logica de negocio
import java.util.List;

import datos.AlumnoDAO;

import modelo.Alumno;

public class GestionAlumnos {

	private AlumnoDAO daoAlumno;
	
	public void setDaoAlumno(AlumnoDAO daoAlumno) {
		
		this.daoAlumno = daoAlumno;
	}
	
	public List<Alumno> getAlumno() {
		
		//Llamamos a la logica de datos (AlumnoDAO)
		return daoAlumno.getAlumnos();
	}
	
}