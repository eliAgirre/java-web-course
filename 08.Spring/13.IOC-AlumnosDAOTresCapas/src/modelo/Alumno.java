package modelo;

public class Alumno {

	private int codigoAlumno;
	private String nombre;
	
	public Alumno(int codigoAlumno, String nombre) {
		
		super();
		this.codigoAlumno = codigoAlumno;
		this.nombre = nombre;
	}

	
	@SuppressWarnings("unused")
	private int getCodigoAlumno() {
		
		return codigoAlumno;
	}
	
	@SuppressWarnings("unused")
	private void setCodigoAlumno(int codigoAlumno) {
		this.codigoAlumno = codigoAlumno;
	}
	
	@SuppressWarnings("unused")
	private String getNombre() {
		return nombre;
	}
	
	@SuppressWarnings("unused")
	private void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Override
	public String toString() {
		return "Alumno [codigoAlumno=" + codigoAlumno + ", Nombre="
				+ nombre + "]";
	}
	
}
