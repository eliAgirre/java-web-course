package negocio;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import org.springframework.context.ApplicationContext;

import utilidades.SpringUtil;

public class Personal extends HttpServlet {
  

	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}
	
	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException {

	    response.setContentType("text/html"); //Conveniente, no obligatorio 
	     
		//Funciona
	    /*
			ApplicationContext context=WebApplicationContextUtils.getWebApplicationContext(getServletContext());
		
		*/
	    
		//Mejor instancia estatica, recupera la instancia de Spring
	    ApplicationContext context=SpringUtil.getApplicationContext(getServletContext());
		
	    OperarService beanOperar=(OperarService) context.getBean("idOperar");
		
		    
	     /* Saco la salida */
	    ServletOutputStream out=response.getOutputStream();
	    
	    /* Mando una p�gina html f�cil */
	    out.println("<HTML>");
	    out.println("<HEAD><TITLE>Spring</TITLE></HEAD>");
	    out.println("<BODY>");
	    out.println("<FORM>");
	    out.println("<H3>");
	    out.println("Dni=" + beanOperar.getDatosPersonales().getDni());
	    out.println("<BR>");
	    out.println("Nombre=" + beanOperar.getDatosPersonales().getNombre());
	    out.println("<BR>");
	    out.println("Calle=" + beanOperar.getDatosDireccion().getCalle());
	    out.println("<BR>");
	    out.println("Codigo Postal=" + beanOperar.getDatosDireccion().getCcodigoPostal());
	    out.println("</H3>");
	    out.println("<P>");
	    out.println("<a href='inicio.html'>Volver </a>");
	    out.println("</FORM>");
	    out.println("</BODY>");
	    out.println("</HTML>");
	                
	    /* Cierro la salida para mandarla */            
	    out.close();        
	}
  
}