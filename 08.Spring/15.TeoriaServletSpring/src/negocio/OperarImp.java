package negocio;

import modelo.DatosDireccionService;
import modelo.DatosPersonalesService;

public class OperarImp implements OperarService {
	
	private DatosPersonalesService datosPersonales ;
	private DatosDireccionService datosDireccion ;	
	
	
	public DatosPersonalesService getDatosPersonales() {
		return datosPersonales;
	}

	public void setDatosPersonales(DatosPersonalesService datosPersonales) {
		this.datosPersonales = datosPersonales;
	}

	public DatosDireccionService getDatosDireccion() {
		return datosDireccion;
	}

	public void setDatosDireccion(DatosDireccionService datosDireccion) {
		this.datosDireccion = datosDireccion;
	}

	public OperarImp(){
		
	}
	
	public OperarImp(DatosPersonalesService datosPersonales,
			DatosDireccionService datosDireccion) {
		
		this.datosPersonales = datosPersonales;
		this.datosDireccion = datosDireccion;
	}

}
