package negocio;

import modelo.DatosDireccionService;
import modelo.DatosPersonalesService;

public interface OperarService {

	public DatosPersonalesService getDatosPersonales();

	public void setDatosPersonales(DatosPersonalesService datosPersonales);
	
	public DatosDireccionService getDatosDireccion();

	public void setDatosDireccion(DatosDireccionService datosDireccion);			
	
}
