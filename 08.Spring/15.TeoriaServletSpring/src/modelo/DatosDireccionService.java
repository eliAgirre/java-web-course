package modelo;

public interface DatosDireccionService {
	public String getCalle();
	public int getCcodigoPostal();
}
