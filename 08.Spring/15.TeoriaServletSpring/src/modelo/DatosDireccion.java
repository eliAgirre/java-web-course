package modelo;

public class DatosDireccion implements DatosDireccionService {
	
	private String calle;
	private int codigoPostal;
	
	public DatosDireccion( String calle,int codigoPostal) {
		this.calle = calle;
		this.codigoPostal = codigoPostal;
	}	
	
	public String getCalle() {
		return calle;
	}

	public int getCcodigoPostal() {
		return codigoPostal;
	}
}
