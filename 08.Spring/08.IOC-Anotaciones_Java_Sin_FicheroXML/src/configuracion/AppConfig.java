package configuracion;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;



//***Con import  (se usan las clases ConfigBean1.class y ConfigBean2.class)

@Configuration
@Import({ ConfigBean1.class, ConfigBean2.class })

public class AppConfig {}
	


//***Sin Import (Sobran las clases ConfigBean1.class y ConfigBean2.class)
/*
@Configuration
public class AppConfig 
{
	
	@Bean(name="miBean1")//Renombrar
	public Bean1 creaBean1()
	{
 		return new Bean1("hola 1"); 
	}
	
	@Bean
	public Bean2 creaBean2()
	{
 		return new Bean2(); 
	}	
}
*/
