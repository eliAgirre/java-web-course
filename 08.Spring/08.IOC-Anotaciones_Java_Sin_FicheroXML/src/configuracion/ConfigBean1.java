package configuracion;

import modelo.Bean1;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigBean1 {
 
	@Bean(name="miBean1")
	public Bean1 creaBean1(){
		
 		return new Bean1("hola 1"); 
	}
 
}