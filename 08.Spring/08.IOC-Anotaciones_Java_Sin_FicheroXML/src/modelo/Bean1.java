package modelo;

public class Bean1 {
	 
	private String msg;
	
	public Bean1(String msg){	
		
		this.msg=msg;
	}
	
	public void printMsg() {
 
		System.out.println("Bean1 : " + msg);
	}
 
}
