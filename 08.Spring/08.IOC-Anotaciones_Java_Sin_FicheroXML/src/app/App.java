package app;

import modelo.Bean2;
import modelo.Bean1;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import configuracion.AppConfig;

public class App {
	
	@SuppressWarnings("resource")
	public static void main(String[] args) {
 
		AnnotationConfigApplicationContext context=new AnnotationConfigApplicationContext(AppConfig.class);
 
		Bean1 bean1=(Bean1) context.getBean("miBean1");
		bean1.printMsg();
		
		Bean2 bean2=(Bean2) context.getBean("creaBean2");
		bean2.printMsg("Hola 2"); 
	}
}