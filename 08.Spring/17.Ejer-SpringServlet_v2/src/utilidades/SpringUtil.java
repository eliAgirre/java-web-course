package utilidades;

import javax.servlet.ServletContext;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class SpringUtil {
	
	private static ApplicationContext appFactoria;
	 
	// ServletContext = ApplicationContext
	private static void iniciar(ServletContext sc){
		
		try {
			
			// devuelve objeto ApplicationContext
			appFactoria=WebApplicationContextUtils.getWebApplicationContext(sc);
			
	    } 
	    catch (Throwable ex){ //Super clase de cualquier excepcion lanzable 
	    
			System.err.println("Fallo de inicializacion"+ex);
			throw new ExceptionInInitializerError(ex);
	    }
	 }

	 public static ApplicationContext getApplicationContext(ServletContext sc) {
		 
		iniciar(sc);
		return appFactoria;
	 }
}
