package negocio;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.Personal;

import org.springframework.context.ApplicationContext;

import utilidades.Navegar;
import utilidades.SpringUtil;

public class ServletDatosEconomicos extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public ServletDatosEconomicos() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		datosEconomicos(request,response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		datosEconomicos(request,response);
	}
	
	protected void datosEconomicos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ApplicationContext context=SpringUtil.getApplicationContext(getServletContext());
		
		HttpSession sesion=request.getSession();
		
		Personal p1=(Personal) sesion.getAttribute("p1");
		Personal p2=(Personal) sesion.getAttribute("p2");
		
		p1.getNomina().setSueldo(Integer.valueOf(request.getParameter("sueldo1")));
		p1.getNomina().setPlus(Integer.valueOf(request.getParameter("plus1")));
		
		p2.getNomina().setSueldo(Integer.valueOf(request.getParameter("sueldo2")));
		p2.getNomina().setPlus(Integer.valueOf(request.getParameter("plus2")));
		
		OperarImp operar=(OperarImp) context.getBean("operarID");
		
		sesion.setAttribute("total1", operar.getTotal(p1));
		sesion.setAttribute("total2", operar.getTotal(p2));
		
		Navegar.navegar("resultados.jsp", request, response);
	}

}
