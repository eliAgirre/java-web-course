package negocio;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.Personal;

import org.springframework.context.ApplicationContext;

import utilidades.Navegar;
import utilidades.SpringUtil;

public class ServletInicio extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

    public ServletInicio() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		inicio(request,response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		inicio(request,response);
	}
	
	protected void inicio(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ApplicationContext context=SpringUtil.getApplicationContext(getServletContext());
		
		HttpSession sesion=request.getSession();
		
		String sDni1="", sDni2="";
		String sNombre1="", sNombre2="";
		
		sDni1=request.getParameter("dni1");
		sNombre1=request.getParameter("nombre1");
		
		sDni2=request.getParameter("dni2");
		sNombre2=request.getParameter("nombre2");
		
		Personal p1=(Personal) context.getBean("personalID");
		Personal p2=(Personal) context.getBean("personalID");
		
		p1.setDni(sDni1);
		p1.setNombre(sNombre1);
		
		p2.setDni(sDni2);
		p2.setNombre(sNombre2);
		
		sesion.setAttribute("p1", p1);
		sesion.setAttribute("p2", p2);
		
		Navegar.navegar("datosEconomicos.html", request, response);
		
	}

}
