package modelo;

public class Personal {
	
	private String dni;
	private String nombre;
	private Nomina nomina;
	
	public String getDni() {
		return dni;
	}
	public String getNombre() {
		return nombre;
	}
	public Nomina getNomina() {
		return nomina;
	}
	
	public void setDni(String dni) {
		this.dni = dni;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setNomina(Nomina nomina) {
		this.nomina = nomina;
	}
	
}
