<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Resultados</title>
</head>
<body>

	<h4>JSP</h4>
	<jsp:useBean id="p1" class="modelo.Personal" scope="session"/>
	
	<label>Dni:</label>
	<jsp:getProperty name="p1" property="dni"/><br/><br/>

	<label>Nombre:</label>
	<jsp:getProperty name="p1" property="nombre"/><br/><br/>

	<label>Nomina:</label>
	<%=session.getAttribute("total1") %><br/><br/>
	
	<hr>
	
	<h4>JSTL</h4>
	<label>Dni:</label>
	<c:out value="${sessionScope.p2.dni}"/><br><br>

	<label>Nombre:</label>
	<c:out value="${sessionScope.p2.nombre}"/><br><br>
	
	<label>Nomina:</label>
	<c:out value="${sessionScope.total2}"/><br><br>
	
	<input type="button" value="Inicio" onclick="document.location.href='index.html'">
	
</body>
</html>