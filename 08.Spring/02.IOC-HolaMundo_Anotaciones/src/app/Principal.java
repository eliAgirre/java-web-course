package app;

import negocio.HolaMundoService;


//import org.springframework.beans.factory.BeanFactory;
//import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
//import org.springframework.core.io.ClassPathResource;
//import org.springframework.core.io.FileSystemResource;
//import org.springframework.core.io.Resource;

public class Principal {

	/*
	 *Obtiene la implementacion de la interfaz HolaMundoService por medio de Spring Framework,
	 *aunque en tiempo de ejecuci�n se utiliza una referencia a HolaMundoImpl  
	 */
	@SuppressWarnings("resource")
	public static void main(String args[]) {

	// La primera tarea necesaria para la ejecucion de cualquier programa
	// de spring require la instanciacion de un contenedor de beans, para ello
	// las siguientes lineas muestran tres formas diferentes de hacerlo
	
	// 1) Obtenemos una factoria a partir de un fichero del sistema
		/*	Resource recurso = new FileSystemResource("../conf/spring.xml");
			BeanFactory factoria = new XmlBeanFactory(recurso);*/
	
			
	// 2) Obtenemos una factoria a partir de un fichero del ClassPath
	//		ClassPathResource recurso = new ClassPathResource("spring.xml");
	//		BeanFactory factoria = new XmlBeanFactory(recurso);
	//		System.out.println("Despu�s creado");
	
	// 3) Obtenemos una factoria a partir de un fichero del ClassPath
	//		ApplicationContext contexto = new ClassPathXmlApplicationContext(
	//		new String[] {"applicationContext.xml", "applicationContext-part2.xml"});
		
		ApplicationContext factoria = new ClassPathXmlApplicationContext("spring.xml");
		System.out.println("Despu�s creado");
		
		// Un ApplicationContext es un BeanFactory con mas funcionalidades que se veran en detalle
		//		BeanFactory factoria = (BeanFactory) contexto;
		

		// Una vez obtenida la factoria somos capaces de instanciar cualquier bean que hemos
		// definido en nuestro fichero para spring
		
		/*
		Funciona si en "@Component() o @Configuration " no los renombramos
		a�adiendo el nombre como argumento Ej @Component("HolaMundoImp")
		HolaMundoService bean = (HolaMundoService)factoria.getBean("holaMundoImp");
		*/
		
		//Anotacion
		HolaMundoService bean = (HolaMundoService)factoria.getBean("HolaMundoImp");
		
		// ejecutamos la funcionalidad propia del bean obtenido
		bean.saludo();
		System.out.println("Saludo 2=" + bean.saludo2());
		
		//Spring.xml
		HolaMundoService bean2 = (HolaMundoService)factoria.getBean("servicioHolaDos");		
		bean2.saludo();
		
		System.out.println("Saludo 2=" + bean.saludo2());
		
	}
	
}
