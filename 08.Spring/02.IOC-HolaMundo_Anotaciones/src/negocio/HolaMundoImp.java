package negocio;

//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

//import org.springframework.stereotype.Component;

/*
las clases anotadas como component ser�n registrados por Spring 
como beans de la aplicaci�n igual que lo har�a con uno definido con la etiqueta <bean>
 */

/*
@Component() y @Configuration() 
el ID del bean se llama la primera con minusculas
HolaMundoService bean = (HolaMundoService)factoria.getBean("holaMundoImp");
*/

//Spring
@Component("HolaMundoImp")

//Java
//@Configuration("HolaMundoImp")


public class HolaMundoImp implements HolaMundoService{

	public HolaMundoImp() {
	
		System.out.println("Constructor Hola Mundo");
	}
		
	public HolaMundoImp(int i) {}
		
	
	public void saludo() {
		
		System.out.println("Hola Mundo");
	}
	
	//@Bean (Para anotar metodos con return)
	//@Bean 
	//@Scope("prototype") //Pueden existir varias instancias del bean
	//@Scope("singleton") //(Solo puede existir una instancia del bean) por defecto, mismo objeto todo el rato
	public String saludo2(){
		
		return ("Hola");
	}
}
