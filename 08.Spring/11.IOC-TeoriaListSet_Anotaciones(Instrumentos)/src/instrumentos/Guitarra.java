package instrumentos;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order (2) // orden de la grabacion en el list
public class Guitarra implements Instrumento {
	
	public void play() {
		
		System.out.println("###Tocando guitarra###");
		
	}
}
