package instrumentos;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order (1) // orden de la grabacion en el list
public class Trompeta implements Instrumento {

	public void play() {
		
		System.out.println("###Tocando trompeta###");
	}

}
