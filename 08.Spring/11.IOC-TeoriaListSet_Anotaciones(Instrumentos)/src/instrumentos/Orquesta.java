package instrumentos;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//Spring
@Component("orquesta")
public class Orquesta {
	
	//@Autowired //Inyeccion por propiedad
	private List<Instrumento> instrumentos;
	
	public List<Instrumento> getInstrumentos() {
		return instrumentos;
	}
	
	@Autowired //Inyeccion set
	public void setInstrumentos(List<Instrumento> instrumentos) {
		this.instrumentos = instrumentos;
	}
	
	public void play(){
	
		
		System.out.println("###Inicio Reproduciendo orquesta###");
		
		Iterator<Instrumento> it = instrumentos.iterator();
		while (it.hasNext()) {
			
			it.next().play();
		}
		
		System.out.println("###Fin Reproduciendo orquesta###");
	}	
}
