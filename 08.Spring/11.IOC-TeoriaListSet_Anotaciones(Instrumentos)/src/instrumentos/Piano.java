package instrumentos;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order (0) // orden de la grabacion en el list
public class Piano implements Instrumento {

	public void play() {
		
		System.out.println("###Tocando piano###");
	}
}
