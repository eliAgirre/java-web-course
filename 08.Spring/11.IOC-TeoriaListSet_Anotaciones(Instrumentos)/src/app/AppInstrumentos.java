package app;

import instrumentos.Orquesta;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppInstrumentos {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		ApplicationContext appFactoria=new ClassPathXmlApplicationContext("spring.xml");
	
		Orquesta orq=(Orquesta) appFactoria.getBean("orquesta");
		
		orq.play();
	}
}
