package controlador;

import java.util.Map;

import org.springframework.web.context.WebApplicationContext;

import negocio.OperarImp;
import utilidades.SpringUtil;
import modelo.Cliente;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class Resultado extends ActionSupport{
	
	private static final long serialVersionUID = 1L;
	
	private String dni;
	private String nombre;
	private String pantalla;
	private String sMicro;
	
	@SuppressWarnings("unchecked")
	public String execute() {
		
		Map<String,Cliente> session=(Map<String,Cliente>)ActionContext.getContext().getSession();
		
		WebApplicationContext context=SpringUtil.getWebApplicationContext();
		
		OperarImp operar=(OperarImp)context.getBean("operarID");
		
	    // obtiene el tipo de micro desde la sesion
		Cliente cliente=(Cliente)session.get("cliente");
		
		int iPantalla=Integer.parseInt(getPantalla());
		
		cliente.getPc().setPantalla(iPantalla);
		
		int iTotal=operar.getTotal(cliente);
		
		Map<String,Integer> sessionTotal=(Map<String,Integer>)ActionContext.getContext().getSession();
		
		sessionTotal.put("total", iTotal); // se guarda en la session el total
		
		int iMicro=cliente.getPc().getMicro();
		
		sMicro=String.valueOf(iMicro);
		
		return "SUCCESS";
	}
	
	// getters
	public String getDni() {
		return dni;
	}

	public String getNombre() {
		return nombre;
	}
	public String getPantalla() {
		return pantalla;
	}

	// setters
	public void setPantalla(String pantalla) {
		this.pantalla = pantalla;
	}
	
	// otros metodos
	public String getNombreMicro() {
		
		if(sMicro.equals("0"))return "Intel";
		else return "Amd";
	}
	
	public String getSizePantalla(){
		
		String size="";
		
		if(pantalla.equals("0"))size="19'";
		else if(pantalla.equals("1"))size="21'";
		else if(pantalla.equals("2"))size="23'";
		
		return size;
	}
	
}