package controlador;

import java.util.List;
import java.util.Map;

import modelo.Cliente;

import org.springframework.web.context.WebApplicationContext;

import utilidades.SpringUtil;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class Micro extends ActionSupport{
	
	private static final long serialVersionUID = 1L;
	
	private String dni;
	private String nombre;
	private List<String> micro;
	
	@SuppressWarnings("unchecked")
	public String execute(){
		
		WebApplicationContext context=SpringUtil.getWebApplicationContext();
		
		Cliente cliente=(Cliente) context.getBean("clienteID");
		
		Map<String,Cliente> session=(Map<String,Cliente>)ActionContext.getContext().getSession();
		
		String sMicro=getMicro().get(0); // se obtiene si es 0 o 1
		int iMicro=Integer.parseInt(sMicro);
		
		cliente.setDni(getDni());
		cliente.setNombre(getNombre());
		cliente.getPc().setMicro(iMicro);
		
		session.put("cliente", cliente); // se guarda en la sesion
		
		return "SUCCESS";
	}

	public String getDni() {
		return dni;
	}
	public String getNombre() {
		return nombre;
	}
	public List<String> getMicro() {
		return micro;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setMicro(List<String> micro) {
		this.micro = micro;
	}
}
