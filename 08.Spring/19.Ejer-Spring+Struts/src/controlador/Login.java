package controlador;

import org.springframework.web.context.WebApplicationContext;

import utilidades.SpringUtil;
import modelo.Usuario;

import com.opensymphony.xwork2.ActionSupport;

public class Login extends ActionSupport{
	
	private static final long serialVersionUID = 1L;
	
	private String username;
	private String password;
	
	
	// business logic
	public String execute() {
		
		WebApplicationContext context=SpringUtil.getWebApplicationContext();
		
		Usuario usuario=(Usuario)context.getBean("usuarioID");
		
		usuario.setUsername(getUsername());
		usuario.setPassword(getPassword());
		
		if(usuario.getUsername().equalsIgnoreCase("angel") && usuario.getPassword().equalsIgnoreCase("miguel")){
			return "SUCCESS_BIEN";		
		}
		else{
			return "SUCCESS_MAL";	
		}
	}

	// simple validation
	public void validate(){
		
		if(getUsername().length()==0){
			addFieldError("error_user", getText("login.user.requerido"));
			//addFieldError("username", getText("username.required"));
		}
		if(getUsername().length()>10){
			addFieldError("error_user_maximo", getText("<font color='green'>El usuario debe ser menor de 10 caracteres</font>"));
		}
		if(getPassword().length()==0){
			addFieldError("errorpassword", getText("login.password.requerido"));
			//addFieldError("password", getText("password.required"));
		}
		
	}
	
	// getters
	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	// setters
	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}