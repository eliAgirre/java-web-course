package modelo;

public class Pc {
	
	private int micro;
	private int pantalla;
	
	public Pc(){}

	public Pc(int micro, int pantalla) {

		this.micro = micro;
		this.pantalla = pantalla;

	}

	public int getMicro() {

		return this.micro;
	}


	public int getPantalla() {

		return this.pantalla;
	}

	
	public void setMicro(int micro) {
	
		this.micro=micro;
	}


	public void setPantalla(int pantalla) {
		
		this.pantalla=pantalla;
	}

	@Override
	public String toString() {
		
		return "Pc => micro: "+micro+", pantalla: "+pantalla;
	}

}
