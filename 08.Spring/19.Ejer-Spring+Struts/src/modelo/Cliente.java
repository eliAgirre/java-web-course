package modelo;

public class Cliente {
	
	private String dni;
	private String nombre;
	private Pc pc;
	
	public String getDni() {
		return dni;
	}
	public String getNombre() {
		return nombre;
	}
	public Pc getPc() {
		return pc;
	}
	
	public void setDni(String dni) {
		this.dni = dni;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setPc(Pc pc) {
		this.pc = pc;
	}
	@Override
	public String toString() {
		return "Cliente => dni: "+dni+", nombre: "+nombre+", pc: "+pc;
	}

}
