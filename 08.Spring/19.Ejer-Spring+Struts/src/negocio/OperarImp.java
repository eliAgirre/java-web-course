package negocio;

import modelo.Cliente;

public class OperarImp implements OperarService {

	@Override
	public int getTotal(Cliente c) {
		
		int iMicro=0;
		
		if(c.getPc().getMicro()==0){
			iMicro=100;
		}
		else{
			iMicro=50;
		}	
		
		return iMicro+50;
	}

}
