<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><s:text name="resultado.bien.titulo"/></title>
<script type="text/javascript" >
	
	function seleccionar(){
		
		document.forms[0].rb0.checked="true";			
	}

</script>
</head>
<body onload="document.forms[0].dniID.focus();">

	<s:form theme="css_xhtml">
		<table>
			<tr>
				<td><h4><s:text name="resultado.bien.usuario" />:</h4></td>
				<td><s:property value="username"/></td>
			</tr>
		</table>
	</s:form>
	
	<!--formulario struts-->
	<s:form action="accionMicro">
	
		<s:textfield name="dni" id="dniID" key="resultado.bien.dni" />
		<s:textfield name="nombre" id="nombreID" key="resultado.bien.nombre" />
		
		
		<s:radio id="rb" name="micro" list="#{'0':'Intel'}" />
		<s:radio id="rb" name="micro" list="#{'1':'Amd'}"/>
	
		<s:submit value="Enviar" />
	
	</s:form>
	
	<p>
	
	<a href="<s:url action='inicio_fin_accion'/>"><s:text name="resultado.bien.salir"/></a>

</body>
</html>