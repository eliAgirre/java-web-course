<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Resultado</title>
</head>
<body>

	Dni: <s:property value="#session.cliente.dni"/><br/><br/>
	
	Nombre: <s:property value="#session.cliente.nombre"/><br/><br/>
	
	Micro: <s:property value="nombreMicro"/><br/><br/>
	
	Pantalla: <s:property value="sizePantalla"/><br/><br/>
	
	Total <s:property value="#session.total"/><br/><br/>
	
	<a href="<s:url action='accionVolver'/>"><s:text name="resultado.final.volver"/></a>
	<a href="<s:url action='Salir'/>">Salir</a>
	
</body>
</html>