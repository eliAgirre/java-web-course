package datos.conexion_dao;
import java.sql.*;
public class ConexionMySql 
{
	private Connection cnn=null;
	@SuppressWarnings("finally")
	public Connection abrirConexion()
	{
		//No existe entrada en el Panel de control
		try
		{
			//mysql (grabar el jar 
			// "mysql-connector-java-5.1.12-bin"  
			///en el lib del tomcat"
			Class.forName("com.mysql.jdbc.Driver");
			cnn = DriverManager.getConnection
			   ("jdbc:mysql://localhost/base2000",
			        "user1", "user1");
			
		}
		catch(ClassNotFoundException e)
		{
			System.err.println("Error..Clase no encontrada");
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			
			// Devuelve el codigo deerror
		    //(Lo escribo en //F:\JRun\jsm-default\logs\stderr.log)
			
			//Describe el error (String)
			System.err.println("Error=" +e.getMessage());
			
			//Describe el error (en numero) seg�n 
			//a las convenciones X/OPEN de SqlState (String)
			System.err.println("Estado sql=" 
					+ e.getSQLState());
			
			//Codigo de error del Drivers. Especifico para cada driver (N�merico)
		    System.err.println("El cod de error  " +
		    		"en SQuery es " + e.getErrorCode());
		}
		finally
		{
			return cnn;
		}
    }
	public void cerrarConexion()
	{
		try
		{
			cnn.close(); //(Lanza una SQLException)
		}
		catch(SQLException e)
		{
			// Devuelve el codigo deerror
		    //(Lo escribo en //F:\JRun\jsm-default\logs\stderr.log)
			
			//Describe el error (String)
			System.err.println("Error=" +e.getMessage());
			
			//Describe el error (en numero) seg�n 
			//a las convenciones X/OPEN de SqlState (String)
			System.err.println("Estado sql=" + e.getSQLState());
			
			//Codigo de error del Drivers. Especifico para cada driver (N�merico)
		    System.err.println("El cod de error  en SQuery es " + e.getErrorCode());
		}
	}
}
