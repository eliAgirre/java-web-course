package datos.conexion_dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexionAccess 
{
	private Connection cnn=null;
	@SuppressWarnings("finally")
	public Connection abrirConexion()
	{
		
		try
		{
			String url="jdbc:odbc:odbcbase"; 			
			
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			
			cnn=DriverManager.getConnection(url);			
				}
		catch(ClassNotFoundException e)
		{
			System.err.println("Error..Clase no encontrada");
		}
		catch(SQLException e)
		{

			e.printStackTrace();
			
			// Devuelve el codigo deerror
		    //(Lo escribo en //F:\JRun\jsm-default\logs\stderr.log)
			
			//Describe el error (String)
			System.err.println("Error=" +e.getMessage());
			
			//Describe el error (en numero) seg�n 
			//a las convenciones X/OPEN de SqlState (String)
			System.err.println("Estado sql=" 
					+ e.getSQLState());
			
			//Codigo de error del Drivers. Especifico para cada driver (N�merico)
		    System.err.println("El cod de error  " +
		    		"en SQuery es " + e.getErrorCode());
		}
		finally
		{
			return cnn;
		}
    }
	public void cerrarConexion()
	{
		try
		{
			cnn.close(); //(Lanza una SQLException)
		}
		catch(SQLException e)
		{

			System.err.println("Error=" +e.getMessage());
			
			
			System.err.println("Estado sql=" + e.getSQLState());
			
			
		    System.err.println("El cod de error  en SQuery es " + e.getErrorCode());
		}
	}
	
}