package datos.service;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import datos.conexion_dao.ConexionSqlServerNativo;
import datos.interface_dao.InterfacePersonalDao;
import modelo.Nominas;
import modelo.Personal;

public class PersonalService implements InterfacePersonalDao{
	
	public List<Personal> mostrarTodos(){
		
		List<Personal>personas=new ArrayList<Personal>();		
		
		//ConexionMySql conexion=
		//		new ConexionMySql();		
		
		//ConexionSqlServer conexion=
		//		new ConexionSqlServer();			
		
		//ConexionAccess conexion=
		//		new ConexionAccess();
		
		//ConexionOracle conexion=
		//		new ConexionOracle();
		
		//ConexionOracleNativo conexion=
		//	new ConexionOracleNativo();
		
		ConexionSqlServerNativo conexion=new ConexionSqlServerNativo();	
		
		Connection cn=conexion.abrirConexion();
		
		try{
			
		   Statement stmt=cn.createStatement();
		   ResultSet rs=null;
		   String sQuery="Select personal.dni,nombre,apellido,direccion,sueldo,plus from personal,nominas "+
			   "where personal.dni=nominas.dni order by personal.dni";	
		   rs=stmt.executeQuery(sQuery); // Lanza una SQLException)
		   
		   while(rs.next()){
			   
			   Personal persona=new Personal();
			   Nominas nomina=new Nominas();

			   crearBeanPersonal(rs,persona,nomina);
			   personas.add(persona); 
		   }  
		   
		}
		catch (SQLException e){
			
			//Describe el error (String)
			System.err.println("Error=" +e.getMessage());
			System.err.println("causa=" +e.getCause());
	
		}
		conexion.cerrarConexion();
		return personas;
	}
	
	@SuppressWarnings("finally")
	public Personal mostrarUno(int dni){
		
		Personal persona=null;
		
		ConexionSqlServerNativo conexion=new ConexionSqlServerNativo();	
		
		//ConexionMySql conexion=
		//		new ConexionMySql();	
		
		//ConexionSqlServer conexion=
		//		new ConexionSqlServer();	
		
		//ConexionAccess conexion=
		//		new ConexionAccess();
		
		//ConexionOracle conexion=
		//		new ConexionOracle();
		
		//ConexionOracleNativo conexion=
		//		new ConexionOracleNativo();
		
		Connection cn=conexion.abrirConexion();
			
		try{
			
			Statement stmtBuscarUno=cn.createStatement();
			String sQuery="Select * from personal,nominas where personal.dni=nominas.dni and personal.dni=" + dni;	
				
			ResultSet rsBuscarUno=null;
			// Lanza una SQLException)
			rsBuscarUno=stmtBuscarUno.executeQuery(sQuery);
			
			if(rsBuscarUno.next()){
				persona=new Personal();
				Nominas nomina= new Nominas();	
				crearBeanPersonal(rsBuscarUno,persona,nomina);
			}
		}
		catch (SQLException e){
			//Describe el error (String)
			System.err.println("Error=" +e.getMessage());
			//System.err.println("MostrarUno");
		}
		finally{
			
			conexion.cerrarConexion();
			return (persona);
		}
	}
	
	@SuppressWarnings("finally")
	public boolean grabarSql(Personal persona){

		boolean resultado=false;
				
		ConexionSqlServerNativo conexion=new ConexionSqlServerNativo();
		
		//ConexionMySql conexion=
		//		new ConexionMySql();
		
		//ConexionSqlServer conexion=
		//		new ConexionSqlServer();	
		
		//ConexionAccess conexion=
		//		new ConexionAccess();
		
		//ConexionOracle conexion=
		//		new ConexionOracle();
		
		//ConexionOracleNativo conexion=
		//		new ConexionOracleNativo();
		
		Connection cn=conexion.abrirConexion();
		
		/*Si no permitiera nulos la tabla	
		String sQuerypersonal="insert into personal (dni,nombre,"
				+ "apellido,direccion) values (" +
		  + persona.getDni()
			+ ",'" + persona.getNombre() + "','" 
				+ persona.getApellido() + "','" 
					+ persona.getDireccion() + "')";
		*/
		
		String sQuerypersonal="insert into personal (dni,nombre,apellido,direccion) values ("+persona.getDni()+",";
		
		if(persona.getNombre().isEmpty())sQuerypersonal += null + ",";	
		else sQuerypersonal += "'" + persona.getNombre() + "'," ;
		
		if(persona.getApellido().isEmpty())sQuerypersonal += null + ",";	
		else sQuerypersonal += "'" + persona.getApellido() + "'," ;
		
		if(persona.getDireccion().isEmpty())sQuerypersonal += null + ")";
		else sQuerypersonal += "'" + persona.getDireccion() + "')" ;
		
		String sQuerynominas="insert into nominas values("+persona.getDni()+","+ persona.getNomina().getSueldo()+","+persona.getNomina().getPlus()+")";
	
		try{
			
			//Principio de Transacion
			//Lanza siempre una SQLException
			cn.setAutoCommit(false);
		
			//Ejecuta una actualizacion
			//(devuelve el numero de registros grabados)
			// Lanza una SQLException) en caso de error
			Statement stmtGrabarSql=cn.createStatement();
			stmtGrabarSql.executeUpdate(sQuerypersonal); 	
			stmtGrabarSql.executeUpdate(sQuerynominas);
			
			cn.commit(); //Confirmamos la grabaci�n
			resultado= true;
		}

		catch (SQLException e){
			
			//Describe el error (String)
			System.err.println("Error=" +e.getMessage());
			try
			{
				//Lanza siempre una SQLException
				cn.rollback(); //Deshacemos la grabaci�n
			} 
			catch(SQLException e2){}
			
		}
		finally {
			
			try{
				//Lanza siempre una SQLException
				cn.setAutoCommit(true);
				conexion.cerrarConexion();
			}
			catch(SQLException e){
				
				System.err.println("Error=" +e.getMessage());
			}
			return (resultado);
		}
	}
	@SuppressWarnings("finally")
	public int modificarSentenciasPreparadas(List<Personal>per){
		
		//ConexionMySql conexion=
		//		new ConexionMySql();
		
		//ConexionSqlServer conexion=
		//		new ConexionSqlServer();			
		
		//ConexionAccess conexion=
		//		new ConexionAccess();
		
		//ConexionOracle conexion=
		//		new ConexionOracle();
		
		//ConexionOracleNativo conexion=new ConexionOracleNativo();
		
		ConexionSqlServerNativo conexion=new ConexionSqlServerNativo();
		
		Connection cn=conexion.abrirConexion();
		PreparedStatement pstmt=null;
		
		String sUpdate="update personal set nombre=?"+",apellido=? where dni=?";
		int numero=0;
		
		try{
			cn.setAutoCommit(false);
			pstmt=cn.prepareStatement(sUpdate);
			Iterator<Personal> itPersonas=per.iterator();
			
			while (itPersonas.hasNext()){
				
				Personal p=itPersonas.next();
				
				if(p.getNombre()=="")pstmt.setString(1,null);
				else pstmt.setString(1,p.getNombre());
				
				if(p.getApellido()=="")pstmt.setString(2,null);
				else pstmt.setString(2,p.getApellido());
				
				pstmt.setInt(3,p.getDni());
	
				pstmt.setInt(3,p.getDni());
				numero+=pstmt.executeUpdate();
			}		
			cn.commit(); 
			cn.setAutoCommit(true);
			
		}
		catch(SQLException e){
			numero=0;
			cn.rollback();
			System.err.println("Error=" +e.getMessage());
		}
		finally{
			
			conexion.cerrarConexion();
			return numero;
		}			
	}
	

	@SuppressWarnings("finally")
	public Vector<String> mostrarVectorDni (){
		
		//ConexionMySql conexion=
		//		new ConexionMySql();
		
		//ConexionSqlServer conexion=
		//		new ConexionSqlServer();	
		
		//ConexionAccess conexion=
		//		new ConexionAccess();
		
		//ConexionOracle conexion=
		//		new ConexionOracle();
		
		//ConexionOracleNativo conexion=new ConexionOracleNativo();
		
		ConexionSqlServerNativo conexion=new ConexionSqlServerNativo();
		
		Connection cn=conexion.abrirConexion();
		
		Vector<String> vectorDni=new Vector<String>();	
		
		try{
			
			Statement stmtVectorDni=cn.createStatement();
			
			String sQuery="Select personal.dni from personal,nominas where personal.dni=nominas.dni order by personal.dni";
				
			ResultSet rsDni=null;
			// Lanza una SQLException)
			rsDni=stmtVectorDni.executeQuery(sQuery); 
				
			while(rsDni.next()){
				
				vectorDni.add(String.valueOf(
				rsDni.getInt("dni")));
			}					
		}
		catch(SQLException e){
			
			vectorDni=null;
			System.err.println("Error=" +e.getMessage());
		}
		finally{
			
			conexion.cerrarConexion();
			return vectorDni;
		}
	}
	@SuppressWarnings("finally")
	public int borrarDniLotes(Vector<String> vector){
		
		//ConexionMySql conexion=
		//		new ConexionMySql();
		
		//ConexionSqlServer conexion=
		//		new ConexionSqlServer();	
		
		//ConexionAccess conexion=
		//		new ConexionAccess();
		
		//ConexionOracle conexion=
		//		new ConexionOracle();
		
		//ConexionOracleNativo conexion=new ConexionOracleNativo();
		
		ConexionSqlServerNativo conexion=new ConexionSqlServerNativo();
		
		Connection cn=conexion.abrirConexion();
		
		int contador=0;
		
		try{
			
			Statement stmtVectorLotes=cn.createStatement();
			cn.setAutoCommit(false);
			
			for(int a=0;a<vector.size();a++){
				
				String sBorrar_nominas="delete from nominas where dni="+vector.get(a);
				
				String sBorrar_personal="delete from personal where dni=" +vector.get(a);
				
				//A�adimo la orden de borrar al proceso por lotes
				stmtVectorLotes.addBatch(sBorrar_nominas);
				stmtVectorLotes.addBatch(sBorrar_personal);
			}
			
			int arr[]=stmtVectorLotes.executeBatch();
			cn.commit();
			contador=0;
			
			for(int a=0;a<arr.length;a++){
				
				contador+=arr[a];
			}
				
		}//Fin Try
		
		catch(SQLException e){
			contador=0;
			cn.rollback();
			System.err.println("Error=" +e.getMessage());
		}
		
		finally{
			
			try {
				
				cn.setAutoCommit(true);
				conexion.cerrarConexion();
			} 
			catch (SQLException e) {
				
				e.printStackTrace();
			}
			return contador;
		}
	}
	
	@SuppressWarnings("finally")
	
	public int grabarResultSet(Personal persona){
		
        /******Constantes de ResulSet*********
        - Primer argumento
           * TYPE_FORWARD_ONLY
                Por defecto
                El cursor solo se nueve hacia adelante
           * TYPE_SCROLL_INSENSITIVE
                No se reflejan los cambios producidos en la base de 
                  datos mientras la hoja estuviese abierta 
                    (Hay que cerrar y volver a abrir)
           * TYPE SCROLL_SENSITIVE
                Si se reflejan los cambios producidos en la base de 
                  datos mientras la hoja estuviese abierta 
                    (se refrescan los cambios en los datos de forma automatica)
        - Segundo argumento
           * CONCUR_READ_ONLY
                Por defecto
                Solo de lectura
           * CONCUR_UPDATABLE
                Lectura y escritura
        *********/
		
		 ResultSet rs=null;
	     ResultSet rspersonal=null;
	     ResultSet rsnominas=null;
	     Statement stmt=null;
	     
	     int resultado=0;
	     Statement stmtSoloLectura=null;
		 
		 //ConexionMySql conexion=
		 //		new ConexionMySql();
	     
	     //ConexionSqlServer conexion=
		 //		new ConexionSqlServer();	
		
		 //ConexionAccess conexion=
		 //		new ConexionAccess();
	     
		 //ConexionOracle conexion=
		 //		new ConexionOracle();
	     
		// ConexionOracleNativo conexion=
		 //		new ConexionOracleNativo();
	     
	     ConexionSqlServerNativo conexion=new ConexionSqlServerNativo();
			
		 Connection cn=conexion.abrirConexion();
		 try
	     {
	    	stmtSoloLectura=cn.createStatement();    	  
	                
            int dni=persona.getDni();
            
            rs=stmtSoloLectura.executeQuery
            	("Select count(personal.dni)from " +
            		"personal,nominas where personal.dni=nominas.dni "+
            			"and personal.dni=" +dni);            
	            
           rs.next();          
            
          //Comprobamos si ya existe. Lanza una SQLException
           if (rs.getString(1).equals("1"))
        	   resultado=0; //Ya existe           
                    
	       else //No existe
           {
	    	   	 //	Solo SqlServerNativo
	    	   	 stmt=cn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
	    	   	 
	    	   	/* 
	    	   	   * //Para todos menos SqlNativo
	    	   	  	stmt=cn.createStatement
                 	(ResultSet.TYPE_SCROLL_INSENSITIVE, 
                        ResultSet.CONCUR_UPDATABLE);
	    	   	   */
            	 
            	 int sueldo=persona.getNomina().getSueldo();
                 int plus=persona.getNomina().getPlus();
                 String nombre=persona.getNombre();
                 String apellido=persona.getApellido();
                 String direccion=persona.getDireccion();
                 
                 //Creamos el ResulSet de escritura
                 //Todos menos Oracle nativo
                 //rspersonal=stmt.executeQuery("Select * from personal");
                 
                 //Obligatorio para Oracle nativo
                 rspersonal=stmt.executeQuery("select dni,nombre,apellido,direccion from personal");
        
                 //Nos colocamos al final del ResulSet
               
                 rspersonal.moveToInsertRow();
                 //Actualizamos los campos del ResulSet
                 
                 rspersonal.updateInt("dni",dni);
                 
                 if(nombre.equals(""))
                	 rspersonal.updateString("nombre",null);
                 else
                	 rspersonal.updateString("nombre",nombre);
                 if(apellido.equals(""))
                	 rspersonal.updateString("apellido",null);
                 else                	 
                	 rspersonal.updateString("apellido",apellido);
                 
                 if(direccion.equals("")) 
                	 rspersonal.updateString("direccion",null);
                 else
                	 rspersonal.updateString("direccion",direccion);
                 //Grabamos los datos en la tabla
                 rspersonal.insertRow();
                  
                 //Obligatorio terminar de grabar en una tabla antes de 
                 // empezar a grabar en otra.
                 //Para todos menos Oracle Nativo
                 //rsnominas=stmt.executeQuery("Select * from nominas");  
                 
                 //Obligatorio para Oracle nativo
                 rsnominas=stmt.executeQuery("Select dni,sueldo,plus from nominas");  
                
                 rsnominas.moveToInsertRow();
                
                 rsnominas.updateInt("dni",dni);
                 rsnominas.updateInt("sueldo",sueldo);
                 rsnominas.updateInt("plus",plus);
                 rsnominas.insertRow();
              		
                 resultado=1;
            }
	     }//Fin try
        catch (SQLException e){
        	
            System.err.println("Error=" + e.getMessage());
            
            try{
              //Para deshacer los cambios (No es una transacion, ya que solo
              // afecta a un ResultSet, Ej si el error s�lo es en nominas, el
              // registro se ha grabado en personal
                rspersonal.cancelRowUpdates();
                rsnominas.cancelRowUpdates();
                resultado=2;
            }
            catch(SQLException e2){
                //"Error en grabacion";
            	resultado=2;
            }                 
         }	
		finally{
			
			conexion.cerrarConexion();
			return resultado;
		}
	}
	
	public int borrarResultSet(int dni){
		
		 ResultSet rsLectura=null;
	     ResultSet rspersonal=null;
	     ResultSet rsnominas=null;
	     Statement stmtEscritura=null;
	     Statement stmtSoloLectura=null;
	     
	     int resultado=0;
	     
		 //ConexionMySql conexion=
		 //		new ConexionMySql();
	     
	     //ConexionSqlServer conexion=
		 //		new ConexionSqlServer();	
			
		 //ConexionAccess conexion=
		 //		new ConexionAccess();
	     
		 //ConexionOracle conexion=
		 //		new ConexionOracle();
	     
		 //ConexionOracleNativo conexion=new ConexionOracleNativo();
	     
	     ConexionSqlServerNativo conexion=new ConexionSqlServerNativo();
		
	     Connection cn=conexion.abrirConexion();
		 
	     try{
	    	 
	    	stmtSoloLectura=cn.createStatement();
	    	
      		rsLectura=stmtSoloLectura.executeQuery("Select count(*) from personal,nominas where " +"personal.dni=nominas.dni and " +"personal.dni=" + dni);    
            		
            rsLectura.next();
            
            if(!rsLectura.getString(1).equals("0")){ //Existe el dni
          		
            	// Solo SqlServerNativo
          		stmtEscritura=cn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
	    	   	 
	    	   	/* 
	    	   	   * //Para todos menos SqlNativo
	    	   	  	stmt=cn.createStatement
                	(ResultSet.TYPE_SCROLL_INSENSITIVE, 
                       ResultSet.CONCUR_UPDATABLE);
	    	   	   */
          		
          		//Para todos menos Oracle Nativo y Oracle Microsoft
          		//rsnominas=stmtEscritura.executeQuery
            	//  ("Select * from nominas where dni=" + dni);
          		
          		//Obligatorio para Oracle nativo
          		rsnominas=stmtEscritura.executeQuery
            			("select dni,nombre,apellido,direccion from personal where dni=" + dni);
                rsnominas.absolute(1);
                //Funciona
                //rsnominas.next();
                rsnominas.deleteRow();
            	
            	//Para todos menos Oracle Nativo  y Oracle Microsoft
                // rspersonal=stmtEscritura.executeQuery
                //  ("Select * from personal where dni=" + dni);
            	
            	//Obligatorio para Oracle nativo y Oracle Microsoft
                rspersonal=stmtEscritura.executeQuery
                        ("Select dni,sueldo,plus from nominas where dni=" + dni);
                //Para colocarse en una fila
                rspersonal.absolute(1);
                
                //Funciona
                //rspersonal.next();
                rspersonal.deleteRow();         
                resultado=1;               
                
            }
            else{ //No existe
                
                 resultado=0;
            }
        }
        catch (SQLException e){
        	
            System.err.println("Error=" + e.getMessage());
            try{
            	
              //Para deshacer los cambios (No es una transacion, ya que solo
              // afecta a un ResultSet, Ej si el error s�lo es en nominas, el
              // registro se ha borrado en personal
                rspersonal.cancelRowUpdates();
                rsnominas.cancelRowUpdates();
            }
            catch(SQLException e2){
            	
                resultado=2;
            }
	     }
		conexion.cerrarConexion();	
		return resultado;		
	}
	public boolean modificarResultSet(Personal persona){
		
		ResultSet rspersonal=null;
	    ResultSet rsnominas=null;
	    
	    Statement stmtEscritura=null;
	    
		 //ConexionMySql conexion=
		 //		new ConexionMySql();
	    
	    //ConexionSqlServer conexion=
		//		new ConexionSqlServer();	
			
		 //ConexionAccess conexion=
		 //		new ConexionAccess();
	    
		 //ConexionOracle conexion=
		 //		new ConexionOracle();
	    
		//ConexionOracleNativo conexion=
			//	new ConexionOracleNativo();
	    
	    ConexionSqlServerNativo conexion=new ConexionSqlServerNativo();
		 
	     Connection cn=conexion.abrirConexion();
		 try
	     {
	    	   stmtEscritura=cn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
	    	   //Creamos el ResulSet de escritura
	           //Obligatorio posicionarse en la fila a modificar
	           rspersonal=stmtEscritura.executeQuery
	        	   ("Select nombre,apellido,direccion from personal where dni=" + persona.getDni());
	           rspersonal.absolute(1);
	           
	          
	           //Actualizamos todos los campos del resulset (obligatorio)
	           if(persona.getNombre().equals(""))
	        	   rspersonal.updateString("nombre",null);
	           else
	        	   rspersonal.updateString("nombre",persona.getNombre());
	           
	           if(persona.getApellido().equals(""))
	        	   rspersonal.updateString("apellido",null);
	           else
	        	   rspersonal.updateString("apellido",persona.getApellido());
	           
	           if(persona.getDireccion().equals(""))
	        	   rspersonal.updateString("direccion",null);
	           else
	        	   rspersonal.updateString("direccion",persona.getDireccion());
	           
	           //Grabamos los datos en la tabla
	           rspersonal.updateRow();
	                  
	           //Obligatorio terminar de grabar en una tabla antes de 
	           // empezar a actualizar en otra.
	           rsnominas=stmtEscritura.executeQuery
	        	("Select sueldo,plus from nominas where dni=" + persona.getDni());                    
	           rsnominas.absolute(1);                        
	           rsnominas.updateInt("sueldo",persona.getNomina().getSueldo());
	           rsnominas.updateInt("plus",persona.getNomina().getPlus());
	           rsnominas.updateRow();
	           return true;
	     }
         catch(SQLException e2)
         {
        	try 
        	{
				 rspersonal.cancelRowUpdates();
				 rsnominas.cancelRowUpdates();
			} 
        	catch (SQLException e) 
			{
				System.err.println("Error=" + e.getErrorCode());
				e.printStackTrace();
			}
        	conexion.cerrarConexion();
        	return false;
         }	
	}
	
	@SuppressWarnings("unused")
	private void crearBeanPersonal(ResultSet rsCrearUno,Personal persona, Nominas nomina) throws SQLException{	
		
	   persona.setDni(rsCrearUno.getInt("dni"));
	   nomina.setDni(persona.getDni());
	   
	   String nombre=rsCrearUno.getString("nombre");
	   if(nombre==null) nombre="No grabado";
	   persona.setNombre(nombre);
	   
	   String apellido=rsCrearUno.getString("apellido");
	   if(apellido==null)apellido="No grabado";
	   persona.setApellido(apellido);
	   
	   String direccion=rsCrearUno.getString("direccion");
	   if(direccion==null) direccion="No grabado";
	   persona.setDireccion(direccion);
	   
	   Integer sueldo=new Integer
			   	((String.valueOf(rsCrearUno.getInt("sueldo"))));
	   if(sueldo==null) sueldo=0;	 
	   
	   nomina.setSueldo(sueldo.intValue());
	   
	   Integer plus=new Integer
			   ((String.valueOf(rsCrearUno.getInt("plus"))));
	   if(plus==null) plus=0;
	   nomina.setPlus(plus.intValue());	   
	 	   
	   persona.setNomina(nomina);	   
	}
	
}
