package datos.interface_dao;

import java.util.List;
import java.util.Vector;

import modelo.Personal;

public interface InterfacePersonalDao {
	
	public List<Personal> mostrarTodos();
	public Personal mostrarUno(int dni);
	public boolean grabarSql(Personal persona);
	public int modificarSentenciasPreparadas(List<Personal>per);
	public Vector<String> mostrarVectorDni ();
	public int borrarDniLotes(Vector<String> vector);
	public int grabarResultSet(Personal persona);
	public int borrarResultSet(int dni);
	public boolean modificarResultSet(Personal persona);

}