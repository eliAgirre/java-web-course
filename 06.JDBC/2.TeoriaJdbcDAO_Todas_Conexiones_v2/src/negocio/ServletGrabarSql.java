package negocio;
import java.io.*;

import modelo.Personal;
import modelo.Nominas;

import javax.servlet.*;
import javax.servlet.http.*;

import datos.interface_dao.InterfacePersonalDao;
import datos.service.PersonalService;
import utilidades.Navegar;

public class ServletGrabarSql extends HttpServlet{
	
	 private static final long serialVersionUID = -963270029877822085L;

	 private InterfacePersonalDao personalService=new PersonalService();

	 public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException{
		 // Para que funcione tanto con el doGet como con el doPost
		 doPost(request,response);
	 }
	 
	 public void doPost(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException{
	  	
		String Salida="";
				
		HttpSession hs=request.getSession(true);
		
		if (!hs.isNew()){
			
			hs.invalidate();
			hs=request.getSession(true);				
		}
		
		
		int dni=Integer.parseInt(request.getParameter("txtdni"));
		int sueldo=Integer.parseInt(request.getParameter("txtsueldo"));
		int plus=Integer.parseInt(request.getParameter("txtplus"));
		
		String nombre=request.getParameter("txtnombre");
		String apellido=request.getParameter("txtapellido");
		String direccion=request.getParameter("txtdireccion");
		
		if(personalService.mostrarUno(dni)==null){
			
			Personal persona=new Personal();
			
			persona.setDni(dni);
			persona.setNombre(nombre);					
			persona.setApellido(apellido);
			persona.setDireccion(direccion);
			
			Nominas nomina= new Nominas();				
			
			nomina.setDni(persona.getDni());										
			nomina.setSueldo(sueldo);
			nomina.setPlus(plus);					
										
			persona.setNomina(nomina);	
			if(personalService.grabarSql(persona))
				Salida="Registro grabado";
			else 
				Salida="Error en grabacion";
		}
		else  Salida="Ya existe el registro";
		
		hs.setAttribute("parametro",Salida);	
		
		Navegar.navegar("salida.jsp",request,response);
		
	}//Fin metodo
 }//Fin de la clase
