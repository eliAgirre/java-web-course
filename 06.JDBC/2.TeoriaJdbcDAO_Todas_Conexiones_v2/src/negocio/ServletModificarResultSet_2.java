package negocio;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

import modelo.Personal;
import datos.interface_dao.InterfacePersonalDao;
import datos.service.PersonalService;
import utilidades.Navegar;

public class ServletModificarResultSet_2 extends HttpServlet
{

	 private static final long serialVersionUID = 1L;

     public void doGet(HttpServletRequest request,
        HttpServletResponse response)
			throws ServletException, IOException
     {
    	 // Para que funcione tanto con el doGet como con el doPost
    	 doPost(request,response);
     }
     public void doPost(HttpServletRequest request,
        HttpServletResponse response)
			throws ServletException, IOException 
    {
       String salida="";
       
       HttpSession hs=request.getSession(true);
	   if (hs.isNew())
	   {
			hs.invalidate();
			hs=request.getSession(true);
	   }
	   InterfacePersonalDao operar =new PersonalService();		 
		   	  
	   Personal persona=(Personal)hs.getAttribute("bean");
	   
	   persona.setNombre(request.getParameter("txtnombre"));
	   persona.setApellido(request.getParameter("txtapellido"));
	   persona.setDireccion(request.getParameter("txtdireccion"));
	   persona.getNomina().setSueldo(Integer.parseInt
			   (request.getParameter("txtsueldo")));
	   persona.getNomina().setPlus(Integer.parseInt
			   (request.getParameter("txtplus")));	   
	   	   
	   if(operar.modificarResultSet(persona))             		
           salida="Registro modificado";
	   else  
		   salida="Error en modificacion";
         
		hs.setAttribute("parametro",salida);

		Navegar.navegar("salida.jsp", request,response);
   	}
}
 
  