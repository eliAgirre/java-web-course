<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Mostrar ResultSet</title>
<style>
	.clase1{font-size:20px;color:red;text-align:center;}
</style>
</head>
<script type="text/javascript">

function movimiento(arg)
{
	document.forms[0].txtMovi.value=arg;
	document.forms[0].submit();
}

</script>

<body>
<jsp:useBean id="mibean" class="modelo.Personal" 
	scope="request" />


<form method="post" id="form1" action="ServletBeanResultset" >

<table cellspacing="2" cellpadding="2" border="0" align="center">
<tr>
    <td colspan="4" class="clase1">Mostrar ResultSet</td>

</tr>
<tr>
    <td colspan="2" align="left"> </td>
    <td colspan="2"> </td>
</tr>
<tr>
    <td colspan="1" align="left">Dni</td>
    <td colspan="2"><input type="text" name="txtdni" size="20" 
			maxlength="20" readonly 
              value="<jsp:getProperty name="mibean" property="dni"/> ">
        </td>
                        
</tr>
<tr>
    <td colspan="1" align="left">Nombre</td>
    <td colspan="2"><input type="text" name="txtnombre" size="20" 
		maxlength="20" readonly 
           value="<jsp:getProperty name="mibean" property="nombre"/>" ></td>
                        
</tr>
<tr>
    <td colspan="1" align="left">Apellido</td>
    <td colspan="2"><input type="text" name="txtapellido" size="20"
		 maxlength="20" readonly 
          value="<jsp:getProperty name="mibean" property="apellido"/>"></td>
</tr>
<tr>
    <td colspan="1" align="left">Direccion</td>
    <td colspan="2"><input type="text" name="txtdireccion" size="20"
		 maxlength="20" readonly 
           value="<jsp:getProperty name="mibean" property="direccion"/>"></td>
</tr>
<tr>
<%
String sueldo=String.valueOf(mibean.getNomina().getSueldo());
String plus=String.valueOf(mibean.getNomina().getPlus());
%>
    <td colspan="1" align="left">Sueldo</td>
    <td colspan="2"><input type="text" name="txtsueldo" size="20"
		maxlength="9" readonly 
          value="<%=sueldo%>"></td>
</tr>
<tr>
    <td colspan="1" align="left">Plus</td>
    <td colspan="2"><input type="text" name="txtplus" size="9" 
		  maxlength="9" readonly
          value="<%=plus%>"></td>
</tr>
<tr>
    <td align="center"><input type="button" name="irprimero"
		 value="Primero" onclick="movimiento('0');"></td>

    <td align="center"><input type="button" name="iranterior" 
		value="Anterior" onclick="movimiento('1');"></td>
              
    <td align="center"><input type="button" name="irsigiente" 
		value="Siguiente" onclick="movimiento('2');"></td>

    <td align="center"><input type="button" name="irultimo" 
		value="Ultimo" onclick="movimiento('3');"></td>

</tr>
<tr>
      <td colspan="4" align="center"><input type="button"  name="btnVolver"
           value="Volver" onclick="document.location.href='inicio.html';"></td>
</tr>
</table>

<input type="hidden" name="txtMovi" id="txtMovi" >
</form>
</body>
</html>

