package negocio;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

import datos.interface_dao.InterfacePersonalDao;
import datos.service.PersonalService;
import utilidades.Navegar;

public class ServletGrabarSql extends HttpServlet{
	
	 private static final long serialVersionUID = -963270029877822085L;

	 private InterfacePersonalDao personalService=new PersonalService();

	 public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		 doPost(request,response);
	 }
	 
	 public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  	
		String sSalida="";
				
		HttpSession sesion=request.getSession(true);
		
		if (!sesion.isNew()){
			
			sesion.invalidate(); // limpia la sesion
			sesion=request.getSession(true);
		}
		
		String prov=request.getParameter("txtprov");			
		
		if(personalService.existeProvincia(prov)==false){ // si no existe la provincia
			sSalida="No existe la provincia "+prov;
		}
		else{
			if(personalService.grabaCombinadoSql(prov)) sSalida="Registro grabado";
			else sSalida="Error en grabacion";
		}
		
		sesion.setAttribute("parametro",sSalida);	
		
		Navegar.navegar("salida.jsp",request,response);
		
	}//Fin metodo
	 
 }//Fin de la clase
