package modelo;

public class Personal 
{
	private int dni;
	private String nombre;
	private String apellido;
	private String direccion;
	
	private Nominas nomina;

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Nominas getNomina() {
		return nomina;
	}

	public void setNomina(Nominas nomina) {
		this.nomina = nomina;
	}

	@Override
	public String toString() {
		return "Personal [dni=" + dni + ", nombre=" + nombre + ", apellido="
				+ apellido + ", direccion=" + direccion + ", nomina=" + nomina
				+ "]";
	}
	
	
}
