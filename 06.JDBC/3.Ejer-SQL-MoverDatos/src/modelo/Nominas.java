package modelo;

public class Nominas 
{
	private int dni;
	private int sueldo;
	private int plus;
	
	public int getDni() {
		return dni;
	}
	public void setDni(int dni) {
		this.dni = dni;
	}
	public int getSueldo() {
		return sueldo;
	}
	public void setSueldo(int sueldo) {
		this.sueldo = sueldo;
	}
	public int getPlus() {
		return plus;
	}
	public void setPlus(int plus) {
		this.plus = plus;
	}
	@Override
	public String toString() {
		return "Nominas [dni=" + dni + ", sueldo=" + sueldo + ", plus=" + plus
				+ "]";
	}	
}
