package datos.service;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import org.apache.commons.lang.StringEscapeUtils;

import datos.conexion_dao.ConexionSqlServerNativo;
import datos.interface_dao.InterfacePersonalDao;

public class PersonalService implements InterfacePersonalDao {
	
	public boolean existeProvincia(String provincia){
		
		boolean existe=false;
		
		ConexionSqlServerNativo conexion=new ConexionSqlServerNativo();
		
		Connection cn=conexion.abrirConexion();
		
		String sQueryDni="select dni from personal where provincia='"+StringEscapeUtils.escapeSql(provincia)+"'";
		
		try	{
			Statement stmtGetDni=cn.createStatement();
			ResultSet rsDni=stmtGetDni.executeQuery(sQueryDni);
			
			if(rsDni.next()){ // si hay datos
				existe=true;
			}
		}
		catch(SQLException e){
			System.err.print(e.getMessage());
		}
		finally{
			conexion.cerrarConexion();
		}
		
		
		return existe;
	}
	
	public boolean grabaCombinadoSql (String provincia){
		
		boolean grabado=true;
		
		ConexionSqlServerNativo conexion=new ConexionSqlServerNativo();
	
		Connection cn=conexion.abrirConexion();
		
		String sQueryDni="select dni from personal where provincia='"+StringEscapeUtils.escapeSql(provincia)+"'";
		
		int dni=0;
		String nombre="";
		int total=0;
		
		Vector<Integer> listaDni=new Vector<Integer>();
			
		try	{
			
			cn.setAutoCommit(false);
			
			Statement stmtGetDni=cn.createStatement();
			ResultSet rsDni=stmtGetDni.executeQuery(sQueryDni);
			
			while(rsDni.next()){
			
				listaDni.add(rsDni.getInt(1));
				
			}
			
			String sQueryInfo = "select nombre, sueldo, plus from personal, nominas where personal.dni=? and nominas.dni=?";
			
			PreparedStatement pstmt=null;
			Statement stmt=null;
			
			pstmt=cn.prepareStatement(sQueryInfo);
			
			for (int i=0;i<listaDni.size();i++){
				
				dni=listaDni.get(i); // guarda cada dni del vector
				
				pstmt.setInt(1, dni); // personal
				pstmt.setInt(2, dni); // nominas
				
				ResultSet rsQueryInfo = pstmt.executeQuery();
				rsQueryInfo.next();
				
				nombre=rsQueryInfo.getString("nombre");
				total=rsQueryInfo.getInt("sueldo")+rsQueryInfo.getInt("plus");
				
				String sQueryInsert = "insert into replica (dni, nombre, provincia, total) values("+dni+", '"+nombre+"', '"+provincia+"', "+total+")";
				stmt=cn.createStatement();
				stmt.execute(sQueryInsert);
				
				String sBorrar_nominas= "delete from nominas where dni=" + dni;
				String sBorrar_personal= "delete from personal where dni=" + dni;
				
				stmt.execute(sBorrar_nominas); //primero nominas por dependecia
				stmt.execute(sBorrar_personal);		
			
			} // cierre for
		
			cn.commit(); 
			
			
		}catch(SQLException e){

			try {
				
				cn.rollback();
				grabado = false;
				
			} catch (SQLException e1) { e1.printStackTrace();}
			
			System.err.println("Error=" +e.getMessage());
			
		}
		finally{
			
				try {
					
					cn.setAutoCommit(true);
					conexion.cerrarConexion();
					
				} catch (SQLException e) { e.printStackTrace(); }
	
		}	
		
		return grabado;
		
	}

}

