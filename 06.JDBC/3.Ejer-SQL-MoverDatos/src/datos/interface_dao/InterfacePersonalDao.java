package datos.interface_dao;

public interface InterfacePersonalDao {
	
	public boolean grabaCombinadoSql (String provincia);
	public boolean existeProvincia(String provincia);

}
