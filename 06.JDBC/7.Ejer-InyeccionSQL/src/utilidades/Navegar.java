package utilidades;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Navegar 
{
	public static void navegar
		(String direccion,HttpServletRequest req,
			HttpServletResponse resp)
				throws ServletException, IOException 
	{
	
		//Lanza la pagina
		RequestDispatcher despachador=
				req.getRequestDispatcher(direccion);
		despachador.forward(req,resp); 
		
	}

}
