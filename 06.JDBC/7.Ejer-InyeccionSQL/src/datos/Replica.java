package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.lang.StringEscapeUtils;

public class Replica {

	@SuppressWarnings("finally")
	public int replica(String provincia) {
		
		ConexionSqlServerNativo conexion = new ConexionSqlServerNativo();
		
		Connection cn = conexion.abrirConexion();
		
		int numero=0;
		
		try {
			Statement stmt=cn.createStatement();
			
			ResultSet rs=null;
			
			String	queryOrigen="select  personal.dni,nombre, (sueldo+ plus) "
					+ "as 'total' from personal,nominas " 
					+ 	"where personal.dni = nominas.dni "
					+ 	"and provincia='" 
					+ StringEscapeUtils.escapeSql(provincia) + "';";
			
			rs = stmt.executeQuery(queryOrigen);
			
			PreparedStatement pstmt= null;
			
			String queryMover = "insert into replica values (?,?,?,?)";
			
			cn.setAutoCommit(false);
			pstmt=cn.prepareStatement(queryMover);
			
			while(rs.next()) {
				
				pstmt.setInt(1, rs.getInt("dni"));
				pstmt.setString(2, rs.getString("nombre"));
				pstmt.setString(3, provincia);
				pstmt.setInt(4, rs.getInt("total"));
				
				numero+=pstmt.executeUpdate();
				
			}
			
			
			String	queryDeleteNominas="delete from nominas "
					+ 	"where dni in (select dni from personal where "
					+ 	"provincia='" 
					+ StringEscapeUtils.escapeSql(provincia) + "');";
			
			
			String	queryDeletePersonal="delete from personal where "
					+ 	"provincia='" 
					+ StringEscapeUtils.escapeSql(provincia) + "';";
			
			int numBorradosNominas = stmt.executeUpdate(queryDeleteNominas);
			int numBorradosPersonal = stmt.executeUpdate(queryDeletePersonal);
			
			if(numero==numBorradosPersonal)
				cn.commit();
			else 
				cn.rollback();
			
		} catch (SQLException e) {
			numero=0;
			cn.rollback();
			e.printStackTrace();
		}
		finally {
			
			try {
				cn.setAutoCommit(true);
				conexion.cerrarConexion();
			} catch (SQLException e) {

				e.printStackTrace();
			}
			return numero;
		}
		
		
	}

}
