package datos.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.commons.lang.StringEscapeUtils;

import modelo.Cliente;
import modelo.Pc;
import datos.conexion_dao.ConexionSqlServerNativo;
import datos.interface_dao.InterfaceDaoCliente;

public class ServiceCliente implements InterfaceDaoCliente {
	
	private static final int PRECIOMICRO=100;
	private static final int PRECIOPANTALLA=50;

	@SuppressWarnings("finally")
	@Override
	public boolean grabarSqlCliente(Cliente cliente) {
		
		boolean resultado=false;
		
		ConexionSqlServerNativo conexion=new ConexionSqlServerNativo();
		
		Connection cn=conexion.abrirConexion();
		
		String sQueryCliente="INSERT INTO clientes (dni,nombre,apellido,micro,pantalla,total) VALUES ('"+cliente.getDni()+"',";
		
		if(cliente.getNombre().isEmpty()){
			sQueryCliente+=null + ",";	
		}
		else{
			sQueryCliente+="'"+cliente.getNombre()+"',";
		}
		
		if(cliente.getApellido().isEmpty()){
			sQueryCliente+=null+",";	
		}
		else{
			sQueryCliente+="'"+cliente.getApellido()+"',";
		}
		

		sQueryCliente+=cliente.getPc().getMicro()+",";
		sQueryCliente+=cliente.getPc().getPantalla()+",";
		
		if(cliente.getPc().getTotal()==0){
			sQueryCliente+=null+")";
		}
		else{
			sQueryCliente+=cliente.getPc().getTotal()+")";
		}
		
		try{
			cn.setAutoCommit(false);
			Statement stmtGrabarSql=cn.createStatement();
			stmtGrabarSql.executeUpdate(sQueryCliente);
			cn.commit(); //Confirmamos la grabación
			resultado=true;
			
		}catch (SQLException e){
			
			System.err.println("Error: "+e.getMessage());
			
			try{
				cn.rollback(); //Deshacemos la grabación
			}
			catch(SQLException e2){}
			
		}
		finally {
			
			try{
				
				//Lanza siempre una SQLException
				cn.setAutoCommit(true);
				conexion.cerrarConexion();
			}
			catch(SQLException e){
				
				System.err.println("Error: " +e.getMessage());
			}
			return (resultado);
			
		}
		
	} // grabarSqlDatosCliente
	
	@Override
	public List<Cliente> mostrarTodos() {
		
		List<Cliente> clientes=new ArrayList<Cliente>();
		
		ConexionSqlServerNativo conexion=new ConexionSqlServerNativo();	
		
		Connection cn=conexion.abrirConexion();
		
		try{
			
			Statement stmt=cn.createStatement();
			ResultSet rs=null;
			String sQuery="SELECT dni,nombre,apellido,micro,pantalla,total FROM clientes ORDER BY dni";
			rs=stmt.executeQuery(sQuery); // Lanza una SQLException)
			
			while(rs.next()){
				
				
				Cliente cliente=new Cliente();
				Pc pc=new Pc();
				
				crearBeanCliente(rs,cliente,pc);
				clientes.add(cliente); // agrega al list el cliente
			}
			
		}catch(SQLException e){
			
			System.err.println("Error: " +e.getMessage());
			System.err.println("causa: " +e.getCause());
		}
		
		conexion.cerrarConexion();
		return clientes;
		
	} // mostrarTodos
	
	@Override
	public Cliente buscarCliente(String dni) {
		
		Cliente cliente=null;
		
		ConexionSqlServerNativo conexion=new ConexionSqlServerNativo();
		
		Connection cn=conexion.abrirConexion();
		
		String sEscapadaDni=StringEscapeUtils.escapeSql(dni);
		
		try{
			
			Statement stmtBuscar=cn.createStatement();
			String sQuery="SELECT dni,nombre,apellido,micro,pantalla,total FROM clientes WHERE dni='"+sEscapadaDni+"';";
			ResultSet rsBuscarUno=null;
			// Lanza una SQLException)
			rsBuscarUno=stmtBuscar.executeQuery(sQuery);
			
			if(rsBuscarUno.next()){
				
				cliente=new Cliente();
				Pc pc=new Pc();
				crearBeanCliente(rsBuscarUno,cliente,pc);
				
			}
			
		}catch (SQLException e){
			
			System.err.println("Error: " +e.getMessage());
		}
		finally{
			conexion.cerrarConexion();
		}
		return cliente;

	} // existeCliente
	
	@Override
	public int borrarCliente(String dni) {
		
		int resultado=0;
		 
		ConexionSqlServerNativo conexion=new ConexionSqlServerNativo();
		 
		Connection cn=conexion.abrirConexion();
		
		String sEscapadaDni=StringEscapeUtils.escapeSql(dni);
		 
		try{
			
			Statement stmtDelete=cn.createStatement();
			String sDelete="DELETE FROM clientes WHERE dni='"+sEscapadaDni+"';";
			resultado=stmtDelete.executeUpdate(sDelete);
			cn.commit();
			
		}catch (SQLException e){
			
			resultado=0;
			try {
				cn.rollback();
				System.err.println("Error: " +e.getMessage());
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
		}
		finally{
			try {
				cn.setAutoCommit(true);
				conexion.cerrarConexion();
			} catch (SQLException e) {

				e.printStackTrace();
			}
		}
		
		return resultado;
		
	} // borrarCliente
	
	@Override
	public Vector<String> mostrarVectorDni() {
		
		ConexionSqlServerNativo conexion=new ConexionSqlServerNativo();
		
		Connection cn=conexion.abrirConexion();
		
		Vector<String> vectorDni=new Vector<String>();
		
		try{
			
			Statement stmtVectorDni=cn.createStatement();
			
			String sQuery="SELECT dni FROM clientes ORDER BY dni";
				
			ResultSet rsDni=null;
			rsDni=stmtVectorDni.executeQuery(sQuery); // Lanza una SQLException)
				
			while(rsDni.next()){
				
				vectorDni.add(rsDni.getString("dni"));
			}					
		}
		catch(SQLException e){
			
			vectorDni=null;
			System.err.println("Error: " +e.getMessage());
		}
		finally{
			
			conexion.cerrarConexion();
		}
		
		return vectorDni;
		
	} // mostrarVectorDni
	
	@Override
	public int borrarDniLotes(Vector<String> vector) {
		
		ConexionSqlServerNativo conexion=new ConexionSqlServerNativo();
		
		Connection cn=conexion.abrirConexion();
		
		int cont=0;
		
		try{
			
			Statement stmtVectorLotes=cn.createStatement();
			cn.setAutoCommit(false);
			
			for(int i=0;i<vector.size();i++){
				
				String sDelete="DELETE FROM clientes WHERE dni='"+vector.get(i)+"';";
				
				stmtVectorLotes.addBatch(sDelete);
			}
			
			int arryBatch[]=stmtVectorLotes.executeBatch();
			cn.commit();
			cont=0;
			
			for(int i=0;i<arryBatch.length;i++){
				
				cont+=arryBatch[i];
			}
			
		}
		catch(SQLException e){
			
			cont=0;
			try {
				cn.rollback();
				System.err.println("Error: " +e.getMessage());
			}
			catch (SQLException e1) { e1.printStackTrace(); }
			
		}
		finally{
			
			try {
				
				cn.setAutoCommit(true);
				conexion.cerrarConexion();
			} 
			catch (SQLException e) {
				
				e.printStackTrace();
			}
		}
		
		return cont;
		
	} // borrarDniLotes
	
	@Override
	public boolean modificarResultSet(Cliente cliente) {
		
		ResultSet rsCliente=null;
		
		Statement stmtEscritura=null;
		
		ConexionSqlServerNativo conexion=new ConexionSqlServerNativo();
		
		Connection cn=conexion.abrirConexion();
		
		try{
			stmtEscritura=cn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			
			rsCliente=stmtEscritura.executeQuery("SELECT nombre,apellido,micro,pantalla,total FROM clientes WHERE dni='" + cliente.getDni()+"';");
			
			rsCliente.absolute(1);
			
			if(cliente.getNombre().equals("")){
				rsCliente.updateString("nombre",null);
			}
			else{
				rsCliente.updateString("nombre",cliente.getNombre());
			}
			
			if(cliente.getApellido().equals("")){
				rsCliente.updateString("apellido",null);
			}
			else{
				rsCliente.updateString("apellido",cliente.getApellido());
			}
			
			rsCliente.updateInt("micro",cliente.getPc().getMicro());
			rsCliente.updateInt("pantalla",cliente.getPc().getPantalla());
			rsCliente.updateInt("total",cliente.getPc().getTotal());
			
			rsCliente.updateRow();
	        return true;
			
		}
		catch(SQLException e2){
			try{
				rsCliente.cancelRowUpdates();
			} 
        	catch (SQLException e){
        		
				System.err.println("Error: " + e.getErrorCode());

			}
        	conexion.cerrarConexion();
		}
	
		return false;
		
	} // modificarResultSet
	
	public int calcularTotal(int iMicro, int iPantalla){
		
		int precioMicro=0;
		int precioPantalla=0;
		
		if(iMicro==0){
			precioMicro=(int) (PRECIOMICRO-(PRECIOMICRO*0.2));
		}else{
			precioMicro=(int) (PRECIOMICRO-(PRECIOMICRO*0.1));
		}
		
		if(iPantalla==0){
			precioPantalla=(int) (PRECIOPANTALLA-(PRECIOPANTALLA*0.05));
		}else{
			precioPantalla=(int) (PRECIOPANTALLA-(PRECIOPANTALLA*0.07));
		}
		
		return precioMicro+precioPantalla;
		
	} // calcularTotal
	
	@SuppressWarnings("unused")
	private void crearBeanCliente(ResultSet rs, Cliente cliente, Pc pc) throws SQLException{
		
		cliente.setDni(rs.getString("dni"));
		
		String sNombre=rs.getString("nombre");
		if(sNombre.isEmpty()) sNombre="No grabado";
		cliente.setNombre(sNombre);
		
		String sApellido=rs.getString("apellido");
		if(sApellido.isEmpty()) sApellido="No grabado";
		cliente.setApellido(sApellido);
		
		Integer iMicro=new Integer((String.valueOf(rs.getInt("micro"))));
		if(iMicro==null) iMicro=-1;
	  	pc.setMicro(iMicro);
	  	
	  	Integer iPantalla=new Integer((String.valueOf(rs.getInt("pantalla"))));
		if(iPantalla==null) iPantalla=-1;
	  	pc.setPantalla(iPantalla);
	  	
	  	Integer iTotal=new Integer((String.valueOf(rs.getInt("total"))));
		if(iTotal==null) iTotal=0;
	  	pc.setTotal(iTotal);
	  	
	  	cliente.setPc(pc);
	  	
	} // crearBeanCliente

}
