package datos.interface_dao;

import java.util.List;
import java.util.Vector;

import modelo.Cliente;

public interface InterfaceDaoCliente {
	
	public boolean grabarSqlCliente(Cliente cliente);
	public List<Cliente> mostrarTodos();
	public Cliente buscarCliente(String dni);
	public int borrarCliente(String dni);
	public Vector<String> mostrarVectorDni ();
	public int borrarDniLotes(Vector<String> vector);
	public boolean modificarResultSet(Cliente cliente);
	public int calcularTotal(int iMicro, int iPantalla);

}