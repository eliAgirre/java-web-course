package utilidades;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;

public class EscribeHtml {
	
	private ServletOutputStream objEscribir;
	
	public EscribeHtml(ServletOutputStream objEscribir) {
		
		super();
		this.objEscribir=objEscribir;
		
	} // constructor
	
	public void escribeCabecera(String titulo) throws ServletException, IOException {
		
		objEscribir.print("<html>");
		objEscribir.print("<head>");
		objEscribir.print("<title>"+titulo+"</title>"); 	
		objEscribir.print("</head>");
		objEscribir.print("<body>");
		
	} // escribeCabecera
	
	public void tablaCabecera() throws ServletException, IOException {
		
		objEscribir.println("<tr>");
		objEscribir.println("<th>DNI</th>");
		objEscribir.println("<th>Nombre</th>");
		objEscribir.println("<th>Apellido</th>");
		objEscribir.println("<th>Micro</th>");
		objEscribir.println("<th>Pantalla</th>");
		objEscribir.println("<th>Total</th>");
		objEscribir.println("</tr>");
		
	} // tablaCabecera
	
	public void escribePie() throws ServletException, IOException {
		
		objEscribir.print("</body>");
		objEscribir.print("</html>");
		
	} // escribePie
}
