package negocio;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utilidades.Navegar;
import modelo.Cliente;
import datos.interface_dao.InterfaceDaoCliente;
import datos.service.ServiceCliente;

public class BuscarServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private InterfaceDaoCliente serviceCliente=new ServiceCliente();

    public BuscarServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		buscar(request,response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		buscar(request,response);
	}
	
	protected void buscar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		String sPagina="";
		
		HttpSession sesion=request.getSession();
		
		String sDni=request.getParameter("dni");
		Cliente cliente=serviceCliente.buscarCliente(sDni);
		
		if(cliente==null){ // si no existe el cliente
			
			sesion.setAttribute("msgError","No existe registro");
			sPagina="salida";
		}
		else{
			
			sesion.setAttribute("clienteUno", cliente);
			sPagina="mostrarCliente";
		}
		
		Navegar.navegar(sPagina, request, response);
		
	}

}
