package negocio;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.interface_dao.InterfaceDaoCliente;
import datos.service.ServiceCliente;
import utilidades.EscribeHtml;

@WebServlet("/MostrarDniServlet")
public class MostrarDniServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private String titulo="Mostrar lista DNI";
	private InterfaceDaoCliente serviceCliente=new ServiceCliente();
       
    public MostrarDniServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		mostrarDni(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		mostrarDni(request,response);
	}
	
	protected void mostrarDni(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html"); // conveniente, no obligatorio
		
		ServletOutputStream out=response.getOutputStream();
		
		EscribeHtml escribe=new EscribeHtml(out); 
		
		escribe.escribeCabecera(titulo);
		
		Vector<String> vector=serviceCliente.mostrarVectorDni();

		boolean resultado=false; // se cambia a true, si encuentra dato
		
		if(vector!=null){ // si no contiene datos
			
			out.println("<form method=post action=BorrarBatchServlet>");
			out.println("Registros por lotes</font>");
			out.println("<p>Seleccione DNI</p>");
			out.println("<select name='listaDni' size='3' multiple>");
			
			int i=0;
			Enumeration<String> enumeracion=vector.elements();
			
			while (enumeracion.hasMoreElements()){ // mientras que haya datos
				
				String sDni=enumeracion.nextElement();
				
				if (!resultado){ // si existe el dato
					
					i++;
					
					if(i==0){
						out.println("<option selected>"+sDni+ "</option>"); // sale uno seleccionado
					}
					out.println("<option>"+sDni+ "</option>"); // salen los demas sin seleccionar
				}
				
			} // cierre while
			
			out.println("</select><br/><br/>");
			out.println("<br/><input type=submit value=Borrar>");
			
		}
		else{
			// si no hay datos
			out.println("<h3>No existen registros<h3><p>");
		}
		out.println("<input type='button' value='Volver'");
		out.println("onClick=document.location.href='login.html'>");
		
		escribe.escribePie();
		
		
	}
}
