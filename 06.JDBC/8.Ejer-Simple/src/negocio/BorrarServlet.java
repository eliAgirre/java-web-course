package negocio;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.Cliente;
import utilidades.Navegar;
import datos.interface_dao.InterfaceDaoCliente;
import datos.service.ServiceCliente;

/**
 * Servlet implementation class BorrarServlet
 */
@WebServlet("/BorrarServlet")
public class BorrarServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private InterfaceDaoCliente serviceCliente=new ServiceCliente();
       
    public BorrarServlet() {
        super();

    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		borrar(request,response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		borrar(request,response);
	}
	
	protected void borrar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		String sPagina="";
		
		HttpSession sesion=request.getSession();
		
		String sDni=request.getParameter("dni");
		Cliente cliente=serviceCliente.buscarCliente(sDni);
		
		if(cliente==null){ // si no existe el cliente
			
			sesion.setAttribute("msgError","No existe registro");
			sPagina="salida";
		}
		else{
			
			int iRegistro=serviceCliente.borrarCliente(sDni);
			sesion.setAttribute("msg", iRegistro+" borrado");
			sPagina="clienteBorrado";
		}
		
		Navegar.navegar(sPagina, request, response);
		
	}

}
