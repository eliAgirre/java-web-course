package negocio;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datos.interface_dao.InterfaceDaoCliente;
import datos.service.ServiceCliente;
import modelo.Cliente;
import modelo.Pc;
import utilidades.Navegar;


public class ResultadoServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private InterfaceDaoCliente serviceCliente=new ServiceCliente();

    public ResultadoServlet() { super();}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		pantalla(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		pantalla(request,response);
	}
	
	private void pantalla(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession sesion=request.getSession();

		Cliente cliente=(Cliente)sesion.getAttribute("cliente");
		
		String sPantalla=request.getParameter("opPantalla");
		
		int iPantalla=0;
		int iTotal=0;
		String sPpagina="";
		String sSalida="";
		
		switch(sPantalla){
		
			case "19":
				iPantalla=0;
				break;
			case "21":
				iPantalla=1;
				break;
			case "23":
				iPantalla=2;
				break;
			default:
				iPantalla=-1;
		}
			
		
		Pc pc=(Pc)sesion.getAttribute("pc");
		
		pc.setPantalla(iPantalla);
		
		iTotal=serviceCliente.calcularTotal(pc.getMicro(), pc.getPantalla());
		
		pc.setTotal(iTotal);
		
		cliente.setPc(pc);
		
		if(serviceCliente.grabarSqlCliente(cliente)){ // si graba el cliente en la BD
			
			sSalida="Registro grabado";
			
			sesion.setAttribute("msgExito",sSalida);
			
			sPpagina="resultado";
			
		}
		else{
			
			sSalida="Error en la grabacion";
			
			sesion.setAttribute("msgError",sSalida);
			
			sPpagina="salida";
		}
		
		Navegar.navegar(sPpagina, request, response);
		
	}

}
