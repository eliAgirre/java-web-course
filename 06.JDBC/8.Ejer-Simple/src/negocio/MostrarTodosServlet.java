package negocio;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Cliente;
import datos.interface_dao.InterfaceDaoCliente;
import datos.service.ServiceCliente;
import utilidades.EscribeHtml;

public class MostrarTodosServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private InterfaceDaoCliente serviceCliente=new ServiceCliente();
	private String titulo="Resultados";
       
    public MostrarTodosServlet() {
        super();

    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		mostrarTodos(request,response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		mostrarTodos(request,response);
	}
	
	protected void mostrarTodos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html"); // conveniente, no obligatorio
		
		List<Cliente> listaClientes=serviceCliente.mostrarTodos();
		String sMicro="";
		String sPantalla="";
		
		ServletOutputStream out=response.getOutputStream();
		
		EscribeHtml escribe=new EscribeHtml(out); 
		
		escribe.escribeCabecera(titulo);
		
			out.println("<table border='1'>");
				
				escribe.tablaCabecera();
				
				for(int i=0;i<listaClientes.size();i++){
					
					out.println("<tr>");
					
						out.println("<td>" + listaClientes.get(i).getDni() + "</td>");
						out.println("<td>" + listaClientes.get(i).getNombre() + "</td>");
						out.println("<td>" + listaClientes.get(i).getApellido()+ "</td>");
						if(listaClientes.get(i).getPc().getMicro()==0){
							sMicro="Intel";
						}
						else{
							sMicro="Amd";
						}
						out.println("<td>" + sMicro + "</td>");
						
						switch(listaClientes.get(i).getPc().getPantalla()){
							case 1:
								sPantalla="21'";
								break;
							case 2:
								sPantalla="23'";
								break;
							default:
								sPantalla="19'";
								break;
						}
						out.println("<td>" + sPantalla + "</td>");
						out.println("<td>" + listaClientes.get(i).getPc().getTotal() + "</td>");
					
					out.println("</tr>");
				}
			
			out.println("</table><br/><br/>");
			
			out.println("<a href='login.html'>Volver</a>");
		
		escribe.escribePie();
		
	}

}
