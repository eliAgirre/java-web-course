package negocio;

import java.io.IOException;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utilidades.Navegar;
import datos.interface_dao.InterfaceDaoCliente;
import datos.service.ServiceCliente;


@WebServlet("/BorrarBatchServlet")
public class BorrarBatchServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private InterfaceDaoCliente serviceCliente=new ServiceCliente();

    public BorrarBatchServlet() {
        super();
    
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		borrarBatch(request,response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		borrarBatch(request,response);
	}
	
	protected void borrarBatch(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession sesion=request.getSession();
		
		String sSalida="";
		
		Vector<String> vector=new Vector<String>();
		
		String[] arrayDni=request.getParameterValues("listaDni");
		
		if(arrayDni==null){
			
			sSalida="No existe registros a borrar";
		}
		else{
			
			for(int i=0;i<arrayDni.length;i++){
				
				vector.add(arrayDni[i]);
				
			}
			int resultado=serviceCliente.borrarDniLotes(vector);
			sSalida=resultado+" registros borrados";
		}
		
		sesion.setAttribute("msgError",sSalida);
		
		Navegar.navegar("salida", request, response);
	}

}
