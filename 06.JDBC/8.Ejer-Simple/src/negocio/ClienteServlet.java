package negocio;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.Cliente;
import utilidades.Navegar;
import datos.interface_dao.InterfaceDaoCliente;
import datos.service.ServiceCliente;

public class ClienteServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private InterfaceDaoCliente serviceCliente=new ServiceCliente();
       
    public ClienteServlet() {super();}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cliente(request,response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cliente(request,response);
	}
	
	protected void cliente(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String sSalida="";
		
		HttpSession sesion=request.getSession();
		
		String dni=request.getParameter("dni");
		String nombre=request.getParameter("nombre");
		String apellido=request.getParameter("apellido");
		
		if(serviceCliente.buscarCliente(dni)==null){ // si no existe el dni 
			
			Cliente c=new Cliente();
			
			c.setDni(dni);
			c.setNombre(nombre);
			c.setApellido(apellido);
			
			sesion.setAttribute("cliente", c);
			
			Navegar.navegar("micro", request, response);
			
		}
		else{
			sSalida="Ya existe el registro";
			
			sesion.setAttribute("msgError",sSalida);
			
			Navegar.navegar("salida", request, response);
		}
		
	}

}