package negocio;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utilidades.Navegar;
import modelo.Cliente;
import datos.interface_dao.InterfaceDaoCliente;
import datos.service.ServiceCliente;

@WebServlet("/ModificarDatosServlet")
public class ModificarDatosServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private InterfaceDaoCliente serviceCliente=new ServiceCliente();

    public ModificarDatosServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		modificarDatos(request,response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		modificarDatos(request,response);
	}
	
	protected void modificarDatos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String sSalida="";
		
		HttpSession sesion=request.getSession();
		
		Cliente cliente=(Cliente)sesion.getAttribute("bean");
		
		int iMicro=0;
		int iPantalla=0;
		int iTotal=0;
		
		String sMicro=request.getParameter("micro");
		String sPantalla=request.getParameter("pantalla");
		String sTotal=request.getParameter("total");
		
		cliente.setDni(request.getParameter("dni"));
		cliente.setNombre(request.getParameter("nombre"));
		cliente.setApellido(request.getParameter("apellido"));
		
		if(sMicro.equals("Intel")){
			iMicro=0;
		}
		else{
			iMicro=1;
		}
		
		cliente.getPc().setMicro(iMicro);
		
		switch(sPantalla){
		
			case "19":
				iPantalla=0;
				break;
			case "21":
				iPantalla=1;
				break;
			case "23":
				iPantalla=2;
				break;
			
		}
		
		cliente.getPc().setPantalla(iPantalla);
		
		iTotal=new Integer(sTotal).intValue();
		
		cliente.getPc().setTotal(iTotal);
		
		if(serviceCliente.modificarResultSet(cliente)){
			sSalida="Registro modificado";
		}
		else{
			sSalida="Error en modificacion";
		}
		
		sesion.setAttribute("msgError", sSalida);
		Navegar.navegar("salida", request,response);
		
	}

}
