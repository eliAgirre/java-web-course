package negocio;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utilidades.Navegar;
import modelo.Cliente;
import datos.interface_dao.InterfaceDaoCliente;
import datos.service.ServiceCliente;

@WebServlet("/ModificarClienteServlet")
public class ModificarClienteServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private InterfaceDaoCliente serviceCliente=new ServiceCliente();

    public ModificarClienteServlet() {
        super();

    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		modificarCliente(request,response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		modificarCliente(request,response);
	}
	
	protected void modificarCliente(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession sesion=request.getSession();
		
		String sPagina="";
		
		String sDni=request.getParameter("dni");
		Cliente cliente=serviceCliente.buscarCliente(sDni);
		
		if(cliente==null){ 
			
			sesion.setAttribute("msgError", "No existe registro");
			sPagina="salida";
			
		}
		else{
			
			sesion.setAttribute("bean", cliente);
			sPagina="modificarCliente";
		}
		
		Navegar.navegar(sPagina, request, response);
		
	}

}
