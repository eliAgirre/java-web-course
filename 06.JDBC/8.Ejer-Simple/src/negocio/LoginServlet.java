package negocio;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private Hashtable<String,String> hashTable=new Hashtable<String,String>();

    public LoginServlet() {super();}
    
    public void init(ServletConfig config) throws ServletException {
		
		//No es obligatorio pero si conveniente
    	super.init(config);
    	
    	// se obtienen los nombres desde el xml
    	Enumeration<String> e=config.getInitParameterNames();
    	
    	while(e.hasMoreElements()){ // mientras que contenga datos
			
    		String key=e.nextElement(); // se guarda el usuario siguiente
			String valor=config.getInitParameter(key); // obtiene el valor	
			hashTable.put(key,valor);
		}
    	
    } // init

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		login(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		login(request,response);
	}
	
	protected void login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// obtener los parametros desde form
		String user=request.getParameter("user");
		String pass=request.getParameter("pass");
		
		if(hashTable.containsKey(user)){ // si el hashtable contiene la clave usuario
			
			// obtiene el valor del key y comprueba si es igual al parametro del form
			if(hashTable.get(user).equals(pass)) {
				// llama a la pagina de HTML sin datos
				response.sendRedirect("login.html");
			}
			else{
				// envia el error y obtiene el xml
				response.sendError(402);				
			}
		}
		else{  
			// envia el error
			response.sendError(401, "No esta autorizado");
		}
	}

}