package modelo;

import interfaces.Pc_I;

public class Pc implements Pc_I{
	
	private int micro;
	private int pantalla;
	private int total;
	
	public Pc(){}

	public Pc(int micro, int pantalla) {
		super();
		this.micro = micro;
		this.pantalla = pantalla;
	}

	public Pc(int micro, int pantalla, int total) {
		super();
		this.micro = micro;
		this.pantalla = pantalla;
		this.total = total;
	}

	@Override
	public int getMicro() {

		return this.micro;
	}

	@Override
	public int getPantalla() {

		return this.pantalla;
	}

	@Override
	public int getTotal() {

		return this.total;
	}

	@Override
	public void setMicro(int micro) {
	
		this.micro=micro;
	}

	@Override
	public void setPantalla(int pantalla) {
		
		this.pantalla=pantalla;
	}

	@Override
	public void setTotal(int total) {
		
		this.total=total;
	}

	@Override
	public String toString() {
		return "Pc => micro: "+micro+", pantalla: "+pantalla+", total: "+total;
	}

}
