package interfaces;

public interface Pc_I {
	
	public int getMicro();
	public int getPantalla();
	public int getTotal();
	
	public void setMicro(int micro);
	public void setPantalla(int pantalla);
	public void setTotal(int total);
	
}