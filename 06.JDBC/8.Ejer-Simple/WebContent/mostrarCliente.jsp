<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<fmt:setBundle basename="properties.Messages" scope="session"/>
<title>Mostrar un cliente</title>
</head>
<body>
	<jsp:useBean id="clienteUno" class="modelo.Cliente" scope="session" />
	
	<label style="font-weight:bold;"><fmt:message key="cliente.label.dni"/></label>
	<jsp:getProperty name="clienteUno" property="dni"/><br><br>
	
	<label style="font-weight:bold;"><fmt:message key="cliente.label.nombre"/></label>
	<jsp:getProperty name="clienteUno" property="nombre"/><br><br>
	
	<label style="font-weight:bold;"><fmt:message key="cliente.label.apellido"/></label>
	<jsp:getProperty name="clienteUno" property="apellido"/><br><br>
	
	<label style="font-weight:bold;"><fmt:message key="resultado.micro"/></label>
	<%
		String sMicro="";
		
		if(clienteUno.getPc().getMicro()==0){
			sMicro="Intel";
		}
		else{
			sMicro="Amd";
		}
		
		out.println(sMicro);
		
	%><br><br>
	
	<label style="font-weight:bold;"><fmt:message key="resultado.pantalla"/></label>
	<%
		String sPantalla="";
	
		switch(clienteUno.getPc().getPantalla()){
			case 1:
				sPantalla="21'";
				break;
			case 2:
				sPantalla="23'";
				break;
			default:
				sPantalla="19'";
				break;
		}
		
		out.println(sPantalla);
		
	%><br><br>
	
	<label style="font-weight:bold;"><fmt:message key="resultado.total"/></label>
	<%=clienteUno.getPc().getTotal() %><br><br>
	
	<a href="login.html">Volver</a>
	
</body>
</html>