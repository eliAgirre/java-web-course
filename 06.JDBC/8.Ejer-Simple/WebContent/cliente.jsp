<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<fmt:setBundle basename="properties.Messages" scope="session"/>
<title><fmt:message key="cliente.titulo"/></title>
<script type="text/javascript" src=js/validarCliente.js></script>
</head>
<body onload="document.forms[0].userID.focus();">

	<p style="font-size:16pt;font-weight:bold;"><fmt:message key="cliente.titulo"/></p>
	
	<form action="Cliente" method="post" onsubmit="return validarCliente();">
	
		<label><fmt:message key="cliente.label.dni"/></label>
		<input type="text" name="dni" id="dniID" /><br/><br/>
		
		<label><fmt:message key="cliente.label.nombre"/></label>
		<input type="text" name="nombre" id="nombreID" /><br/><br/>
		
		<label><fmt:message key="cliente.label.apellido"/></label>
		<input type="text" name="apellido" id="apellidoID" /><br/><br/>
		
		<input type="submit" value="<fmt:message key='index.boton.aceptar'/>" />
	
	</form>
	
	<p/>
	
	<%--Formato el fichero property--%>
	<span id="errorDNI" style="color:red;font-weight:bold;visibility:hidden;"><fmt:message key="cliente.error.dni"/></span><br/>
	<span id="errorNombre" style="color:red;visibility:hidden;"><fmt:message key="cliente.error.nombre"/></span><br/>
	<span id="errorApellido" style="color:blue;visibility:hidden;"><fmt:message key="cliente.error.apellido"/></span>

</body>
</html>