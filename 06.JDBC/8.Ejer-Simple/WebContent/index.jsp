<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<fmt:setBundle basename="properties.Messages" scope="session"/>
<title><fmt:message key="index.titulo"/></title>
<script type="text/javascript" src=js/validar.js></script>
</head>
<body onload="document.forms[0].userID.focus();">

	<p style="font-size:16pt;font-weight:bold;"><fmt:message key="index.titulo"/></p>
	
	<form action="Login" method="post" onsubmit="return validar();">
	
		<label><fmt:message key="index.label.user"/></label>
		<input type="text" name="user" id="userID" /><br/><br/>
		
		<label><fmt:message key="index.label.pass"/></label>
		<input type="password" name="pass" id="passID" /><br/><br/>
		
		<input type="submit" value="<fmt:message key='index.boton.aceptar'/>" />
		<input type="reset" value="<fmt:message key='index.boton.borrar'/>" />
	
	</form>
	
	<p/>
	
	<%--Formato el fichero property--%>
	<span id="errorUser" style="color:red;font-weight:bold;visibility:hidden;"><fmt:message key="index.error.user"/></span><br/>
	<span id="errorPass" style="color:red;visibility:hidden;"><fmt:message key="index.error.pass"/></span><br/>

</body>
</html>