<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<fmt:setBundle basename="properties.Messages" scope="session"/>
<title>Modificar los datos del cliente</title>
<style>
	.claseError{ font-size:20px;color:red; }
</style>
<script type="text/javascript" src=js/verificar.js></script>
</head>
<body>

	<jsp:useBean id="bean" class="modelo.Cliente" scope="session"></jsp:useBean>
	
	<form method="post" action="ModificarDatosServlet" onsubmit="return verificar();">
	
		<label><fmt:message key="cliente.label.dni"/></label>
		<input type="text" name="dni" id="dniID" value='<jsp:getProperty property="dni" name="bean"/>'/><br/><br/>
		
		<label><fmt:message key="cliente.label.nombre"/></label>
		<input type="text" name="nombre" id="nombreID" value='<jsp:getProperty property="nombre" name="bean"/>'/><br/><br/>
		
		<label><fmt:message key="cliente.label.apellido"/></label>
		<input type="text" name="apellido" id="apellidoID" value='<jsp:getProperty property="apellido" name="bean"/>'/><br/><br/>
		
		<label style="font-weight:bold;"><fmt:message key="resultado.micro"/></label>
		<%
		
			String sMicro="";
			
			if(bean.getPc().getMicro()==0){
				sMicro="Intel";
			}
			else{
				sMicro="Amd";
			}
		
		
		%>
		<input type="text" name="micro" id="microID" value='<%=sMicro %>'/><br><br>
		
		<label style="font-weight:bold;"><fmt:message key="resultado.pantalla"/></label>
		<%
			String sPantalla="";
		
			switch(bean.getPc().getPantalla()){
				case 1:
					sPantalla="21'";
					break;
				case 2:
					sPantalla="23'";
					break;
				default:
					sPantalla="19'";
					break;
			}
			
		%>
		<input type="text" name="pantalla" id="pantallaID" value='<%=sPantalla %>'/><br><br>
		
		<label style="font-weight:bold;"><fmt:message key="resultado.total"/></label>
		<%
			int iTotal=bean.getPc().getTotal();
			
		%>
		<input type="text" name="total" id="totalID" value='<%=iTotal %>'/><br><br>
		
		<input type="submit" value="<fmt:message key='index.boton.aceptar'/>" />
		
	</form>
	
	<p><br/><br/>
	
	<span id="error" class="claseError"></span>

</body>
</html>