function validar(){
	
	// se obtienen los datos desde los fields
	var user=document.forms[0].userID.value;
	var pass=document.forms[0].passID.value;
	
	
	// se obtienen las etiquetas span para cambiar la visibilidad
	document.getElementById("errorUser").style.visibility='hidden';
	document.getElementById("errorPass").style.visibility='hidden';
	
	
	// variable booleano a devolver
	var resultado=true;
	
	if(user.length==0){
		
		// se cambia la visibilidad del span
		document.getElementById("errorUser").style.visibility ='visible';
		// se establece el valo a nulo
		document.forms[0].userID.value="";
		document.forms[0].userID.focus();
		resultado=false;
	}
	if(pass.length==0){
		
		// se cambia la visibilidad del span
		document.getElementById("errorPass").style.visibility ='visible';
		// se establece el valo a nulo
		document.forms[0].passID.value="";
		document.forms[0].passID.focus();
		resultado=false;
	}
	
	return resultado;
}