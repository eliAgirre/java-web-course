function verificar(){
	
	var sw=true;
	var cadena="";
	
	if (!document.forms[0].dniID.value.length){
		cadena+="El campo dni es obligatorio <br>";
		sw=false;
	}
	if (!document.forms[0].nombreID.value.length){
		cadena+="El campo nombre es obligatorio <br>";
		sw=false;
	}
	if (!document.forms[0].apellidoID.value.length){
		cadena+="El campo apellido es obligatorio <br>";
		sw=false;
	}
	if (!document.forms[0].microID.value.length){
		cadena+="El campo micro es obligatorio <br>";
		sw=false;
	}
	if (!document.forms[0].pantallaID.value.length){
		cadena+="El campo pantalla es obligatorio <br>";
		sw=false;
	}
	if (!document.forms[0].totalID.value.length){
		cadena+="El campo total es obligatorio <br>";
		sw=false;
	}
	if (isNaN(document.forms[0].pantallaID.value)){
		
		cadena+="El campo pantalla debe ser numerico <br>";
		document.forms[0].pantallaID.value="";
		sw=false;
	}
	if (isNaN(document.forms[0].totalID.value)){
		
		cadena+="El campo total debe ser numerico <br>";
		document.forms[0].totalID.value="";
		sw=false;
	}
	
	if(!sw)
		document.getElementById("error").innerHTML=cadena;
		return sw;
}