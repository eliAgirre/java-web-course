function validarDni(){
	
	// se obtienen los datos desde los fields
	var dni=document.forms[0].dniID.value;
	
	
	// se obtienen las etiquetas span para cambiar la visibilidad
	document.getElementById("errorDNI").style.visibility='hidden';
	
	// variable booleano a devolver
	var resultado=true;
	
	if(dni.length==0){
		
		document.getElementById("errorDNI").innerHTML="El campo DNI es obligatorio";
		// se cambia la visibilidad del span
		document.getElementById("errorDNI").style.visibility ='visible';
		// se establece el valo a nulo
		document.forms[0].dniID.value="";
		// se pone el cursos en el field de usuario
		document.forms[0].dniID.focus();
		resultado=false;
	}
	
	return resultado;
}