<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<fmt:setBundle basename="properties.Messages" scope="session"/>
<title><fmt:message key="resultado.titulo"/></title>
</head>
<body>

	<p style="font-size:16pt;font-weight:bold;"><fmt:message key="resultado.titulo"/></p>
	
	<label style="font-weight:bold;"><fmt:message key="cliente.label.dni"/></label>
	<c:out value="${sessionScope.cliente.dni}"/><br><br>
	
	<label style="font-weight:bold;"><fmt:message key="cliente.label.nombre"/></label>
	<c:out value="${sessionScope.cliente.nombre}"/><br><br>
	
	<label style="font-weight:bold;"><fmt:message key="cliente.label.apellido"/></label>
	<c:out value="${sessionScope.cliente.apellido}"/><br><br>
	
	<label style="font-weight:bold;"><fmt:message key="resultado.micro"/></label>
	
	<c:if test="${sessionScope.cliente.pc.micro==0}">
		<fmt:message key="micro.opcion0"/>
	</c:if>
	<c:if test="${sessionScope.cliente.pc.micro==1}">
		<fmt:message key="micro.opcion1"/>
	</c:if><br><br>
	
	<label style="font-weight:bold;"><fmt:message key="resultado.pantalla"/></label>

	<c:choose> 
	  	<c:when test="${sessionScope.cliente.pc.pantalla==0}" > 
	   		<fmt:message key="pantalla.opcion0"/>
	  	</c:when>
	   
	  	<c:when test="${sessionScope.cliente.pc.pantalla==1}" > 
	   		<fmt:message key="pantalla.opcion1"/>
	  	</c:when> 
	  
	  	<c:otherwise> 
	   		<fmt:message key="pantalla.opcion2"/>
	  	</c:otherwise> 
	</c:choose><br><br>
	
	<label style="font-weight:bold;"><fmt:message key="resultado.total"/></label>
	<c:out value="${sessionScope.cliente.pc.total}"/><br><br>
	
	<a href="<c:url value="login.html" />">Inicio</a>

</body>
</html>