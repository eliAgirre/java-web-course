package JDBC;
import java.sql.*;
public class ClsBeanResultset
{
        private String dni="";
        private String nombre="";
        private String apellido="";
        private String direccion="";
        private String sueldo="";
        private String plus="";
        
        private ResultSet rs1=null;
        private Connection cnn=null;
        private Statement stmt=null;
        private ClsConexion myconexion=null;
        
        public String getResultset1()
        {
            String valor=""; 
            try
            {
                myconexion=new ClsConexion();
                cnn=myconexion.abrirConexion();

                String comandounion = "select personal.dni,nombre,apellido,direccion,sueldo,plus " +
                 "from personal,nominas where personal.dni=nominas.dni order by personal.dni" ;
           
               /******Constantes de ResulSet*********
                - Primer argumento
                   * TYPE_FORWARD_ONLY
                        Por defecto
                        El cursor solo se nueve hacia adelante
                   * TYPE_SCROLL_INSENSITIVE
                        No se reflejan los cambios producidos en la base de 
                          datos mientras la hoja estuviese abierta 
                            (Hay que cerrar y volver a abrir)
                   * TYPE SCROLL_SENSITIVE
                        Si se reflejan los cambios producidos en la base de 
                          datos mientras la hoja estuviese abierta 
                            (se refrescan los cambios en los datos de forma automatica)
                - Segundo argumento
                   * CONCUR_READ_ONLY
                        Por defecto
                        Solo de lectura
                   * CONCUR_UPDATABLE
                        Lectura y escritura
                *********/
                stmt=cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, 
                  ResultSet.CONCUR_READ_ONLY);

                rs1=stmt.executeQuery(comandounion);
            }
            catch(SQLException e)
	    {
                //Si hay problemas
                valor="0";
                //Describe el error (String)
		System.err.println("Error=" +e.getMessage());
            }
            finally
            {
                return(valor);
           }
        }
       
        public String getSiguiente()
        {
            String valor="";
            try
            {
                //Si es el ultimo me muevo al primero
                if(rs1.isLast()) 
                    rs1.first();
                else
                //Mover el cursor al siguiente registro
                    rs1.next();
           }
           catch(SQLException e)
	   {
                //Si hay problemas
                valor="0";
                //Describe el error (String)
		System.err.println("Error=" +e.getMessage());
	   }
            finally
            {
                return(valor);
            }
        }
        
        public String getAnterior()
        {
           String valor="";
           try
           {
               //Si es el primer registro nos movemos al ultimo
               if (rs1.isFirst())
                   rs1.last();
                else
               //Mover el cursor al registro anterior
                    rs1.previous();
            }
            catch(SQLException e)
	    {
                  //Si hay problemas
                  valor="0";
                  //Describe el error (String)
		  System.err.println("Error=" +e.getMessage());
	    }
            finally
            {
                return (valor);
            }
        }
        
        public String getPrimero()
        {
            String valor="";
            try
            {
                rs1.first();
            }
            catch(SQLException e)
	    {
                  //Si hay problemas
                  valor="0";
                  //Describe el error (String)
		  System.err.println("Error=" +e.getMessage());
	    }
            finally
            {
                return (valor);
            }
        }
        
        public String getUltimo()
        {
            String valor="";
            try
            {
                rs1.last();
            }
            catch(SQLException e)
	    {
                  //Si hay problemas
                  valor="0";
                  //Describe el error (String)
		  System.err.println("Error=" +e.getMessage());
	    }
            finally
            {
                return (valor);
            }
        }
        public void setDni(String dni)
        {
            this.dni=dni;
        }
         
        public String getDni()
        {
           try
           {
                dni=String.valueOf(rs1.getLong("dni"));
                if(dni==null) dni="0";
               
           }
           catch(SQLException e)
	   {
               //Describe el error (String)
	       System.err.println("Error=" +e.getMessage());
               dni="Error";
	   }
           finally
           {    
               return(dni);
           }
        }
        
        public void setNombre(String nombre)
        {
            this.nombre=nombre;
        }
        public String getNombre()
        {
           try
           {
                nombre=String.valueOf(rs1.getString(2));
                if(nombre==null) nombre="";
                return(nombre);
           }
           catch(SQLException e)
	   {
                 //Describe el error (String)
		System.err.println("Error=" +e.getMessage());
                return("Error");
	   }
        }
        
        public void setApellido(String apellido)
        {
            this.apellido=apellido;
        }
        public String getApellido()
        {
            try
            {
                apellido=rs1.getString("apellido");
  
                //Sinonimo
                //apellido=rs1.getString(3);
                 if(apellido==null) apellido="";
                return(apellido);
            }
            catch(SQLException e)
	    {
                 //Describe el error (String)
		System.err.println("Error=" +e.getMessage());
                return("Error");
	    }
        }
        
        public void setDireccion(String direccion)
        {
            this.direccion=direccion;
        }
        public String getDireccion()
        {
            try
            {
                direccion=rs1.getString("direccion");
			 
                //Sinonimo
                //direccion=rs1.getString(4);
                if(direccion==null) direccion="";
                return(direccion);
            }
            catch(SQLException e)
	    {
                //Describe el error (String)
		System.err.println("Error=" +e.getMessage());
                return("Error");
	    }
        }
        
        public void setSueldo(String sueldo)
        {
            this.sueldo=sueldo;
        }
        public String getSueldo()
        {
            try
            {
                sueldo= String.valueOf(rs1.getLong("sueldo"));
					 
                //Sinonimo
                // sueldo= String.valueOf(rs1.getLong(5));
                if (sueldo==null) sueldo="0";
                return(sueldo);
            }
            catch(SQLException e)
	    {
                //Describe el error (String)
		System.err.println("Error=" +e.getMessage());
                return("Error");
	    }
        }
        
        public void setPlus(String plus)
        {
            this.plus=plus;
        }
        public String getPlus()
        {
            try
            {
                plus=String.valueOf(rs1.getLong(6));
                if(plus==null)plus="0";
                return(plus);
            }
            catch(SQLException e)
	    {
               //Describe el error (String)
	       System.err.println("Error=" +e.getMessage());
               return("Error");
	    }
        }
        public String getCerrarConexion()
        {
            String valor="";
            try
            {
                myconexion.cerrarConexion();
                return(valor);
            }
            catch(Exception e)
            {
               System.err.println("Error=" +e.getMessage());
               return("Error");
            }
       }
 }
