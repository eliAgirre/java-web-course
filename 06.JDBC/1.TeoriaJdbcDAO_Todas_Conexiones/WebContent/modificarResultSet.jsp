<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>Modificar con ResultSet</title>
	<style>
		.claseError{font-size:20px;color:blue;text-align:center;}
		.clase1{font-size:20px;color:red;text-align:center;}
	</style>
	
<script type="text/javascript">

function verificar()
{
	sw=true;
	var cadena="";
    if (!document.forms[0].txtsueldo.value.length)
	{
		//cadena+="El campo sueldo es obligatorio\n";
		cadena+="El campo sueldo es obligatorio <br>";
		sw=false;
	}
	if (!document.forms[0].txtplus.value.length)
	{
		//cadena+="El campo plus es obligatorio\n";
		cadena+="El campo plus es obligatorio <br>";
		sw=false;
	}
	if (isNaN(document.forms[0].txtsueldo.value))
	{
		//cadena+="El campo sueldo debe ser num�rico\n";
		cadena+="El campo sueldo debe ser num�rico <br>";
		document.forms[0].txtsueldo.value="";
		sw=false;
	}
	if (isNaN(document.forms[0].txtplus.value))
	{
		cadena+="El campo plus debe ser num�rico";
		document.forms[0].txtplus.value="";
		sw=false;
	}
	if(!sw)
		//alert(cadena);
		document.getElementById("error").innerHTML=cadena;
		return sw;
}

</script>
</head>

<body>
<jsp:useBean id="bean" class="modelo.Personal"
	scope="session"></jsp:useBean>
	
<form method="post" action="ServletModificarResultSet_2" 
	onsubmit="return verificar();">
	
<p class="clase1">Modificar con ResultSet</p>
<table cellspacing="2" cellpadding="2" 
	border="0" align="center">

<tr>
    <td colspan="2" align="left"> </td>
    <td colspan="2"> </td>
</tr>
<tr>
    <td colspan="1" align="left">Dni</td>
    <td colspan="2"><input type="text" name="txtdni" size="20" 
		  maxlength="20" readonly  
		   value='<jsp:getProperty property="dni" name="bean"/>'>
	</td>
</tr>
<tr>
    <td colspan="1" align="left">Nombre</td>
    <td colspan="2"><input type="text" name="txtnombre" size="20" 
			maxlength="20"  
			value='<jsp:getProperty property="nombre" name="bean"/>'>
	</td>
</tr>
<tr>
    <td colspan="1" align="left">Apellido</td>
    <td colspan="2"><input type="text" name="txtapellido" size="20"
			 maxlength="20" 
			 value='<jsp:getProperty property="apellido" name="bean"/>'>
	</td>
</tr>
<tr>
    <td colspan="1" align="left">Direccion</td>
    <td colspan="2"><input type="text" name="txtdireccion" size="20"
			 maxlength="20" 
			 value='<jsp:getProperty property="direccion" name="bean"/>'>
	</td>
</tr>
<tr>
    <td colspan="1" align="left">Sueldo</td>
    <td colspan="2"><input type="text" name="txtsueldo" size="20"
			 maxlength="9"  
			 value="<%=bean.getNomina().getSueldo()%>" >
	</td>
</tr>
<tr>
    <td colspan="1" align="left">Plus</td>
    <td colspan="2"><input type="text" name="txtplus" size="9" 
			maxlength="9" 
			value="<%=bean.getNomina().getPlus()%>">
	</td>
</tr>

<tr>
    <td colspan="2" align="center"><input type="submit" name="subModificar" value="Modificar"></td>
    <td colspan="2" align="center"><input type="button" name="btnVolver" 
		value="volver" onclick="document.location.href='inicio.html';"></td>
</tr>
</table>
<p>
<span id="error" class="claseError"></span>
</form>
</body>
</html>


