package negocio;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utilidades.Navegar;

import modelo.Personal;

import datos.Operar;

public class ServletBeanResultset extends HttpServlet {

	private List<Personal> listaPersonas = null;
	private Operar op = new Operar();

	private static final long serialVersionUID = 7805127457273914350L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		// Para que funcione tanto con el doGet como con el doPost
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{		
		int ixPosicion = 0;

		String movimiento = request.getParameter("txtMovi");

		if (movimiento == null) 
		{
			listaPersonas = op.mostrarTodos();
			movimiento = "0";
		}

		Personal persona = null;

		try 
		{
			ixPosicion = Integer.parseInt
				(request.getSession(true)
					.getAttribute("indice").toString());
		} 
		catch (NullPointerException e) 
		{
			ixPosicion = 0;
		}

		if (movimiento.equals("0")) 
		{
			ixPosicion = 0;
			persona=listaPersonas.get(ixPosicion);
			
		} 
		else if (movimiento.equals("1")) 
		{
			ixPosicion--;
			if (ixPosicion < 0) 
				ixPosicion = listaPersonas.size() - 1;
			
			persona=listaPersonas.get(ixPosicion);
			
		} 
		else if (movimiento.equals("2")) 
		{
			ixPosicion++;
			if (ixPosicion >= listaPersonas.size()) 
				ixPosicion = 0;
			
			persona = listaPersonas.get(ixPosicion);
			
		} 
		else if (movimiento.equals("3")) 
		{
			ixPosicion = listaPersonas.size() - 1;
			persona= listaPersonas.get(ixPosicion);			
		}

		request.setAttribute("mibean", persona);
		
		request.getSession(true).setAttribute
			("indice",ixPosicion);
		

		Navegar.navegar("mostrarResultSet.jsp", 
				request, response);

	}

}
