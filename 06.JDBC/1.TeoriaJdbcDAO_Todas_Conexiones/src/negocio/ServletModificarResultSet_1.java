package negocio;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import modelo.Personal;
import datos.Operar;
import utilidades.Navegar;

public class ServletModificarResultSet_1 extends HttpServlet
{

   private static final long serialVersionUID = 1L;
 
   public void doGet(HttpServletRequest request,
     HttpServletResponse response)
			throws ServletException, IOException
   {
	  // Para que funcione tanto con el doGet como con el doPost
	  mostrar(request,response);
   }
   public void doPost(HttpServletRequest request,
      HttpServletResponse response)
			throws ServletException, IOException 
   {
	  mostrar(request,response);
   }
   private void mostrar(HttpServletRequest request,
	 HttpServletResponse response)
			throws ServletException, IOException
   {
   	   String direccion_pagina="";
	   
	   HttpSession hs=request.getSession(true);
	   if (!hs.isNew())
	   {
			hs.invalidate();
			hs=request.getSession(true);
	   }
	   int dni=Integer.parseInt(request.getParameter("txtdni"));
	   Personal persona=new Operar().mostrarUno(dni);
	   
	   if(persona==null)
	   {
			hs.setAttribute("parametro","No existe registro");
			direccion_pagina="salida.jsp";  
	   }
	   else
	   {
		   direccion_pagina="modificarResultSet.jsp"; 
		   hs.setAttribute("bean", persona);
	   }		  
 	
 		Navegar.navegar(direccion_pagina, 
 				request,response);		
    }
}
