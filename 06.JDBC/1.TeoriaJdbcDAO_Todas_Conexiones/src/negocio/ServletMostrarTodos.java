package negocio;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import java.util.List;

import modelo.Personal;
import datos.Operar;


public class ServletMostrarTodos extends HttpServlet
{

  private static final long serialVersionUID = 9040995876643672900L;

  private Operar operar=new Operar();

  public void doGet(HttpServletRequest request,
     HttpServletResponse response)
    		 throws ServletException, IOException
  {
	  // Para que funcione tanto con el doGet como con el doPost
	  doPost(request,response);
  }
   public void doPost(HttpServletRequest request,
      HttpServletResponse response)
		throws ServletException, IOException 
   {
			mostrar(request,response);
   }
   private void mostrar(HttpServletRequest request,
	HttpServletResponse response)
		throws ServletException, IOException
   {  		
  		List<Personal> personas=
  			operar.mostrarTodos();
	    ServletOutputStream out=response.getOutputStream();
	    response.setContentType("text/html");
	    	   
		out.println("<html><head><title>Mostrar todos</title></head>");
		out.println("<body><form>");
		out.println("<p style='font-size:20px;color:red;");
		out.println("text-align:center'>Mostrar ");
		out.println("todos los registros"); 
		out.println("<table border='1'>");
		 
		// si hay datos en la lista 
	    if(personas.size()>0) 
		{
	    	out.println("<tr>");
			out.println("<th>DNI</th>");
			out.println("<th>NOMBRE</th>");
			out.println("<th>APELLIDO</th>");
			out.println("<th>DIRECCION</th>");
			out.println("<th>SUELDO</th>");
			out.println("<th>PLUS</th>");
			out.println("</tr>");					
			
			 //Una linea por registro
			for(int a=0;a<personas.size();a++)
			{
				out.println("<tr>");				 
	   		    out.println("<td>" + personas.get(a).getDni() + "</td>");
				out.println("<td>" + personas.get(a).getNombre() + "</td>");
		        out.println("<td>" + personas.get(a).getApellido() + "</td>");
			    out.println("<td>" + personas.get(a).getDireccion() + "</td>");
			    out.println("<td>" + personas.get(a).getNomina().getSueldo() + "</td>");
		        out.println("<td>" + personas.get(a).getNomina().getPlus() + "</td>");
				out.println("</tr>");
			}
	  }
      else //Si no hay registros
	  {
			out.println("<h3>No existen registros<h3>");
	  }
			  
	  out.println("</table><br>");
   	  out.println("<input type='button' value='Volver'");
	  out.println("onClick=document.location.href='inicio.html'>");
	  out.println("</p>");
	  out.println("</form></body></html>");
	  out.close();
   }
}
