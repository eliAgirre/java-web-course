package negocio;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import datos.Operar;

import java.util.Enumeration;
import java.util.Vector;

public class ServletMostrarDni extends HttpServlet
{
  private Operar myoperar=new Operar();
  private static final long serialVersionUID = -5702462457404774835L;  

  public void doGet(HttpServletRequest request,
       HttpServletResponse response)
			throws ServletException, IOException
  {
	  // Para que funcione tanto con el doGet como con el doPost
	  doPost(request,response);
  }
   public void doPost(HttpServletRequest request,
        HttpServletResponse response)
			throws ServletException, IOException 
   {
			mostrar(request,response);
   }
   private void mostrar(HttpServletRequest request,
		HttpServletResponse response)
			throws ServletException, IOException
   {
	   	  // si encuentra el dato lo pongo a verdadero
		   boolean resultado=false;
		   Vector<String> vector=null;
		   ServletOutputStream out=response.getOutputStream();
		   response.setContentType("text/html");
		   vector=myoperar.mostrarVectorDni();
		   if(vector!=null)
		   {
			   
			   out.println("<html><head>");
			   out.println("<title>Borrado por lotes</title></head>");
			   out.println("<body><form method=post action=ServletBorrarBatch>");
			   out.println("<center>");
			   out.print("<font size=4 color=red>Borrar ");
			   out.println("Registros por lotes</font>"); 
			   out.println("<p>");
			   byte i=0;
			   Enumeration<String> enumeracion=vector.elements();
			   // si hay datos 
			   while (enumeracion.hasMoreElements()) 
			   {
				    //Lanza una SQLException
				    String midni=enumeracion.nextElement();
		
				    if (!resultado) //Para sacar la cabecera una vez
				    {
						resultado=true;
						out.println("Seleccione DNI<p>");
						out.println("<select name='lstdni' size='4' multiple>");
						//Para que salga seleccionado el primero por defecto
						out.println("<option selected>Dni numero " +
					    ++i + "=" + midni + "</option>");
				    }
					else
					{
						out.println("<option>Dni numero " + ++i 
									+ "=" + midni + "</option>");
					}
			   }
			   if(resultado)
			   {
				   out.println("</select>");
				   out.println("<p>");
				   out.println("<input type=submit value=Borrar>");
				   out.println("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
			   }
					
		  }  
		  else//Si no hay registros
		  {
				out.println("<h3>No existen registros<h3><p>");
		  }
			  
		  out.println("<input type='button' value='Volver'");
		  out.println("onClick=document.location.href='inicio.html'>");
			  
		  out.println("</center>");
		  out.println("</form></body></html>");
		  out.close();
			 
   }
}
