package negocio;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import datos.Operar;

import java.util.Vector;

public class ServletBorrarBatch extends HttpServlet
{
	 private Operar operar=new Operar();
	 private static final long serialVersionUID = 7805127457273914350L;
     
	 public void doGet(HttpServletRequest request,
         HttpServletResponse response)
			throws ServletException, IOException
	 {
		 // Para que funcione tanto con el doGet como con el doPost
		 doPost(request,response);
	 }
	 public void doPost(HttpServletRequest request,
          HttpServletResponse response)
				throws ServletException, IOException
	 {
	  	String Salida="";
		RequestDispatcher despachador=null;
		Vector<String> vector=new Vector<String> ();
				
		String[]arr_dni=request.getParameterValues("lstdni");
		if(arr_dni==null)
		{
			Salida="No existe registros a borrar";
		}
		else
		{
			for(int i=0;i<arr_dni.length;i++)
			{
				//Para extraer el dni de la cadena
				// Dni numero=xxx
				//Empieza en 0
				int pos=arr_dni[i].lastIndexOf("="); 
			
				//arr_dni[i].length()) Empieza en 1
			
				String dni=arr_dni[i].substring
						(pos+1,arr_dni[i].length());									
				vector.add(dni);
								
			}
			int resultado=operar.borrarDniLotes(vector);
				
				Salida="Numero de registros borrados de la "
						+ "base de datos= " +resultado;
		}//Fin else

		HttpSession hs=request.getSession(true);
		if (!hs.isNew())
		{
			hs.invalidate();
			hs=request.getSession(true);
		}
		hs.setAttribute("parametro",Salida);
		
		despachador=getServletContext().getRequestDispatcher("/salida.jsp");
		despachador.forward(request,response);
	 }
}
