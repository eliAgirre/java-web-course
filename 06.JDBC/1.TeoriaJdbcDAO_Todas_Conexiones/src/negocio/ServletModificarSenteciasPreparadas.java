package negocio;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import datos.Operar;

import modelo.Personal;

import utilidades.Navegar;

import java.util.ArrayList;
import java.util.List;

public class ServletModificarSenteciasPreparadas extends HttpServlet
{
	 private static final long serialVersionUID = 4651358281836156999L;
		
	 public void doGet(HttpServletRequest request,
       HttpServletResponse response)
			throws ServletException, IOException
	 {
		 // Para que funcione tanto con el doGet como con el doPost
		 doPost(request,response);
	 }
	 public void doPost(HttpServletRequest request,
         HttpServletResponse response)
        	throws ServletException, IOException
	 {
	  	
		HttpSession hs=request.getSession(true);
		if (!hs.isNew())
		{
			hs.invalidate();
			hs=request.getSession(true);
		}
		List<Personal>personas=new ArrayList<Personal>();
		Personal persona_1=new Personal();		
		Personal persona_2=new Personal();
		Personal persona_3=new Personal();
		
		persona_1.setDni(Integer.parseInt(
				request.getParameter("txtdni1")));
		persona_2.setDni(Integer.parseInt(
				request.getParameter("txtdni2")));
		persona_3.setDni(Integer.parseInt(
				request.getParameter("txtdni3")));
		
		persona_1.setNombre
			(request.getParameter("txtnombre1"));
		persona_2.setNombre
			(request.getParameter("txtnombre2"));		
		persona_3.setNombre
			(request.getParameter("txtnombre3"));
		
		persona_1.setApellido
			(request.getParameter("txtapellido1"));
		persona_2.setApellido
			(request.getParameter("txtapellido2"));
		persona_3.setApellido
			(request.getParameter("txtapellido3"));
		
		personas.add(persona_1);
		personas.add(persona_2);
		personas.add(persona_3);		
		
		String Salida="Registros modificados=" 
			+ new Operar().modificarSentenciasPreparadas(personas);
		hs.setAttribute("parametro",Salida);
		Navegar.navegar("salida.jsp",request,response);
	}		
}
