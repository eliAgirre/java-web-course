package negocio;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import modelo.Personal;

import datos.Operar;


import utilidades.Navegar;

public class ServletBuscarUnoSql extends HttpServlet
{

  private static final long serialVersionUID = -7155700529513631640L;
  private Operar operar=new Operar();

  public void doGet(HttpServletRequest request,
    HttpServletResponse response)
		throws ServletException, IOException
  {
	  // Para que funcione tanto con el doGet como con el doPost
	  mostrar(request,response);
  }
  public void doPost(HttpServletRequest request,
       HttpServletResponse response)
		  throws ServletException, IOException 
  {
	   mostrar(request,response);
   }
   private void mostrar(HttpServletRequest request,
	HttpServletResponse response)
		throws ServletException, IOException
   {
	   response.setContentType("text/html");
	   String direccion_pagina="";
		   
	   HttpSession hs=request.getSession(true);
	   if (!hs.isNew())
	   {
			hs.invalidate();
			hs=request.getSession(true);
	   }
	   

	    int dni=Integer.parseInt(request.getParameter("txtdni"));
	    Personal persona=operar.mostrarUno(dni);
	   
	    //Hay datos en el Bean persona 
	    if(persona!=null) // 
	    {	
           hs.setAttribute("bean",persona);                        
		   direccion_pagina="mostraruno.jsp";				
		}	
		else //Si no datos en el Bean persona 
	    {
			hs.setAttribute("parametro","No existe registro");
			direccion_pagina="salida.jsp";
	    }			 

	    Navegar.navegar(direccion_pagina,request,response); 
   }
}

