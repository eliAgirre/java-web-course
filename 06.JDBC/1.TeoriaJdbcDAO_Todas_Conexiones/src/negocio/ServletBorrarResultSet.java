package negocio;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import datos.Operar;
import utilidades.Navegar;

public class ServletBorrarResultSet extends HttpServlet
{
	 private static final long serialVersionUID = 1L;

     public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException{
    	 
    	 doPost(request,response);
     }
     
     public void doPost(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException {
    	 
  	   	 HttpSession hs=request.getSession(true);
  	   	 
  	   	 if (!hs.isNew()){
  	   		 
			hs.invalidate();
			hs=request.getSession(true);
  	   	 }
  	   	 
    	 String salida="";
    	 
    	 int dni=Integer.parseInt(request.getParameter("txtdni"));
                
      	 int resultado=new Operar().borrarResultSet(dni);
      	 
      	 if(resultado==0)salida="No existe registro";
      	 if(resultado==1)salida="Registro borrado";
      	 if(resultado==2)salida="Error en borrado";    

      	 hs.setAttribute("parametro",salida);
      	 
      	 Navegar.navegar("salida.jsp", request, response);      
   	}
}
