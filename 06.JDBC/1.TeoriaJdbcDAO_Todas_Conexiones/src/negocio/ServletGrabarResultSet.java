package negocio;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import datos.Operar;

import modelo.Nominas;
import modelo.Personal;


import utilidades.Navegar;

public class ServletGrabarResultSet extends HttpServlet{

	 private static final long serialVersionUID = 4183707404397292303L;
	 private Operar operar=new Operar();
	 
     public void doGet(HttpServletRequest request,
        HttpServletResponse response)
			throws ServletException, IOException
     {
	  // Para que funcione tanto con el doGet como con el doPost
	  doPost(request,response);
     }
     public void doPost(HttpServletRequest request,
         HttpServletResponse response)
			throws ServletException, IOException 
    {
    	 String salida="";  
    	 Personal persona=new Personal();
    	 
    	 int sueldo=Integer.parseInt(request.getParameter("txtsueldo"));
         int plus=Integer.parseInt(request.getParameter("txtplus"));
         String nombre=request.getParameter("txtnombre");
         String apellido=request.getParameter("txtapellido");
         String direccion=request.getParameter("txtdireccion");

         
         persona.setDni(Integer.parseInt
        		 (request.getParameter("txtdni")));
         
         persona.setNombre(nombre);
         persona.setApellido(apellido);
         persona.setDireccion(direccion);
         
         Nominas nomina=new Nominas();
         nomina.setDni(persona.getDni());
         nomina.setSueldo(sueldo);
         nomina.setPlus(plus);
         
         persona.setNomina(nomina);
         
         int valor=operar.grabarResultSet(persona);
         
         if(valor==1) salida="Registro grabado";
         else if (valor==0)salida="Ya existe registro";
         else if (valor==2)salida="Error general";        
       
	 	 HttpSession hs=request.getSession(true);
		 if (!hs.isNew())
		 {
			hs.invalidate();
			hs=request.getSession(true);
		 }
		 hs.setAttribute("parametro",salida);		 

		 Navegar.navegar("salida.jsp", request, response);	
    }
}
 
  