package datos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexionOracleNativo 	
{
	private Connection cnn=null;
	@SuppressWarnings("finally")
	public Connection abrirConexion()
	{
		//No existe entrada en el Panel de control
		//(grabar el jar "ojdbc6.jar " 
		///en el lib del tomcat"
		try
		{	
				
			Class.forName("oracle.jdbc.driver.OracleDriver");
			
			String url = "jdbc:oracle:thin:@localhost:1521:XE";
			
			cnn=DriverManager.getConnection(url,"user1","user1");
	  			
	      			
		}
		catch(ClassNotFoundException e)
		{
			System.err.println("Error..Clase no encontrada");
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			
			// Devuelve el codigo deerror
		    //(Lo escribo en //F:\JRun\jsm-default\logs\stderr.log)
			
			//Describe el error (String)
			System.err.println("Error=" +e.getMessage());
			
			//Describe el error (en numero) seg�n 
			//a las convenciones X/OPEN de SqlState (String)
			System.err.println("Estado sql=" 
					+ e.getSQLState());
			
			//Codigo de error del Drivers. Especifico para cada driver (N�merico)
		    System.err.println("El cod de error  " +
		    		"en SQuery es " + e.getErrorCode());
		}
		finally
		{
			return cnn;
		}
	}
	public void cerrarConexion()
	{
		try
		{
			cnn.close(); //(Lanza una SQLException)
		}
		catch(SQLException e)
		{
	
			System.err.println("Error=" +e.getMessage());
			
			
			System.err.println("Estado sql=" + e.getSQLState());
			
			
		    System.err.println("El cod de error  en SQuery es " + e.getErrorCode());
		}
	}
}
