<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<fmt:setBundle basename="properties.Messages" scope="session"/>
<title><fmt:message key="resultado.titulo"/></title>
</head>
<body>
	
	<fmt:message key="resultado.operacion"/>
	<c:out value="${sessionScope.operacion}"/><br><br>
	<fmt:message key="resultado.cantidad"/>
	<c:out value="${sessionScope.cantidad}"/><br><br>
	
	<fmt:message key="resultado.info"/><br>
	<!--<c:out value="${sessionScope.infoMoneda}"/><br><br>-->
	<c:forEach var="item" items="${sessionScope.infoMoneda}">
   		
		<c:choose> 
			
		  	<c:when test="${item.codigo==0}" > 
		  		<c:out value="${item.nombre}"/>
		  		<c:out value="${item.cambio}"/><br>
		  	</c:when>
		   
		  	<c:when test="${item.codigo==1}" > 
		   		<c:out value="${item.nombre}"/>
		  		<c:out value="${item.cambio}"/><br>
		  	</c:when> 
		  	
		  	<c:when test="${item.codigo==2}" > 
		   		<c:out value="${item.nombre}"/>
		  		<c:out value="${item.cambio}"/><br>
		  	</c:when> 
		  
		  	<c:otherwise> 
		   		<c:out value="${item.nombre}"/>
		  		<c:out value="${item.cambio}"/><br>
		  	</c:otherwise> 
	  	
		</c:choose>
	
	</c:forEach><br>
	
	<!--<c:out value="${sessionScope.totales}"/><br><br>-->
	
	<fmt:message key="resultado.total"/><br>
	<c:forEach var="item" items="${sessionScope.totales}">
   		<c:out value="${item}"/>
	</c:forEach><br>
	
	<input type="button" value="Inicio" onclick="document.location.href='index.html'">
	
</body>
</html>