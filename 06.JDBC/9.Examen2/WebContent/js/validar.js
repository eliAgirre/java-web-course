function validar(){
	
	// se obtienen los datos desde los fields
	var euros=document.forms[0].eurosID.value;
	var monedas=document.getElementById("monedasID").value;
	
	// se obtienen las etiquetas span para cambiar la visibilidad
	document.getElementById("errorEuros").style.visibility='hidden';
	document.getElementById("errorMonedas").style.visibility ='hidden';
	
	
	// variable booleano a devolver
	var resultado=true;
	
	if(euros.length==0){
		
		document.getElementById("errorEuros").innerHTML="El campo Euros es obligatorio";
		// se cambia la visibilidad del span
		document.getElementById("errorEuros").style.visibility ='visible';
		// se establece el valo a nulo
		document.forms[0].eurosID.value="";
		document.forms[0].eurosID.focus();
		resultado=false;
	}
	if (isNaN(euros)){
		
		document.getElementById("errorEuros").innerHTML="El campo Euros tiene que ser numerico";
		// se cambia la visibilidad del span
		document.getElementById("errorEuros").style.visibility ='visible';
		// se establece el valo a nulo
		document.forms[0].eurosID.value="";
		document.forms[0].eurosID.focus();
		resultado=false;
	}
	if(euros>1000000){
		
		document.getElementById("errorEuros").innerHTML="El campo Euros tiene que ser menor que un millon";
		// se cambia la visibilidad del span
		document.getElementById("errorEuros").style.visibility ='visible';
		// se establece el valo a nulo
		document.forms[0].eurosID.value="";
		document.forms[0].eurosID.focus();
		resultado=false;
	}
	if(euros<0){
		document.getElementById("errorEuros").innerHTML="El campo Euros tiene que ser positivo";
		// se cambia la visibilidad del span
		document.getElementById("errorEuros").style.visibility ='visible';
		// se establece el valo a nulo
		document.forms[0].eurosID.value="";
		document.forms[0].eurosID.focus();
		resultado=false;
	}
	if(monedas==""){
		
		document.getElementById("errorMonedas").innerHTML="El campo Monedas es obligatorio";
    	// se cambia la visibilidad del span
 		document.getElementById("errorMonedas").style.visibility ='visible';
 		resultado=false;
	}
		
	return resultado;
}