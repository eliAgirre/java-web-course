package modelo;

import interfaces.Moneda_I;

public class Moneda implements Moneda_I{
	
	private int codigo;
	private String nombre;
	private float cambio;
	
	@Override
	public int getCodigo() {

		return this.codigo;
	}
	@Override
	public String getNombre() {
	
		return this.nombre;
	}
	@Override
	public float getCambio() {
		
		return this.cambio;
	}
	@Override
	public void setCodigo(int codigo) {
		
		this.codigo=codigo;
		
	}
	@Override
	public void setNombre(String nombre) {
		
		this.nombre=nombre;
		
	}
	@Override
	public void setCambio(float cambio) {
		
		this.cambio=cambio;
	}
	
	@Override
	public String toString() {
		
		return "Moneda => codigo: "+codigo+", nombre:"+nombre+", cambio: "+cambio;
	}
	
}
