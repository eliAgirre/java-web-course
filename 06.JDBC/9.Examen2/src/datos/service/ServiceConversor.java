package datos.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.commons.lang.StringEscapeUtils;

import modelo.Moneda;
import datos.conexion_dao.ConexionSqlServerNativo;
import datos.interface_dao.InterfaceDaoConversor;

public class ServiceConversor implements InterfaceDaoConversor {
	
	private static final float COMISIONCOMPRA=0.95f;
	private static final float COMISIONVENTA=0.99f;
		
	public static float getComisioncompra() {
		return COMISIONCOMPRA;
	}

	public static float getComisionventa() {
		return COMISIONVENTA;
	}

	@Override
	public Vector<String> mostrarVectorMoneda() {
		
		ConexionSqlServerNativo conexion=new ConexionSqlServerNativo();
		
		Connection cn=conexion.abrirConexion();
		
		Vector<String> vectorMonedas=new Vector<String>();
		String sMoneda="";
		String letraMayus="";
		int size=0;
		String completa="";
		
		try{
			
			Statement stmtVectorMoneda=cn.createStatement();
			
			String sQuery="SELECT nombre FROM moneda";
				
			ResultSet rsDni=null;
			rsDni=stmtVectorMoneda.executeQuery(sQuery); // Lanza una SQLException)
				
			while(rsDni.next()){
				
				sMoneda=rsDni.getString("nombre");
				letraMayus=sMoneda.substring(0,1);
				letraMayus=letraMayus.toUpperCase();
				size=sMoneda.length();
				sMoneda=sMoneda.substring(1, size);
				completa=letraMayus+sMoneda;
				vectorMonedas.add(completa);
				//vectorMonedas.add(rsDni.getString("nombre"));
			}					
		}
		catch(SQLException e){
			
			vectorMonedas=null;
			System.err.println("Error: " +e.getMessage());
		}
		finally{
			
			conexion.cerrarConexion();
		}
		
		return vectorMonedas;
	}
	
	@Override
	public Moneda buscarMoneda(String moneda) {
		
		Moneda monedaModel=null;
		
		ConexionSqlServerNativo conexion = new ConexionSqlServerNativo();
		
		Connection cn=conexion.abrirConexion();
		
		try {
			
			Statement stmt=cn.createStatement();
			ResultSet rs=null;
			String sQuery="SELECT codigo,nombre,cambio FROM moneda WHERE nombre='"+StringEscapeUtils.escapeSql(moneda)+"';";
			rs=stmt.executeQuery(sQuery);
			
			while(rs.next()) {
				
				
				monedaModel=new Moneda();
				crearBeanMoneda(rs,monedaModel);

			}
		}catch (SQLException e){
			
			System.err.println("Error: " +e.getMessage());
		}
		finally{
			conexion.cerrarConexion();
		}
		return monedaModel;
	}

	@Override
	public Vector<Float> conversion(float euros, Vector<String> listaMonedas, String opcion) {
		
		String sMoneda="";
		float fCambio=0.0f;
		float total=0.0f;
		
		Moneda moneda=new Moneda();
		Vector<Float> totales=new Vector<Float>();
		
		List<Moneda> arrayList=new ArrayList<Moneda>();
		List<Float> listaCambio=new ArrayList<Float>();
		
		for(int i=0;i<listaMonedas.size();i++){
			
			sMoneda=listaMonedas.get(i);
			sMoneda=minuscula(sMoneda);
			moneda=buscarMoneda(sMoneda);
			arrayList.add(moneda);
			
		}
		
		for(int i=0;i<arrayList.size();i++){
			
			sMoneda=listaMonedas.get(i);
			sMoneda=minuscula(sMoneda);
			if(arrayList.get(i).getNombre().equals(sMoneda)){
				
				listaCambio.add(arrayList.get(i).getCambio());
			}
		}
		
		if(opcion.equals("0")){ // Compra
			for(int i=0;i<listaCambio.size();i++){
				
				fCambio=listaCambio.get(i);
				total=euros*fCambio*getComisioncompra();
				totales.add(total);
				//System.out.println(listaCambio.get(i));
			}
		}
		else if(opcion.equals("1")){ // Venta
			for(int i=0;i<listaCambio.size();i++){
				
				fCambio=listaCambio.get(i);
				total=euros*fCambio*getComisionventa();
				totales.add(total);
				//System.out.println(listaCambio.get(i));
			}
		}
		
		return totales;
	}
	

	private void crearBeanMoneda(ResultSet rs, Moneda moneda) throws SQLException{
		
		moneda.setCodigo(rs.getInt("codigo"));
		
		String sNombre=rs.getString("nombre");
		moneda.setNombre(sNombre);
		
		Float fCambio=rs.getFloat("cambio");
		moneda.setCambio(fCambio);
	 
	  	
	} // crearBeanMoneda
	
	
	private String minuscula(String mayuscula){
		
		String minuscula="";
		String letraMinus="";
		int size=0;
		String sMoneda="";
		
		letraMinus=mayuscula.substring(0,1);
		letraMinus=letraMinus.toLowerCase();
		size=mayuscula.length();
		sMoneda=mayuscula.substring(1, size);
		minuscula=letraMinus+sMoneda;		
		
		return minuscula;
	}

	@Override
	public List<Moneda> info(Vector<String> listaMonedas) {
		
		Moneda moneda=null;
		String sMoneda="";
		List<Moneda> arrayList=new ArrayList<Moneda>();
		
		for(int i=0;i<listaMonedas.size();i++){
			
			sMoneda=listaMonedas.get(i);
			sMoneda=minuscula(sMoneda);
			moneda=buscarMoneda(sMoneda);
			arrayList.add(moneda);
			
		}
		
		return arrayList;
	}
}
