package datos.interface_dao;

import java.util.List;
import java.util.Vector;

import modelo.Moneda;

public interface InterfaceDaoConversor {
	
	public Vector<String> mostrarVectorMoneda ();
	public Moneda buscarMoneda(String moneda);
	public List<Moneda> info(Vector<String> listaMonedas);
	public Vector<Float> conversion(float euros,Vector<String> listaMonedas,String opcion);
	
	//public List<Moneda> mostrarTodos();

}