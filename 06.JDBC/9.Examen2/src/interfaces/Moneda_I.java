package interfaces;

public interface Moneda_I {
	
	public int getCodigo();
	public String getNombre();
	public float getCambio();
	
	public void setCodigo(int codigo);
	public void setNombre(String nombre);
	public void setCambio(float cambio);
	

}
