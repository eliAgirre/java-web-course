package negocio;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.interface_dao.InterfaceDaoConversor;
import datos.service.ServiceConversor;
import utilidades.EscribeHtml;

public class InicioServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private String titulo="Conversor";
	private InterfaceDaoConversor serviceConversor=new ServiceConversor();
       

    public InicioServlet() {
        super();

    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		inicio(request,response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		inicio(request,response);
	}
	
	protected void inicio(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html"); // conveniente, no obligatorio
		
		ServletOutputStream out=response.getOutputStream();
		
		EscribeHtml escribe=new EscribeHtml(out); 
		
		escribe.escribeCabecera(titulo);
		
		Vector<String> vector=serviceConversor.mostrarVectorMoneda();
		
		boolean resultado=false; // se cambia a true, si encuentra dato
		
		if(vector!=null){ // si no contiene datos
			
			out.println("<form method=post action=DatosServlet onsubmit='return validar();'>");
			out.println("Conversor de monedas</font>");
			out.println("<p>Introduzca Euros:");
			out.println("<input type='text' name='euros' id='eurosID' /></p><br/>");
			out.println("<select name='listaMonedas' id='monedasID' size='4' multiple>");
			
			int i=0;
			Enumeration<String> enumeracion=vector.elements();
			
			while (enumeracion.hasMoreElements()){ // mientras que haya datos
				
				String sMoneda=enumeracion.nextElement();
				
				if (!resultado){ // si existe el dato
					
					i++;
					
					if(i==0){
						out.println("<option selected>"+sMoneda+ "</option>"); // sale uno seleccionado
					}
					out.println("<option>"+sMoneda+ "</option>"); // salen los demas sin seleccionar
					
				}
				
				
			} // cierre de while
			
			out.println("</select><br/><br/>");
			
			// radioButtons
			out.println("<input type='radio' name='opcion' value='0' checked='checked' />Compra");
			out.println("<input type='radio' name='opcion' value='1' />Venta<br/>");
			
			// botones
			out.println("<br/><input type=submit value=Aceptar>");
			out.println("<input type=reset value=Borrar><br/><br/>");
			out.println("<span id='errorEuros' style='color:red;font-weight:bold;visibility:hidden;'></span><br/>");
			out.println("<span id='errorMonedas' style='color:blue;visibility:hidden;'></span><br/>");			
			
		}
		
		escribe.escribePie();
		
	}

}
