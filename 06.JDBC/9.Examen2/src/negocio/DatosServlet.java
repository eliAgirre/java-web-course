package negocio;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utilidades.Navegar;
import modelo.Moneda;
import datos.interface_dao.InterfaceDaoConversor;
import datos.service.ServiceConversor;

public class DatosServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private InterfaceDaoConversor serviceConversor=new ServiceConversor();

    public DatosServlet() {
        super();

    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		datos(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		datos(request,response);
	}
	
	protected void datos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession sesion=request.getSession();
		String sSalida="";
		
		// Se obtienen los datos
		String sEuros=request.getParameter("euros");
		float fEuros=new Float(sEuros).floatValue();
		
		// select multiple
		String[] arrayMonedas=request.getParameterValues("listaMonedas");
		
		Vector<String> vector=new Vector<String>();
		Vector<Float> totales=new Vector<Float>();
		List<Moneda> infoMoneda=new ArrayList<Moneda>();
		//List<String> tabla=new ArrayList<String>();
		
		if(arrayMonedas==null){
			
			sSalida="No has elegido moneda";
			sesion.setAttribute("msg",sSalida);
			Navegar.navegar("salida", request, response);
		}
		else{
			
			for(int i=0;i<arrayMonedas.length;i++){
				
				vector.add(arrayMonedas[i]);
				
			}
			//int resultado=serviceCliente.borrarDniLotes(vector);
			//sSalida=resultado+" registros borrados";
		}
		
		// radios
		String sRadios=request.getParameter("opcion");
		String sAccion="";
		if(sRadios.equals("0")){
			sAccion="Compra";
		}
		else if(sRadios.equals("1")){
			sAccion="Venta";
		}
		
		infoMoneda=serviceConversor.info(vector);		
		totales=serviceConversor.conversion(fEuros, vector, sRadios);
		
		sesion.setAttribute("operacion",sAccion);
		sesion.setAttribute("cantidad",sEuros);
		sesion.setAttribute("infoMoneda",infoMoneda);
		sesion.setAttribute("totales",totales);
		Navegar.navegar("resultados", request, response);
		
	}


}
