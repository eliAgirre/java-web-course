package utilidades;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;

public class EscribeHtml {
	
	private ServletOutputStream objEscribir;
	
	public EscribeHtml(ServletOutputStream objEscribir) {
		
		super();
		this.objEscribir=objEscribir;
		
	} // constructor
	
	public void escribeCabecera(String titulo) throws ServletException, IOException {
		
		objEscribir.print("<html>");
		objEscribir.print("<head>");
		objEscribir.print("<title>"+titulo+"</title>"); 	
		objEscribir.print("<script type='text/javascript' src='js/validar.js'></script>");
		objEscribir.print("</head>");
		objEscribir.print("<body>");
		
	} // escribeCabecera
	
	public void escribePie() throws ServletException, IOException {
		
		objEscribir.print("</body>");
		objEscribir.print("</html>");
		
	} // escribePie
}
