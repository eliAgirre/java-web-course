package exception;

public class NoExistePersonaException extends Exception
{
	private static final long serialVersionUID = 1L;

	public NoExistePersonaException(String msg)
	{
		super(msg);
	}
}
