import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

import modelo.Nomina;
import modelo.Personal;
import datos.FactoryDao;
import datos.PersonalDaoService;
import exception.NoExistePersonaException;
import exception.PersonaRepetidaException;

public class App {

	private FactoryDao txtFactory = FactoryDao.getFactory(1);

	//Trabajamos con el servicio que corresponda
	private PersonalDaoService personalDaoImp = txtFactory.getUsuarioDao();
	
	public static void main(String[] args) 
	{
		App app=new App();
		
		//Devuelve el Imp que corresponda
		System.out.println("Servicio=" + app.personalDaoImp.getClass().getName());
		
		List<Personal> personas=new ArrayList<Personal>();	
		
		Personal persona1 = new Personal("1", "A", "AA","AAA", new Nomina("1",111,11));
		Personal persona2 = new Personal("2", "B", "BB","BBB", new Nomina("2",222,22));
		Personal persona3 = new Personal("3", "C", "CC","CCC", new Nomina("3",333,33));
		Personal persona4 = new Personal("4", "D", "DD","DDD", new Nomina("4",444,44));
		Personal persona5 = new Personal("5", "E", "EE","EEE", new Nomina("5",555,55));
		
		personas.add(persona1);
		personas.add(persona2);
		personas.add(persona3);
		personas.add(persona4);
		personas.add(persona5);
		
		try
		{
			String dni="1";
			
			app.grabar(personas);
			
			Personal persona=app.buscar(dni);			
			
			if(persona!=null) app.verPersona(persona);
			else System.out.println("No existe el dni " + dni);
			
			app.modificar("5");		
			
			app.verPersona(personas.get(4));	
		} 
		
		catch (PersonaRepetidaException e)
		{
			System.out.println(e.getMessage());
		}
		
		catch (NoExistePersonaException e) 
		{
			System.out.println(e.getMessage());
		}
		
		catch (UnsupportedOperationException e2)
		{
			System.out.println(e2.getMessage());
		}
	}


	private void grabar(List<Personal> p) throws PersonaRepetidaException{
		
		Iterator<Personal> it=p.iterator();
				
		while(it.hasNext())
		{
			Personal persona=it.next();
			personalDaoImp.insertarPersonal(persona);
			System.out.println("***** Graba persona ********");
			verPersona(personalDaoImp.buscarPersonal(persona.getDni()));
		}

	}
	private Personal buscar(String dni){

		System.out.println("**** Buscar persona********");
		return personalDaoImp.buscarPersonal(dni);
	}
	
	private void modificar(String dni) throws NoExistePersonaException
	{
		System.out.println("**** Persona modificada ********");
		personalDaoImp.modificarPersonal(dni);		 
	}
	
	private void verPersona(Personal p)
	{
		System.out.println("Dni=" + p.getDni());
		System.out.println("Nombre=" + p.getNombre());
		System.out.println("Apellido=" + p.getApellido());
		System.out.println("Direccion=" + p.getDireccion());
		System.out.println("Sueldo=" + p.getNomina().getSueldo());
		System.out.println("Plus=" + p.getNomina().getPlus());
	}
}
