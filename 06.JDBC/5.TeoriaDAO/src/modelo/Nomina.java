package modelo;

public class Nomina 
{
	private String dni;
	private int sueldo;
	private int plus;
	
	public Nomina(){}
	
	public Nomina(String dni, int sueldo, int plus) {
		super();
		this.dni = dni;
		this.sueldo = sueldo;
		this.plus = plus;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public int getSueldo() {
		return sueldo;
	}
	public void setSueldo(int sueldo) {
		this.sueldo = sueldo;
	}
	public int getPlus() {
		return plus;
	}
	public void setPlus(int plus) {
		this.plus = plus;
	}
	
}
