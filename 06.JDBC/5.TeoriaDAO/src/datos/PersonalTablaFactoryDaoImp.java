package datos;


import java.util.Hashtable;

import exception.NoExistePersonaException;
import exception.PersonaRepetidaException;
import modelo.Personal;

public class PersonalTablaFactoryDaoImp implements PersonalDaoService {

   private Hashtable<String,Personal> personas=new Hashtable<String,Personal>();
   
	public PersonalTablaFactoryDaoImp() {
       
    }

    public Personal buscarPersonal(String dni) {
    	Personal persona=personas.get(dni);
    	return persona;    	
    }

    public void insertarPersonal(Personal persona) throws PersonaRepetidaException {
    	
    	if(buscarPersonal(persona.getDni()) != null)
    		throw new PersonaRepetidaException("Error, persona duplicada");
    	
    	personas.put(persona.getDni(), persona);
    }

    public void modificarPersonal(String dni) throws NoExistePersonaException {
    	
    	if(buscarPersonal(dni) == null)
    		throw new NoExistePersonaException("Error, no existe la persona a modificar");
    	
    	Personal p=personas.get(dni);
    	p.setNombre("X");
    	p.setApellido("XX");
    	p.setDireccion("XXX");           
    }   

} 
