package datos;

public class SqlServerFactoryDao extends FactoryDao {

    @Override
    public PersonalDaoService getUsuarioDao() {
        return new PersonalSqlServerFactoryDaoImp();
    }

}
