package datos;

import exception.NoExistePersonaException;
import exception.PersonaRepetidaException;
import modelo.Personal;


public class PersonalXMLFactoryDaoImp implements PersonalDaoService {

 	public PersonalXMLFactoryDaoImp() {
       
    }

	@Override
	public Personal buscarPersonal(String dni) {
		 throw new UnsupportedOperationException("Falta implementar (Xml buscar)");
	}

	@Override
	public void insertarPersonal(Personal persona) throws PersonaRepetidaException
	{
		throw new UnsupportedOperationException("Falta implementar (Xml insertar)");
		
	}

	@Override
	public void modificarPersonal(String dni) throws NoExistePersonaException
	{
		throw new UnsupportedOperationException("Falta implementar (Xml modificar)");		
	}

} 
