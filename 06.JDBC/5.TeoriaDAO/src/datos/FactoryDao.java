package datos;

public abstract class FactoryDao {

    private static final int TABLA_FACTORY = 1;
    private static final int SQLSERVER_FACTORY = 2;
    private static final int XML_FACTORY = 3;

    public abstract PersonalDaoService getUsuarioDao();

    public static FactoryDao getFactory(int claveFactory){
    	
        switch(claveFactory)
        {
    		case TABLA_FACTORY:
    			return new TablaFactoryDao();
    		case SQLSERVER_FACTORY:   
    			return new SqlServerFactoryDao();
    		case XML_FACTORY:
        		return new XmltFactoryDao();               
            default:
                throw new IllegalArgumentException();
        }
    }
}
