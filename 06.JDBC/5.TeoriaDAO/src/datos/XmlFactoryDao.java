package datos;

public class XmlFactoryDao extends FactoryDao {

    @Override
    public PersonalDaoService getUsuarioDao() {
        return new PersonalSqlServerFactoryDaoImp();
    }

}
