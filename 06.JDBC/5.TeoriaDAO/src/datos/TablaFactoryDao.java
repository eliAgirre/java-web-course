package datos;

public class TablaFactoryDao extends FactoryDao{

    @Override
    public PersonalDaoService getUsuarioDao() {
        return new PersonalTablaFactoryDaoImp();
    }
}