package datos;

import exception.NoExistePersonaException;
import exception.PersonaRepetidaException;
import modelo.Personal;

public class PersonalSqlServerFactoryDaoImp implements PersonalDaoService {
	   
    //private ConnectionDao cn;

    public PersonalSqlServerFactoryDaoImp() {
     
    }

    @Override
    public Personal buscarPersonal(String dni) {
        throw new UnsupportedOperationException("Falta implementar (SqlServer buscar)");
    }
    
    @Override
    public void insertarPersonal(Personal persona) throws PersonaRepetidaException
    {
        throw new UnsupportedOperationException("Falta implementar (SqlServer insertar)");
    }

    
	@Override
	public void modificarPersonal(String dni) throws NoExistePersonaException
	{
		throw new UnsupportedOperationException("Falta implementar (SqlServer modificar)");		
	}
}
