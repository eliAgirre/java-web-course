package datos;

import exception.NoExistePersonaException;
import exception.PersonaRepetidaException;
import modelo.Personal;

public interface PersonalDaoService {

    Personal buscarPersonal(String dni);
    void insertarPersonal(Personal persona) throws PersonaRepetidaException;
    void modificarPersonal(String dni) throws NoExistePersonaException;

}
