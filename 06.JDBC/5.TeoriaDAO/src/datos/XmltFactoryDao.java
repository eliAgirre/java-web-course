package datos;

public class XmltFactoryDao extends FactoryDao{

    @Override
    public PersonalDaoService getUsuarioDao() {
        return new PersonalXMLFactoryDaoImp();
    }
}