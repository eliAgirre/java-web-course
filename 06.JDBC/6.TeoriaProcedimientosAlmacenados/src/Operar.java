import java.io.IOException;

import java.sql.Connection;

import java.sql.SQLException;

import java.sql.CallableStatement;

import java.sql.ResultSet;

import java.sql.Types;

public class Operar 
{
	private ConexionSqlServerNativo miConexion=new ConexionSqlServerNativo();
	private Connection cn=miConexion.abrirConexion();
	public void proc_Alma_Suma_Sin_Return()
	{
		
		// El procedimiento tiene un parametro de entrada y dos de salida (output)
		
		try
		{
	
			String procSumaSinReturn = "{call SUMA_SIN_RETURN( ?,?,? ) }";			
					
			CallableStatement cstmt =cn.prepareCall(procSumaSinReturn);
			
			// Se introduce el parametro de entrada
			int datoIn=99;       //Parametro 1
			int datoInOut_1=100; //Parametro 2
			int datoInOut_2=200; //Parametro 3
			
			cstmt.setInt( 1,datoIn);
			cstmt.setInt( 2,datoInOut_1);
			cstmt.setInt( 3,datoInOut_2);
 

			//Se definen los parametros de output.
			cstmt. registerOutParameter( 2, Types.INTEGER );
			cstmt. registerOutParameter( 3, Types.INTEGER ); 

			// Se ejecuta el procedimiento
			cstmt.execute(); 
			
			// Se obtienen los parametros de output. 
			int numero1 = cstmt.getInt(2);
			int numero2 = cstmt.getInt(3);
			
			System.out.println("Numero1=" +numero1);
 			System.out.println("Numero2=" +numero2);


		}
	 	catch (SQLException e)
	 	{
	 		 e.printStackTrace();

	 	}



	}
	public void proc_Alma_Suma_Con_Return()
	{
		
		try
		{
			// El procedimiento tiene 
			//     un parametro de entrada 
			//     dos de salida (output)
			//     Return
			
		
			//Para Procedimientos con Return		
			String procSumaConReturn = "{?= call SUMA_CON_RETURN( ?,?,? ) }";
			
			CallableStatement cstmt =cn.prepareCall(procSumaConReturn);			
			
			// Se introduce el parametro de entrada
			int datoReturn=0;     //Parametro 1
			int datoIn=99;        //Parametro 2			
			int datoInOut_1=100;  //Parametro 3
			int datoInOut_2=200;  //Parametro 4
			
			
			cstmt.setInt( 2,datoIn);
			cstmt.setInt( 3,datoInOut_1);
			cstmt.setInt( 4,datoInOut_2);
			
			cstmt. registerOutParameter( 1, Types.INTEGER ); //Return
			cstmt. registerOutParameter( 3, Types.INTEGER ); //OUT
			cstmt. registerOutParameter( 4, Types.INTEGER ); //OUT
			
			// Se ejecuta el procedimiento
			cstmt.executeUpdate(); 
			
			// Se obtienen los parametros de return
			datoReturn=cstmt.getInt(1);
			
			// Se obtienen los parametros de output. 
			int numero1 = cstmt.getInt(3);
			int numero2 = cstmt.getInt(4);
			
			System.out.println("Numero1=" + numero1);
 			System.out.println("Numero2=" + numero2);
 			System.out.println("Return="  + datoReturn);
		}
		catch (SQLException e)
	 	{
	 		 e.printStackTrace();

	 	}

	}
	public void proc_Alma_Buscar_Uno() throws IOException
	{
		boolean swExiste=false;
		boolean swDni=false;
		Teclado miTeclado=new Teclado();
		
		int dniBsucar=0;
		
		while(!swDni)
		{
			try
			{
				dniBsucar=Integer.parseInt(miTeclado.teclado("Introduce dni a buscar"));
				swDni=true;
			}
			catch (NumberFormatException e)
			{
				System.out.println("El dni debe ser numerico");
			}	
		}

		try
		{
			String procBuscarUno = "{call BuscarUno( ? ) }";
			CallableStatement cstmt =cn.prepareCall(procBuscarUno);	
			
			cstmt.setInt( 1,dniBsucar);
			
			ResultSet rs=cstmt.executeQuery();
	
			while (rs.next())
			{
 				//Lanza una SQLException
				 String midni=String.valueOf(rs.getLong("dni"));
					 
				 // Sinonimo
				 //String midni=String.valueOf(rs.getLong(1));
				 if (midni==null) midni="0";
						 
				 //Lanza una SQLException
				 //Sinonimo
				 //String minombre=rs.getString("nombre"); 
					 
				 String minombre=rs.getString(2);
				 if(minombre==null) minombre="";
					 
					 
				 //Lanza una SQLException
				  String miapellido=rs.getString("apellido");
					  
				 //Sinonimo
				 //String miapellido=rs.getString(3);
				 if(miapellido==null) miapellido="";				
					
					 
				  //Lanza una SQLException
				 String midireccion=rs.getString("direccion");
					 
				 //Sinonimo
				 //String midireccion=rs.getString(4);
				 if(midireccion==null) midireccion="";
								 
				 //Lanza una SQLException		
				 String misueldo= String.valueOf(rs.getLong("sueldo"));
					 
				 //Sinonimo
				 //String misueldo= String.valueOf(rs.getLong(5));
				 if (misueldo==null) misueldo="0";
								 
				 //Lanza una SQLException
				 //Sinonimo
				 // String miplus=String.valueOf(rs.getLong("plus"));
					 
				 String miplus=String.valueOf(rs.getLong(6));
				 if(miplus==null) miplus="0";

						
				System.out.println("Dni=" + midni);
				System.out.println("Nombre=" + minombre);
				System.out.println("Apellido=" + miapellido);
				System.out.println("Direccion=" + midireccion);
				System.out.println("Sueldo=" + misueldo);
				System.out.println("Plus=" + miplus);
				swExiste=true;
				
			}
			if(!swExiste) System.out.println("No existe registro");
					
			
		}
	
		catch (SQLException e)
	 	{
	 		 e.printStackTrace();

	 	}
	}
}
