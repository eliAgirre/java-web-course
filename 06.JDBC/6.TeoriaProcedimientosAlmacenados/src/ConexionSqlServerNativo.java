

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexionSqlServerNativo {
  
	private java.sql.Connection cnn = null; 

    public ConexionSqlServerNativo() { }  

    public Connection abrirConexion()
    {  
		//No existe entrada en el Panel de control
		//(grabar el jar sqljdbc4.jar  el lib del tomcat")
	
    	try 
        {  
        	String url = "jdbc:sqlserver://localhost:1433;databaseName=base1";
        	
        	//Funciona
        	//Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
        	
        	Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        	cnn = DriverManager.getConnection(url, "user1", "user1");
          
        } 
        catch (Exception e) 
        {  
            e.printStackTrace();  
            System.out.println("Error Trace in getConnection() : " + e.getMessage());  
        }  
        return cnn;  
    }  

	public void cerrarConexion()
	{
		try
		{
			cnn.close(); //(Lanza una SQLException)
		}
		catch(SQLException e)
		{

			System.err.println("Error=" +e.getMessage());


			System.err.println("Estado sql=" + e.getSQLState());


			System.err.println("El cod de error  en SQuery es " + e.getErrorCode());
		}
	}
	
}
