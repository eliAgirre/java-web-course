import java.io.IOException;
public class Principal 
{

	public static void main(String[] args) throws IOException
	{
		Operar miOp=new Operar();
		Teclado miTeclado=new Teclado();
		char op=' ';
		
			while(op!='4')
			{
				try
				{
					System.out.println("** Menu de Procedimientos Almacenados **");
					System.out.println("****** 1 Consultar suma sin return *****");
					System.out.println("****** 2 Consultar suma con return *****");
					System.out.println("****** 3 Buscar dni en Base de Datos ***");
					System.out.println("****** 4 Salir *************************");
					op=(miTeclado.teclado("***  Elige Opcion ***")).charAt(0);
					
					//System.out.println("op=" + op);
					switch(op)
					{
						case '1':
							System.out.println("***** Procedimiento sin return  *****");
							miOp.proc_Alma_Suma_Sin_Return();
							break;
						case '2':
							System.out.println("***** Procedimiento con return  *****");
							miOp.proc_Alma_Suma_Con_Return();
							break;
						case '3':					
							System.out.println("** Procedimiento para buscar un dni**");
							miOp.proc_Alma_Buscar_Uno();
							break;
						case '4':
							System.out.println("Fin");
							break;
						default:
							System.out.println("Error, en eleccion");
						
					}
			
				}
				catch (StringIndexOutOfBoundsException e)
				{
					System.out.println("Error tienes que escribir un numero");
				}
			}
	}	
}
