function validar(){
	
	// se obtienen los datos desde los fields
	var dni=document.forms[0].dniID.value;
	var nombre=document.forms[0].nombreID.value;
	var apellido=document.forms[0].apellidoID.value;
	
	
	// se obtienen las etiquetas span para cambiar la visibilidad
	document.getElementById("errorDNI").style.visibility='hidden';
	document.getElementById("errorNombre").style.visibility='hidden';
	document.getElementById("errorApellido").style.visibility='hidden';
	
	
	// variable booleano a devolver
	var resultado=true;
	
	if(dni.length==0){
		
		// se cambia la visibilidad del span
		document.getElementById("errorDNI").style.visibility ='visible';
		// se establece el valo a nulo
		document.forms[0].dniID.value="";
		// se pone el cursos en el field de usuario
		document.forms[0].dniID.focus();
		resultado=false;
	}
	if(nombre.length==0){
		
		// se cambia la visibilidad del span
		document.getElementById("errorNombre").style.visibility ='visible';
		// se establece el valo a nulo
		document.forms[0].nombreID.value="";
		// se pone el cursos en el field de usuario
		document.forms[0].nombreID.focus();
		resultado=false;
	}
	if(apellido.length==0){
			
		// se cambia la visibilidad del span
		document.getElementById("errorApellido").style.visibility ='visible';
		// se establece el valo a nulo
		document.forms[0].apellidoID.value="";
		// se pone el cursos en el field de usuario
		document.forms[0].apellidoID.focus();
		resultado=false;
	}
	
	return resultado;
}