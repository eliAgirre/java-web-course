<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<fmt:setBundle basename="properties.Messages" scope="session"/>
<title><fmt:message key="grabar.sql.titulo"/></title>
<script type="text/javascript" src=js/validar.js></script>
</head>
<body onload="document.forms[0].dniID.focus();">

	<p style="font-size:16pt;font-weight:bold;"><fmt:message key="grabar.sql.titulo"/></p>

	<form action="Inicio" method="post" onsubmit="return validar();">
		
		<label><fmt:message key="grabar.sql.label.dni"/></label>
		<input type="text" name="dni" id="dniID" /><br/><br/>
		
		<label><fmt:message key="grabar.sql.label.nombre"/></label>
		<input type="text" name="nombre" id="nombreID" /><br/><br/>
		
		<label><fmt:message key="grabar.sql.label.apellido"/></label>
		<input type="text" name="apellido" id="apellidoID" /><br/><br/>
		
		<input type="submit" value="<fmt:message key='grabar.sql.boton.aceptar'/>" />
		<input type="reset" value="<fmt:message key='grabar.sql.boton.borrar'/>" />
	
	</form>
	
	<%--Formato el fichero property--%>
	<span id="errorDNI" style="color:red;font-weight:bold;visibility:hidden;"><fmt:message key="grabar.sql.error1.dni"/></span><br/>
	<span id="errorNombre" style="color:red;visibility:hidden;"><fmt:message key="grabar.sql.error.nombre"/></span><br/>
	<span id="errorApellido" style="color:red;visibility:hidden;"><fmt:message key="grabar.sql.error.apellido"/></span>

</body>
</html>