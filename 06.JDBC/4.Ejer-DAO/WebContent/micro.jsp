<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<fmt:setBundle basename="properties.Messages" scope="session"/>
<title><fmt:message key="micro.titulo"/></title>
</head>
<body>

	<p style="font-size:16pt;font-weight:bold;"><fmt:message key="micro.titulo"/></p>

	<form action="Micro" method="post">
		
		<label><fmt:message key="micro.label"/></label><br/><br/>
		
		<input type="radio" name="opMicro" value="0" checked="checked" /><fmt:message key="micro.opcion0"/><br/>
		
		<input type="radio" name="opMicro" value="1" /><fmt:message key="micro.opcion1"/><br/><br/>
		
		<input type="submit" value="<fmt:message key='grabar.sql.boton.aceptar'/>" />
	
	</form>

</body>
</html>