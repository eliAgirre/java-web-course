<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<fmt:setBundle basename="properties.Messages" scope="session"/>
<title><fmt:message key="extras.titulo"/></title>
</head>
<body>

	<p style="font-size:16pt;font-weight:bold;"><fmt:message key="extras.titulo"/></p>
	
	<form action="Resultado" method="post">
	
		<input type="checkbox" name="opExtras" value="0" checked="checked"><fmt:message key="extras.opcion0"/><br/>
		
		<input type="checkbox" name="opExtras" value="1"><fmt:message key="extras.opcion1"/><br/>
		
		<input type="checkbox" name="opExtras" value="2"><fmt:message key="extras.opcion2"/><br/><br/>
		
		<input type="submit" value="<fmt:message key='grabar.sql.boton.aceptar'/>" />
	
	</form>

</body>
</html>