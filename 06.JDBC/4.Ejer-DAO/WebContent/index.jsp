<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="ISO-8859-1">
<fmt:setBundle basename="properties.Messages" scope="session"/>
<title><fmt:message key="index.titulo"/></title>
</head>
<body>

	<p style="font-size:16pt;font-weight:bold;"><fmt:message key="extras.titulo"/></p>
	
	<ul>
		<li><a href="grabar_sql.jsp"><fmt:message key="grabar.sql.link"/></a></li>
		<li><a href="mostrar_todos.jsp"><fmt:message key="mostrar.todos.link"/></a></li>
	</ul>

</body>
</html>