package modelo;

import java.util.ArrayList;

import interfaces.Pc_I;

public class Pc implements Pc_I{
	
	private int micro;
	private int pantalla;
	private ArrayList<Integer> extras;
	private int total;
	
	public Pc(){}

	public Pc(int micro, int pantalla, ArrayList<Integer> extras) {
		super();
		this.micro = micro;
		this.pantalla = pantalla;
		this.extras = extras;
	}

	public Pc(int micro, int pantalla, ArrayList<Integer> extras, int total) {
		super();
		this.micro = micro;
		this.pantalla = pantalla;
		this.extras = extras;
		this.total = total;
	}

	@Override
	public int getMicro() {

		return this.micro;
	}

	@Override
	public int getPantalla() {

		return this.pantalla;
	}

	@Override
	public ArrayList<Integer> getExtras() {

		return this.extras;
	}

	@Override
	public int getTotal() {

		return this.total;
	}

	@Override
	public void setMicro(int micro) {
	
		this.micro=micro;
	}

	@Override
	public void setPantalla(int pantalla) {
		
		this.pantalla=pantalla;
	}

	@Override
	public void setExtras(ArrayList<Integer> extras) {
		
		this.extras=extras;
	}

	@Override
	public void setTotal(int total) {
		
		this.total=total;
	}

	@Override
	public String toString() {
		return "Pc => micro: "+micro+", pantalla: "+pantalla+", extras:"+extras+", total: "+total;
	}

}
