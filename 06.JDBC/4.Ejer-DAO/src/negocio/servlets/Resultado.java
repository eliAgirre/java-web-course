package negocio.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datos.interface_dao.InterfaceDaoCliente;
import datos.service.ServiceCliente;
import utilidades.Navegar;
//import negocio.clases.Operar;
import modelo.Cliente;
import modelo.Pc;

public class Resultado extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private ArrayList<Integer> extrasUser=new ArrayList<Integer>();
	private InterfaceDaoCliente serviceCliente=new ServiceCliente();
       
    public Resultado() {super();}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		resultado(request,response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		resultado(request,response);
	}
	
	private void resultado(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String sSalida="";
		
		HttpSession sesion=request.getSession();
		Cliente c=(Cliente)sesion.getAttribute("cliente");
		
		String[] arrayExtras=new String[3];
		
		arrayExtras=request.getParameterValues("opExtras");
		int iExtra=0;
		int iTotal=0;
		
		if (arrayExtras!=null){
			
			for(int i=0;i<arrayExtras.length;i++){
				//System.out.println(arrayExtras[i]);
				
				if(arrayExtras[i].equals("0")){
					
					iExtra=0;
					extrasUser.add(iExtra);
				}
				if(arrayExtras[i].equals("1")){
					
					iExtra=1;
					extrasUser.add(iExtra);
				}
				if(arrayExtras[i].equals("2")){
					
					iExtra=2;
					extrasUser.add(iExtra);
				}
				
			}
			iExtra=-1;
			extrasUser.add(iExtra);
			
		}
		
		Pc pc=(Pc)sesion.getAttribute("pc");
		
		pc.setExtras(extrasUser);
		
		request.setAttribute("lista", extrasUser);
		
		iTotal=serviceCliente.calcularTotal(pc.getMicro(), pc.getPantalla(), pc.getExtras());
	
		pc.setTotal(iTotal);
		
		if(serviceCliente.grabarSqlPc(c.getDni(), pc)){ // si graba el pc en la BD
			
			c.setPc(pc);
			
			sesion.setAttribute("cliente", c);
			
			Navegar.navegar("resultado", request, response);
			
		}
		else{
			
			sSalida="Error en la grabacion";
			
			sesion.setAttribute("msgError",sSalida);
			
			Navegar.navegar("salida", request, response);
		}
		
	}

}
