package negocio.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datos.interface_dao.InterfaceDaoCliente;
import datos.service.ServiceCliente;
import utilidades.Navegar;
import modelo.Cliente;

public class Inicio extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private InterfaceDaoCliente serviceCliente=new ServiceCliente();
       
    public Inicio() {super();}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		inicio(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		inicio(request,response);
	}
	
	private void inicio(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String sSalida="";
		
		HttpSession sesion=request.getSession(true);
		
		if (!sesion.isNew()){
			
			sesion.invalidate();
			sesion=request.getSession(true);				
		}
		
		String dni=request.getParameter("dni");
		
		if(serviceCliente.existeCliente(dni)==false){ // si no existe el dni 
			
			Cliente c=new Cliente();
			
			c.setDni(dni);
			c.setNombre(request.getParameter("nombre"));
			c.setApellido(request.getParameter("apellido"));
			
			sesion.setAttribute("cliente", c);
			
			if(serviceCliente.grabarSqlDatosCliente(c)){ // si ha grabado cliente en la BD
				
				Navegar.navegar("micro", request, response);
			}
			else{
				sSalida="Error en la grabacion";
			}
			
		}
		else{
			sSalida="Ya existe el registro";
			
			sesion.setAttribute("msgError",sSalida);
			
			Navegar.navegar("salida", request, response);
		}
		
	}
}
