package interfaces;

import modelo.Pc;

public interface Cliente_I {
	
	public String getDni();
	public String getNombre();
	public String getApellido();
	public Pc getPc();

	public void setDni(String dni);
	public void setNombre(String nombre);
	public void setApellido(String apellido);
	public void setPc(Pc pc);
}
