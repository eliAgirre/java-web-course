package datos.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import modelo.Cliente;
import modelo.Pc;
import datos.conexion_dao.ConexionSqlServerNativo;
import datos.interface_dao.InterfaceDaoCliente;

public class ServiceCliente implements InterfaceDaoCliente {
	
	private ArrayList<Integer> listaExtras=new ArrayList<Integer>();
	private static final int PRECIOEXTRA=50;

	@SuppressWarnings("finally")
	@Override
	public boolean grabarSqlDatosCliente(Cliente cliente) {
		
		boolean resultado=false;
		
		ConexionSqlServerNativo conexion=new ConexionSqlServerNativo();
		
		Connection cn=conexion.abrirConexion();
		
		String sQueryCliente="INSERT INTO cliente (dni,nombre,apellido) VALUES ("+cliente.getDni()+",";
		
		if(cliente.getNombre().isEmpty()){
			sQueryCliente+=null + ",";	
		}
		else{
			sQueryCliente+="'"+cliente.getNombre()+"',";
		}
		
		if(cliente.getApellido().isEmpty()){
			sQueryCliente+=null+")";	
		}
		else{
			sQueryCliente+="'"+cliente.getApellido()+"')";
		}
		
		try{
			cn.setAutoCommit(false);
			Statement stmtGrabarSql=cn.createStatement();
			stmtGrabarSql.executeUpdate(sQueryCliente);
			cn.commit(); //Confirmamos la grabación
			resultado=true;
			
		}catch (SQLException e){
			
			System.err.println("Error: "+e.getMessage());
			
			try{
				cn.rollback(); //Deshacemos la grabación
			}
			catch(SQLException e2){}
			
		}
		finally {
			
			try{
				
				//Lanza siempre una SQLException
				cn.setAutoCommit(true);
				conexion.cerrarConexion();
			}
			catch(SQLException e){
				
				System.err.println("Error: " +e.getMessage());
			}
			return (resultado);
			
		}
		
	} // grabarSqlDatosCliente
	
	@Override
	public List<Cliente> mostrarTodos() {
		
		List<Cliente> clientes=new ArrayList<Cliente>();
		
		ConexionSqlServerNativo conexion=new ConexionSqlServerNativo();	
		
		Connection cn=conexion.abrirConexion();
		
		try{
			
			Statement stmt=cn.createStatement();
			ResultSet rs=null;
			String sQuery="SELECT cliente.dni,nombre,apellido,micro,pantalla,extras FROM cliente,pc WHERE cliente.dni=pc.dni ORDER BY cliente.dni";
			rs=stmt.executeQuery(sQuery); // Lanza una SQLException)
			
			while(rs.next()){
				
				
				Cliente cliente=new Cliente();
				Pc pc=new Pc();
				
				crearBeanCliente(rs,cliente,pc);
				clientes.add(cliente); // agrega al list el cliente
			}
			
		}catch(SQLException e){
			
			System.err.println("Error: " +e.getMessage());
			System.err.println("causa: " +e.getCause());
		}
		
		conexion.cerrarConexion();
		return clientes;
		
	} // mostrarTodos
	
	@SuppressWarnings("finally")
	@Override
	public boolean existeCliente(String dni) {
		
		boolean encontrado=false;
		
		ConexionSqlServerNativo conexion=new ConexionSqlServerNativo();
		
		Connection cn=conexion.abrirConexion();
		
		try{
			
			Statement stmtBuscar=cn.createStatement();
			String sQuery="SELECT * FROM cliente WHERE dni="+dni;
			ResultSet rsBuscarUno=null;
			rsBuscarUno=stmtBuscar.executeQuery(sQuery);
			
			if(rsBuscarUno.next()){
				
				encontrado=true;
			}
			
		}catch (SQLException e){
			
			System.err.println("Error: " +e.getMessage());
			
		}
		finally{
			conexion.cerrarConexion();
			return (encontrado);
		}

	} // existeCliente
	
	@SuppressWarnings("finally")
	public boolean grabarSqlPc(String dni, Pc pc){
		
		boolean grabado=false;
		
		if(existeCliente(dni)){ // si el dni existe, se guarda el pc
			
			ConexionSqlServerNativo conexion=new ConexionSqlServerNativo();
			
			Connection cn=conexion.abrirConexion();
			
			String sQueryPc="INSERT INTO pc (dni,micro,pantalla,extra0,extra1,extra2,total) values ("+dni+",";
			
			String sMicro=String.valueOf(pc.getMicro());
			String sPantalla=String.valueOf(pc.getPantalla());
			String sTotal=String.valueOf(pc.getTotal());
			int iExtra=-1;
			int pos=0;
			
			if(sMicro.isEmpty()){
				sQueryPc+=null+",";	
			}
			else{
				sQueryPc+=pc.getMicro()+",";
			}
			
			if(sPantalla.isEmpty()){
				sQueryPc+=null+",";	
			}
			else{
				sQueryPc+=pc.getPantalla()+",";
			}
			
			if(pc.getExtras().equals("")){
				sQueryPc+=iExtra+",";
				sQueryPc+=iExtra+",";
				sQueryPc+=iExtra+",";
			}
			else{
				
				for(int i=0;i<pc.getExtras().size();i++){
				 
				  //System.out.println(pc.getExtras().get(i));
				  
				  if(pc.getExtras().get(i)==-1){
					  
					  pos=i;
					  
					  if(pos==0){
						  iExtra=-1;
						  sQueryPc+=iExtra+",";
					  }
					  if(pos==1){
						  iExtra=-1;
						  sQueryPc+=iExtra+",";
					  }
					  if(pos==2){
						  iExtra=-1;
						  sQueryPc+=iExtra+",";
					  }
					  
				  	}
					else{
						if(pc.getExtras().get(i)==0){
								
							iExtra=0;
							sQueryPc+=iExtra+",";
						}
						if(pc.getExtras().get(i)==1){
							
							iExtra=1;
							sQueryPc+=iExtra+",";
						}
						if(pc.getExtras().get(i)==2){
							
							iExtra=2;
							sQueryPc+=iExtra+",";
						}
					}
				}

			}
			
			if(sTotal.isEmpty()){
				sQueryPc+=null+")";	
			}
			else{
				sQueryPc+=pc.getTotal()+")";
			}
			
			//System.out.println(sQueryPc);
			
			try{
				cn.setAutoCommit(false);
				Statement stmtGrabarSql=cn.createStatement();
				stmtGrabarSql.executeUpdate(sQueryPc);
				cn.commit(); //Confirmamos la grabación
				grabado=true;
				
			}catch (SQLException e){
				
				System.err.println("Error: "+e.getMessage());
				
				try{
					cn.rollback(); //Deshacemos la grabación
				}
				catch(SQLException e2){}
				
			}
			finally {
				
				try{
					
					//Lanza siempre una SQLException
					cn.setAutoCommit(true);
					conexion.cerrarConexion();
				}
				catch(SQLException e){
					
					System.err.println("Error: " +e.getMessage());
				}
				return (grabado);
				
			}
			
		}
		
		return grabado;
	}
	
	@SuppressWarnings("finally")
	public int calcularTotal(int iMicro, int iPantalla, ArrayList<Integer> extras){
		
		ConexionSqlServerNativo conexion=new ConexionSqlServerNativo();
		
		Connection cn=conexion.abrirConexion();
		int total=0;
		
		try{
			
			Statement stmtBuscar=cn.createStatement();
			String sQuery="SELECT * FROM micro WHERE id="+iMicro;
			ResultSet rsBuscarUno=null;
			rsBuscarUno=stmtBuscar.executeQuery(sQuery);
			int iPrecioMicro=0;
			int iPrecioPantalla=0;
			int iPrecioExtras=0;
			
			
			if(rsBuscarUno.next()){
				
				iPrecioMicro=rsBuscarUno.getInt("precio");
			}
			
			sQuery="SELECT * FROM pantalla WHERE id="+iPantalla;
			rsBuscarUno=null;
			rsBuscarUno=stmtBuscar.executeQuery(sQuery);
			if(rsBuscarUno.next()){
				
				iPrecioPantalla=rsBuscarUno.getInt("precio");
			}
			
			for(int i=0;i<extras.size();i++){
				
				iPrecioExtras+=PRECIOEXTRA;
			}
			
			total=iPrecioMicro+iPrecioPantalla+iPrecioExtras;
			
		}catch (SQLException e){
			
			System.err.println("Error: " +e.getMessage());
			
		}
		finally{
			
			conexion.cerrarConexion();
			return (total);
		}
	}
	
	@SuppressWarnings("unused")
	private void crearBeanCliente(ResultSet rs, Cliente cliente, Pc pc) throws SQLException{
		
		cliente.setDni(rs.getString("dni"));
		
		String sNombre=rs.getString("nombre");
		if(sNombre.isEmpty()) sNombre="No grabado";
		cliente.setNombre(sNombre);
		
		String sApellido=rs.getString("apellido");
		if(sApellido.isEmpty()) sApellido="No grabado";
		cliente.setApellido(sApellido);
		
		Integer iMicro=new Integer((String.valueOf(rs.getInt("micro"))));
		if(iMicro==null) iMicro=-1;
	  	pc.setMicro(iMicro);
	  	
	  	Integer iPantalla=new Integer((String.valueOf(rs.getInt("pantalla"))));
		if(iPantalla==null) iPantalla=-1;
	  	pc.setPantalla(iPantalla);
	  	
	  	Integer iExtra0=new Integer((String.valueOf(rs.getInt("extra0"))));
		if(iExtra0==null) iExtra0=-1;
		listaExtras.add(iExtra0);
	  	
	  	Integer iExtra1=new Integer((String.valueOf(rs.getInt("extra1"))));
		if(iExtra1==null) iExtra1=-1;
		listaExtras.add(iExtra1);
		
		Integer iExtra2=new Integer((String.valueOf(rs.getInt("extra2"))));
		if(iExtra2==null) iExtra2=-1;
		listaExtras.add(iExtra2);
	  	pc.setExtras(listaExtras);
	  	
	  	Integer iTotal=new Integer((String.valueOf(rs.getInt("total"))));
		if(iTotal==null) iTotal=-1;
	  	pc.setTotal(iTotal);
	  	
	} // crearBeanCliente

}
