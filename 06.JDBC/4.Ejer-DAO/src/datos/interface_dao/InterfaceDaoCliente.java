package datos.interface_dao;

import java.util.ArrayList;
import java.util.List;

import modelo.Cliente;
import modelo.Pc;

public interface InterfaceDaoCliente {
	
	public boolean grabarSqlDatosCliente(Cliente cliente);
	public List<Cliente> mostrarTodos();
	public boolean existeCliente(String dni);
	public boolean grabarSqlPc(String dni, Pc pc);
	public int calcularTotal(int iMicro, int iPantalla, ArrayList<Integer> extras);

}