<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Leer Parametros</title>
	  <style>
       .clase1 {color:blue;font-size:13pt;font-weight:bold}
       .clase2 {color:Sienna;font-size:15pt;font-weight:bold}       
      </style>
 </head>
<body>
<form>
<div class="clase1">
<span class="clase2">Leer Parametros</span><hr/>
<table>
      <tr>
      	<td>Nombre:</td>
        <td><c:out value="${param.txtnombre}" /></td>
      </tr>
      <tr>
      	<td>Localidad:</td>
        <td><c:out value="${param.txtlocalidad}" /></td>
      </tr>
      <tr>
      	<td>Edad:</td>
        <td><c:out value="${param.txtedad}" /></td>
      </tr>
      <%--El siguiente parametro no es pasado desde la pagina
      anterior (probamos el atributo default) --%>
      <tr>
      	<td>Nuevo</td>
        <td><c:out value="${param.nuevo}" default="teoria default" /></td>
      </tr>
    </table>
</div>
<hr/>
<%-- Sinomino
<a href="inicio.jsp">Volver</a>
--%>
<a href="<c:url value="inicio.jsp" />">Volver</a>
</form>
</body>
</html>