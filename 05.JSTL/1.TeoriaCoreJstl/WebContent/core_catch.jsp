<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Catch</title>
	  <style>
       .clase1 {color:blue;font-size:13pt;font-weight:bold}
       .clase2 {color:Sienna;font-size:15pt;font-weight:bold}       
      </style>
 </head>
<body>
<form>

<div class="clase1">
<span class="clase2">CATCH</span><hr/>
	<c:catch var="ex">
	
 		<c:set var="num1" value="${10}"/>
 		<c:set var="num2" value="${0}"/>
 		
 		<%--Sinonimo (No produce la excepcion--%>
 		<%--<c:set var="num3" value="${num1 / num2}"/>--%>
 		
 		<%--No produce la excepcion--%>
 		<c:set var="num3" value="${num1 div num2}"/> 
 		
 		<label>Num3=</label><c:out value="${num3}"/>
 		<hr/>
 		 		
		<%--Si produce la excepcion--%>
		<% 
		int num11=10;
		int num22=0; 	
		int num33 = num11/num22;
		%>
 
	</c:catch>
	
	<c:if test="${ex!=null}">
 		 <label>Error=</label><c:out value="${ex.getMessage()}"/>
 	</c:if>	
	
	<hr/>
	<c:catch var="ex2">
		<%--Si produce la excepcion al no existir la pagina--%>
		<c:set var="pagina" value="prueba.jsp"/>
		<c:import url="${pagina}"/>
	</c:catch>
	
	<c:if test="${ex2!=null}">
 		 <label>No existe la Pagina <c:out value="${pagina}"/></label>
 		 <br/>
	</c:if>	
</div>
<hr/>
<%-- Sinomino
<a href="inicio.jsp">Volver</a>
--%>
<a href="<c:url value="inicio.jsp" />">Volver</a>

</form>
</body>
</html>