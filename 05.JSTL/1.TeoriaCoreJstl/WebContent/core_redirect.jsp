<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Redirect</title>
	  <style>
       .clase1 {color:blue;font-size:13pt;font-weight:bold}
       .clase2 {color:Sienna;font-size:15pt;font-weight:bold}       
      </style>
 </head>
<body>
<form>
<div class="clase1">
<span class="clase2">REDIRECT</span><hr/>

	<c:set var="pagina" value="hola"/>
	
	<c:if test="${pagina=='hola'}">
		<%-- redirect no transmite datos, llama de jsp a jsp --%>
		<c:redirect url="core_redirect2.jsp"/>
		<%-- pero url si transmite datos --%>
	</c:if>
	
</div>
</form>
</body>
</html>