<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style>
.clase1 {
	font-size: 20px;
	color: blue;
}

.clase2 {
	font-size: 15px;
	color: blue;
}

body {
	background-color: #AAA
}
</style>
</head>
<body>
	<form>
		<p class="clase1">
			<label>JSTL</label>
		<hr />
		<div class="clase1">Elija opcion</div>
		<p />
		<ul>
			<li><a href="core_if.jsp">If</a></li>
			<li><a href="core_if_else.jsp">If Else (Choose)</a></li>
			<li><a href="core_choose.jsp">Choose</a></li>
			<li><a href="ForEach_Vector_List_ArrayList_Array">ForEach(Vector, List, ArrayList, Array)</a></li>
			<li><a href="ForEach_Map_TreeMap_Hashtable">ForEach (Map,TreeMap, Hashtable)</a></li>
			<li><a href="core_tokens.jsp">Tokens</a></li>
			<li><a href="core_redirect.jsp">Redirect</a></li>
			<li><a href="core_LeerParametros.html">Leer Parametros</a></li>
			<li><a href="core_import.jsp">Import</a></li>
			<li><a href="core_url.jsp">Url</a></li>
		</ul>
	</form>
</body>
</html>