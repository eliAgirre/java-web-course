<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Import</title>
<script type="text/javascript">

	function importpag(arg){
		/*
			Metodo get(Sin campo hidden)
			
			solo funciona mediante el tratamiento de excepciones
			ya que siempre seria get y nunca cambiaria por lo que no 
			podriamos preguntar por	la segunda vez
			
			document.location.href="core_import.jsp?pag="+arg;
		*/
		
		document.getElementById("id_oculto").value=arg;
		document.forms[0].method="post";
		document.forms[0].action="core_import.jsp";
		document.forms[0].submit();		
	}

</script>
	<style>
       .clase1 {color:blue;font-size:13pt;font-weight:bold}
       .clase2 {color:Sienna;font-size:15pt;font-weight:bold}       
   </style>
 </head>
<body>
<form>  
<div class="clase1">
<span class="clase2">IMPORT</span><hr/>

<%--
	 Funciona controlamos si es la primera vez mendiante
     el tratamiento de excepciones
<%--
<c:catch var="ex">

	<c:set var="arg_pagina" value="${param.pag}"/>
	
	<c:choose> 
	  	<c:when test="${arg_pagina=='1'}" > 
	   		<c:set var="pagina" value="import1.jsp"/>
	  	</c:when>
	   
	  	<c:when test="${arg_pagina=='2'}" > 
	   		<c:set var="pagina" value="import2.html"/>
	  	</c:when> 
	  
	  	<c:when test="${arg_pagina=='3'}" > 
	    	<c:set var="pagina" value="/carpeta/archivo.txt"/>
	  	</c:when> 
	</c:choose> 
	
	<c:import url="${pagina}"/>
<p>
</c:catch> 

<c:if test="${ex!=null}">
 	  <label>Primera vez</label>
</c:if>
 --%>

<%--Controlamos si es la primera vez interrogando al pageContext--%>

<%--La primera vez siempre es get --%> 
<c:if test="${pageContext.request.method=='GET'}">
	<label>Primera vez</label>
</c:if>

<%--No es la primera vez--%>
<c:if test="${pageContext.request.method =='POST'}">

	<c:set var="arg_pagina" value="${param.pag}"/>
	
	<c:choose> 
	  	<c:when test="${arg_pagina=='1'}" > 
	   		<c:set var="pagina" value="import1.jsp"/>
	  	</c:when>
	   
	  	<c:when test="${arg_pagina=='2'}" > 
	   		<c:set var="pagina" value="import2.html"/>
	  	</c:when> 
	  
	  	<c:when test="${arg_pagina=='3'}" > 
	    	<c:set var="pagina" value="/carpeta/archivo.txt"/>
	  	</c:when> 
	</c:choose> 
	
	<c:import url="${pagina}"/> 
</c:if>

<p style="font-size:25px;color:red">
	<label>y continuamos con nuestra hoja</label>
</p>

	<input type="button" name="btn1" value="Pagina Jps" onclick="importpag('1');"/><br/>
		
	<input type="button" name="btn2" value="Pagina Html" onclick="importpag('2');"/><br/>
			
	<input type="button" name="btn3" value="Archivo" onclick="importpag('3');"/><br/>
		
</div>
<input type="hidden" id="id_oculto" name="pag"/>
<hr/>
<%-- Sinomino
<a href="inicio.jsp">Volver</a>
--%>
<a href="<c:url value="inicio.jsp" />">Volver</a>
</form>
</body>
</html>