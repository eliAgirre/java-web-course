<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ForEach (Map,TreeMap,Hashtable)</title>
	  <style>
       .clase1 {color:blue;font-size:13pt;font-weight:bold}
       .clase2 {color:Sienna;font-size:15pt;font-weight:bold}       
      </style>
 </head>
<body>
<form>
<div class="clase1">
<span class="clase2">ForEach (Map TreeMap Hashtable)</span><hr/>
	<table border="1">
  		<tr style="color:red;font-weight:bold">
  			<th>Clave</th>
  			<th>Valor</th>  			
  		</tr>
    	<%--Formato simple--%>
		<c:forEach var="item" items="${requestScope.lista}">
		<tr>
			<td>
				<%--Funciona
				<c:out value="${item.key}" />
				--%>
				<c:set var="valor" value="${item.key}"/>
				<c:out value="${valor}"/>
			</td> 
			<%-- accede al valor y los muestra --%>
			<td><c:out value="${requestScope.lista[item.key]}" /></td>
		</tr>
	</c:forEach>
	</table>
</div>
<hr/>
<%-- Sinomino
<a href="inicio.jsp">Volver</a>
--%>
<a href="<c:url value="inicio.jsp" />">Volver</a>
</form>
</body>
</html>