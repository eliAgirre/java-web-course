<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tokens</title>
	  <style>
       .clase1 {color:blue;font-size:13pt;font-weight:bold}
       .clase2 {color:Sienna;font-size:15pt;font-weight:bold}       
      </style>
 </head>
<body>
<form>
<div class="clase1">
<span class="clase2">TOKENS</span><hr/>

<c:set var="dias" value="Lunes;Martes;Miercoles;Jueves;Viernes;Sabado;Domingo"/>

  <table border="1">
  	<tr style="color:red;font-weight:bold">
  		<th>Valor</th>
  		<th>Elemento</th>
  		<th>Indice</th>
  	</tr>
  	
  	<%--Esta etiqueta es igual a foreach, pero itera por marcas que son 
  	separados por delimitadores. El atributo que se utiliza para delimitar es delims, --%>
  	
	<c:forTokens var = "dia" items="${dias}" delims=";" varStatus= "estado">
	 <tr> 
		<td> <c:out value="${dia}"/> </td>
		<td> <c:out value="${estado.count}" /></td>
		<td> <c:out value="${estado.index}" /></td>		
	  </tr>
	</c:forTokens>
  </table>
</div>
<hr/>
<%-- Sinomino
<a href="inicio.jsp">Volver</a>
--%>
<a href="<c:url value="inicio.jsp" />">Volver</a>
</form>
</body>
</html>