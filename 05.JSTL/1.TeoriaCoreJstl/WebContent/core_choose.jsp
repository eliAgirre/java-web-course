<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>IF</title>
	  <style>
       .clase1 {color:blue;font-size:13pt;font-weight:bold}
       .clase2 {color:Sienna;font-size:15pt;font-weight:bold}       
      </style>
 </head>
<body>
<form>
<div class="clase1">
<span class="clase2">CHOOSE</span><hr/>
	<%--Funciona
	<c:set var="variableNumerica" value="${15}"/>
	--%>
	<c:set var="variableNumerica" value="15"/>
	
	<label>Valor variable numerica=</label><c:out value="${variableNumerica}"/>
	
	<br/>
	<c:choose> 
	  	<c:when test="${variableNumerica==1}" > 
	   		<label>Valor 1</label>
	  	</c:when>
	   
	  	<c:when test="${variableNumerica>2 && variableNumerica<10}" > 
	   		<label>Entre 3 y 9</label>
	  	</c:when> 
	  
	  	<c:when test="${variableNumerica==11|| variableNumerica==13}" > 
	    	<label>Valor 11 o 13</label>
	  	</c:when> 
	  
	  	<c:otherwise> 
	   		<label>otros</label>
	  	</c:otherwise> 
	</c:choose>  

	<hr/>

	<%--Funciona
	<c:set var="variableAlfabetica" value="Hola"/>
	--%>
	
	<c:set var="variableAlfabetica" value="${'Hola'}"/>
	
	<label>Valor variable alfabetica=</label><c:out value="${variableAlfabetica}"/>
	
	<br/>
	<c:choose> 
		<c:when test="${variableAlfabetica =='hola'}"> 
	   		<label>Valor hola</label>
	  	</c:when>
	   
	 	<c:when test="${variableAlfabetica =='Adios'}"> 
	   		<label>Valor Adios</label>
	  	</c:when> 
	  
	  	<c:when test="${variableAlfabetica =='Hola'}"> 
	   	    <label>Valor Hola</label>
	 	 </c:when> 
	  
	  	<c:otherwise> 
	   		<label>otros</label>
	  	</c:otherwise> 
	</c:choose> 
</div>
<hr/>
<%-- Sinomino
<a href="inicio.jsp">Volver</a>
<a href="<c:url value="inicio.jsp" />">Volver</a>
--%>
<c:set var="ir_pagina" value="inicio.jsp"/>
<a href="<c:url value="${ir_pagina}"/>">Volver</a>
</form>
</body>
</html>