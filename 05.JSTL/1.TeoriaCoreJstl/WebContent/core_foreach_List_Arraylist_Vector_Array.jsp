<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Vector List ArrayList Array</title>
	  <style>
       .clase1 {color:blue;font-size:13pt;font-weight:bold}
       .clase2 {color:Sienna;font-size:15pt;font-weight:bold}       
      </style>
 </head>
<body>
<form>
<div class="clase1">
<span class="clase2">ForEach (Vector,List,ArrayList,Array</span><hr/>
  <table>
    <%--Formato simple--%>
	<c:forEach var="item" items="${requestScope.lista}">
	  <tr> 
    	<td align="left" bgcolor="#ffffff"> 
   			 <c:out value="${item}"/>
  		</td>
  	  </tr>
	</c:forEach>	
  </table>	
  
  <hr/>
   <%--Formato completo --%>
  <c:set var="inicio" value="2"/>
  <c:set var="fin" value="5"/>
  <c:set var="paso" value="2"/>  
 
  <table border="1">
  	<tr style="color:red;font-weight:bold">
  		<th>Valor</th>
  		<th>Elemento</th>
  		<th>Indice</th>
  	</tr>
 	<c:forEach var="item" items="${requestScope.lista}" varStatus="current"
  	 	begin="${inicio}" end="${fin}" step="${paso}">
  	  <tr> 
		<td> <c:out value="${item}"/> </td>
		<td> <c:out value="${current.count}" /></td>
		<td> <c:out value="${current.index}" /></td>		
	  </tr>
	 </c:forEach>
	</table>
	
	<hr/>
	
 	<select name="meses" size="5"> 
  		<c:forEach var="item" items="${requestScope.lista}"  varStatus="current"> 	
  		  		
  		<c:choose> 
	  		<c:when test="${current.index == 2}">
	    		<option selected="selected"><c:out value="${item}"/></option>
	  		</c:when>
	  			  	 
	  		<c:otherwise> 
	   	 		<option><c:out value="${item}"/></option>
	  		</c:otherwise> 
		</c:choose> 		 
  		
	</c:forEach>	
</select>
<br/>

</div>
<hr/>
<%-- Sinomino
<a href="inicio.jsp">Volver</a>
--%>
<a href="<c:url value="inicio.jsp" />">Volver</a>
</form>
</body>
</html>