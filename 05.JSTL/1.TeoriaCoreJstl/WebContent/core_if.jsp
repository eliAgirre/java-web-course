<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>IF</title>
	  <style>
       .clase1 {color:blue;font-size:13pt;font-weight:bold}
       .clase2 {color:Sienna;font-size:15pt;font-weight:bold}       
      </style>
 </head>
<body>
<form>
<div class="clase1">

	<span class="clase2">IF</span><hr/>
	
	<c:set var="variableNumerica" value="${15}"/>
	<%--Funciona
	<c:set var="variableNumerica" value="15"/>
	 --%>
	<label>Valor variable numerica=</label><c:out value="${variableNumerica}"/>
	
	<br/>
	<c:if test="${(variableNumerica >= 10)  && (variableNumerica <= 20)}">
	   <label>Entre 10 y 20</label>  
	</c:if>
	
	<c:if test="${(variableNumerica < 10)  || (variableNumerica > 20)}">
	   <label>Menor a 10 o mayor a 20</label>
	</c:if>

	<%--Borramos la variable--%>
	<c:remove var="variableNumerica"/>

	<hr/>

	<c:set var="variableAlfabetica" value="${'Hola'}"/>
	
	<label>Valor variable alfabetica=</label><c:out value="${variableAlfabetica}"/>
	
	<br/>
	<c:if test="${(variableAlfabetica=='hola')}">
	   <label>Entra hola</label>
	</c:if>
	
	<c:if test="${(variableAlfabetica=='Hola')}">
	   <label>Entra Hola</label>
	</c:if>

	<hr/>

	<c:set var="variableAlfabetica" value="Hola"/>
	
	<label>Valor variable alfabetica=</label><c:out value="${variableAlfabetica}"/>
	<br/>
	<c:if test="${(variableAlfabetica=='hola')}">
	   <label>Entra hola</label>
	</c:if>
	
	<c:if test="${(variableAlfabetica=='Hola')}">
	   <label>Entra Hola</label>
	</c:if>
</div>

<hr/>
<%-- Sinomino
<a href="inicio.jsp">Volver</a>
--%>
<a href="<c:url value="inicio.jsp" />">Volver</a>
</form>
</body>
</html>