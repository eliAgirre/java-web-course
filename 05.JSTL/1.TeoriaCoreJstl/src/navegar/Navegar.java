package navegar;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Navegar {
	
	public static void navegar(String direccion,HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		
		// el m�todo RequestDispatcher es mejor 
		//que le sendRedirect para las jsp ya que
		// en este �ltimo necesita que el cliente se 
		//vuelva a conectar al
		// nuevo recurso mientras que con el RequestDispatcher
		//se maneja directamente
		// en el servidor
		
		
		//*****Sinonimo*****
		//RequestDispatcher despachador=
		//	getServletContext().
		//		getRequestDispatcher(direccion);
		//despachador.forward(req,resp); //Lanza la pagina
		
		//Lanza la pagina
		RequestDispatcher despachador=request.getRequestDispatcher(direccion+".jsp");
		
		despachador.forward(request,response); 
	}
}
