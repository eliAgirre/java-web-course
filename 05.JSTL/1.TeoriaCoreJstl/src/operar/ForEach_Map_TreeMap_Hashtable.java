package operar;

import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;






import navegar.Navegar;

@WebServlet("/ForEach_Map_TreeMap_Hashtable")
public class ForEach_Map_TreeMap_Hashtable extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			continuar(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		continuar(request,response);
	}
		
	protected void continuar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Funiona
		//Hashtable<String,String>lista=new Hashtable<String,String>();	
		//Map<String,String>lista=new HashMap<String,String>();
		TreeMap<String,String>lista=new TreeMap<String,String>();
		
		
		lista.put("1","Lunes");
		lista.put("2","Martes");
		lista.put("3","Miercoles");
		lista.put("4","Jueves");
		lista.put("5","Viernes");		
		
		request.setAttribute("lista", lista);
		
		Navegar.navegar("core_foreach_Map_TreeMap_Hashtable", request, response);	
		
	}

}