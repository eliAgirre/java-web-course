package operar;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import navegar.Navegar;

@WebServlet("/ForEach_Vector_List_ArrayList_Array")
public class ForEach_Vector_List_ArrayList_Array extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			continuar(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		continuar(request,response);
	}
		
	protected void continuar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Funciona
		//String lista[]={"Lunes","Martes","Miercoles","Jueves","Viernes"};
		//ArrayList<String> lista=new ArrayList<String>();
		//List<String> lista=new ArrayList<String>();		
		
		Vector<String> lista=new Vector<String>();
		
		lista.add("Lunes");
		lista.add("Martes");
		lista.add("Miercoles");
		lista.add("Jueves");
		lista.add("Viernes");		
		
		request.setAttribute("lista", lista);
		
		Navegar.navegar("core_foreach_List_Arraylist_Vector_Array",request, response);	
		
	}

}
