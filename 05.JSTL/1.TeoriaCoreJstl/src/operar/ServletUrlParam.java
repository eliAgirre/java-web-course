package operar;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import navegar.Navegar;

@WebServlet("/ServletUrlParam")
public class ServletUrlParam extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		continuar(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		continuar(request,response);
	}
			
	protected void continuar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
		request.setAttribute("dato1",request.getParameter("dato1"));
		request.setAttribute("dato2",request.getParameter("dato2"));
		
		Navegar.navegar("resultadoUrlParam", request, response);
		
	}

}
