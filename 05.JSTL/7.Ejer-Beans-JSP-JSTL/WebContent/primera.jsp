<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    
     <%--Para el fichero de idiomas--%>
	<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<fmt:setBundle basename="idioma.Messages" scope="session"/>

<title><fmt:message key="primera.titulo"/></title>

</head>
<body>







<form action="servletPrimera" method="post">
	<label style="color:blue;font-size:15pt"><fmt:message key="primera.titulo"/></label>
	<p/>
	<table>	

	<tr>
		<td>
			<fmt:message key="primera.apellido"/>	
			<input type="text" name="txtapellido" id="id_txtapellido"/>			
		</td>
	</tr>
	<tr>
		<td>
			<fmt:message key="primera.direccion"/>	
			<input type="text" name="txtdireccion" id="id_txtdireccion"/>			
		</td>
	</tr>
	<tr>
		<td>
			<input type="submit" value="<fmt:message key='inicio.continuar'/>"/>
		</td>
	</tr>
	</table>
</form>
</body>
</html>