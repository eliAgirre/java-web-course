function validar(){
	
	// se obtienen los datos desde los fields
	dni=document.forms[0].id_txtdni.value;
	nombre=document.forms[0].id_txtnombre.value;
	
	// se obtienen las etiquetas span para cambiar la visibilidad
	document.getElementById("errorDNI").style.visibility='hidden';
	document.getElementById("errorDNInum").style.visibility='hidden';
	document.getElementById("errorNombre").style.visibility='hidden';
	
	// variable booleano a devolver
	var resultado=true;
	
	if(dni.length==0){
		
		// se cambia la visibilidad del span
		document.getElementById("errorDNI").style.visibility ='visible';
		// se establece el valo a nulo
		document.forms[0].id_txtdni.value="";
		// se pone el cursos en el field de usuario
		document.forms[0].id_txtdni.focus();
		resultado=false;
	}
	if(nombre.length==0){
		
		// se cambia la visibilidad del span
		document.getElementById("errorNombre").style.visibility ='visible';
		// se establece el valo a nulo
		document.forms[0].id_txtnombre.value="";
		// se pone el cursos en el field de usuario
		document.forms[0].id_txtnombre.focus();
		resultado=false;
	}
	if(isNaN(dni)){
		// se cambia la visibilidad del span
		document.getElementById("errorDNInum").style.visibility ='visible';
		// se establece el valo a nulo
		document.forms[0].id_txtdni.value="";
		// se pone el cursos en el field de usuario
		document.forms[0].id_txtdni.focus();
		resultado=false;
	}
	
	return resultado;
}