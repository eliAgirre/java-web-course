<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    
	 <%--Para el fichero de idiomas--%>
	<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<fmt:setBundle basename="idioma.Messages" scope="session"/>

<title><fmt:message key="inicio.titulo"/></title>
<script type="text/javascript" src=js/validar.js></script>
</head>
<body>

<form action="servletInicio" method="post" onsubmit="return validar();">
	<label style="color:blue;font-size:15pt"><fmt:message key="inicio.titulo"/></label>
	<p/>
	<table>	

	<tr>
		<td>
			<fmt:message key="inicio.id"/>	
			<input type="text" name="txtdni" id="id_txtdni"/>			
		</td>
	</tr>
	<tr>
		<td>
			<fmt:message key="inicio.nombre"/>	
			<input type="text" name="txtnombre" id="id_txtnombre"/>			
		</td>
	</tr>
	<tr>
		<td>
			<input type="submit" value="<fmt:message key='inicio.continuar'/>"/>
		</td>
	</tr>
	</table>
</form>

	<%--Formato el fichero property--%>
	<span id="errorDNI" style="color:red;font-weight:bold;visibility:hidden;"><fmt:message key="inicio.errdni1"/></span><br/>
	<span id="errorDNInum" style="color:blue;visibility:hidden;"><fmt:message key="inicio.errdni2"/></span><br/>
	<span id="errorNombre" style="color:red;visibility:hidden;"><fmt:message key="inicio.errnombre"/></span>

</body>
</html>