<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 

<%--Para el fichero de idiomas--%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<jsp:useBean id="empleado" class="modelo.Persona" scope="session"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><fmt:message key="resultado.titulo"/></title>
</head>
<body>

	<h1><fmt:message key="resultado.subtitulo1"/></h1>

	<label style="font-weight:bold;"><fmt:message key="inicio.id"/></label>
	<jsp:getProperty property="dni" name="empleado"/>
	<br />
	<label style="font-weight:bold;"><fmt:message key="inicio.nombre"/></label>
	<jsp:getProperty property="nombre" name="empleado"/>
	<br />
	<label style="font-weight:bold;"><fmt:message key="primera.apellido"/></label>
	<jsp:getProperty property="apellido" name="empleado"/>
	<br />
	<label style="font-weight:bold;"><fmt:message key="primera.direccion"/></label>
	<jsp:getProperty property="direccion" name="empleado"/>
	<br />
	<label style="font-weight:bold;"><fmt:message key="segunda.sueldo"/></label>
	<%=empleado.getNomina().getSueldo() %>
	<br />
	<label style="font-weight:bold;"><fmt:message key="segunda.plus"/></label>
	<%=empleado.getNomina().getPlus() %>
	<br />
	<label style="font-weight:bold;"><fmt:message key="resultado.total"/></label>
	<%=request.getAttribute("total") %>
	<br />
	
	<h1><fmt:message key="resultado.subtitulo2"/></h1>

	<label style="font-weight:bold;"><fmt:message key="inicio.id"/></label>
	<c:out value="${sessionScope.empleado.dni}"/>
	<br />
	<label style="font-weight:bold;"><fmt:message key="inicio.nombre"/></label>
	<c:out value="${sessionScope.empleado.nombre}"/>
	<br />
	<label style="font-weight:bold;"><fmt:message key="primera.apellido"/></label>
	<c:out value="${sessionScope.empleado.apellido}"/>
	<br />
	<label style="font-weight:bold;"><fmt:message key="primera.direccion"/></label>
	<c:out value="${sessionScope.empleado.direccion}"/>
	<br />
	<label style="font-weight:bold;"><fmt:message key="segunda.sueldo"/></label>
	<c:out value="${sessionScope.empleado.nomina.sueldo}"/>
	<br />
	<label style="font-weight:bold;"><fmt:message key="segunda.plus"/></label>
	<c:out value="${sessionScope.empleado.nomina.plus}"/>
	<br />
	<label style="font-weight:bold;"><fmt:message key="resultado.total"/></label>
	<c:out value="${requestScope.total}"/>
	<br /><br />

<label><a href="inicio.jsp"><fmt:message key="resultado.volver"/>	</a></label>

</body>
</html>