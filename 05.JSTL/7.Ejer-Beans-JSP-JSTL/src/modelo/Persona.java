package modelo;

public class Persona {
	
	private String dni;
	private String Nombre;
	private String direccion;
	private String apellido;
	private Nomina nomina;
	
	public String getDni() {
		return dni;
	}
	public Persona(){}
	public Persona(String dni, String nombre, 
			String direccion, Nomina nomina) {
		super();
		this.dni = dni;
		Nombre = nombre;
		this.direccion = direccion;
		this.nomina = nomina;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public Nomina getNomina() {
		return nomina;
	}
	public void setNomina(Nomina nomina) {
		this.nomina = nomina;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	@Override
	public String toString() {
		return "Persona [dni=" + dni + ", Nombre=" + Nombre + ", direccion="
				+ direccion + ", apellido=" + apellido + ", nomina=" + nomina
				+ "]";
	}
	
	
}
