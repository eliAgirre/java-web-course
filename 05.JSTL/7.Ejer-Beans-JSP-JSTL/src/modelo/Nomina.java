package modelo;

public class Nomina 
{
	private float sueldo;
	private float plus;
	

	public Nomina(){}
	
	public Nomina(float sueldo, float plus) {
		super();
		this.sueldo = sueldo;
		this.plus = plus;
	}

	public float getSueldo() {
		return sueldo;
	}
	public void setSueldo(float sueldo) {
		this.sueldo = sueldo;
	}
	public float getPlus() {
		return plus;
	}
	public void setPlus(float plus) {
		this.plus = plus;
	}
	@Override
	public String toString() {
		return "Nomina [sueldo=" + sueldo + ", plus=" + plus + "]";
	}
	
}
