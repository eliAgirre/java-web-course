package negocio;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import operaciones.Operar;
import navegar.Navegar;
import modelo.Nomina;
import modelo.Persona;

/**
 * Servlet implementation class servletInicio
 */
@WebServlet("/servletSegunda")
public class servletSegunda extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public servletSegunda() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		gestionaSesion(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		gestionaSesion(request, response);
		
	}
	
	
	private void gestionaSesion (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession sesion = request.getSession();
		
		Persona p = (Persona) sesion.getAttribute("empleado");
		
		Nomina n = new Nomina();
		
		n.setSueldo(Float.parseFloat(request.getParameter("txtsueldo")));
		n.setPlus(Float.parseFloat(request.getParameter("txtplus")));
		
		
		p.setNomina(n);
		
		
		sesion.setAttribute("empleado", p);
		
		Operar o = new Operar();
		
		o.setSueldo(n.getSueldo());
		o.setPlus(n.getPlus());
		
		request.setAttribute("total", o.getTotal());
		
	
		Navegar.navegar("resultado", request, response);
		
	
	}
	
	

}
