package negocio;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import navegar.Navegar;
import modelo.Persona;

/**
 * Servlet implementation class servletInicio
 */
@WebServlet("/servletInicio")
public class servletInicio extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public servletInicio() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		gestionaSesion(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		gestionaSesion(request, response);
		
	}
	
	
	private void gestionaSesion (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession sesion = request.getSession();
		
		Persona p = new Persona();
		
		p.setDni(request.getParameter("txtdni"));
		p.setNombre(request.getParameter("txtnombre"));
		
		
		sesion.setAttribute("empleado", p);
		
		Navegar.navegar("primera", request, response);
		
	
	}
	
	

}
