package operaciones;

public class Operar {

	private float sueldo;
	private float plus;
	
	public Operar(){}
	
	public Operar(float sueldo, float plus){
		
		this.sueldo = sueldo;
		this.plus = plus;
		
	}

	public float getSueldo() {
		return sueldo;
	}

	public void setSueldo(float sueldo) {
		this.sueldo = sueldo;
	}

	public float getPlus() {
		return plus;
	}

	public void setPlus(float plus) {
		this.plus = plus;
	}
	
	public float getTotal (){
		
		return (sueldo+plus)*0.75f;
		
	}
	
	
}
