<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<fmt:setBundle basename="idiomas.Messages" scope="session"/>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
   	<title><fmt:message key="inicio.titulo"/></title>
   
</head>
<body>
	<form>
		<label style="color:red;font-size:15pt">
			<fmt:message key="salida.texto" />
			<br/>			
			<fmt:message key="salida.cabecera" />
		</label>
		
		<p style="color:blue;font-size:15pt">		
			<fmt:message key="salida.dato"/>
			<c:out value="${requestScope.dato}"/>
		</p>
		
		<p/>
		
		<a href='inicio.jsp'><fmt:message key="salida.volver"/></a>
	</form>
</body>
</html>