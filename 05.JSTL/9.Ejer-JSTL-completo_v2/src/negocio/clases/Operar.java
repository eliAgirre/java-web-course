package negocio.clases;

import java.util.ArrayList;

import datos.Datos;

public class Operar {
	
	public int calcularTotal(int iMicro, int iPantalla, ArrayList<Integer> extras){
		
		Datos datos=new Datos();
		
		int precioMicro=datos.precioMicro(iMicro);
		
		int precioPantalla=datos.precioPantalla(iPantalla);
		
		int preciosExtras=datos.precioExtras(extras);
		
		return precioMicro+precioPantalla+preciosExtras;
	}
	
}
