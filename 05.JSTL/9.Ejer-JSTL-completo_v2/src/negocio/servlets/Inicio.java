package negocio.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utilidades.Navegar;
import modelo.Cliente;

public class Inicio extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    public Inicio() {super();}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		inicio(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		inicio(request,response);
	}
	
	private void inicio(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession sesion=request.getSession();
		
		Cliente c=new Cliente();
		
		c.setDni(request.getParameter("dni"));
		c.setNombre(request.getParameter("nombre"));
		c.setApellido(request.getParameter("apellido"));
		
		sesion.setAttribute("cliente", c);
		
		Navegar.navegar("micro", request, response);
		
	}
}
