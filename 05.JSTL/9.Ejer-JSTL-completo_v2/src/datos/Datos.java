package datos;

import java.util.ArrayList;
import java.util.Vector;

public class Datos {
	
	private Vector<Integer> preciosMicro= new Vector<Integer>();
	private Vector<Integer> preciosPantalla= new Vector<Integer>();
	private ArrayList<Integer> preciosExtras = new ArrayList<Integer>();
	
	public Datos(){
		
		preciosMicro.add(100); // intel
		preciosMicro.add(50); // amd
		
		preciosPantalla.add(50); // 19'
		preciosPantalla.add(75); // 21'
		preciosPantalla.add(100); // 23'
		
		preciosExtras.add(50);
		preciosExtras.add(50);
		preciosExtras.add(50);
		
	}
	
	public int precioMicro(int micro){
		
		int precio=0;
		
		for(int i=0;i<preciosMicro.size();i++){
			
			if(i==micro){
				precio=preciosMicro.get(i);
			}

		}
		
		return precio;
	}
	
	public int precioPantalla(int pantalla){
		
		int precio=0;
		
		for(int i=0;i<preciosPantalla.size();i++){
			
			if(i==pantalla){
				precio=preciosPantalla.get(i);
			}

		}
		
		return precio;
	}
	
	public int precioExtras(ArrayList<Integer> extras){
		
		int total=0;
		
		for(int i=0;i<extras.size();i++){
			
			total+=preciosExtras.get(i);
			
		}
		
		return total;
	}

}
