package interfaces;

import java.util.ArrayList;

public interface Pc_I {
	
	public int getMicro();
	public int getPantalla();
	public ArrayList<Integer> getExtras();
	public int getTotal();
	
	public void setMicro(int micro);
	public void setPantalla(int pantalla);
	public void setExtras(ArrayList<Integer> extras);
	public void setTotal(int total);
	
}