<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<fmt:setBundle basename="properties.Messages" scope="session"/>
<title><fmt:message key="pantalla.titulo"/></title>
</head>
<body>

	<p style="font-size:16pt;font-weight:bold;"><fmt:message key="pantalla.titulo"/></p>
	
	<form action="Pantalla" method="post">
		
		<label><fmt:message key="micro.label"/></label><br/><br/>
		
		<select name="opPantalla" size="3" >
			<c:forEach var="item" items="${requestScope.lista}" varStatus="current">

				<c:choose>
					<c:when test="${current.index == 0}">
						<option selected="selected"><c:out value="${item}" /></option>
					</c:when>

					<c:otherwise>
						<option><c:out value="${item}" /></option>
					</c:otherwise>
				</c:choose>

			</c:forEach>
		</select><br/><br/>
		
		<input type="submit" value="<fmt:message key='index.boton.aceptar'/>" />
	
	</form>

</body>
</html>