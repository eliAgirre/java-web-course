package negocio;

import datos.Datos;

public class Operar {
	
	public int calcularTotal(int iMicro, int iPantalla){
		
		Datos datos=new Datos();
		
		int precioMicro=datos.precioMicro(iMicro);
		
		int precioPantalla=datos.precioPantalla(iPantalla);
		
		return precioMicro+precioPantalla;
	}
	
}
