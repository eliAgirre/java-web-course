package servlets;

import java.io.IOException;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import navegar.Navegar;
import modelo.Pc;

public class Micro extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

    public Micro() { super(); }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		micro(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		micro(request,response);
	}
	
	private void micro(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession sesion=request.getSession();
		
		Vector<Integer> lista=new Vector<Integer>();
		
		String sMicro=request.getParameter("opMicro");
		String sPpagina="pantalla";
		int iMicro=0;
		
		if(sMicro.equals("0"))iMicro=0;
		else iMicro=1;
		
		Pc pc=new Pc();
		
		pc.setMicro(iMicro);
		
		sesion.setAttribute("pc", pc);
		
		lista.add(19);
		lista.add(21);
		lista.add(23);
		
		request.setAttribute("lista", lista);
		
		Navegar.navegar(sPpagina, request, response);
		
	}
	
}
