package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import navegar.Navegar;
import negocio.Operar;
import modelo.Cliente;
import modelo.Pc;

public class Resultado extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    public Resultado() { super(); }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		pantalla(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		pantalla(request,response);
	}
	
	private void pantalla(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession sesion=request.getSession();
		
		Cliente c=(Cliente)sesion.getAttribute("cliente");
		
		String sPantalla=request.getParameter("opPantalla");
		int iPantalla=0;
		int iTotal=0;
		
		switch(sPantalla){
		
			case "19":
				iPantalla=0;
				break;
			case "21":
				iPantalla=1;
				break;
			case "23":
				iPantalla=2;
				break;
			default:
				iPantalla=-1;
		}
			
		
		Pc pc=(Pc)sesion.getAttribute("pc");
		
		pc.setPantalla(iPantalla);
		
		c.setPc(pc);
		
		Operar o=new Operar();
		
		iTotal=o.calcularTotal(pc.getMicro(), pc.getPantalla());
		
		pc.setTotal(iTotal);
		
		c.setPc(pc);
		
		sesion.setAttribute("pc", pc);
		
		sesion.setAttribute("cliente", c);
		
		Navegar.navegar("resultado", request, response);
		
	}

}
