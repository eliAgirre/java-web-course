package navegar;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Navegar {
	
	public static void navegar(String direccion,HttpServletRequest req,HttpServletResponse resp) throws ServletException, IOException {
		
		RequestDispatcher despachador=req.getRequestDispatcher(direccion+".jsp");
		
		despachador.forward(req,resp); 
		
	} // navegar

}