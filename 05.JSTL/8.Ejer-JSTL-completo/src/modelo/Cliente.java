package modelo;

import interfaces.Cliente_I;

public class Cliente implements Cliente_I{
	
	private String dni;
	private String nombre;
	private String apellido;
	private Pc pc;
	
	public Cliente(){}

	public Cliente(String dni, String nombre, String apellido) {

		this.dni = dni;
		this.nombre = nombre;
		this.apellido = apellido;
	}

	public Cliente(String dni, String nombre, String apellido, Pc pc) {

		this.dni = dni;
		this.nombre = nombre;
		this.apellido = apellido;
		this.pc = pc;
	}

	@Override
	public String getDni() {
		
		return this.dni;
	}

	@Override
	public String getNombre() {
		
		return this.nombre;
	}

	@Override
	public String getApellido() {
	
		return this.apellido;
	}

	@Override
	public Pc getPc() {
	
		return this.pc;
	}

	@Override
	public void setDni(String dni) {
		
		this.dni=dni;
	}

	@Override
	public void setNombre(String nombre) {
		
		this.nombre=nombre;
	}

	@Override
	public void setApellido(String apellido) {
		
		this.apellido=apellido;
	}

	@Override
	public void setPc(Pc pc) {
		
		this.pc=pc;
	}

	@Override
	public String toString() {

		return "Cliente => dni: "+dni+", nombre: "+nombre+", apellido: "+apellido+", pc: "+pc;
	}
}
