package negocio;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import navegar.Navegar;

@WebServlet("/Validar")
public class Validar extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    public Validar() { super(); } // constructor vacio

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		operar(request,response);
		
	} // doGet


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		operar(request,response);
		
	} // doPost
	
	private void operar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// se obtienen los datos desde el form
		String sUser=request.getParameter("user");
		String sPass=request.getParameter("pass");
		
		String sPagina="";
		
		if(sUser.equalsIgnoreCase("angel") &&  sPass.equals("miguel")){ // si coinciden el usuario y la clave
			
			// se pasan 2 atributos desde la request
			request.setAttribute("user", sUser);
			request.setAttribute("pass", sPass);
			// la pagina donde se muestran los datos
			sPagina="ResultadoBien";
			
		}
		else{
			
			// Si no coincide ni el usuario ni la clave
			sPagina="ResultadoMal";
		}
		
		Navegar.navegar(sPagina,request,response);
		
	} // operar

}
