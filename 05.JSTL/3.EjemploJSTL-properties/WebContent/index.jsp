<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%--Accedemos al fichero property--%>
<fmt:setBundle basename="properties.Messages" scope="session"/>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><fmt:message key="msg.titulo"/></title>
<script type="text/javascript" src=js/validar.js></script>
</head>
<body onload="document.forms[0].userID.focus();">

	<h4><fmt:message key="msg.texto"/></h4>
	
	<form action="Validar" method="post" onsubmit="return validar();">
	
		<label><fmt:message key="msg.user"/></label>
		<input type="text" name="user" id="userID" /><br/><br/>
		
		<label><fmt:message key="msg.pass"/></label>
		<input type="password" name="pass" id="passID" /><br/><br/>
		
		<input type="submit" value="<fmt:message key='login.botonSubmit'/>" />
		<input type="reset" value="<fmt:message key='login.botonReset'/>" />
		
	</form><br/>
	
	<%--Formato el fichero property--%>
	<span id="errorUser" style="color:blue;visibility:hidden;"><fmt:message key="login.user.error"/></span><br/>
	<span id="errorPass" style="color:red;visibility:hidden;"><fmt:message key="login.pass.error"/></span>

</body>
</html>