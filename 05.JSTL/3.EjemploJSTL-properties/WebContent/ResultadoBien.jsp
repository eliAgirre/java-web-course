<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><fmt:message key="ResultadoBien.titulo"/></title>
</head>
<body>

	<%--desde el fichero properties--%>
	<label style="font-weight:bold;"><fmt:message key="ResultadoBien.user"/></label>
	
	<%--muestra el core la request que ha guardado servlet--%>
	<c:out value="${requestScope.user}"/><br>
	
	<label style="font-weight:bold;"><fmt:message key="ResultadoBien.pass"/></label>
	<c:out value="${requestScope.pass}"/><br>
	
	<a href="index.jsp"><fmt:message key="Resultado.volver"/></a>
	
</body>
</html>