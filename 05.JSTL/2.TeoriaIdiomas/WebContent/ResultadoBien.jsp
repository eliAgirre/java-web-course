<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<html>
	<head>
	 <style>
	    .clase1 {color:navy}
        .clase2 {color:red;font-size:15pt;font-weight:bold}
      </style>
      
    <title><fmt:message key="Resultado.titulo"/></title>
	</head>

	<body>

	  <hr class="clase1"/>
	    <label class="clase2">
	    	<fmt:message key="ResultadoBien.nombre" /> <c:out value="${requestScope.nombre}"/>
	    </label>
		<br/>
		 <label class="clase2">
		    <fmt:message key="ResultadoBien.password"/> <c:out value="${requestScope.pass}"/>
        </label> 
      <hr class="clase1"/>
	  
	  <a href="Registrar.jsp"><fmt:message key="ResultadoBien.volver"/></a>
	</body>
	
</html>
