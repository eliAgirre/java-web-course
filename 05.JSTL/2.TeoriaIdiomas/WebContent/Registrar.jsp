<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--Para el fichero de idiomas--%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
 <html>
	<%--Accedemos al fichero property--%>
	<fmt:setBundle basename="idiomas.Messages" scope="session"/>
	<head>	
	<title><fmt:message key="registrar.titulo"/></title>
	  <style>
       .clase1 {color:red;font-size:15pt;font-weight:bold}
      </style>
      
      <script type="text/javascript">
      function validar(){
    	  
    	  var msg="";
    	  var nombre=document.getElementById("idNombre").value;
    	  var pass=document.getElementById("idPass").value;
    	  var sw=true;
    	  
    	  //Cambiamos la visibilidad a falso 
    	  document.getElementById("errorNombre").style.visibility ='hidden';
    	  document.getElementById("errorPass").style.visibility ='hidden';
    	 
    	  if(nombre.length==0){
    		  
    		  sw=false;
    		  //Cambiamos la visibilidad a verdadero
    		  document.getElementById("errorNombre").style.visibility ='visible';    		 
    		  document.forms[0].idPass.value="";
    		  document.forms[0].idNombre.focus();
    	  }
    		
    	  if(pass.length==0){
    		  sw=false;
    		  //Cambiamos la visibilidad a verdadero
    		  document.getElementById("errorPass").style.visibility ='visible';
    		  document.forms[0].idNombre.value="";
    		  document.forms[0].idNombre.focus();
    	  }
    	  
    	  return sw;
      }
      
      </script>
      
 	</head>

	<body onload="document.forms[0].idNombre.focus();">
	
		<div class="clase1">
			<fmt:message key="registrar.texto"/>
		</div>
		<br/>
		<form action="Validar" method="post" onsubmit="return validar();"> 
		
		<table>
			<tr>
				<td>
					<fmt:message key="registrar.nombre"/>
				</td>
				<td>
					<input type="text" id="idNombre" name="txtnombre"/>
				</td>
			</tr>
			
			<tr>
				<td>
					<fmt:message key="registrar.password"/>
				</td>
				<td>
					<input type="password" id="idPass" name="txtpassword"/>
				</td>				
			</tr>
			
			<tr>
				<td align="center">
					<input type="submit" value="<fmt:message key='registrar.botonsubmit'/>"/>					
				</td>
				<td width="50">
					<input type="reset" value="<fmt:message key='registrar.botonreset'/>"/>					 
				</td>
				<td></td>														
			</tr>	
						
		</table> 
		<p/>
		
		<%--Formato el fichero property--%>
		<span id="errorNombre" style="visibility:hidden">
			<fmt:message key="registrar.nombre.requerido"/>
			<br/>
		</span>  
		
		<span id="errorPass" 
			style="visibility:hidden;color:red;font-size:12pt;font-weight:bold">
				<fmt:message key="registrar.password.requerido"/>
		</span>	
		
		</form>		 
	</body>	
</html>
