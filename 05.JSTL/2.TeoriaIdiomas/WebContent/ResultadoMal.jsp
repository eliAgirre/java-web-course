<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<html>
	<head>
	 <style>
         .clase1 {color:red;font-size:15pt;font-weight:bold}
      </style>
      
    <title><fmt:message key="ResultadoMal.titulo"/></title>
	</head>

	<body>

	<div class="clase1"><fmt:message key="ResultadoMal.error"/></div>
      <hr/>
	   <a href="Registrar.jsp"><fmt:message key="ResultadoMal.volver"/></a>
	 
	</body>
	
</html>
