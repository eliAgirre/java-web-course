package operar;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import navegar.Navegar;

@WebServlet("/Validar")
public class SeleccionarIdioma extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public SeleccionarIdioma() {
        super();
        
    }

	
	protected void doGet(HttpServletRequest request, 
		HttpServletResponse response) 
			throws ServletException, IOException 
	{
		continuar(request,response);
	}

	protected void doPost(HttpServletRequest request, 
		HttpServletResponse response) 
			throws ServletException, IOException 
	{
		continuar(request,response);
	}
	
	protected void continuar(HttpServletRequest request, 
			HttpServletResponse response) 
				throws ServletException, IOException 
	{
		String nombre=request.getParameter("txtnombre");
		String pass=request.getParameter("txtpassword");
		String pagina;
		
		if(nombre.equalsIgnoreCase("angel") &&  pass.equals("miguel"))
		{
			pagina="ResultadoBien";
			request.setAttribute("nombre", nombre);
			request.setAttribute("pass", pass);
		}			
		else
			pagina="ResultadoMal";
		
		Navegar.navegar(pagina, request, response);
	}

}
