package operar;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import navegar.Navegar;

@WebServlet("/Continuar")
public class Continuar extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, 
		HttpServletResponse response) 
				throws ServletException, IOException 
	{
			continuar(request,response);
	}

	protected void doPost(HttpServletRequest request, 
		HttpServletResponse response) 
			throws ServletException, IOException 
	{
		continuar(request,response);
	}
		
	protected void continuar(HttpServletRequest request, 
			HttpServletResponse response) 
				throws ServletException, IOException 
	{
		HttpSession sesion=request.getSession();
		
		//Teoria (Acceder al idioma desde un servlet)
		Locale locale = (Locale) sesion.getAttribute
				("javax.servlet.jsp.jstl.fmt.locale.session");		
				 
		if (locale != null)
		{
			String lang = (String) locale.getLanguage();
			System.out.println(lang);		
		}
		
		request.setAttribute("dato", request.getParameter("txtdato"));
		
		Navegar.navegar("salida", request, response);	
		
	}

}
