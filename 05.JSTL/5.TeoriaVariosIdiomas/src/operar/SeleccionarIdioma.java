package operar;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import navegar.Navegar;

@WebServlet("/SeleccionarIdioma")
public class SeleccionarIdioma extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public SeleccionarIdioma() {
        super();
        
    }
    
	protected void doGet(HttpServletRequest request, 
		HttpServletResponse response) 
			throws ServletException, IOException 
	{
		continuar(request,response);
	}

	protected void doPost(HttpServletRequest request, 
		HttpServletResponse response) 
			throws ServletException, IOException 
	{
		continuar(request,response);
	}
	
	protected void continuar(HttpServletRequest request, 
			HttpServletResponse response) 
				throws ServletException, IOException 
	{
		String idioma=request.getParameter("opidioma");
		String pagina="inicio";
		
		if(idioma.equals("0")) idioma="espanol";
		else idioma="ingles";	
		
		//Devolvemos el idioma
		request.setAttribute("idioma", idioma);
			
		Navegar.navegar(pagina, request, response);
	}
}
