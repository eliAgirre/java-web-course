<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%--Para el fichero de idiomas--%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript">
	function continuar(arg)
	{
		var servlet="";
		if(arg=="0")servlet="Continuar";
		else servlet="SeleccionarIdioma";
		
		formulario=document.forms[0];
		formulario.method="post";
		formulario.action=servlet;
		formulario.submit();			
	}
</script>

<c:set var="varsesion" value="${requestScope.idioma}"/>

<%--La primera vez en castellano--%>
<c:if test="${varsesion==null}">
	<fmt:setLocale value="es" scope="session"/>	
</c:if>

<%--Cambiamos el idioma--%>
<c:if test="${varsesion!=null}">

	<c:if test="${requestScope.idioma=='espanol'}">
		<fmt:setLocale value="es" scope="session"/>
	</c:if>
	
	<c:if test="${requestScope.idioma=='ingles'}">
		<fmt:setLocale value="en" scope="session"/>
	</c:if>
	
</c:if>

<fmt:setBundle basename="idiomas.Messages" scope="session"/>

<title><fmt:message key="inicio.titulo"/></title>

</head>
<body onload="document.forms[0].id_txtdato.focus();">

<form>
	<label style="color:red;font-size:15pt"><fmt:message key="inicio.titulo"/></label>
	<p/>
	<table>
	<tr>
		<td> 
			<input type="radio" name="opidioma" value="0" checked="checked"/>
			<fmt:message key="inicio.op1"/>	
		</td>
	</tr>
	<tr>
		<td>
			<input type="radio" name="opidioma" value="1"/>
			<fmt:message key="inicio.op2"/>	
		</td>
	</tr>
	<tr>
		<td>
			<fmt:message key="inicio.texto"/>	
			<input type="text" name="txtdato" id="id_txtdato"/>			
		</td>
	</tr>
	<tr>
		<td>
			<input type="button" 
				value="<fmt:message key='inicio.BotonEnviar'/>"
					onclick="continuar('0');"/>
		</td>
	</tr>
	<tr>
		<td>
			<input type="button" value="<fmt:message key='inicio.BotonCambiarIdioma'/>"
				onclick="continuar('1');"/>
		</td>
	</tr>
	</table>
</form>
</body>
</html>