<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Inicio</title>
</head>
<body>

	<form method="post" action="leerParametros.jsp">
	
		<label>Nombre:</label>
		<input type="text" name="nombre" /><br/><br/>

		<%--lista desplegable--%>
		<label>Opciones:</label>
		<select name="opcion" size="5" <%--multiple="multiple"--%>>
			<c:forEach var="item" items="${requestScope.lista}" varStatus="current">

				<c:choose>
					<c:when test="${current.index == 0}">
						<option selected="selected"><c:out value="${item}" /></option>
					</c:when>

					<c:otherwise>
						<option><c:out value="${item}" /></option>
					</c:otherwise>
				</c:choose>

			</c:forEach>
		</select><br/><br/>
		
		<input type="submit" value="Enviar" />

	</form>
	
</body>
</html>