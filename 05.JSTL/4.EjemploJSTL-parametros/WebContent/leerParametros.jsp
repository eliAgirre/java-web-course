<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Leer parametros</title>
</head>
<body>
	<label style="font-weight:bold;">Nombre: </label> <c:out value="${param.nombre}" /><br/><br/>
	
	<label style="font-weight:bold;">Opciones: </label> <c:out value="${param.opcion}" /><br/><br/>
	
	<%--<c:forEach var="item" items="${param.opcion}">
	  <tr> 
    	<td align="left"> 
   			 <c:out value="${item}"/>
  		</td>
  	  </tr>
	</c:forEach>--%>
	
	<a href="<c:url value="Operar" />">Volver</a>
	
</body>
</html>