package negocio;

import java.io.IOException;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import navegar.Navegar;


@WebServlet("/Operar")
public class Operar extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

    public Operar() {super();} // constructor vacio

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		operar(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		operar(request,response);
	}
	
	private void operar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// se crea una lista de tipo Vector => String
		Vector<String> lista=new Vector<String>();
		
		// se agregan elementos a la lista
		lista.add("Garaje");
		lista.add("Jardin");
		lista.add("Terraza");
		lista.add("WiFi");
		lista.add("Salon");
		
		// se guarda en la request
		request.setAttribute("lista", lista);
		
		Navegar.navegar("index",request, response);
		
	}

}
