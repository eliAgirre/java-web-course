package teoria_etiquetas;

import com.opensymphony.xwork2.ActionSupport;

import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class GeneratorSubset extends ActionSupport 
{
	private List<String> listaNombres;
	
	public String execute() 
	{
		listaNombres = new ArrayList<String>();
		listaNombres.add("Juan");
		listaNombres.add("Ana Lopez");
		listaNombres.add("Pedro Gonzalez");
		listaNombres.add("Luis Fernandez");
		listaNombres.add("Maria Garcia");
		
		return "SUCCESS";
	}
	
	public List<String> getListaNombres() 
	{
		return listaNombres;
	}

	public void setListaNombres(List<String> listaNombres) 
	{
		this.listaNombres = listaNombres;
	}	
}
