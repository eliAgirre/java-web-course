package teoria_etiquetas;

import com.opensymphony.xwork2.ActionSupport;

import java.util.*;

public class CursosElegidosDinamico extends ActionSupport
{

	private static final long serialVersionUID = 1L;
	private List<String> cursos=new ArrayList<String>();
	private List<String> nombresCursos=new ArrayList<String>();
	
	private String[] arrcurso={"Java","C++","Visual Basic"};
	
	public String execute()
	{
		return "SUCCESS";
	}
	
	public List<String> getPosCursos()
	{
		return cursos;
	}
	public List<String> getNombresCursos()
	{
		return nombresCursos;
	}

	public void setCursos(List<String> cursos) 
	{
		this.cursos = cursos;
		Iterator<String> it=cursos.iterator();
		while(it.hasNext())
		{
			nombresCursos.add(arrcurso[Integer.parseInt(it.next())]);
		}

	}
}

