package teoria_etiquetas;

import com.opensymphony.xwork2.ActionSupport;

import java.util.Date;


public class Fecha extends ActionSupport 
{
	private static final long serialVersionUID = 1L;
	
	private Date miFecha;
	
	public String execute() 
	{
		setMiFecha(new Date());
    
		return "SUCCESS";
	}
	
	public void setMiFecha(Date date)
	{
    	this.miFecha = date;
  	}
  	
  	public Date getMiFecha()
  	{
    	return miFecha;
  	}

}
