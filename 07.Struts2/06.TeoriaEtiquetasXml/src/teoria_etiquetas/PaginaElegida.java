package teoria_etiquetas;

import com.opensymphony.xwork2.ActionSupport;


import java.util.*;

public class PaginaElegida extends ActionSupport
{

	private static final long serialVersionUID = 1L;
	
	private ArrayList<String> pagina;
	
	public String execute()
	{
		return "SUCCESS";
	}

	public ArrayList<String> getPagina()
	{
		return pagina;
	}

	public void setPagina(ArrayList<String> pagina) 
	{
		this.pagina = pagina;
	}	
}
