package teoria_etiquetas;

import com.opensymphony.xwork2.ActionSupport;


import java.util.HashMap;

@SuppressWarnings("serial")
public class LenguajesMap extends ActionSupport 
{
	private HashMap<String, String> lenguajesEstructurados;
	private HashMap<String, String> lenguajesPoo;	
	

	public HashMap<String, String> getLenguajesEstructurados() 
	{
		return lenguajesEstructurados;
	}

	public void setLenguajesEstructurados
		(HashMap<String, String> lenguajesEstructurados) 
	{
		this.lenguajesEstructurados = lenguajesEstructurados;
	}

	public HashMap<String, String> getLenguajesPoo() 
	{
		return lenguajesPoo;
	}

	public void setLenguajesPoo(HashMap<String, String> lenguajesPoo) 
	{
		this.lenguajesPoo = lenguajesPoo;
	}

	public String execute() 
	{
		lenguajesPoo = new HashMap<String, String>();
		lenguajesPoo.put("java", "Java 6");
		lenguajesPoo.put("C++", "C++");
		lenguajesPoo.put("Asp", "Visual ASP.Net");
		
		lenguajesEstructurados = new HashMap<String, String>();
		lenguajesEstructurados.put("rpg", "RPG");
		lenguajesEstructurados.put("C", "C");
		lenguajesEstructurados.put("Cobol", "Cobol");

		return "SUCCESS";
	}
}