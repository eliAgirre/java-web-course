package teoria_etiquetas;


public class BeanPersonal 
{
		
	// JavaBeans
	
	private String nombre="";
	private String apellido="";
	private String dni="";
	
	public void setNombre(String nombre) 
	{
		this.nombre = nombre; 
	}

	public void setApellido(String apellido) 
	{
		this.apellido = apellido; 
	}

	public void setDni(String dni) 
	{
		this.dni = dni; 
	}

	public String getNombre() 
	{
		return (this.nombre); 
	}

	public String getApellido() 
	{
		return (this.apellido); 
	}

	public String getDni() 
	{
		return (this.dni); 
	}
	

}
