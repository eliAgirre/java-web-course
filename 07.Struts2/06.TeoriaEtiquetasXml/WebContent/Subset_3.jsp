<%@ taglib prefix="s" uri="/struts-tags" %>

<html> 
<%--
Genera un simple "Iterator a partir de una clase Action (Si se declara el arreglo dentro de la jsp
solo se crea una unica propiedad)" 
count: Delitima el numero de elementos del iterator
start: Para elegir el elemento de inicio 
--%>
  <head>
    <title> Subnet Tag  </title>
  </head>
  <body>
  <h1><span style="background-color: #FFFFcc">Teoria de la etiqueta Subnet (Eligiendo el inicio)</span></h1>
    <h3><font color="#0000FF"> Genera un simple Iterator </font></h3>
      <s:subset source="listaNombres" count="2" start="2">  <%-- empieza en el tercer elemento --%>      
        	<s:iterator>
          		<s:property /><br/>
        	</s:iterator>
      </s:subset>
	  <hr/>
	<a href="<s:url action='Volver'/>">Volver</a>	
  </body>
</html>  
