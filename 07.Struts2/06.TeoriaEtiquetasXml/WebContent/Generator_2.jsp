<%@ taglib prefix="s" uri="/struts-tags" %>

<html> 
<%--
Genera un simple "Iterator" 
--%>
  <head>
    <title> Generator Tag  </title>
  </head>
  <body>
  <div style="background-color:#FFFFcc;font-size:20pt;font-weight:bold">
  	Teoria de la etiqueta Generator (Segunda forma)
  </div>
    <h3><font color="#0000FF"> Genera un simple Iterator </font></h3>
      <s:generator val="listaNombres" separator=",">
        <s:iterator>
          <s:property /><br/>
        </s:iterator>
      </s:generator>
	  <hr/>
	<a href="<s:url action='Volver'/>">Volver</a>	
  </body>
</html>  
