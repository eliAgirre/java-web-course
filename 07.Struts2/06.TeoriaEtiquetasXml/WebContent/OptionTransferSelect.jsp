<%@ taglib uri="/struts-tags" prefix="s"%>

<html>
<%--
Crea un componente consistente en dos selects cuyos elementos pueden traspasarse de uno a otro.
org.apache.struts2.views.jsp.ui.OptionTransferSelectTag

doubleList (requerido): El iterador con los valores que tendr� el segundo select al comenzar.
doubleName (requerido): Nombre a utilizar para el elemento de la segunda lista
leftTitle: T�tulo del primer select.
list: El iterador con los valores que tendr� el primer select al comenzar.
rightTitle: T�tulo del segundo select

--%>
<body>

<s:form action="Resultado_OptionTransferSelect_Tag">
	<s:optiontransferselect  name="cursos" 
	list="lenguajesEstructurados" 
	doubleList="lenguajesPoo" 
	
	leftTitle="Lenguajes Estructrurados" 
	rightTitle="Lenguajes POO" doubleName="cursos"/>
	<s:submit value="Enviar" />
</s:form>

</body>
</html>
