<%@ taglib uri="/struts-tags" prefix="s"%>

<html>
<%--
Crea un checkbox HTML. 
org.apache.struts2.views.jsp.ui.CheckboxTag
value: Booleano que indica si el checkbox est� marcado o no.
--%>

<body>
<div style="background-color:#FFFFcc;font-size:20pt;font-weight:bold">
	Teoria de la etiqueta checkbox
</div>
<s:form action="AccionCheckbox">
	<s:checkbox label="Condicion 1" 
		name="condicion_1" value="true" />
	<s:checkbox label="Condicion 2" 
		name="condicion_2" value="false" />
	<hr/>
	<s:submit value="Enviar" />
</s:form>
</body>
</html>
