<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html> 

<%--
escapeAmp:Para que en la url del navegador aparezca 
"arg1=1&arg2=2" 
(en caso de no escribirlo aparece 
"arg1=1&amp;arg2=2 y no funciona)
--%>

<div style="background-color:#FFFFcc;font-size:20pt;font-weight:bold">
	<s:label value="Teoria de Url:" />
</div>
<br/>
<s:set name="miVariable" value="1"/>

<s:url id="Url" action="UrlAccionTag" escapeAmp="false">
 		<s:param name="arg1" value="#miVariable" />
        <s:param name="arg2" value="2" />		
</s:url>
<hr/>
<%--<a href="<s:property value="#Url"/>">Continuar</a>Funciona)--%> 
<%-- <a href="<s:property value="%{Url}"/>">Continuar</a>  (Funciona)--%> 
<a href="<s:property value="%{#Url}"/>">Continuar</a>  
</html>
