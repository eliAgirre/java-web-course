<%@ taglib uri="/struts-tags" prefix="s"%>

<html>  

<head>  

<title>Cursos</title>  

</head>  

<body>  
<!-- recuperamos los cursos elegidos desde la clase  CheckBoxList_2.java-->
<div style="background-color: #FFFFcc;font-size:15;font-weight:bold">
		Cursos elegidos (CheckBox Dinamico) :
</div>
<!--  
<s:iterator status="rowStatus" value='{0, 1}'>
      Index: <s:property value="%{#rowStatus.index}" /> <br />
      Count: <s:property value="%{#rowStatus.count}" /> <br />  
</s:iterator>
-->
<s:set name="numero" value='nombresCursos.size'/>
<hr/>

<s:if test="#numero==\"0\""> No existen cursos a mostrar</s:if>

<s:else>
	Numero de cursos elegidos=<s:property value='nombresCursos.size'/>
	<hr/>
	<s:iterator value="posCursos" status="rowStatus">  
		<br/>
		Indice curso=&nbsp;<s:property />&nbsp;&nbsp;&nbsp;&nbsp;	
		Nombre curso=<s:property value="nombresCursos[#rowStatus.index]"/> 
	</s:iterator> 
</s:else>
<hr/>
<a href="<s:url action='Volver'/>">Volver</a>	 
</body>  
 </html> 

