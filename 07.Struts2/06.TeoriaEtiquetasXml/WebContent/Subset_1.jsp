<%@ taglib prefix="s" uri="/struts-tags" %>

<html> 
<%--
Genera un simple "Iterator a partir de una clase Action (Si se declara el arreglo dentro de la jsp
solo se crea una unica propiedad)" 
--%>
  <head>
    <title> Subnet Tag  </title>
  </head>
  <body>
  <h1><span style="background-color: #FFFFcc">Teoria de la etiqueta Subnet (Primera forma)</span></h1>
    <h3><font color="#0000FF"> Genera un simple Iterator </font></h3>
      <s:subset source="listaNombres" >
        	<s:iterator>
          		<s:property /><br/>
        	</s:iterator>
      </s:subset>
	  <hr/>
	<a href="<s:url action='Volver'/>">Volver</a>	
  </body>
</html>  
