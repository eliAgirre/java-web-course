<%@ taglib uri="/struts-tags" prefix="s"%>

<html>
<%--
Crea un nuevo iterador a partir de varios iteradores pasados como par�metro en forma de etiquetas param.
org.apache.struts2.views.jsp.iterator.AppendIteratorTag

id: el nombre que tendr� el iterador resultante en el ValueStack.

--%>
<body>
<s:append id="personas">
	<s:param value="%{listaNombres}" />
	<s:param value="%{listaApellidos}" />
</s:append>

Usuarios:
<ul>
	<s:iterator value="personas">
		<li><s:property /></li>
	</s:iterator>
</ul>
<a href="<s:url action='Volver'/>">Volver</a>	
</body>
</html>
