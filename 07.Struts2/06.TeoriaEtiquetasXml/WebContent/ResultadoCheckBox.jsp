<%@ taglib uri="/struts-tags" prefix="s"%>

<html>
<%--
if-elseif-else
La t�pica sentencia condicional.
org.apache.struts2.views.jsp.IfTag
org.apache.struts2.views.jsp.ElseIfTag
org.apache.struts2.views.jsp.ElseTag

test (requerido para if y elseif): la expresi�n a comprobar

--%>
<body>
<s:if test="condicion_1">Condicion 1 a verdadero</s:if>
<s:else>Condicion 1 a falso</s:else>
<br/>
<s:if test="condicion_2">Condicion 2 a verdadero</s:if>
<s:else>Condicion 2 a falso</s:else>
<hr/>
<a href="<s:url action='Volver'/>">Volver</a>	
</body>
</html>
