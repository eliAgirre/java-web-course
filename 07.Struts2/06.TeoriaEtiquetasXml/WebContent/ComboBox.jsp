<%@ taglib uri="/struts-tags" prefix="s"%>

<html>
<%--
Crea una combinaci�n de select y caja de texto.
El valor de la caja de texto se 
auto rellena seg�n el elemento seleccionado en el select.
org.apache.struts2.views.jsp.ui.ComboBoxTag
--%>
<body>
<%--
headerKey="-1" Para que el literal de la propiedad
"headerValue" tome la posicion -1 de la lista y no
interfiera con el resto de las posiciones
 --%>

<s:form action="Resultado_ComboBox_Tag">
	<s:combobox list="cursos" name="cursos" 
		headerKey="-1" headerValue="-Selecciona-"
		label="Lenguaje preferido" 
		readonly="true" />
	<s:submit value="Enviar" />
</s:form>
</body>
</html>
