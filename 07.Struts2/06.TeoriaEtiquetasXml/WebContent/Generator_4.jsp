<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page language="java" import="java.util.*"%> <%-- Obligatorio--%>

<html> 
<%--
Genera un simple "Iterator" 
Count: Para delimitar el numero del iterator
id:Identificador del atributo
--%>
  <head>
    <title> Generator Tag  </title>
  </head>
  <body>
  <div style="background-color:#FFFFcc;font-size:20pt;font-weight:bold">
  		Teoria de la etiqueta Generator (Con identificador de atributos)
  </div>
    <h3><font color="#0000FF"> Genera un simple Iterator </font></h3>
      <s:generator val="%{'Juan,Ana Lopez,Pedro Gonzalez,Luis Fernandez,Maria Garcia'}" 
	  		separator="," count="2" id="myAtributo" />
			
   	  <%
        Iterator i = (Iterator) pageContext.getAttribute("myAtributo");
        while(i.hasNext()) 
		{
             String s = (String) i.next(); 
	   %>
            <%=s%> <br/>
      <%   
	  	}
      %>

	  <hr/>
	<a href="<s:url action='Volver'/>">Volver</a>	
  </body>
</html>  
