<%@ taglib uri="/struts-tags" prefix="s"%>

<html>
<body>
<%--
Crea un nuevo elemento HTML optgroup (un grupo de opciones para un elemento select)
org.apache.struts2.views.jsp.ui.OptGroupTag

list: El objeto del que se tomar�n los valores con los que rellenar el elemento

--%>
<s:form action="Resultado_OptGroupTag_Tag">
	<s:select list="{}" name="cursos">
		<s:optgroup label="Leguajes POO" list="lenguajesPoo" />
		<s:optgroup label="Leguajes estructurados" list="lenguajesEstructurados" />
	</s:select>
	<s:submit value="Enviar" />
</s:form>

</body>
</html>