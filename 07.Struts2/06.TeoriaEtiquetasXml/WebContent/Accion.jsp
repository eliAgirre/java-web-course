<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html> 
<!--
action
Permite ejecutar una accion desde una vista indicando 
el nombre de la accion y, opcionalmente, el espacio de nombres. 
Se puede pasar parametros utilizando la etiqueta param.
org.apache.struts2.views.jsp.ActionTag

executeResult: determina si el resultado de la accion 
(normalmente otra vista) se debe ejecutar / 
renderizar tambien.
ignoreContextParams: 
	indica si se deben incluir los parametros de la 
		peticion actual al invocar la accion.
name (requerido): el nombre de la accion a ejecutar.
namespace: el espacio de nombres de la accion a ejecutar.
--> 
	<body>  
		<div style="background-color:#FFFFcc;font-size:20pt;font-weight:bold">
			Teoria de la etiqueta Action
		</div>
		<br/>
		<div style="font-size:15pt;font-weight:bold">Antes de s:action</div>
		<br/>  
		
		<%--Los <s:param son opcionales--%>
		<s:set name="arg1" value="11"/>
		<s:action name="Accion" executeResult="true"> 
			<s:param name="arg1" value="%{#arg1}" />
			<s:param name="arg2" value="22" />
        </s:action>
        
        <br/>
        
		<div style="font-size:15pt;font-weight:bold">
			Despu&eacute;s de s:action 
		</div>		
		<hr/> 
		<a href="<s:url action='Volver'/>">Volver</a>	
	</body>  
</html>
