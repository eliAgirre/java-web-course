package modelo;

public class Intel {
	
	private String dni;
	private String pantalla;
	
	public Intel(){} // vacio
	
	public Intel(String dni, String pantalla) {
		
		this.dni = dni;
		this.pantalla = pantalla;
		
	} // constructor

	// getters
	public String getDni() {
		return dni;
	}
	public String getPantalla() {
		return pantalla;
	}
	
	// setters
	public void setDni(String dni) {
		this.dni = dni;
	}
	public void setPantalla(String pantalla) {
		this.pantalla = pantalla;
	}
	
}
