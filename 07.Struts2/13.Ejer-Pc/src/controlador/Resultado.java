package controlador;

import java.util.Map;

import modelo.Amd;
import modelo.Intel;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class Resultado extends ActionSupport{
	
	private static final long serialVersionUID = 1L;
	
	private String dni;
	private String pantalla;
	private String nombre;
	private String[] extras;
	
	private String micro;
	
	private Intel intel;
	private Amd amd;
	
	@SuppressWarnings("unchecked")
	public String execute() {
		
		Map<String,String> session=(Map<String,String>)ActionContext.getContext().getSession();
		
	    // obtiene el tipo de micro desde la sesion
		micro=session.get("micro");  
		
		if(micro.equals("0")){ // Intel
			
			cargarIntel(); // carga dni+pantalla
			
			Map<String,Intel> sessionIntel=(Map<String,Intel>)ActionContext.getContext().getSession();
			 
			sessionIntel.put("intel", intel);
			
		}
		else{ // Amd
			
			cargarAmd(); // carga nombre+extras
			
			Map<String,Amd> sessionAmd=(Map<String,Amd>)ActionContext.getContext().getSession();
			
			sessionAmd.put("amd", amd);
		}
		
		return "SUCCESS";
	}
	
	// getters
	public String getDni() {
		return dni;
	}
	public String getPantalla() {
		return pantalla;
	}
	public String getNombre() {
		return nombre;
	}
	public String[] getExtras() {
		return extras;
	}

	// setters
	public void setPantalla(String pantalla) {
		this.pantalla = pantalla;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setExtras(String[] extras) {
		this.extras = extras;
	}
	
	public String getNombreMicro() {
		
		if(micro.equals("0"))return "Intel";
		else return "Amd";
	}
	
	private void cargarIntel(){
		
		intel=new Intel(dni,pantalla);
	}
	
	private void cargarAmd(){
		
		amd=new Amd(nombre,extras);
	}
	
}