package holaMundo;

public class HolaMundo {
	
	private static final String SALUDO = "Hola ";
    private String customSaludo;
    private String nombre="";
   
	
    // no es obligatorio poner extends de HttpServlet porque pone execute
    public String execute() { // es un interceptor
    	
    	// execute es parecido a doGet y doPost para mostrar datos o llamar a otras clases 
    	   	
    	setCustomSaludo(SALUDO+getNombre()+getApellido());   
    	
    	return "SUCCESS"; // no es obligatorio la palabra SUCCESS
    }   
   
    public String getNombre() {
    	
    	System.out.println("get "+nombre);
        return nombre;
    }
    public String getCustomSaludo(){
    	
    	return  customSaludo;
    }

	public void setNombre(String nombre){
    	
    	System.out.println("set "+nombre);
        this.nombre = nombre;
    }   
    public void setCustomSaludo( String customSaludo ){
    	
    	this.customSaludo = customSaludo;
    }
    
}