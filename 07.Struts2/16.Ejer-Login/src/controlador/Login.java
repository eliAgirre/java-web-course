package controlador;

import com.opensymphony.xwork2.ActionSupport;

public class Login extends ActionSupport{
	
	private static final long serialVersionUID = 1L;
	
	private String username;
	private String password;
	
	// business logic
	public String execute() {

		if(username.equalsIgnoreCase("angel") && password.equalsIgnoreCase("miguel")){
			return "SUCCESS_BIEN";		
		}
		else{
			return "SUCCESS_MAL";	
		}
	}
	
	// getters
	public String getUsername() {
		return username;
	}
	public String getPassword() {
		return password;
	}
	
	// setters
	public void setUsername(String username) {
		this.username = username;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	// simple validation
	public void validate(){
		
		if(getUsername().length()==0){
			addFieldError("error_user", getText("login.user.requerido"));
			//addFieldError("username", getText("username.required"));
		}
		if(getUsername().length()>10){
			addFieldError("error_user_maximo", getText("<font color='green'>El usuario debe ser menor de 10 caracteres</font>"));
		}
		if(getPassword().length()==0){
			addFieldError("errorpassword", getText("login.password.requerido"));
			//addFieldError("password", getText("password.required"));
		}
		
	}
	
}