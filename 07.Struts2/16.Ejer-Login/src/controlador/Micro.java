package controlador;

import java.util.List;
import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class Micro extends ActionSupport{
	
	private static final long serialVersionUID = 1L;
	
	private List<String> micro;
	
	@SuppressWarnings("unchecked")
	public String execute(){
		
		Map<String,String> session=(Map<String,String>)ActionContext.getContext().getSession();
		
		String sMicro=getMicro().get(0); // se obtiene si es 0 o 1
		
		session.put("micro", sMicro); // se guarda en la sesion
		
		if(sMicro.equals("0")) return "SUCCESS_1"; // Intel
		
		else return "SUCCESS_2"; // amd
	}

	public List<String> getMicro() {
		return micro;
	}

	public void setMicro(List<String> micro) {
		this.micro = micro;
	}
}
