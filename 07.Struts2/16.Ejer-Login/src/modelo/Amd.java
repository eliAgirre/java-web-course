package modelo;

public class Amd {
	
	private String nombre;
	private String[] extras;
	
	public Amd(){} // vacio
	
	public Amd(String nombre, String[] extras) {
		
		this.nombre = nombre;
		this.extras = extras;
		
	} // constructor

	// getters
	public String getNombre() {
		return nombre;
	}
	public String[] getExtras() {
		return extras;
	}
	
	// setters
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setExtras(String[] extras) {
		this.extras = extras;
	}
	
}
