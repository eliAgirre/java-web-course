<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Resultado</title>
</head>
<body>

	<!--metodo getNombreMicro del interceptor Resultado-->
	Micro: <s:property value="nombreMicro"/><br/>
	
	<s:if test="%{#session.micro==\"0\"}"> <!--Intel-->
	
		Dni: <s:property value="#session.intel.dni"/><br/>

		Pantalla:
		<s:if test="%{#session.intel.pantalla==\"0\"}">19</s:if>
		<s:elseif test="%{#session.intel.pantalla==\"1\"}">21</s:elseif>
		<s:elseif test="%{#session.intel.pantalla==\"2\"}">23</s:elseif>
		
	</s:if>
	<s:else><!--Amd-->
	
		Nombre: <s:property value="#session.amd.nombre"/><br/>
		
		Extras:
		
		<s:set name="extra" value="#session.amd.extras" /><!--variable extra-->
		<s:property value="#extra"/>
	
		<!--
		<s:if test="{#extra==\"0\}">Blue ray</s:if>
		<s:if test="{#extra==\"1\"}">Lector tarjeta</s:if>
		<s:if test="{#extra==\"2\"}">Altavoz</s:if>-->

			
	</s:else>
	
	<br/><br/>
	
	<a href="<s:url action='accionVolver'/>"><s:text name="resultado.final.volver"/></a>
	<a href="<s:url action='Salir'/>">Salir</a>
	
</body>
</html>