<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Pc - Intel</title>
</head>
<body>

	<s:form action="accionResultado">
	
		<s:textfield label="Introduce el DNI" name="dni" />
		
		<s:combobox label="Seleccione una opcion" name="pantalla" list="#{'0':'19', '1':'21', '2':'23'}" />
		
		<s:submit value="Enviar" />
	
	</s:form>

</body>
</html>