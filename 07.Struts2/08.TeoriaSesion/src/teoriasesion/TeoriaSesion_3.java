package teoriasesion;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

public class TeoriaSesion_3  extends ActionSupport
{
		
	private static final long serialVersionUID = 1L;
	
	private String dato5="";
	private String dato6="";
	
	public String execute()
	{      
	    //Grabar en request
	    HttpServletRequest request = 
	    		ServletActionContext.getRequest();
		
		//request.setAttribute("personaRequest", persona);
		return "SUCCESS";
	}

	
	public void setDato5(String dato5) 
	{
		this.dato5 = dato5; 
	}

	public void setDato6(String dato6) 
	{
		this.dato6 = dato6; 
	}

	public String getDato5() 
	{
		return (this.dato5); 
	}

	public String getDato6() 
	{
		return (this.dato6); 
	}
}

