<%@ taglib uri="/struts-tags" prefix="s"%>

<html>
<%--
textfield
Crea una caja de texto.
org.apache.struts2.views.jsp.ui.TextFieldTag

maxlength: Atributo maxlength de HTML (n�mero m�ximo de caracteres permitidos)
readonly: Determina si el campo ser� de solo lectura.
size: Atributo size de HTML (tama�o de la caja de texto en caracteres)

property
Muestra una propiedad de ValueStack u otro objeto de ActionContext.
org.apache.struts2.views.jsp.PropertyTag

default: Valor a mostrar en caso de que el valor pedido sea nulo.
value: Valor a mostrar

password
Crea un elemento password de HTML.
org.apache.struts2.views.jsp.ui.PasswordTag

reset
Crea un bot�n que borra los datos introducidos en el formulario.
org.apache.struts2.views.jsp.ui.ResetTag

value: Texto a utilizar para el bot�n

set
Asigna un valor a una variable, opcionalmente indicando el �mbito al que a�adirla. 
El valor se puede indicar utilizando el atributo value, o encerrando el valor en la propia etiqueta.
org.apache.struts2.views.jsp.SetTag

name: Nombre a utilizar para la variable.
scope: �mbito en el que a�adir la variable. 
	Puede ser application, session, request, page, o action. Por defecto se utiliza action.
value: Valor a asignar.

--%>
<head> 

<script type="text/javascript">
function ColocarCursor()
{
	document.forms[0].id_dato1.focus();	
}

</script>
</head>

<body onLoad="ColocarCursor();">
<div style="background-color:#FFFFcc;font-size:20px">
	<s:label value="Pagina 1" /> :
</div>
<s:form action ="Teoria_Sesion_1_Tag">
	
	<s:textfield label='Introduce Dato 1' id="id_dato1" 
		name='dato1'/> 
	
	<s:textfield label="Introduce Dato 2" maxlength="10" 
		size="10" name="dato2" tabindex="0" id="dato2" />
	<br/>
	
	<s:reset value="Borrar" tabindex="1" id="idBorrar"/>
	<s:submit value="Enviar" tabindex="2" id="idEnviar"/> 	

</s:form>

</body>
</html>
