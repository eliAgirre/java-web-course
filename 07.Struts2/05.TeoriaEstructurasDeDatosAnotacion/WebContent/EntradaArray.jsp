<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>

	<head>
    <title>EntradaArray</title>
	</head>

	<body>
	    <hr>
		
		<s:form action="array" method="post">
		
		<!--Grabamos la edad[0] en el array edad" -->
		<s:textfield name="edad" label="Array Edad"/>
		
		<!--Grabamos la edad[1] en el array edad" -->
		<s:textfield name="edad" label="Array Edad"/>
		
		<!--Grabamos la edad[2] en el array edad" -->
		<s:textfield name="edad" label="Array Edad"/>
		
		<%--************************************************* --%>
		
		<!-- Sinonimo (Otra forma de cargar un Array)
		<!--Grabamos el nombre[0] en el array nombres" -->
		<s:textfield name="nombres[0]" label="Array Nombre"/>
		
		<!--Grabamos el nombre[1] en el array nombres" -->
		<s:textfield name="nombres[1]" label="Array Nombre"/>
		
		<!--Grabamos el nombre[2] en el array nombres" -->
		<s:textfield name="nombres[2]" label="Array Nombre"/>
		
		<%--************************************************* --%>
		
		<!--Grabamos el Apellido1 en la posicion 0 en el vector apellido1--> 
		<s:textfield name="apellido1[0]" label="Vector Apellido1"/>
		
		<!--Grabamos el Apellido1 en la posicion 1 en el vector apellido1--> 
		<s:textfield name="apellido1[1]" label="Vector Apellido1"/>
		
		<!--Grabamos el Apellido1 en la posicion 2 en el vector apellido1"-->
		<s:textfield name="apellido1[2]" label="Vector Apellido1"/>
		
		<%--************************************************* --%>
		
		<!--Grabamos el Apellido2 en la posicion 0 en el List apellido2--> 
		<s:textfield name="apellido2[0]" label="List Apellido2"/>
		
		<!--Grabamos el Apellido2 en la posicion 1 en el List apellido2--> 
		<s:textfield name="apellido2[1]" label="List Apellido2"/>
		
		<!--Grabamos el Apellido2 en la posicion 2 en el List apellido2"-->
		<s:textfield name="apellido2[2]" label="List Apellido2"/>
		
		<%--************************************************* --%>
		
		
		<!--Grabamos la direccion en la posicion 0 en el ArrayList direccion--> 
		<s:textfield name="direccion[0]" label="ArrayList Direccion"/>
		
		<!--Grabamos la direccion en la posicion 1 en el ArrayList direccion--> 
		<s:textfield name="direccion[1]" label="ArrayList Direccion"/>
		
		<!--Grabamos la direccion en la posicion 2 en el ArrayList direccion"-->
		<s:textfield name="direccion[2]" label="ArrayList Direccion"/>
		
		<%--************************************************* --%>		
		<!--Grabamos calle en el Map calles"-->
		<s:textfield name="calle.a" label="Map calle 1"/>
		<s:textfield name="calle.b" label="Map calle 2"/>
		<s:textfield name="calle.c" label="Map calle 3"/>
		
		<%--************************************************* --%>		
		<!-- Sinonimo (Otra forma de cargar un Map)
		<!--Grabamos provincia en el Map Pronvicia"-->
		<s:textfield name="provincia[0]" label="Map provincia 1"/>
		<s:textfield name="provincia[1]" label="Map provincia 2"/>
		<s:textfield name="provincia[2]" label="Map provincia 3"/>
		
		
    	<s:submit value="Aceptar"/>
		</s:form>
		
	    <hr>	
	</body>
	
</html>