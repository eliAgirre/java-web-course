<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
	<head>
    <title>ResultadoArray</title>
	</head>
	<body>
	    <hr>		
		<font color="red" size="3"> Array edades</font>
		<br>
		
		Edad 1=<s:property value="edad[0]"/><br>
		
		Edad 2=<s:property value="edad[1]"/><br>
		
		Edad 3=<s:property value="edad[2]"/><br>
		
		<hr>
		<font color="blue" size="3">Array nombres</font>
		<br>
		
		Nombre 1=<s:property value="nombres[0]"/><br>
		
		Nombre 2=<s:property value="nombres[1]"/><br>
		
		Nombre 3=<s:property value="nombres[2]"/><br>
		
		<hr>
		<font color="red" size="3">ArrayList apellido1</font>
		<br>
		
		Apellido 1=<s:property value="apellido1[0]"/><br>
		
		Apellido 2=<s:property value="apellido1[1]"/><br>
		
		Apellido 3=<s:property value="apellido1[2]"/><br>
	    <hr>
	    
	    <font color="blue" size="3">List apellido2</font>
		<br>
		
		Apellido 1=<s:property value="apellido2[0]"/><br>
		
		Apellido 2=<s:property value="apellido2[1]"/><br>
		
		Apellido 3=<s:property value="apellido2[2]"/><br>
	    <hr>
	    
	    <font color="red" size="3">ArrayList Direccion</font>
		<br>
		
		Direccion 1=<s:property value="direccion[0]"/><br>
		
		Direccion 2=<s:property value="direccion[1]"/><br>
		
		Direccion 3=<s:property value="direccion[2]"/><br>
	    <hr>
	   
	   
	    <font color="blue" size="3">Map calles</font>
		<br>		
		Calle 1=<s:property value="calle.a"/><br>
		Calle 2=<s:property value="calle.b"/><br>
		Calle 3=<s:property value="calle.c"/><br>  
		
		<hr>
		<font color="red" size="3">Map provincia</font>
		<br>
		Provincia 1=<s:property value="provincia[0]"/><br>
		Provincia 2=<s:property value="provincia[1]"/><br>
		Provincia 3=<s:property value="provincia[2]"/><br>  

	    <hr>
	    <a href="<s:url action='anotacionContinuar'/>">Volver</a>		
	</body>
	
</html>