package Estructuras;

import org.apache.struts2.config.Result;
import com.opensymphony.xwork2.ActionSupport;
import java.util.*;

@Result(name="irResultado", value="ResultadoArray.jsp" )
public class ArrayAction extends ActionSupport 
{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -1043156353959860495L;


	public String execute()
	{
			return "irResultado";	
	}
	
	// JavaBeans
	
	//Transformacion interna de Double a String
	private Double[] edad;	
	private String[] nombres=new String[10]; 
	private Vector<String> apellido1=new Vector<String>(); 
	private List<String> apellido2; //Clase Abstracta
	private ArrayList<String> direccion=new ArrayList<String>();
	private Map<String,String> calle; //Clase Abstracta
	private Map<String,String> provincia; //Clase Abstracta	


	public void setEdad(Double[] edad) 
	{
		this.edad = edad; 
	}

	public Double[] getEdad() 
	{
		return (this.edad); 
	}

	//Clase Array
	public void setNombres(String[] nombres) 
	{
		this.nombres = nombres; 
	}

	public String[] getNombres() 
	{
		return (this.nombres); 
	}
	
	//Clase Vector	
	public void setApellido1(Vector<String> apellido1) 
	{
		this.apellido1 = apellido1; 
	}

	public Vector<String> getApellido1() 
	{
		return (this.apellido1); 
	}	
	
	//Clase List
	public void setApellido2(List<String> apellido2) 
	{
		this.apellido2 = apellido2; 
	}

	public List<String> getApellido2() 
	{
		return (this.apellido2); 
	}
	
	//Clase ArrayList
	public ArrayList<String> getDireccion() 
	{
		return direccion;
	}

	public void setDireccion(ArrayList<String> direccion) 
	{
		this.direccion = direccion;
	}
	
	//Clase Map
	public void setCalle(Map<String, String> calle) 
	{
		this.calle = calle; 
	}

	public Map<String, String> getCalle() 
	{	
		return (this.calle); 
	}
	
	//Clase Map
	public Map<String, String> getProvincia() 
	{
		return provincia;
	}

	public void setProvincia(Map<String, String> provincia) 
	{
		this.provincia = provincia;
	}
}

