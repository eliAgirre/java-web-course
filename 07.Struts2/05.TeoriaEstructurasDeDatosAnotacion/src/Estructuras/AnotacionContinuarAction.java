package Estructuras;

import org.apache.struts2.config.Result;
import com.opensymphony.xwork2.ActionSupport;


//Obligatorio extender de ActionSupport ya que no lleva
//metodo execute()
@Result( value="/index.html" )

public class AnotacionContinuarAction extends ActionSupport 

{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4255498927504833145L;
	//Solo manda a la pagina index.jsp para continuar con 
	// nuestro programa	

}
