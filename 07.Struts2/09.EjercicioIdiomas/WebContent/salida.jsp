<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

	<s:if test="idioma[0]=='Castellano'">
		<s:set name="lang" value="'idiomas.castellano'"></s:set>
	</s:if>
	<s:else>
		<s:set name="lang" value="'idiomas.ingles'"></s:set>
	</s:else>

	<s:i18n name="%{#lang}"> 
    	<title><s:text name="titulo"/></title>
    </s:i18n>
</head>
<body>
	<s:i18n name="%{#lang}"> 
		<s:text name="cabecera" />
		<br/>
		<s:text name="texto" />
		<br/>
		<a href='<s:url action="Volver"/>'><s:text name="volver" /></a>
	</s:i18n>  
</body>
</html>