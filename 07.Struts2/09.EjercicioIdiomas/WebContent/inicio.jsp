<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ejercicio Idiomas</title>

<script type="text/javascript">
	function seleccionar() 
	{
		//Seleccionamos el segundo radio
		document.forms[0].idioma[0].checked = "true";
	}
</script>
</head>
<body onload="seleccionar();">

<s:form action="AccionIdioma" theme="css_xhtml"> 	
<s:i18n name="idiomas.castellano"> 

	<table>
	<tr>
		<td> 
			<s:radio list="'Castellano'" name="idioma" />	
		</td>
		</tr>
	<tr>
		<td>
			<s:radio list="'Ingles'"  name="idioma"/>
		</td>
	</tr>
	<tr>
		<td><s:submit key="textBoton" /></td>
	</tr>
	</table>
</s:i18n>	
</s:form>

</body>
</html>