package modelo;

public class Sevilla {
	
	private String nif;
	private String apellido;
	
	public Sevilla(String nif, String apellido) {
		
		this.nif = nif;
		this.apellido = apellido;
		
	} // constructor
	
	// getters
	public String getNif() {
		return nif;
	}
	public String getApellido() {
		return apellido;
	}
	
	// setters
	public void setNif(String nif) {
		this.nif = nif;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
}
