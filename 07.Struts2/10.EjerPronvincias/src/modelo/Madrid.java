package modelo;

public class Madrid {
	
	private String nombre;
	private String dni;
		
	public Madrid(String dni, String nombre) {
		
		this.dni = dni;
		this.nombre = nombre;
		
	} // constructor
	
	// getters
	public String getNombre() {
		return nombre;
	}
	public String getDni() {
		return dni;
	}
	
	// setters
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public void setDni(String dni) {
		this.dni = dni;
	}
	
}