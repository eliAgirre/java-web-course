package controlador;
import java.util.List;
import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class Provincia extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private List<String> provincia;
	
	public String execute() {
		
	    @SuppressWarnings("unchecked")
	    Map<String,String> session = (Map<String,String>)ActionContext.getContext().getSession();
	    
	    String varProvincia= getProvincia().get(0);
	    
		session.put("provincia",varProvincia);
		
	    if(varProvincia.equals("0"))return "SUCCESS_1";
		else return "SUCCESS_2";
	}

	public List<String> getProvincia() {

		return provincia;
	}

	public void setProvincia(List<String> provincia) {
		this.provincia = provincia;
	}
	
}
