package controlador;

import java.util.Map;

import modelo.Madrid;
import modelo.Sevilla;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class Resultado extends ActionSupport{
	
	private static final long serialVersionUID = 1L;
	private String dni;
	private String nombre;
	private String nif;
	private String apellido;
	private String provincia;
	private Sevilla sevilla;
	private Madrid madrid;
	
	@SuppressWarnings("unchecked")
	public String execute(){
		
	    Map<String,String> session=(Map<String,String>)ActionContext.getContext().getSession();  
	    
	    // obtiene la provincia
	    provincia=session.get("provincia");  
	       
	    if(provincia.equals("0")){ // Madrid
	    	
	    	cargaMadrid(); // carga dni y nombre
	    	
		    Map<String,Madrid> sessionMadrid = (Map<String,Madrid>)ActionContext.getContext().getSession();
	    	
		    sessionMadrid.put("madrid", madrid); // guarda en la sesion
	    }
	    else{ // Sevilla
	    	
	    	cargaSevilla(); // carga nif y apellido
	    	
		    Map<String,Sevilla> sessionSevilla = (Map<String,Sevilla>)ActionContext.getContext().getSession();
	    	
		    sessionSevilla.put("sevilla", sevilla); // guarda en la sesion
	    }
	    
		return "SUCCESS";
	
	} // execute
	
	// getters
	public String getDni() {
		return dni;
	}
	public String getNombre() {
		return nombre;
	}
	public String getNif() {
		return nif;
	}
	public String getApellido() {
		return apellido;
	}
	
	public String getNombreProvincia() {
		
		if(provincia.equals("0"))return "Madrid";
		else return "Sevilla";
	}
	
	// setters
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public void setNif(String nif) {
		this.nif = nif;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	private void cargaMadrid(){
		madrid=new Madrid(dni,nombre);
	}
	
	private void cargaSevilla(){
		
		sevilla=new Sevilla(nif,apellido);
	}
	
}