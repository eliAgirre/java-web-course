<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Resultados</title>
</head>
<body>
<s:form>
	
	Provincia: <s:property value="nombreProvincia"/>
	
	<s:if test="%{#session.provincia==\"0\"}"> <!--Madrid-->
	
		<%--Funciona:
		Dni=<s:property value="#session.madrid.dni"/>
		Nombre=<s:property value="#session.madrid.nombre"/>--%>
		
		<s:include value="resultadoMadrid.jsp"/>
	</s:if>
	
	<s:else><!--Sevilla-->
	
		<%--Funciona:
		Nif=<s:property value="#session.sevilla.nif"/>
		Apellido=<s:property value="#session.sevilla.apellido"/>--%>
	
		<s:include value="resultadoSevilla.jsp"/>
		
	</s:else><br/><br/>
	
	
	<a href="<s:url action='Volver'/>">Volver</a>
	
</s:form>
</body>
</html>