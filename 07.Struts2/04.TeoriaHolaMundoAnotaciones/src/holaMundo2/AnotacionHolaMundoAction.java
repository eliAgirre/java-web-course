package holaMundo2;

import org.apache.struts2.config.Result;


//import com.opensymphony.xwork2.ActionSupport;

@Result(name="SUCCESS", value="/Paginas/HolaMundo.jsp" )
/*
	Como contiene el metodo execute no es obligatorio
	extender de ActionSupport
 */


//public class AnotacionHolaMundoAction extends ActionSupport //(funciona) 
public class AnotacionHolaMundoAction {
	

	private static final String SALUDO = "Hola ";
    private String customSaludo;
    private String nombre="";
   
	
    public String execute()  
    {
    	   	
    	setCustomSaludo( SALUDO + getNombre());   
    	
    	return "SUCCESS";
    }   
   

 
    public String getNombre() 
    {
        return nombre;
    }

    public void setNombre(String nombre) 
    {
        this.nombre = nombre;
    }   

    
    public String getCustomSaludo()
    {
    	return  customSaludo;
    }
    
    public void setCustomSaludo( String customSaludo )
    {
    	this.customSaludo = customSaludo;
    }
}
