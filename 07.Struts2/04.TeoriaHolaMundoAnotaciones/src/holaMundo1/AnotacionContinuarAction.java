package holaMundo1;

import org.apache.struts2.config.Result;
import com.opensymphony.xwork2.ActionSupport;


//Obligatorio extender de ActionSupport ya que no lleva
//metodo execute()
@Result( value="/Paginas/Nombre.jsp" )

public class AnotacionContinuarAction extends ActionSupport {

	private static final long serialVersionUID = 5193981484194978476L;
	//Solo manda a la pagina Nombre.Jsp para continuar con 
	//nuestro programa	

}
