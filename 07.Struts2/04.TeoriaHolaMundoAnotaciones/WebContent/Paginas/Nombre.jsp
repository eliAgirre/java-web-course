<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>

	<head>
    <title>Nombre</title>
	</head>

		<body onload="document.forms[0].idNombre.focus();">
	    <hr/>
		<h4>Introduce nombre</h4> 	

		<!--Llamanos al Servlet AnotacionHolaMundoAction
		El nombre del sevlet debe sustituir en la primera letra por una mayuscula e implementar
		al final la palabra Action esto es el nombre del servlet es "AnotacionHolaMundoAction" 
		-->
		<s:form action="anotacionHolaMundo">
		
			<!--Grabamos el nombre en la propiedad setNombre() del servlet-->
			<s:textfield id="idNombre" name="nombre" label="Tu nombre"/>
	    	<s:submit/>
		</s:form>
	    <hr/>	
	</body>
	
</html>
