<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>

	<head>
	 
	<title><s:text name="registrar.titulo"/></title>
	  <style>
       .clase1 {color:navy;font-size:10pt;font-style:italic}
       .clase2 {color:#2F629F;font-size:10pt;font-style:italic}
      </style>
   <s:head/> <%--Para que funcionen los estilos de cabecera--%>
	</head>

	<body onload="document.forms[0].idNombre.focus();">
	
		<%--Salida de errores (Salen todos juntos cada uno en una linea diferente)
		<s:fielderror />
		 
		<s:fielderror>(Para interceptar uno)
			<s:param>errorpassword</s:param> 	
		</s:fielderror>
		
		<s:fielderror> (Otra forma de escritura)
			<s:param value="%{'error_nombre_maximo'}" />
			<s:param value="%{'error_nombre'}" />	
		</s:fielderror>
				
		--%>
		<h4><s:text name="registrar.texto"/></h4>  
		
		<%--Para utilizar mis estilos, mis tablas y mis capas--%>
		<s:form action="registrar_accion" method="post" theme="css_xhtml"> 
		
		<table>
			<tr>
				<td>
					<s:text name="registrar.nombre"/>
				</td>
				<td>
					<s:textfield id="idNombre" name="nombre"/>
				</td>
				
				<td>
					<%--Juntos (solo puede aparece uno ya, que no se puede dar los
					dos errores a la vez, salen los dos en la misma posicion fisica)
					--%>
					
					<%-- 
						error_nombre_maximo (Fomato en la clase Registrar.java
							"<font color='green'>)
						error_nombre (Formato en el fichero properties
							<font size="3" color="red">)						
					 --%>
					<s:fielderror>
						<s:param value="%{'error_nombre_maximo'}" />
						<s:param value="%{'error_nombre'}" />
					</s:fielderror>
				</td>
			</tr>
			<tr>
				<td>
					<s:text name="registrar.password"/>
				</td>
				<td>
					<s:password name="password"/>
				</td>
				<td> 
					<s:fielderror cssStyle="color:blue">
						<s:param> errorpassword</s:param>
					</s:fielderror>
				</td>
				
			</tr>
			<tr>
				<td align="center">
					<s:submit key="registrar.botonsubmit" cssClass="clase1"/>
				</td>
				<td width="50">
					<s:reset key="registrar.botonreset" cssStyle="text-align:center"/>
				</td>
				<td></td>														
			</tr>				
		</table>    	
		</s:form>		 
	</body>	
</html>
