<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
	<head>
    <title><s:text name="ResultadoMal.titulo"/></title>
	</head>

	<body>

	<font size="4" color="red"><s:text name="ResultadoMal.error"/></font>
      <hr>
	  <a href="<s:url action='inicio_fin_accion'/>"><s:text name="ResultadoMal.volver"/></a>
	</body>
	
</html>
