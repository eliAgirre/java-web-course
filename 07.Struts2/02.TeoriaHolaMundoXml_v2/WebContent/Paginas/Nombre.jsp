<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>

	<head>
    <title>Nombre</title>
	</head>

	<body onload="document.forms[0].idNombre.focus();">
	    <hr/>
		<h4>Introduce nombre</h4> 	

		<!--Llamanos a la accion HolaMundo del struts.xml, por defecto es post-->
		<s:form action="HolaMundo">
			
			<!--Grabamos el nombre en la propiedad setNombre() del servlet-->
			<s:textfield name="nombre" id="idNombre" label="Tu nombre"/>
			<s:textfield name="apelllido" id="idApellido" label="Apellido"/>
	    	<s:submit/>
	    	
		</s:form>
		
	    <hr/>	
	</body>
	
</html>
