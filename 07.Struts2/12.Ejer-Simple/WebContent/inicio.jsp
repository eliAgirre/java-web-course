<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Inicio</title>
</head>
<body>

	<!--formulario struts-->
	<s:form action="accionCliente">
		
		<s:textfield label="Dni" name="dni"></s:textfield>
		<s:textfield label="Nombre" name="nombre"></s:textfield>
		<s:textfield label="Apellido" name="apellido"></s:textfield>
		
		<s:submit value="Enviar" />
	
	</s:form>
	
</body>
</html>