package controlador;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class Cliente extends ActionSupport{
	
	private static final long serialVersionUID = 1L;
	
	private String dni;
	private String nombre;
	private String apellido;

	
	@SuppressWarnings("unchecked")
	public String execute(){
		
		Map<String,String> session=(Map<String,String>)ActionContext.getContext().getSession();
		
		session.put("dni",dni);
		session.put("nombre",nombre);
		session.put("apellido",apellido);
		
		return "SUCCESS";
	}

	// getters
	public String getDni() {
		return dni;
	}
	public String getNombre() {
		return nombre;
	}
	public String getApellido() {
		return apellido;
	}
	
	// setters
	public void setDni(String dni) {
		this.dni = dni;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
}
