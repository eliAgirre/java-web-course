package teoria_etiquetas2;

import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")   

public class Accion extends ActionSupport 
{   
    private String saludo; 
    private String arg1;
    private String arg2;
    
	public String getSaludo() 
    {   
        return saludo;   
    }
    
    public String getArg1() 
    {
		return arg1;
	}

	public void setArg1(String arg1) 
	{
		this.arg1 = arg1;
	}
  
    public String getArg2() 
    {
		return arg2;
	}

	public void setArg2(String arg2) 
	{
		this.arg2 = arg2;
	}


    public String execute() 
    {   
        saludo = "Hola, esto es teoria de "
        		 + "la etiqueta accion";   
        return "SUCCESS";   
    }
  
}  