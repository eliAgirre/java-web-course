package teoria_etiquetas;

import com.opensymphony.xwork2.ActionSupport;

import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("serial")

public class ElegirCursos extends ActionSupport 
{
	private List<String> cursos;

	public List<String> getCursos() 
	{
	
		cargaArray();
		return cursos;
	}
	
	 
	public void setCursos(List<String> cursos) 
	{  

		this.cursos = cursos;  
	} 

	public String execute() 
	{
		return "SUCCESS";
	}
	
	private void cargaArray()
	{
		cursos = new ArrayList<String>();
		
		cursos.add("Java");
		cursos.add("C++");
		cursos.add("Visual Basic");
	
	}
}

