package teoria_etiquetas;

import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import java.util.ArrayList;


@SuppressWarnings("serial")
public class DoubleSelect extends ActionSupport 
{
	private List<Cursos> cursos;

	public List<Cursos> getCursos() 
	{
		return cursos;
	}

	public String execute() 	

	{

		cursos = new ArrayList<Cursos>();

		List<String> misLenguajesPoo = new ArrayList<String>();
		misLenguajesPoo.add("Java");
		misLenguajesPoo.add("C++");
		misLenguajesPoo.add("Visual Basic");
		Cursos poo = new Cursos("Poo", misLenguajesPoo);
		cursos.add(poo);

		List<String> misLenguajesEstructurados = new ArrayList<String>();
		misLenguajesEstructurados.add("C");
		misLenguajesEstructurados.add("Cobol");
		misLenguajesEstructurados.add("Natural");
		Cursos estructurados = new Cursos("Estructurados", misLenguajesEstructurados);
		cursos.add(estructurados);

		return "SUCCESS";
	}
}

