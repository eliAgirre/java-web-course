package teoria_etiquetas;
import java.util.List;

public class Cursos 
{
	private String tipoCurso;
	private List<String> nombreCurso;

	public Cursos(String tipoCurso, List<String> nombreCurso) 
	{
		this.tipoCurso = tipoCurso;
		this.nombreCurso = nombreCurso;
	}

	public String getTipoCurso() 
	{
		return tipoCurso;
	}

	public List<String> getNombreCurso() 
	{
		return nombreCurso;
	}
}