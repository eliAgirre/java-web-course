package teoria_etiquetas;

import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")   

public class ControlesBasicos extends ActionSupport 
{   
    private String nombre; 
    private String apellido;
    private String dni;
    private String campoHidden;
      
    //Nombre
    public void setNombre(String nombre) 
    {   
        this.nombre=nombre;
    } 
    public String getNombre() 
    {   
        return nombre;   
    }  
    //Apellido
    public void setApellido(String apellido) 
    {   
        this.apellido=apellido;
    } 
    public String getApellido() 
    {   
        return apellido;   
    }  
    //Dni
    public void setDni(String dni) 
	{
		this.dni = dni; 
	}

	public String getDni() 
	{
		return (this.dni); 
	}
    //Campo Hidden
    public void setCampoHidden(String campoHidden) 
	{
		this.campoHidden = campoHidden; 
	}

	public String getCampoHidden() 
	{
		return (this.campoHidden); 
	}
  
    public String execute() 
    {   
        return "SUCCESS";   
    }   
} 