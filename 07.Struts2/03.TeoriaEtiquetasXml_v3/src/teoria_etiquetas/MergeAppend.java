package teoria_etiquetas;

import com.opensymphony.xwork2.ActionSupport;

import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class MergeAppend extends ActionSupport 
{
	private List<String> listaNombres;
	private List<String> listaApellidos;

	public List<String> getListaNombres() 
	{
		return listaNombres;
	}

	public void setListaNombres(List<String> listaNombres) 
	{
		this.listaNombres = listaNombres;
	}

	public List<String> getListaApellidos() 
	{
		return listaApellidos;
	}

	public void setListaApellidos(List<String> listaApellidos) 
	{
		this.listaApellidos = listaApellidos;
	}

	public String execute() 
	{
		listaNombres = new ArrayList<String>();
		listaNombres.add("Ana");
		listaNombres.add("Juan");
		listaNombres.add("Pedro");

		listaApellidos = new ArrayList<String>();
		listaApellidos.add("Lopez");
		listaApellidos.add("Garcia");
		listaApellidos.add("Fernandez");

		return "SUCCESS";
	}
}