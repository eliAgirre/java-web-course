package teoria_etiquetas;

import com.opensymphony.xwork2.ActionSupport;

public class Url extends ActionSupport
{

	private static final long serialVersionUID = 1L;
	private String arg1="";
	private String arg2="";
	
	public String execute() 
    {   
         
        return "SUCCESS";   
    }

	
	public void setArg1(String arg1) 
	{
		this.arg1 = arg1; 
	}

	public void setArg2(String arg2) 
	{
		this.arg2 = arg2; 
	}

	public String getArg1() 
	{
		return (this.arg1); 
	}

	public String getArg2() 
	{
		return (this.arg2); 
	}
}
