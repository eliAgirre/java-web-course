<%@ taglib uri="/struts-tags" prefix="s"%>

<html>

<%--
Para incluir paginas
org.apache.struts2.views.jsp.IncludeTag

value (requerido): El recurso a incluir.
--%>
<head>
	<script type="text/javascript">	
		
		function seleccionar()
		{		
			//Seleccionamos el segundo radio
			document.forms[0].pagina[1].checked="true";
		}
	</script>
</head>

<%-- Para seleccionar el segundo elemento solo la primera vez --%>
<s:if test="pagina==null">
	<body onLoad="seleccionar();">
</s:if>	
<s:else>
	<body>
</s:else>
<div style="background-color:#FFFFcc;font-size:20pt;font-weight:bold">
		Teoria de la etiqueta include Pagina
</div>
<br/>
<s:form action="AccionInclude" theme="css_xhtml"> 	

	<s:radio  list="{'Pagina 1'}"  name="pagina" /> <br/>
	<s:radio  list="'Pagina 2'"    name="pagina" /> <br/>
	<s:radio  list="'Pagina 3'"    name="pagina" /> <br/>
	<hr/>
	
	<%-- Nota si la propiedad a leer (return de una clase Action) contiene solo un caracter es obligatorio 
	escapar las comillas dobles
	<s:if test="propiedad == \"a\""> 
		Entra en el if 
	</s:if><br/>  (funciona)
	--%>

	<%-- Sinonimo (funciona) 
	<s:set name="miPagina" value="pagina[0]"/>
	<s:if test="%{#miPagina=='Pagina 1'}">
		entra en el if 1
	</s:if><br/> 
	<s:if test="%{#miPagina=='Pagina 2'}">
		entra en el if 2
	</s:if><br/> 
	<s:if test="%{#miPagina=='Pagina 3'}">
		entra en el if 3
		</s:if><br/>
	--%>
	
	<%--Devuelve un ArrayList por lo que extraemos el primer valor pagina[0]--%> 
	<s:if test="pagina==null">
		<span>Elija pagina </span>
	</s:if><%-- Primera vez --%>
	<s:elseif test="pagina[0]=='Pagina 1'">
		<s:include value='Include1.jsp'/>
	</s:elseif>
	<s:elseif test="pagina[0]=='Pagina 2'">
		<s:include value='Include2.jsp'/>
	</s:elseif>
	<s:elseif test="pagina[0]=='Pagina 3'">
		<s:include value="Include3.jsp"/>
	</s:elseif>
	<hr/>
		<s:submit value="Enviar" align="left"/>	
</s:form>
<hr/>
<a href="<s:url action='Volver'/>">Volver</a>

</body>
</html>
