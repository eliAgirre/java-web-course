<%@ taglib prefix="s" uri="/struts-tags" %>

<html>
  <head>
    <title>Etiqueta Date</title>
  </head>
  <body>
    <div style="font-size:20pt;font-weight:bold">
    	Teoria de la etiqueta Date      
      <table style="width:36%;height:50%;border-width:1;background-color:ffffcc">
        <tr>
          <td width="50%"><b><font color="#000080">Formato </font></b></td>
          <td width="50%"><b><font color="#000080">Date</font></b></td>
        </tr>
        <tr>
          <td width="50%">Dia/Mes/A&ntilde;o</td>
          <td width="50%"><s:date name="miFecha" format="dd/MM/yyyy" /></td>
        </tr>
        <tr>
          <td width="50%">Mes/Dia/A&ntilde;o</td>
          <td width="50%"><s:date name="miFecha" format="MM/dd/yyyy" /></td>
        </tr>
        <tr>
          <td width="50%">Mes/Dia/A&ntilde;o</td>
          <td width="50%"><s:date name="miFecha" format="MM/dd/yy" /></td>
        </tr>
        <tr>
          <td width="50%">Mes/Dia/A&ntilde;o Horas<B>:</B>Minutos</td>
          <td width="50%"><s:date name="miFecha" format="MM/dd/yy hh:mm" /></td>
        </tr>
        <tr>
          <td width="50%">Mes/Dia/A&ntilde;o Horas<B>:</B>Minutos<B>:</B>Segundos</td>
          <td width="50%"><s:date name="miFecha" format="MM/dd/yy hh:mm:ss" /></td>
        </tr>
        <tr>
          <td width="50%">Fecha larga</td>
          <td width="50%"><s:date name="miFecha" nice="false" /></td>
        </tr>
        <tr>
          <td width="50%">No utilizar</td>
          <td width="50%"><s:date name="miFecha" nice="true" /></td>
        </tr>
      </table>
     <hr/>
     </div>
	<a href="<s:url action='Volver'/>">Volver</a>	
  </body>
</html> 
