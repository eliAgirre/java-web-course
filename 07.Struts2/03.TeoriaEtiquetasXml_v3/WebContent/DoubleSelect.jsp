<%@ taglib uri="/struts-tags" prefix="s"%>

<html>
<body>
<%--
Crea dos elementos select HTML, con el segundo de ellos modificando sus valores 
dependiendo del valor seleccionado en el primero.
org.apache.struts2.views.jsp.ui.DoubleSelectTag

list (requerido): Lista con los valores que tendr� el primer select.
ListValue=Propiedad de la coleccion que seran mostrados en la primera select(Por defecto value) 
doubleList (requerido): Lista con los valores que tendr� el segundo select.
doubleMultiple: Determina si en el segundo select se pueden seleccionar varios valores o solo uno.
doubleName (requerido): Nombre del elemento.
--%>


<s:form action="Resultado_DoubleSelect_Tag">

	<s:doubleselect label="Selecciona cursos" list="cursos"
		listValue="tipoCurso" 
		doubleList="nombreCurso" 
		doubleName="cursos" />
		
	<s:submit value="Enviar" />
	
</s:form>
</body>
</html>