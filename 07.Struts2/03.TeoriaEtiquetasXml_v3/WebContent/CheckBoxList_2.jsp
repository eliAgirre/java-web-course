<%@ taglib prefix="s" uri="/struts-tags"%>

<html>
<head>
<title>Checkboxlist</title>
<script type="text/javascript">
function seleccionar() 
{
	//Seleccionamos la segunda casilla
	document.forms[0].cursos[1].checked = "true";
}
</script>
</head>
<body onload="seleccionar()">

<div style="background-color:#FFFFcc;font-size:20pt;font-weight:bold">
		Checkboxlist (Segunda forma)
</div>
 <s:form action="CheckboxListTag_2" method="POST"> 
 <b>Elige curso</b>
<!--creamos directamente el checkboxlist 
	desde la pagina jsp-->
	
<%-- //Funciona 
<s:checkboxlist name="cursos" 
	list="{'Java','C++','Visual Basic'}" />
 --%>	
<br/>
	<s:checkboxlist label="Curso" id="idcurso" 
		list="{'Java'}" name="cursos"/> <br/>
	<s:checkboxlist label="Curso" id="idcurso" 
		list="{'C++'}" name="cursos" /> <br/>
	<s:checkboxlist label="Curso" id="idcurso" 		
		list="{'Visual Basic'}" name="cursos" />
<br/>		
<s:submit value="Enviar"></s:submit>
</s:form>
</body>
</html>
