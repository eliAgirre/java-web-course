<%@ taglib uri="/struts-tags" prefix="s"%>
<%--

Instancia un Java Bean. Se puede pasar valores a las propiedades del bean utilizando etiquetas param.
org.apache.struts2.views.jsp.BeanTag

name (requerido): La clase a instanciar.
id: Nombre con el que se a�adir� la instancia a ValueStack. (versiones anteriores a 2.1)
var: Nombre con el que se a�adir� la instancia a ValueStack. (versiones 2.1 y posteriores)


--%>
<html>
<body>
<%-- <s:set name="varNombre"  value="%{'Juan'}"/> (Funciona) --%>
<s:set name="varNombre" value="'Juan'"/> 

<s:bean name="modelo.BeanPersonal" id="miBeanPersonal">
	<%--<s:param name="nombre"><s:property value="%{#varNombre}"/></s:param> (Funciona)--%>
	<%--<s:param name="nombre" value="%{#varNombre}"/> (Funciona)--%>
	<s:param name="nombre" value="#varNombre"/>
	<s:param name="apellido">Lopez</s:param>
	
	<%--<s:param name="dni" value="123"/>(funciona) --%>
	<s:param name="dni" value="%{123}"/> <%-- 123 son numeros no llevan comillas--%> 

</s:bean>

<s:set name="miBeanPersonal_2" 
	value="%{#miBeanPersonal}" scope="session"/>

<%-- Version 2.1
<s:bean name="Etiquetas_1.BeanPersonal" var="miBeanPersonal">
	<s:param name="nombre">Juan</s:param>
	<s:param name="apellido">Lopez</s:param>
	<s:param name="dni">123</s:param>
</s:bean>
--%>

<div style="background-color:#FFFFcc;font-size:20pt;font-weight:bold">
	<s:label value="Primera forma" /> :
</div>
Nombre=<s:property value="%{#miBeanPersonal.nombre}"/> <br/>
Apellido=<s:property value="#miBeanPersonal.apellido"/> <br/>
Dni=<s:property value="#miBeanPersonal.dni"/>
<%-- Dni=<s:property value="miBeanPersonal.dni"/> No funciona --%>
<hr/>

<%-- Otra forma (Etiqueta push)--%>

<div style="background-color:#FFFFcc;font-size:20pt;font-weight:bold">
	<s:label value="Seguda forma (Etiqueta push)" /> :
</div>
<br/>
<s:push value="miBeanPersonal">
	Nombre=<s:property value="nombre"/> <br/>
	Apellido=<s:property value="apellido"/> <br/>
	Dni=<s:property value="dni"/>
</s:push>
<hr/>
<%-- Otra forma (Etiqueta Set)--%>

<div style="background-color:#FFFFcc;font-size:20pt;font-weight:bold">
	<s:label value="Seguda forma (Etiqueta set)" /> :
</div>
<br/>
Nombre=<s:property value="#session.miBeanPersonal_2.nombre"/>
<br/>
Apellido=<s:property value="#session.miBeanPersonal_2.apellido"/>
<br/>
Dni=<s:property value="#session.miBeanPersonal_2.dni"/>
<hr/>

<%-- Otra forma (Etiqueta push y Set)--%>

<div style="background-color:#FFFFcc;font-size:20pt;font-weight:bold">
	<s:label value="Seguda forma (Etiqueta push y set)" /> :
</div>
<br/>

<s:push value="#session.miBeanPersonal_2">
	Nombre=<s:property value="nombre"/> <br/>
	Apellido=<s:property value="apellido"/> <br/>
	Dni=<s:property value="dni"/>
</s:push>

<hr/>
<a href="<s:url action='Volver'/>">Volver</a>	
</body>
</html>
