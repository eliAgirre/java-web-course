<%@ taglib prefix="s" uri="/struts-tags" %>
<%-- 
org.apache.struts2.views.jsp.I18nTag
key--Llama a los ficheros properties
cssClass--Para usar una hoja de estilo de cabecera o Css
cssStyle --Para usar un estilo interno
	  
table {
	border-collapse: separate;
	border-spacing: 7px 1px 2px 1px; /*arriba, derecha, abajo, izquierda*/
	}			
	   
--%>
<html>
	<head>
	<s:i18n name="idiomas.idiomaesp"> 
    	<title><s:text name="titulo"/></title>
    </s:i18n>
	  <style>
       .clase1 {color:navy;font-size:15pt;font-style:italic}
       .clase2 {color:red;font-size:8pt;font-style:normal}
	   .clase3 {background-color:#FFFFcc;font-style:italic;text-align:center;font-weight:bold}
	   .clase4 {color:blue;text-align:center}
	   .clase5 {color:green;font-size:10pt}
	   .claseTabla1 
	  	{
			border-collapse:separate;
			border-spacing:7px;
			width:50%;
			height:20%;
			border-width:1;
			text-align:center;
			background-color:#85865B;
		}
		.claseTabla2 
	  	{
			border-collapse:separate;
			border-spacing:10px;
			border-width:0;	
		}
   	</style>
    <s:head/> <%--IMPORTANTE, sirve para activar, no olvidar de ponerlo.Para que funcionen los estilos de cabecera--%>
	</head>

	<body>
	<%--Para utilizar mis estilos, mis tablas y mis capas--%>
	<s:form action="Volver" theme="css_xhtml"> 
		
	<s:i18n name="idiomas.idiomaesp"> 
	
		<div style="background-color:#FFFFcc;font-size:10pt">
			<s:label key="idioma" />
		</div>
			
	  	<div class="clase1">			
			<s:text name="dato1"/>
		</div>
        <%--Formato en la propia properties"--%>
        <s:text name="dato2"/> 
		
	</s:i18n>
		
	<%-- *** Para utilizar otro idioma --%>
	<s:i18n name="idiomas.idioma_ingles"> 	
			<table class="claseTabla1">
				<tr height="5%">
					<td colspan="4" class="clase3">	
						<s:text name="idioma"/>
					</td>
				</tr>
				<tr>
					<td align="center">
						<s:text name="dato1"/>
					</td>
					<td align="center">
						<s:text name="dato2"/>
					</td>
					<td width="30%" align="center">
						<s:text name="dato3"/>
					</td>
					<td width="20%">
						<%--<s:textfield name="miCajaTexto" 
							key="dato3" cssStyle="color:red" /> con la propieda key--%>
				       <s:textfield name="miCajaTexto1" cssClass="clase2" />
				     </td>
				</tr>
			</table>
	</s:i18n>
		
	<s:i18n name="idiomas.idiomaesp"> 
		<div style="background-color:#FFFFcc">
			<h5>
				<s:text name="idioma"/>
			</h5>
		</div>
		<%--<s:textfield name="miCajaTexto2" 
			key="dato3" cssStyle="background-color:#FFFFcc;font-size:10pt;color:red"/>  con la propieda key--%>
			<table class="claseTabla2">
			
				<tr>
					<td>
						<s:text name="dato3"/>
					</td>
					<td>
						<s:textfield name="miCajaTexto2" 
							cssStyle="background-color:#FFFFcc;font-size:10pt;color:red"/>
					</td>
				</tr>
			</table>			
			<div class="clase4">
				<s:submit key ="BotonEnviar" 
					cssClass="clase5"/>
			</div>	
     
		<hr/>
		<div class="clase4">
			<a href="<s:url action='Volver'/>">
			<s:text name="BotonEnviar"/></a>
		</div>		
	</s:i18n>
</s:form>
</body>
</html>
