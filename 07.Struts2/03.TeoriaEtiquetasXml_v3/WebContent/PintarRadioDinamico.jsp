<%@ taglib uri="/struts-tags" prefix="s"%>
<%--org.apache.struts2.views.jsp.ui.RadioTag--%>
<html>
<head>
	<script type="text/javascript" >
	
		function seleccionar()
		{
			document.forms[0].radiobutton1.checked="true";
		}

	</script>
</head>

<body onLoad="seleccionar();">
<div style="background-color:#FFFFcc;font-size:20pt;font-weight:bold">
	Teoria de la etiqueta Radio dinamico
</div>
<s:form action="Resultado_Radio_Tag_dinamico" theme="css_xhtml"> 
<hr/>

<s:set name="contador" value="0"/>
	
<s:iterator value="cursos">

	<s:radio id="radiobutton" name="cursos" 
		list="#{#contador  : cursos[#contador]}" /> 
		  
	<s:set name="contador" value="%{#contador+1}"/>
	
</s:iterator>

<s:submit value="Enviar" />
</s:form>
</body>
</html>