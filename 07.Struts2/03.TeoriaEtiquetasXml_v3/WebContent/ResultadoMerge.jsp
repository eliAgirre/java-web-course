<%@ taglib uri="/struts-tags" prefix="s"%>

<html>
<body>
<%--
Crea un nuevo iterador uni�n de los distintos iteradores pasados como par�metro mediante etiquetas param.
org.apache.struts2.views.jsp.iterator.MergeIteratorTag

id: Nombre que se usar� en ValueStack para el nuevo iterador.

--%>
<s:merge id="personas">
	<s:param value="listaNombres" />
	<s:param value="listaApellidos" />
</s:merge>

<ul>
	<s:iterator value="personas">
		<li><s:property /></li>
	</s:iterator>
</ul>
<hr/>
<a href="<s:url action='Volver'/>">Volver</a>	 
</body>
</html>