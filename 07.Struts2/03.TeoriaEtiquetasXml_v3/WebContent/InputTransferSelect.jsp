<%@ taglib uri="/struts-tags" prefix="s"%>

<html>
<%--
Crea un componente consistente en un select de selecci�n m�ltiple, 
un campo de texto y distintos botones que permiten a�adir, eliminar y ordenar los valores.
org.apache.struts2.views.jsp.ui.InputTransferSelectTag

list (requerido): Lista con los valores que tendr� el select al comenzar.
allowRemoveAll: Determina si se mostrar� el bot�n para eliminar todos los valores.(No funciona)
allowUpDown: Determina si se mostrar�n los botones para subir y bajar los valores en la lista. (por defecto-->true)
downLabel: Etiqueta del bot�n que baja los valores.
upLabel: Etiqueta del bot�n que sube los valores.
removeAllLabel: Etiqueta del bot�n que elimina todos los valores. 
addLabel: Etiqueta del boton que graba uno
removeLabel: Etiqueta del bot�n que elimina un valor.
leftTitle:Literal de lado izquierdo
rightTitle;Literal del lado derecho

multiple:permite la eleccion multiple (por defecto-->true)

--%>


<body>
<s:form action="Resultado_InputTransferSelectTag_Tag">
	<s:inputtransferselect label="Cursos"
		multiple="false"		
		leftTitle="Cursos nuevos"
		rightTitle="Cursos elegidos"
		removeAllLabel="Borrar todos"
		removeLabel="Borrar uno"
		addLabel="Grabar uno"		
		upLabel="Subir"
		downLabel="Bajar"
		list="cursos" name="cursos" />
	<s:submit value="Enviar" />
</s:form>
</body>
</html>