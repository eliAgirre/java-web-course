<%@ taglib uri="/struts-tags" prefix="s"%>

<html>

<%--
ActionContext, entendiendo como contexto un sencillo 
contenedor 
para todos los datos y recursos importantes que rodean a 
la ejecucion de una accion dada.
el contenido de dicho contexto son los elementos:

- ValueStack
- Parameters
- request
- session
- application
- session

Value Stack / OGNL
Para acceder a los objetos necesarios para las jsp, 
struts provee una pila de objetos que denominado value stack. 
Ligada a la pila de objeto se encuentra OGNL, 
que significa lenguaje de navegaci�n de grafos de objetos 
(Object Graph Navigational Language). 
Es un lenguaje est�ndar que define como acceder a los objetos 
dentro de la pila. 
**********
textfield
Crea una caja de texto.
org.apache.struts2.views.jsp.ui.TextFieldTag

maxlength: Atributo maxlength de HTML (n�mero m�ximo de caracteres permitidos)
readonly: Determina si el campo ser� de solo lectura.
size: Atributo size de HTML (tama�o de la caja de texto en caracteres)

property
Muestra una propiedad de ValueStack u otro objeto de ActionContext.
org.apache.struts2.views.jsp.PropertyTag

default: Valor a mostrar en caso de que el valor pedido sea nulo.
value: Valor a mostrar

password
Crea un elemento password de HTML.
org.apache.struts2.views.jsp.ui.PasswordTag

reset
Crea un bot�n que borra los datos introducidos en el formulario.
org.apache.struts2.views.jsp.ui.ResetTag

value: Texto a utilizar para el bot�n

set
Asigna un valor a una variable, opcionalmente indicando el �mbito al que a�adirla. 
El valor se puede indicar utilizando el atributo value, o encerrando el valor en la propia etiqueta.
org.apache.struts2.views.jsp.SetTag

name: Nombre a utilizar para la variable.
scope: �mbito en el que a�adir la variable. 
	Puede ser application, session, request, page, o action. Por defecto se utiliza action.
value: Valor a asignar.

--%>
<head> 

<script type="text/javascript">
function ColocarCursor()
{
	//document.forms[0].dni.focus();(funciona
	document.forms[0].idDni.focus();
}

</script>
</head>

<body onLoad="ColocarCursor();">
<div style="background-color:#FFFFcc;font-size:20pt;font-weight:bold">
	<s:label value="Variables (Set)" /> :
</div>
<br/>
<%-- Variables locales a la pagina (Sin scope)--%>
<%-- s:set name="varLocal"  value="Prueba de variable local"/>  (NO Funciona -->
<%-- <s:set name="varLocal"  value="%{'Prueba de variable local'}"/> (funciona)  --%>
<s:set name="varLocal"  value="'Prueba de variable local'"/> 

Varible local (sin scope --Primera forma): 
	<s:property value="%{#varLocal}"/><br/> 
Varible local (sin scope --Segunda forma): 
	<s:property value="varLocal"/> <br/>
Varible local (sin scope --Tercera forma): 
	<s:property value="#varLocal"/> 
<hr/>

<%-- Variables de pagina (scope="page")(solo es struts 2.1 y posteriores)
<s:set name="varPage"  value="%{'Prueba de variable de pagina'}"
	 scope="page"/>  
Varible de pagina2 (Primera forma): 
	<s:property value="%{#varPage}"/> <br/>
Varible de pagina2 (Segunda forma): 
	<s:property value="varPage"/> <br/>
Varible de pagina1 (Tercera forma): 
	<s:property value="#varPage"/> 
<hr/>
--%>

<%-- Variables de accion (scope="action")--%>
<s:set name="varAccion" scope="action" 
	value="%{'Prueba de variable de accion'}"/>  
	
Variable de accion (Primera forma): 
	<s:property value="%{#varAccion}"/> <br/>
Variable de accion (Segunda forma):
	 <s:property value="#varAccion"/> <br/>
Variable de accion (Tercera forma):
	 <s:property value="varAccion"/> 
<hr/>
<%-- Variables de peticion (scope="request")--%>
<s:set name="varRequest" scope="request" 
	value="%{'Prueba de variable de peticion'}"/>  
	
Variable de peticion (Primera forma): 
	<s:property value="#request['varRequest']"/> <br/>
Variable de peticion (Segunda forma): 
	<s:property value="%{#request['varRequest']}"/><br/>
Variable de peticion (Tercera forma): 
	<s:property value="#request.varRequest"/>
<hr/>

<%-- Variables de sesion (scope="session")--%>
<s:set name="varSesion" scope="session" value="%{'Prueba de variable de sesion'}"/>  
Variable de sesion (Primera forma): 
	<s:property value="#session['varSesion']"/> <br/>
Variable de sesion (Segunda forma): 
	<s:property value="%{#session['varSesion']}"/><br/>
Variable de sesion (Tercera forma): 
	<s:property value="#session.varSesion"/>
<hr/>
<%-- Variables scope="application" a la pagina (scope="application")--%>
<s:set name="varApplication" scope="application" 
	value="%{'Prueba de variable de aplicacion'}"/>  
	
Variable de aplicacion (Primera forma): 
	<s:property value="#application['varApplication']"/> <br/>
Variable de aplicacion (Segunda forma): 
	<s:property value="%{#application['varApplication']}"/> <br/>
Variable de aplicacion (Tercera forma): 
	<s:property value="#application.varApplication"/> 
<hr/>
<%-- if else elseif--%>
<div style="background-color:#FFFFcc;font-size:20pt;font-weight:bold">
	<s:label value="if-else-elseif" /> :
</div>
<br/>
<%-- Nota si la propiedad a leer 
	(return de una clase Action) contiene solo un 
	caracter es obligatorio 
escapar las comillas dobles
<s:if test="propiedad == \"a\""> Entra en el if 
</s:if><br/>  (funciona)
--%>
	  <s:if test="%{#session['varSesion']==
	  	'Prueba de variable de sesion'
	  		|| #varLocal=='Java 2'}">
        <div>Prueba de escritura del IF: 
        	<s:property value="#session['varSesion']" /></div>
      </s:if>

      <s:elseif test="%{#varLocal=='Java'}">
        <div><s:property value="%{#varLocal}" /></div>
      </s:elseif>
      
      <s:else>
        <div>Error</div>
      </s:else>

<div style="background-color:#FFFFcc;font-size:20pt;font-weight:bold">
	<s:label value="Controles basicos" /> :</div>
<br/>
<s:form action="Resultado_ControlesBasicos_Tag">
	
	<s:textfield label="Introduce nombre" 
		maxlength="10" size="10" 
		name="nombre" 
		required="true" 
		tabindex="4" 
		id="idNombre"/>
		
	<s:textfield label="Introduce apellido" 
		maxlength="10" 
		size="10" 
		name="apellido" 
		tabindex="3" 
		id="idApellido"/>
		
	<s:password label="Introduce Dni" 
		maxlength="8" 
		size="8" 
		name="dni" 
		tabindex="2" 
		id="idDni"/>
	
	<s:hidden value="Prueba de campo oculto" 
		name="campoHidden" 
		id="idCampoHidden"/>
		
	<s:reset value="Borrar" tabindex="1" id="idBorrar"/>
	<s:submit value="Enviar" tabindex="0" id="idEnviar"/> 	
 
</s:form>
</body>
</html>
