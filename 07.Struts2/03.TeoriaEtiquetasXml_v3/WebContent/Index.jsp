<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>

	<head>
    <title>P&aacute;gina inicio</title>
	</head>

	<body>
	     <h5>Etiquetas Struts 2</h5>
	     <ul> 
		
		  <li><a href="<s:url action='ActionTag'/>">&lt;Action&gt; Tag </a></li>
		  <li><a href="<s:url action='UrlTag'/>">&lt;Url&gt; Tag </a></li>
		  <li><a href="<s:url action='ControlesBasicos'/>">&lt;Controles Basicos&gt; Tag </a></li>
		  <li><a href="<s:url action='BeanTag'/>">&lt;Bean&gt; Tag </a></li>
		  <li><a href="<s:url action='CheckboxTag'/>">&lt;Checkbox&gt; Tag </a></li>
		  <li><a href="<s:url action='CheckboxTag_dinamico'/>">&lt;Checkbox&gt; Tag Dinamico </a></li>
		  <li><a href="<s:url action='CheckboxListTag_primeraforma'/>">&lt;CheckboxList (Primera forma)&gt; Tag </a></li>
		  <li><a href="<s:url action='CheckboxListTag_segundaforma'/>">&lt;CheckboxList (Segunda forma)&gt; Tag </a></li>
		  <li><a href="<s:url action='ComboBoxTag'/>">&lt;ComboBox&gt; Tag </a></li>
		  <li><a href="<s:url action='SelectTag'/>">&lt;Select&gt; Tag </a></li>
		  <li><a href="<s:url action='DoubleSelectTag'/>">&lt;DoubleSelect&gt; Tag </a></li>
		  <li><a href="<s:url action='RadioTag'/>">&lt;Radio&gt; Tag </a></li>
		  <li><a href="<s:url action='RadioTag_dinamico'/>">&lt;Radio&gt; Tag Dinamico </a></li>
		  <li><a href="<s:url action='OptGroupTag'/>">&lt;OptGroup&gt; Tag </a></li>		  
		  <li><a href="<s:url action='OptionTransferSelectTag'/>">&lt;OptionTransferSelect&gt; Tag </a></li>
		  <li><a href="<s:url action='InputTransferSelectTag'/>">&lt;InputTransferSelect&gt; Tag </a></li>
		  <li><a href="<s:url action='DateTag'/>">&lt;Date&gt; Tag </a></li>		  
		  <li><a href="<s:url action='MergeTag'/>">&lt;Merge&gt; Tag </a></li>		  
		  <li><a href="<s:url action='AppendTag'/>">&lt;Append&gt; Tag </a></li>		  
		  <li><a href="<s:url action='GeneratorTag_1'/>">&lt;Generator (primera forma)&gt; Tag </a></li>	
		  <li><a href="<s:url action='GeneratorTag_2'/>">&lt;Generator (segunda forma)&gt; Tag </a></li>	
		  <li><a href="<s:url action='GeneratorTag_3'/>">&lt;Generator (Contando atributos)&gt; Tag </a></li>
		  <li><a href="<s:url action='GeneratorTag_4'/>">&lt;Generator (Con identificador de atributos)&gt; Tag </a></li>
		  <li><a href="<s:url action='SubsetTag_1'/>">&lt;Subset&gt; Tag </a></li>	
		  <li><a href="<s:url action='SubsetTag_2'/>">&lt;Subset (Contando atributos)&gt; Tag </a></li>	
		  <li><a href="<s:url action='SubsetTag_3'/>">&lt;Subset (Eligiendo el inicio)&gt; Tag </a></li>			   
		  <li><a href="<s:url action='IncludeTag'/>">&lt;Include&gt; Tag </a></li>	
		  <li><a href="<s:url action='TextTag'/>">&lt;Text&gt; Tag </a></li>		 
	  	</ul> 	
	</body>
	
</html>
