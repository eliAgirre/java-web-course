<%@ taglib uri="/struts-tags" prefix="s"%>

<html>
<head>
	<title>Resultado</title>
</head>

<body>
<%-- **********Controles ************ --%>
<div style="background-color:#FFFFcc;font-size:20pt;font-weight:bold">
	<s:label value="Controles basicos" /> :
</div>
<%-- Estiqueta textfield --%>
<s:textfield label="El nombre es " name="nombre"/>
<br/>
<%-- Estiqueta Label --%>
<s:label label="El apellido es "  name="apellido"/>
<br/>
<%-- Estiqueta Property --%>
El Dni es: <s:property value="dni" default="123" /> 
<br/>
El valor del campo Hidden es: <s:property value="campoHidden" /> 
<hr/>


<%-- **********Variables ************ --%>
<div style="background-color:#FFFFcc;font-size:20pt;font-weight:bold">
	<s:label value="Variables (Set)" /> :
</div>
<%-- No funcionan (sin ambito) --%>
Varible local (sin scope)= <s:property value="%{#varLocal}"/><br/> 
Variable de accion= <s:property value="%{#varAccion}"/> <br/>
Variable de peticion= <s:property value="%{#request['varRequest']}"/>
<hr/>
<%-- Igualar variables --%>
<s:set name="varSessionNueva" value="%{#session['varSesion']}"/>

<%-- Estiqueta textfield con variables --%>
<s:textfield  size="26" label="Variable de sesion (con variables)=" 
	name="varSesion1"  value="%{varSessionNueva}"/> 
<br/>
<%-- Estiqueta textfield sin variables --%>
<s:textfield size="26" label="Variable de sesion (sin variables)="  
	name="varSesion2"  value="%{#session['varSesion']}"/>
<br/>
<%-- Estiqueta Label --%>
<s:label label="Variable de aplicacion (Etiqueta Label)="
	name="varSesion3"  value="%{#application['varApplication']}"/>
<br/>
<%-- Estiqueta Property --%>
Variable de aplication (Property --Primera forma--) = <s:property value="#application['varApplication']"/> 
<br/>
Variable de aplication (Property --Segunda Forma--) = <s:property value="%{#application['varApplication']}"/> 
<br/>
Variable de aplication (Property --Tercera Forma--) = <s:property value="#application.varApplication"/> 
<hr/>
<a href="<s:url action='Volver'/>">Volver</a>	
</body>
</html>
