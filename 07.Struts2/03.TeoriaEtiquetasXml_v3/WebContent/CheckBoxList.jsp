<%@ taglib uri="/struts-tags" prefix="s"%>

<html>
<body>
<%--

 Crea una lista de checkboxes relacionados (todos con el mismo atributo name).
  Esto implica que el valor del elemento no ser� un booleano indicando 
  si est� marcado o no, como en el caso de checkbox, sino una lista con los valores marcados.
org.apache.struts2.views.jsp.ui.CheckboxListTag

list (requerido): Iterable con los valores con los que generar la lista.

name: Nombre de los checkboxes.


--%>
<div style="background-color:#FFFFcc;font-size:20pt;font-weight:bold">
	Checkboxlist (Primera forma)
</div>
<s:form action="CheckboxListTag_1" method="POST"> 
<!--Creamos el checkboxlist desde la clase 
	CheckBoxList.class-->
<!--Grabamos los elementos seleccionados 
	en la clase CheckBoxList_2.class-->

	<!-- <s:checkboxlist list="cursos" name="cursos" />  
		(funciona)-->
	<s:checkboxlist list="cursos[0]" name="cursos" 
		value="false"/> 
	<s:checkboxlist list="cursos[1]" name="cursos" /> 
	<s:checkboxlist list="cursos[2]" name="cursos" /> 
	<s:submit value="Enviar" />
</s:form>
</body>
</html>

  
