package teoriasesion;

import com.opensymphony.xwork2.ActionSupport;

 //Obligatorio para el ActionContext
import com.opensymphony.xwork2.ActionContext;

import java.util.Map;

import modelo.Personal;

public class TeoriaSesion_2 extends ActionSupport
{
		
	private static final long serialVersionUID = 1L;
	
	private String dato3="";
	private String dato4="";
	
	public String execute()
	{
		//Creamos el objeto session
	    @SuppressWarnings("unchecked")
		Map<String, String> session =( Map<String, String>)
			ActionContext.getContext().getSession();	  
	    
		@SuppressWarnings("unchecked")
		Map<String, Personal> persona = (Map<String, Personal>)
				ActionContext.getContext().getSession();
	    
	    //Ejemplo de lectura de la sesion 
	    String miDato1=session.get("dato1").toString();  
		System.out.println("Dato 1 en la sesion=" + miDato1);
		
		System.out.println("Dato 2 en la sesion="  + session.get("dato2"));
		
		System.out.println("persona.dni=" + persona.get("persona").getDni());
		System.out.println("persona.dni=" + persona.get("persona").getNombre());
		
		//Grabamos en el objeto session
		session.put("dato3", dato3);
	    session.put("dato4", dato4);
	    
		return "SUCCESS";
	} 

	
	public String getDato3() 
	{
		return (this.dato3); 
	}
	
	public void setDato3(String dato3) 
	{
		this.dato3 = dato3; 
	}
	
	public String getDato4() 
	{
		return (this.dato4); 
	}
	
	public void setDato4(String dato4) 
	{
		this.dato4 = dato4; 
	}
}

