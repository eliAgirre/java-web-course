package teoriasesion;

import com.opensymphony.xwork2.ActionSupport;

 //Obligatorio para el ActionContext
import com.opensymphony.xwork2.ActionContext;

import java.util.Map;

import modelo.Personal;

public class TeoriaSesion_1 extends ActionSupport
{
		
	private static final long serialVersionUID = 1L;
	
	private String dato1="";
	private String dato2="";
	
	public String execute()
	{    
	    //Creamos el objeto session
	  
		@SuppressWarnings("unchecked")
		Map<String, String> session = (Map<String, String>)
				ActionContext.getContext().getSession();
		
		@SuppressWarnings("unchecked")
		Map<String, Personal> persona = (Map<String, Personal>)
				ActionContext.getContext().getSession();
			  
	    session.put("dato1", dato1);
	    session.put("dato2", dato2);
	    
	    Personal p=new Personal();
	    p.setDni("1");
	    p.setNombre("A");
	    
	    persona.put("persona", p);
	    
		return "SUCCESS";
	}
		
	// JavaBeans
	
	public void setDato1(String dato1) 
	{
		this.dato1 = dato1; 
	}

	public void setDato2(String dato2) 
	{
		this.dato2 = dato2; 
	}

	public String getDato1() 
	{
		return (this.dato1); 
	}

	public String getDato2() 
	{
		return (this.dato2); 
	}	

}
