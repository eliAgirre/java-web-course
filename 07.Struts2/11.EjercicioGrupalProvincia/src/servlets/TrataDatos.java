package servlets;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class TrataDatos extends ActionSupport{

	private static final long serialVersionUID = 1L;
	
	private String nombre;
	private String apellido;
	private String dni;
	private String nif;
		
	
public String execute(){
		
		
	@SuppressWarnings("unchecked")
	Map<String, Integer> sesion = (Map<String, Integer>)ActionContext.getContext().getSession();
	
	@SuppressWarnings("unchecked")
	Map<String, String> datos = (Map<String, String>)ActionContext.getContext().getSession();
	
	int ComunidadAct = sesion.get("comunidad");
	
	if (ComunidadAct == 0){ //madrid
		
		datos.put("name", nombre);
		datos.put("dni", dni);
		
	}else{ //es valencia
		
		datos.put("apellido", apellido);
		datos.put("nif", nif);
		
	}
	
	
	return "SUCCESS";
	
}


public String getNombre() {
	return nombre;
}


public void setNombre(String nombre) {
	this.nombre = nombre;
}


public String getApellido() {
	return apellido;
}


public void setApellido(String apellido) {
	this.apellido = apellido;
}


public String getDni() {
	return dni;
}


public void setDni(String dni) {
	this.dni = dni;
}


public String getNif() {
	return nif;
}


public void setNif(String nif) {
	this.nif = nif;
}
	



	
	
}
