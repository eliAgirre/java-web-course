package servlets;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class TrataComunidad extends ActionSupport {

	private static final long serialVersionUID = 1L;
	
	private int comunidad;

	public int getComunidad() {
		
		return comunidad;
		
	}


	public void setComunidad(int comunidad) {
		
		this.comunidad = comunidad;
		
	}

	public String execute(){
		
		
		@SuppressWarnings("unchecked")
		Map<String, Integer> sesion = (Map<String, Integer>)ActionContext.getContext().getSession();
		
		
		sesion.put("comunidad", comunidad);
		
		System.out.println(comunidad);
		
		return "SUCCESS";
		
	}
	
}
