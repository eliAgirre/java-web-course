<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>

	<head>
	 
	<title><s:text name="registrar.titulo"/></title>
	  <style>
       .clase1 {color:navy;font-size:10pt;font-style:italic}
      </style>
   <s:head/> <%--Para que funcionen los estilos de cabecera--%>
	</head>

	<body onload="document.getElementById('idNombre').focus();">
	
	
	
		<h4><s:text name="registrar.texto"/></h4>  
		
		<%--Para utilizar mis estilos, mis tablas y mis capas--%>
		<s:form action="registrar" method="post"  theme="css_xhtml"> 
		<table style="width:45%;height:20%;border-width:0;text-align:center">
				<tr>
					<td align="right"><s:text name="registrar.nombre"/></td>
					<td><s:textfield id="idNombre" name="nombre"/></td>
				</tr>
				<tr>
					<td align="right"><s:text name="registrar.password"/></td>
					<td><s:password  name="password"/></td>
				</tr>
				<tr>
					<td align="center">
						<s:submit key="registrar.botonsubmit" cssClass="clase1"/>
					</td>
					<td width="50">
						<s:reset key="registrar.botonreset" cssStyle="text-align:center"/>
					</td>										
				</tr>				
		</table> 
				<%--Salida de errores--%>
		<s:fielderror />    	
		</s:form>		 
	</body>	
</html>
