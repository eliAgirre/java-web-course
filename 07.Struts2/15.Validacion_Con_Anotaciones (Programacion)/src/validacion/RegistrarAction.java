package validacion;

import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import com.opensymphony.xwork2.ActionSupport;

@Results({
    @Result(name="SUCCESS_BIEN", value="/ResultadoBien.jsp"),
    @Result(name="SUCCESS_MAL",  value="/ResultadoMal.jsp"),
    @Result(name="input",  value="/Registrar.jsp")
})

public class RegistrarAction extends ActionSupport 
{
	private static final long serialVersionUID = 1L;

	public String execute()
	{
 
		if(nombre.equalsIgnoreCase("angel") &&
			password.equalsIgnoreCase("miguel"))
		{
			return "SUCCESS_BIEN";		
		}
		else
		{
			return "SUCCESS_MAL";	
		}	
	}
		
	// JavaBeans
	
	private String nombre="";
	private String password="";
	
	public void setNombre(String nombre) 
	{
		this.nombre = nombre; 
	}

	public void setPassword(String password) 
	{
		this.password = password; 
	}

	public String getNombre() 
	{
		return (this.nombre); 
	}

	public String getPassword() 
	{
		return (this.password); 
	}
	
	public void validate()
	{
		if ( getPassword().length() == 0 )
		{			
			addFieldError( "errorpassword", getText("registrar.password.requerido") );
		//	addFieldError( "errorpassword","Password Obligatorio") ;
		}
		
		if ( getNombre().length() == 0 )
		{			
			addFieldError( "error_nombre", getText("registrar.nombre.requerido") );
			//addFieldError( "error_nombre", "Nombre Obligatorio") ;
		}
		//Para grabar errones que no esten en el property
		if( getNombre().length() > 10 ) 
		{   
            addFieldError("error_nombre_maximo",
            	 "<font color='red'>El nombre no puede tener m�s de 10 caracteres </font>");
        }
	}
}
